# react-learncode

## Description

This is a repository that stores the source code I have written, following the tutorials from Learncode.academy in [YouTube](https://www.youtube.com/playlist?list=PLoYCgNOIyGABj2GQSlDRjgvXtqfDxKm5b).

## Additional Notes
For the commmit "Learned webpack from a Medium blog", here's the [link](https://medium.com/@dabit3/beginner-s-guide-to-webpack-b1f1a3638460#.dlcozxxku) to the Medium blog.

## TODO

- Finish watching all videos from the link above.
- Start coding a Markdown previewer in FreeCodeCamp.
  - Push it into Github and Bitbucket