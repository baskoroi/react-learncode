import React from 'react';
import NavBtn from './NavBtn';

export default React.createClass({
    getInitialState() {
        return {collapsed: true};
    },
    toggleCollapse() {
        const collapsed = !this.state.collapsed;
        this.setState({collapsed});
    },
    render() {
        const { location } = this.props;
        const { collapsed } = this.state;

        const navClass = collapsed ? 'collapse' : '';
        return (
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" onClick={this.toggleCollapse}>
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <NavBtn to="/" isIndex={true} toggleCollapse={this.toggleCollapse}>
                                Todos
                            </NavBtn>
                            <NavBtn to="favorites" isIndex={false} toggleCollapse={this.toggleCollapse}>
                                Favorites
                            </NavBtn>
                            <NavBtn to="settings" isIndex={false} toggleCollapse={this.toggleCollapse}>
                                Settings
                            </NavBtn>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
});