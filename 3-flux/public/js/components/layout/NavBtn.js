import React from 'react';
import { Link, IndexLink } from 'react-router';

export default React.createClass({
    render() {
        const { to, isIndex, toggleCollapse } = this.props;
        const Anchor = isIndex ? IndexLink : Link;
        return (
            <li>
                <Anchor activeClassName="active" to={to} onClick={toggleCollapse}>
                    {this.props.children}
                </Anchor>
            </li>
        );
    }
});