import React from 'react';

export default React.createClass({
    render() {
        const { text, complete } = this.props;
        const faClass = 'fa ' + (complete ? 'fa-check' : 'fa-times');
        return (
            <li>{text} <a><i class={faClass}></i></a></li>
        );
    }
});