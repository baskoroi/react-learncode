import React from 'react';

import Todo from '../components/Todo';
import * as TodoActions from '../actions/TodoActions';
import TodoStore from '../stores/TodoStore';

export default React.createClass({
    getInitialState() {
        return {
            todos: TodoStore.getAll()
        };
    },
    getTodos() {
        this.setState({
            todos: TodoStore.getAll()
        });
    },
    componentWillMount() {
        TodoStore.on('change', this.getTodos);
    },
    componentWillUnmount() {
        TodoStore.removeListener('change', this.getTodos);
    },
    reloadTodos() {
        TodoActions.reloadTodos();
    },
    render() {
        const { todos } = this.state;

        const TodoComponents = todos.map((todo) => {
            return <Todo key={todo.id} {...todo} />;
        });

        return (
            <div>
                <h1>Todo List</h1>
                <button onClick={this.reloadTodos}>Reload!</button>
                <input />
                <ul>
                    {TodoComponents}
                </ul>
            </div>
        );
    }
});