var webpack = require("webpack");

module.exports = {
	entry: "./src",
	output: {
		path: "builds",
		filename: "bundle.js",
		chunkFilename: "[name].bundle.js",
		publicPath: "builds/",
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: "vendor",
			filename: "vendor.js",
			children: true,
			minChunks: 2,
		}),
	],
	module: {
		loaders: [
			{
				test: /\.js/,
				loader: "babel",
				exclude: /node_modules/,
			},
			{
				test: /\.scss/,
				loaders: ["style", "css", "sass"],
			},
			{
				test: /\.html/,
				loader: "html",
			}
		],
	}
};