var debug = process.env.NODE_ENV !== 'production';
var webpack = require('webpack');
var path = require('path');

module.exports = {
    context: path.join(__dirname, 'public'),
    devtool: debug ? 'inline-source-map' : null,
    entry: './js/client.js',
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                    plugins: [
                        'react-html-attrs', 
                        'transform-class-properties', 
                        'transform-decorators-legacy'
                    ],
                }
            }
        ],
    },
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, 'public'),
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
    ],
};