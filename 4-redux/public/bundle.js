/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _redux = __webpack_require__(1);
	
	var _axios = __webpack_require__(16);
	
	var _axios2 = _interopRequireDefault(_axios);
	
	var _reduxLogger = __webpack_require__(38);
	
	var _reduxLogger2 = _interopRequireDefault(_reduxLogger);
	
	var _reduxThunk = __webpack_require__(39);
	
	var _reduxThunk2 = _interopRequireDefault(_reduxThunk);
	
	var _reduxPromiseMiddleware = __webpack_require__(40);
	
	var _reduxPromiseMiddleware2 = _interopRequireDefault(_reduxPromiseMiddleware);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var initialState = {
	    fetching: false,
	    fetched: false,
	    users: [],
	    error: null
	};
	
	var reducer = function reducer() {
	    var state = arguments.length <= 0 || arguments[0] === undefined ? initialState : arguments[0];
	    var action = arguments[1];
	
	    switch (action.type) {
	        case 'FETCH_USERS_PENDING':
	            {
	                state = _extends({}, state, {
	                    fetching: true
	                });
	                break;
	            }
	        case 'FETCH_USERS_REJECTED':
	            {
	                state = _extends({}, state, {
	                    fetching: false,
	                    error: action.payload
	                });
	                break;
	            }
	        case 'FETCH_USERS_FULFILLED':
	            {
	                state = _extends({}, state, {
	                    fetching: false,
	                    fetched: true,
	                    users: action.payload
	                });
	                break;
	            }
	    }
	    return state;
	};
	
	var middleware = (0, _redux.applyMiddleware)((0, _reduxPromiseMiddleware2.default)(), _reduxThunk2.default, (0, _reduxLogger2.default)());
	
	var store = (0, _redux.createStore)(reducer, middleware);
	
	store.dispatch({
	    type: 'FETCH_USERS',
	    payload: _axios2.default.get('http://rest.learncode.academy/api/learncode/friends')
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	exports.__esModule = true;
	exports.compose = exports.applyMiddleware = exports.bindActionCreators = exports.combineReducers = exports.createStore = undefined;
	
	var _createStore = __webpack_require__(3);
	
	var _createStore2 = _interopRequireDefault(_createStore);
	
	var _combineReducers = __webpack_require__(11);
	
	var _combineReducers2 = _interopRequireDefault(_combineReducers);
	
	var _bindActionCreators = __webpack_require__(13);
	
	var _bindActionCreators2 = _interopRequireDefault(_bindActionCreators);
	
	var _applyMiddleware = __webpack_require__(14);
	
	var _applyMiddleware2 = _interopRequireDefault(_applyMiddleware);
	
	var _compose = __webpack_require__(15);
	
	var _compose2 = _interopRequireDefault(_compose);
	
	var _warning = __webpack_require__(12);
	
	var _warning2 = _interopRequireDefault(_warning);
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { "default": obj };
	}
	
	/*
	* This is a dummy function to check if the function name has been altered by minification.
	* If the function has been minified and NODE_ENV !== 'production', warn the user.
	*/
	function isCrushed() {}
	
	if (process.env.NODE_ENV !== 'production' && typeof isCrushed.name === 'string' && isCrushed.name !== 'isCrushed') {
	  (0, _warning2["default"])('You are currently using minified code outside of NODE_ENV === \'production\'. ' + 'This means that you are running a slower development build of Redux. ' + 'You can use loose-envify (https://github.com/zertosh/loose-envify) for browserify ' + 'or DefinePlugin for webpack (http://stackoverflow.com/questions/30030031) ' + 'to ensure you have the correct code for your production build.');
	}
	
	exports.createStore = _createStore2["default"];
	exports.combineReducers = _combineReducers2["default"];
	exports.bindActionCreators = _bindActionCreators2["default"];
	exports.applyMiddleware = _applyMiddleware2["default"];
	exports.compose = _compose2["default"];
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';
	
	// shim for using process in browser
	var process = module.exports = {};
	
	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.
	
	var cachedSetTimeout;
	var cachedClearTimeout;
	
	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout() {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	})();
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch (e) {
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch (e) {
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }
	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e) {
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e) {
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }
	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while (len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () {
	    return '/';
	};
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function () {
	    return 0;
	};

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	exports.__esModule = true;
	exports.ActionTypes = undefined;
	exports["default"] = createStore;
	
	var _isPlainObject = __webpack_require__(4);
	
	var _isPlainObject2 = _interopRequireDefault(_isPlainObject);
	
	var _symbolObservable = __webpack_require__(9);
	
	var _symbolObservable2 = _interopRequireDefault(_symbolObservable);
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { "default": obj };
	}
	
	/**
	 * These are private action types reserved by Redux.
	 * For any unknown actions, you must return the current state.
	 * If the current state is undefined, you must return the initial state.
	 * Do not reference these action types directly in your code.
	 */
	var ActionTypes = exports.ActionTypes = {
	  INIT: '@@redux/INIT'
	};
	
	/**
	 * Creates a Redux store that holds the state tree.
	 * The only way to change the data in the store is to call `dispatch()` on it.
	 *
	 * There should only be a single store in your app. To specify how different
	 * parts of the state tree respond to actions, you may combine several reducers
	 * into a single reducer function by using `combineReducers`.
	 *
	 * @param {Function} reducer A function that returns the next state tree, given
	 * the current state tree and the action to handle.
	 *
	 * @param {any} [initialState] The initial state. You may optionally specify it
	 * to hydrate the state from the server in universal apps, or to restore a
	 * previously serialized user session.
	 * If you use `combineReducers` to produce the root reducer function, this must be
	 * an object with the same shape as `combineReducers` keys.
	 *
	 * @param {Function} enhancer The store enhancer. You may optionally specify it
	 * to enhance the store with third-party capabilities such as middleware,
	 * time travel, persistence, etc. The only store enhancer that ships with Redux
	 * is `applyMiddleware()`.
	 *
	 * @returns {Store} A Redux store that lets you read the state, dispatch actions
	 * and subscribe to changes.
	 */
	function createStore(reducer, initialState, enhancer) {
	  var _ref2;
	
	  if (typeof initialState === 'function' && typeof enhancer === 'undefined') {
	    enhancer = initialState;
	    initialState = undefined;
	  }
	
	  if (typeof enhancer !== 'undefined') {
	    if (typeof enhancer !== 'function') {
	      throw new Error('Expected the enhancer to be a function.');
	    }
	
	    return enhancer(createStore)(reducer, initialState);
	  }
	
	  if (typeof reducer !== 'function') {
	    throw new Error('Expected the reducer to be a function.');
	  }
	
	  var currentReducer = reducer;
	  var currentState = initialState;
	  var currentListeners = [];
	  var nextListeners = currentListeners;
	  var isDispatching = false;
	
	  function ensureCanMutateNextListeners() {
	    if (nextListeners === currentListeners) {
	      nextListeners = currentListeners.slice();
	    }
	  }
	
	  /**
	   * Reads the state tree managed by the store.
	   *
	   * @returns {any} The current state tree of your application.
	   */
	  function getState() {
	    return currentState;
	  }
	
	  /**
	   * Adds a change listener. It will be called any time an action is dispatched,
	   * and some part of the state tree may potentially have changed. You may then
	   * call `getState()` to read the current state tree inside the callback.
	   *
	   * You may call `dispatch()` from a change listener, with the following
	   * caveats:
	   *
	   * 1. The subscriptions are snapshotted just before every `dispatch()` call.
	   * If you subscribe or unsubscribe while the listeners are being invoked, this
	   * will not have any effect on the `dispatch()` that is currently in progress.
	   * However, the next `dispatch()` call, whether nested or not, will use a more
	   * recent snapshot of the subscription list.
	   *
	   * 2. The listener should not expect to see all state changes, as the state
	   * might have been updated multiple times during a nested `dispatch()` before
	   * the listener is called. It is, however, guaranteed that all subscribers
	   * registered before the `dispatch()` started will be called with the latest
	   * state by the time it exits.
	   *
	   * @param {Function} listener A callback to be invoked on every dispatch.
	   * @returns {Function} A function to remove this change listener.
	   */
	  function subscribe(listener) {
	    if (typeof listener !== 'function') {
	      throw new Error('Expected listener to be a function.');
	    }
	
	    var isSubscribed = true;
	
	    ensureCanMutateNextListeners();
	    nextListeners.push(listener);
	
	    return function unsubscribe() {
	      if (!isSubscribed) {
	        return;
	      }
	
	      isSubscribed = false;
	
	      ensureCanMutateNextListeners();
	      var index = nextListeners.indexOf(listener);
	      nextListeners.splice(index, 1);
	    };
	  }
	
	  /**
	   * Dispatches an action. It is the only way to trigger a state change.
	   *
	   * The `reducer` function, used to create the store, will be called with the
	   * current state tree and the given `action`. Its return value will
	   * be considered the **next** state of the tree, and the change listeners
	   * will be notified.
	   *
	   * The base implementation only supports plain object actions. If you want to
	   * dispatch a Promise, an Observable, a thunk, or something else, you need to
	   * wrap your store creating function into the corresponding middleware. For
	   * example, see the documentation for the `redux-thunk` package. Even the
	   * middleware will eventually dispatch plain object actions using this method.
	   *
	   * @param {Object} action A plain object representing “what changed”. It is
	   * a good idea to keep actions serializable so you can record and replay user
	   * sessions, or use the time travelling `redux-devtools`. An action must have
	   * a `type` property which may not be `undefined`. It is a good idea to use
	   * string constants for action types.
	   *
	   * @returns {Object} For convenience, the same action object you dispatched.
	   *
	   * Note that, if you use a custom middleware, it may wrap `dispatch()` to
	   * return something else (for example, a Promise you can await).
	   */
	  function dispatch(action) {
	    if (!(0, _isPlainObject2["default"])(action)) {
	      throw new Error('Actions must be plain objects. ' + 'Use custom middleware for async actions.');
	    }
	
	    if (typeof action.type === 'undefined') {
	      throw new Error('Actions may not have an undefined "type" property. ' + 'Have you misspelled a constant?');
	    }
	
	    if (isDispatching) {
	      throw new Error('Reducers may not dispatch actions.');
	    }
	
	    try {
	      isDispatching = true;
	      currentState = currentReducer(currentState, action);
	    } finally {
	      isDispatching = false;
	    }
	
	    var listeners = currentListeners = nextListeners;
	    for (var i = 0; i < listeners.length; i++) {
	      listeners[i]();
	    }
	
	    return action;
	  }
	
	  /**
	   * Replaces the reducer currently used by the store to calculate the state.
	   *
	   * You might need this if your app implements code splitting and you want to
	   * load some of the reducers dynamically. You might also need this if you
	   * implement a hot reloading mechanism for Redux.
	   *
	   * @param {Function} nextReducer The reducer for the store to use instead.
	   * @returns {void}
	   */
	  function replaceReducer(nextReducer) {
	    if (typeof nextReducer !== 'function') {
	      throw new Error('Expected the nextReducer to be a function.');
	    }
	
	    currentReducer = nextReducer;
	    dispatch({ type: ActionTypes.INIT });
	  }
	
	  /**
	   * Interoperability point for observable/reactive libraries.
	   * @returns {observable} A minimal observable of state changes.
	   * For more information, see the observable proposal:
	   * https://github.com/zenparsing/es-observable
	   */
	  function observable() {
	    var _ref;
	
	    var outerSubscribe = subscribe;
	    return _ref = {
	      /**
	       * The minimal observable subscription method.
	       * @param {Object} observer Any object that can be used as an observer.
	       * The observer object should have a `next` method.
	       * @returns {subscription} An object with an `unsubscribe` method that can
	       * be used to unsubscribe the observable from the store, and prevent further
	       * emission of values from the observable.
	       */
	
	      subscribe: function subscribe(observer) {
	        if ((typeof observer === 'undefined' ? 'undefined' : _typeof(observer)) !== 'object') {
	          throw new TypeError('Expected the observer to be an object.');
	        }
	
	        function observeState() {
	          if (observer.next) {
	            observer.next(getState());
	          }
	        }
	
	        observeState();
	        var unsubscribe = outerSubscribe(observeState);
	        return { unsubscribe: unsubscribe };
	      }
	    }, _ref[_symbolObservable2["default"]] = function () {
	      return this;
	    }, _ref;
	  }
	
	  // When a store is created, an "INIT" action is dispatched so that every
	  // reducer returns their initial state. This effectively populates
	  // the initial state tree.
	  dispatch({ type: ActionTypes.INIT });
	
	  return _ref2 = {
	    dispatch: dispatch,
	    subscribe: subscribe,
	    getState: getState,
	    replaceReducer: replaceReducer
	  }, _ref2[_symbolObservable2["default"]] = observable, _ref2;
	}

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var getPrototype = __webpack_require__(5),
	    isHostObject = __webpack_require__(7),
	    isObjectLike = __webpack_require__(8);
	
	/** `Object#toString` result references. */
	var objectTag = '[object Object]';
	
	/** Used for built-in method references. */
	var funcProto = Function.prototype,
	    objectProto = Object.prototype;
	
	/** Used to resolve the decompiled source of functions. */
	var funcToString = funcProto.toString;
	
	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;
	
	/** Used to infer the `Object` constructor. */
	var objectCtorString = funcToString.call(Object);
	
	/**
	 * Used to resolve the
	 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var objectToString = objectProto.toString;
	
	/**
	 * Checks if `value` is a plain object, that is, an object created by the
	 * `Object` constructor or one with a `[[Prototype]]` of `null`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.8.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 * }
	 *
	 * _.isPlainObject(new Foo);
	 * // => false
	 *
	 * _.isPlainObject([1, 2, 3]);
	 * // => false
	 *
	 * _.isPlainObject({ 'x': 0, 'y': 0 });
	 * // => true
	 *
	 * _.isPlainObject(Object.create(null));
	 * // => true
	 */
	function isPlainObject(value) {
	    if (!isObjectLike(value) || objectToString.call(value) != objectTag || isHostObject(value)) {
	        return false;
	    }
	    var proto = getPrototype(value);
	    if (proto === null) {
	        return true;
	    }
	    var Ctor = hasOwnProperty.call(proto, 'constructor') && proto.constructor;
	    return typeof Ctor == 'function' && Ctor instanceof Ctor && funcToString.call(Ctor) == objectCtorString;
	}
	
	module.exports = isPlainObject;

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var overArg = __webpack_require__(6);
	
	/** Built-in value references. */
	var getPrototype = overArg(Object.getPrototypeOf, Object);
	
	module.exports = getPrototype;

/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";
	
	/**
	 * Creates a unary function that invokes `func` with its argument transformed.
	 *
	 * @private
	 * @param {Function} func The function to wrap.
	 * @param {Function} transform The argument transform.
	 * @returns {Function} Returns the new function.
	 */
	function overArg(func, transform) {
	  return function (arg) {
	    return func(transform(arg));
	  };
	}
	
	module.exports = overArg;

/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 * Checks if `value` is a host object in IE < 9.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a host object, else `false`.
	 */
	function isHostObject(value) {
	  // Many host objects are `Object` objects that can coerce to strings
	  // despite having improperly defined `toString` methods.
	  var result = false;
	  if (value != null && typeof value.toString != 'function') {
	    try {
	      result = !!(value + '');
	    } catch (e) {}
	  }
	  return result;
	}
	
	module.exports = isHostObject;

/***/ },
/* 8 */
/***/ function(module, exports) {

	'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	/**
	 * Checks if `value` is object-like. A value is object-like if it's not `null`
	 * and has a `typeof` result of "object".
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
	 * @example
	 *
	 * _.isObjectLike({});
	 * // => true
	 *
	 * _.isObjectLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isObjectLike(_.noop);
	 * // => false
	 *
	 * _.isObjectLike(null);
	 * // => false
	 */
	function isObjectLike(value) {
	  return !!value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) == 'object';
	}
	
	module.exports = isObjectLike;

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {/* global window */
	'use strict';
	
	module.exports = __webpack_require__(10)(global || window || undefined);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 10 */
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function symbolObservablePonyfill(root) {
		var result;
		var _Symbol = root.Symbol;
	
		if (typeof _Symbol === 'function') {
			if (_Symbol.observable) {
				result = _Symbol.observable;
			} else {
				result = _Symbol('observable');
				_Symbol.observable = result;
			}
		} else {
			result = '@@observable';
		}
	
		return result;
	};

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	exports.__esModule = true;
	exports["default"] = combineReducers;
	
	var _createStore = __webpack_require__(3);
	
	var _isPlainObject = __webpack_require__(4);
	
	var _isPlainObject2 = _interopRequireDefault(_isPlainObject);
	
	var _warning = __webpack_require__(12);
	
	var _warning2 = _interopRequireDefault(_warning);
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { "default": obj };
	}
	
	function getUndefinedStateErrorMessage(key, action) {
	  var actionType = action && action.type;
	  var actionName = actionType && '"' + actionType.toString() + '"' || 'an action';
	
	  return 'Given action ' + actionName + ', reducer "' + key + '" returned undefined. ' + 'To ignore an action, you must explicitly return the previous state.';
	}
	
	function getUnexpectedStateShapeWarningMessage(inputState, reducers, action) {
	  var reducerKeys = Object.keys(reducers);
	  var argumentName = action && action.type === _createStore.ActionTypes.INIT ? 'initialState argument passed to createStore' : 'previous state received by the reducer';
	
	  if (reducerKeys.length === 0) {
	    return 'Store does not have a valid reducer. Make sure the argument passed ' + 'to combineReducers is an object whose values are reducers.';
	  }
	
	  if (!(0, _isPlainObject2["default"])(inputState)) {
	    return 'The ' + argumentName + ' has unexpected type of "' + {}.toString.call(inputState).match(/\s([a-z|A-Z]+)/)[1] + '". Expected argument to be an object with the following ' + ('keys: "' + reducerKeys.join('", "') + '"');
	  }
	
	  var unexpectedKeys = Object.keys(inputState).filter(function (key) {
	    return !reducers.hasOwnProperty(key);
	  });
	
	  if (unexpectedKeys.length > 0) {
	    return 'Unexpected ' + (unexpectedKeys.length > 1 ? 'keys' : 'key') + ' ' + ('"' + unexpectedKeys.join('", "') + '" found in ' + argumentName + '. ') + 'Expected to find one of the known reducer keys instead: ' + ('"' + reducerKeys.join('", "') + '". Unexpected keys will be ignored.');
	  }
	}
	
	function assertReducerSanity(reducers) {
	  Object.keys(reducers).forEach(function (key) {
	    var reducer = reducers[key];
	    var initialState = reducer(undefined, { type: _createStore.ActionTypes.INIT });
	
	    if (typeof initialState === 'undefined') {
	      throw new Error('Reducer "' + key + '" returned undefined during initialization. ' + 'If the state passed to the reducer is undefined, you must ' + 'explicitly return the initial state. The initial state may ' + 'not be undefined.');
	    }
	
	    var type = '@@redux/PROBE_UNKNOWN_ACTION_' + Math.random().toString(36).substring(7).split('').join('.');
	    if (typeof reducer(undefined, { type: type }) === 'undefined') {
	      throw new Error('Reducer "' + key + '" returned undefined when probed with a random type. ' + ('Don\'t try to handle ' + _createStore.ActionTypes.INIT + ' or other actions in "redux/*" ') + 'namespace. They are considered private. Instead, you must return the ' + 'current state for any unknown actions, unless it is undefined, ' + 'in which case you must return the initial state, regardless of the ' + 'action type. The initial state may not be undefined.');
	    }
	  });
	}
	
	/**
	 * Turns an object whose values are different reducer functions, into a single
	 * reducer function. It will call every child reducer, and gather their results
	 * into a single state object, whose keys correspond to the keys of the passed
	 * reducer functions.
	 *
	 * @param {Object} reducers An object whose values correspond to different
	 * reducer functions that need to be combined into one. One handy way to obtain
	 * it is to use ES6 `import * as reducers` syntax. The reducers may never return
	 * undefined for any action. Instead, they should return their initial state
	 * if the state passed to them was undefined, and the current state for any
	 * unrecognized action.
	 *
	 * @returns {Function} A reducer function that invokes every reducer inside the
	 * passed object, and builds a state object with the same shape.
	 */
	function combineReducers(reducers) {
	  var reducerKeys = Object.keys(reducers);
	  var finalReducers = {};
	  for (var i = 0; i < reducerKeys.length; i++) {
	    var key = reducerKeys[i];
	    if (typeof reducers[key] === 'function') {
	      finalReducers[key] = reducers[key];
	    }
	  }
	  var finalReducerKeys = Object.keys(finalReducers);
	
	  var sanityError;
	  try {
	    assertReducerSanity(finalReducers);
	  } catch (e) {
	    sanityError = e;
	  }
	
	  return function combination() {
	    var state = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
	    var action = arguments[1];
	
	    if (sanityError) {
	      throw sanityError;
	    }
	
	    if (process.env.NODE_ENV !== 'production') {
	      var warningMessage = getUnexpectedStateShapeWarningMessage(state, finalReducers, action);
	      if (warningMessage) {
	        (0, _warning2["default"])(warningMessage);
	      }
	    }
	
	    var hasChanged = false;
	    var nextState = {};
	    for (var i = 0; i < finalReducerKeys.length; i++) {
	      var key = finalReducerKeys[i];
	      var reducer = finalReducers[key];
	      var previousStateForKey = state[key];
	      var nextStateForKey = reducer(previousStateForKey, action);
	      if (typeof nextStateForKey === 'undefined') {
	        var errorMessage = getUndefinedStateErrorMessage(key, action);
	        throw new Error(errorMessage);
	      }
	      nextState[key] = nextStateForKey;
	      hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
	    }
	    return hasChanged ? nextState : state;
	  };
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ },
/* 12 */
/***/ function(module, exports) {

	'use strict';
	
	exports.__esModule = true;
	exports["default"] = warning;
	/**
	 * Prints a warning in the console if it exists.
	 *
	 * @param {String} message The warning message.
	 * @returns {void}
	 */
	function warning(message) {
	  /* eslint-disable no-console */
	  if (typeof console !== 'undefined' && typeof console.error === 'function') {
	    console.error(message);
	  }
	  /* eslint-enable no-console */
	  try {
	    // This error was thrown as a convenience so that if you enable
	    // "break on all exceptions" in your console,
	    // it would pause the execution at this line.
	    throw new Error(message);
	    /* eslint-disable no-empty */
	  } catch (e) {}
	  /* eslint-enable no-empty */
	}

/***/ },
/* 13 */
/***/ function(module, exports) {

	'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	exports.__esModule = true;
	exports["default"] = bindActionCreators;
	function bindActionCreator(actionCreator, dispatch) {
	  return function () {
	    return dispatch(actionCreator.apply(undefined, arguments));
	  };
	}
	
	/**
	 * Turns an object whose values are action creators, into an object with the
	 * same keys, but with every function wrapped into a `dispatch` call so they
	 * may be invoked directly. This is just a convenience method, as you can call
	 * `store.dispatch(MyActionCreators.doSomething())` yourself just fine.
	 *
	 * For convenience, you can also pass a single function as the first argument,
	 * and get a function in return.
	 *
	 * @param {Function|Object} actionCreators An object whose values are action
	 * creator functions. One handy way to obtain it is to use ES6 `import * as`
	 * syntax. You may also pass a single function.
	 *
	 * @param {Function} dispatch The `dispatch` function available on your Redux
	 * store.
	 *
	 * @returns {Function|Object} The object mimicking the original object, but with
	 * every action creator wrapped into the `dispatch` call. If you passed a
	 * function as `actionCreators`, the return value will also be a single
	 * function.
	 */
	function bindActionCreators(actionCreators, dispatch) {
	  if (typeof actionCreators === 'function') {
	    return bindActionCreator(actionCreators, dispatch);
	  }
	
	  if ((typeof actionCreators === 'undefined' ? 'undefined' : _typeof(actionCreators)) !== 'object' || actionCreators === null) {
	    throw new Error('bindActionCreators expected an object or a function, instead received ' + (actionCreators === null ? 'null' : typeof actionCreators === 'undefined' ? 'undefined' : _typeof(actionCreators)) + '. ' + 'Did you write "import ActionCreators from" instead of "import * as ActionCreators from"?');
	  }
	
	  var keys = Object.keys(actionCreators);
	  var boundActionCreators = {};
	  for (var i = 0; i < keys.length; i++) {
	    var key = keys[i];
	    var actionCreator = actionCreators[key];
	    if (typeof actionCreator === 'function') {
	      boundActionCreators[key] = bindActionCreator(actionCreator, dispatch);
	    }
	  }
	  return boundActionCreators;
	}

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	var _extends = Object.assign || function (target) {
	  for (var i = 1; i < arguments.length; i++) {
	    var source = arguments[i];for (var key in source) {
	      if (Object.prototype.hasOwnProperty.call(source, key)) {
	        target[key] = source[key];
	      }
	    }
	  }return target;
	};
	
	exports["default"] = applyMiddleware;
	
	var _compose = __webpack_require__(15);
	
	var _compose2 = _interopRequireDefault(_compose);
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { "default": obj };
	}
	
	/**
	 * Creates a store enhancer that applies middleware to the dispatch method
	 * of the Redux store. This is handy for a variety of tasks, such as expressing
	 * asynchronous actions in a concise manner, or logging every action payload.
	 *
	 * See `redux-thunk` package as an example of the Redux middleware.
	 *
	 * Because middleware is potentially asynchronous, this should be the first
	 * store enhancer in the composition chain.
	 *
	 * Note that each middleware will be given the `dispatch` and `getState` functions
	 * as named arguments.
	 *
	 * @param {...Function} middlewares The middleware chain to be applied.
	 * @returns {Function} A store enhancer applying the middleware.
	 */
	function applyMiddleware() {
	  for (var _len = arguments.length, middlewares = Array(_len), _key = 0; _key < _len; _key++) {
	    middlewares[_key] = arguments[_key];
	  }
	
	  return function (createStore) {
	    return function (reducer, initialState, enhancer) {
	      var store = createStore(reducer, initialState, enhancer);
	      var _dispatch = store.dispatch;
	      var chain = [];
	
	      var middlewareAPI = {
	        getState: store.getState,
	        dispatch: function dispatch(action) {
	          return _dispatch(action);
	        }
	      };
	      chain = middlewares.map(function (middleware) {
	        return middleware(middlewareAPI);
	      });
	      _dispatch = _compose2["default"].apply(undefined, chain)(store.dispatch);
	
	      return _extends({}, store, {
	        dispatch: _dispatch
	      });
	    };
	  };
	}

/***/ },
/* 15 */
/***/ function(module, exports) {

	"use strict";
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	exports.__esModule = true;
	exports["default"] = compose;
	/**
	 * Composes single-argument functions from right to left. The rightmost
	 * function can take multiple arguments as it provides the signature for
	 * the resulting composite function.
	 *
	 * @param {...Function} funcs The functions to compose.
	 * @returns {Function} A function obtained by composing the argument functions
	 * from right to left. For example, compose(f, g, h) is identical to doing
	 * (...args) => f(g(h(...args))).
	 */
	
	function compose() {
	  for (var _len = arguments.length, funcs = Array(_len), _key = 0; _key < _len; _key++) {
	    funcs[_key] = arguments[_key];
	  }
	
	  if (funcs.length === 0) {
	    return function (arg) {
	      return arg;
	    };
	  } else {
	    var _ret = function () {
	      var last = funcs[funcs.length - 1];
	      var rest = funcs.slice(0, -1);
	      return {
	        v: function v() {
	          return rest.reduceRight(function (composed, f) {
	            return f(composed);
	          }, last.apply(undefined, arguments));
	        }
	      };
	    }();
	
	    if ((typeof _ret === "undefined" ? "undefined" : _typeof(_ret)) === "object") return _ret.v;
	  }
	}

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	module.exports = __webpack_require__(17);

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	var bind = __webpack_require__(19);
	var Axios = __webpack_require__(20);
	
	/**
	 * Create an instance of Axios
	 *
	 * @param {Object} defaultConfig The default config for the instance
	 * @return {Axios} A new instance of Axios
	 */
	function createInstance(defaultConfig) {
	  var context = new Axios(defaultConfig);
	  var instance = bind(Axios.prototype.request, context);
	
	  // Copy axios.prototype to instance
	  utils.extend(instance, Axios.prototype, context);
	
	  // Copy context to instance
	  utils.extend(instance, context);
	
	  return instance;
	}
	
	// Create the default instance to be exported
	var axios = createInstance();
	
	// Expose Axios class to allow class inheritance
	axios.Axios = Axios;
	
	// Factory for creating new instances
	axios.create = function create(defaultConfig) {
	  return createInstance(defaultConfig);
	};
	
	// Expose all/spread
	axios.all = function all(promises) {
	  return Promise.all(promises);
	};
	axios.spread = __webpack_require__(37);
	
	module.exports = axios;
	
	// Allow use of default import syntax in TypeScript
	module.exports.default = axios;

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	var bind = __webpack_require__(19);
	
	/*global toString:true*/
	
	// utils is a library of generic helper functions non-specific to axios
	
	var toString = Object.prototype.toString;
	
	/**
	 * Determine if a value is an Array
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an Array, otherwise false
	 */
	function isArray(val) {
	  return toString.call(val) === '[object Array]';
	}
	
	/**
	 * Determine if a value is an ArrayBuffer
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
	 */
	function isArrayBuffer(val) {
	  return toString.call(val) === '[object ArrayBuffer]';
	}
	
	/**
	 * Determine if a value is a FormData
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an FormData, otherwise false
	 */
	function isFormData(val) {
	  return typeof FormData !== 'undefined' && val instanceof FormData;
	}
	
	/**
	 * Determine if a value is a view on an ArrayBuffer
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
	 */
	function isArrayBufferView(val) {
	  var result;
	  if (typeof ArrayBuffer !== 'undefined' && ArrayBuffer.isView) {
	    result = ArrayBuffer.isView(val);
	  } else {
	    result = val && val.buffer && val.buffer instanceof ArrayBuffer;
	  }
	  return result;
	}
	
	/**
	 * Determine if a value is a String
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a String, otherwise false
	 */
	function isString(val) {
	  return typeof val === 'string';
	}
	
	/**
	 * Determine if a value is a Number
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Number, otherwise false
	 */
	function isNumber(val) {
	  return typeof val === 'number';
	}
	
	/**
	 * Determine if a value is undefined
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if the value is undefined, otherwise false
	 */
	function isUndefined(val) {
	  return typeof val === 'undefined';
	}
	
	/**
	 * Determine if a value is an Object
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an Object, otherwise false
	 */
	function isObject(val) {
	  return val !== null && (typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object';
	}
	
	/**
	 * Determine if a value is a Date
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Date, otherwise false
	 */
	function isDate(val) {
	  return toString.call(val) === '[object Date]';
	}
	
	/**
	 * Determine if a value is a File
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a File, otherwise false
	 */
	function isFile(val) {
	  return toString.call(val) === '[object File]';
	}
	
	/**
	 * Determine if a value is a Blob
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Blob, otherwise false
	 */
	function isBlob(val) {
	  return toString.call(val) === '[object Blob]';
	}
	
	/**
	 * Determine if a value is a Function
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Function, otherwise false
	 */
	function isFunction(val) {
	  return toString.call(val) === '[object Function]';
	}
	
	/**
	 * Determine if a value is a Stream
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Stream, otherwise false
	 */
	function isStream(val) {
	  return isObject(val) && isFunction(val.pipe);
	}
	
	/**
	 * Determine if a value is a URLSearchParams object
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
	 */
	function isURLSearchParams(val) {
	  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
	}
	
	/**
	 * Trim excess whitespace off the beginning and end of a string
	 *
	 * @param {String} str The String to trim
	 * @returns {String} The String freed of excess whitespace
	 */
	function trim(str) {
	  return str.replace(/^\s*/, '').replace(/\s*$/, '');
	}
	
	/**
	 * Determine if we're running in a standard browser environment
	 *
	 * This allows axios to run in a web worker, and react-native.
	 * Both environments support XMLHttpRequest, but not fully standard globals.
	 *
	 * web workers:
	 *  typeof window -> undefined
	 *  typeof document -> undefined
	 *
	 * react-native:
	 *  typeof document.createElement -> undefined
	 */
	function isStandardBrowserEnv() {
	  return typeof window !== 'undefined' && typeof document !== 'undefined' && typeof document.createElement === 'function';
	}
	
	/**
	 * Iterate over an Array or an Object invoking a function for each item.
	 *
	 * If `obj` is an Array callback will be called passing
	 * the value, index, and complete array for each item.
	 *
	 * If 'obj' is an Object callback will be called passing
	 * the value, key, and complete object for each property.
	 *
	 * @param {Object|Array} obj The object to iterate
	 * @param {Function} fn The callback to invoke for each item
	 */
	function forEach(obj, fn) {
	  // Don't bother if no value provided
	  if (obj === null || typeof obj === 'undefined') {
	    return;
	  }
	
	  // Force an array if not already something iterable
	  if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object' && !isArray(obj)) {
	    /*eslint no-param-reassign:0*/
	    obj = [obj];
	  }
	
	  if (isArray(obj)) {
	    // Iterate over array values
	    for (var i = 0, l = obj.length; i < l; i++) {
	      fn.call(null, obj[i], i, obj);
	    }
	  } else {
	    // Iterate over object keys
	    for (var key in obj) {
	      if (obj.hasOwnProperty(key)) {
	        fn.call(null, obj[key], key, obj);
	      }
	    }
	  }
	}
	
	/**
	 * Accepts varargs expecting each argument to be an object, then
	 * immutably merges the properties of each object and returns result.
	 *
	 * When multiple objects contain the same key the later object in
	 * the arguments list will take precedence.
	 *
	 * Example:
	 *
	 * ```js
	 * var result = merge({foo: 123}, {foo: 456});
	 * console.log(result.foo); // outputs 456
	 * ```
	 *
	 * @param {Object} obj1 Object to merge
	 * @returns {Object} Result of all merge properties
	 */
	function merge() /* obj1, obj2, obj3, ... */{
	  var result = {};
	  function assignValue(val, key) {
	    if (_typeof(result[key]) === 'object' && (typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object') {
	      result[key] = merge(result[key], val);
	    } else {
	      result[key] = val;
	    }
	  }
	
	  for (var i = 0, l = arguments.length; i < l; i++) {
	    forEach(arguments[i], assignValue);
	  }
	  return result;
	}
	
	/**
	 * Extends object a by mutably adding to it the properties of object b.
	 *
	 * @param {Object} a The object to be extended
	 * @param {Object} b The object to copy properties from
	 * @param {Object} thisArg The object to bind function to
	 * @return {Object} The resulting value of object a
	 */
	function extend(a, b, thisArg) {
	  forEach(b, function assignValue(val, key) {
	    if (thisArg && typeof val === 'function') {
	      a[key] = bind(val, thisArg);
	    } else {
	      a[key] = val;
	    }
	  });
	  return a;
	}
	
	module.exports = {
	  isArray: isArray,
	  isArrayBuffer: isArrayBuffer,
	  isFormData: isFormData,
	  isArrayBufferView: isArrayBufferView,
	  isString: isString,
	  isNumber: isNumber,
	  isObject: isObject,
	  isUndefined: isUndefined,
	  isDate: isDate,
	  isFile: isFile,
	  isBlob: isBlob,
	  isFunction: isFunction,
	  isStream: isStream,
	  isURLSearchParams: isURLSearchParams,
	  isStandardBrowserEnv: isStandardBrowserEnv,
	  forEach: forEach,
	  merge: merge,
	  extend: extend,
	  trim: trim
	};

/***/ },
/* 19 */
/***/ function(module, exports) {

	'use strict';
	
	module.exports = function bind(fn, thisArg) {
	  return function wrap() {
	    var args = new Array(arguments.length);
	    for (var i = 0; i < args.length; i++) {
	      args[i] = arguments[i];
	    }
	    return fn.apply(thisArg, args);
	  };
	};

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var defaults = __webpack_require__(21);
	var utils = __webpack_require__(18);
	var InterceptorManager = __webpack_require__(23);
	var dispatchRequest = __webpack_require__(24);
	var isAbsoluteURL = __webpack_require__(35);
	var combineURLs = __webpack_require__(36);
	
	/**
	 * Create a new instance of Axios
	 *
	 * @param {Object} defaultConfig The default config for the instance
	 */
	function Axios(defaultConfig) {
	  this.defaults = utils.merge(defaults, defaultConfig);
	  this.interceptors = {
	    request: new InterceptorManager(),
	    response: new InterceptorManager()
	  };
	}
	
	/**
	 * Dispatch a request
	 *
	 * @param {Object} config The config specific for this request (merged with this.defaults)
	 */
	Axios.prototype.request = function request(config) {
	  /*eslint no-param-reassign:0*/
	  // Allow for axios('example/url'[, config]) a la fetch API
	  if (typeof config === 'string') {
	    config = utils.merge({
	      url: arguments[0]
	    }, arguments[1]);
	  }
	
	  config = utils.merge(defaults, this.defaults, { method: 'get' }, config);
	
	  // Support baseURL config
	  if (config.baseURL && !isAbsoluteURL(config.url)) {
	    config.url = combineURLs(config.baseURL, config.url);
	  }
	
	  // Hook up interceptors middleware
	  var chain = [dispatchRequest, undefined];
	  var promise = Promise.resolve(config);
	
	  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
	    chain.unshift(interceptor.fulfilled, interceptor.rejected);
	  });
	
	  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
	    chain.push(interceptor.fulfilled, interceptor.rejected);
	  });
	
	  while (chain.length) {
	    promise = promise.then(chain.shift(), chain.shift());
	  }
	
	  return promise;
	};
	
	// Provide aliases for supported request methods
	utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
	  /*eslint func-names:0*/
	  Axios.prototype[method] = function (url, config) {
	    return this.request(utils.merge(config || {}, {
	      method: method,
	      url: url
	    }));
	  };
	});
	
	utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
	  /*eslint func-names:0*/
	  Axios.prototype[method] = function (url, data, config) {
	    return this.request(utils.merge(config || {}, {
	      method: method,
	      url: url,
	      data: data
	    }));
	  };
	});
	
	module.exports = Axios;

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	var normalizeHeaderName = __webpack_require__(22);
	
	var PROTECTION_PREFIX = /^\)\]\}',?\n/;
	var DEFAULT_CONTENT_TYPE = {
	  'Content-Type': 'application/x-www-form-urlencoded'
	};
	
	function setContentTypeIfUnset(headers, value) {
	  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
	    headers['Content-Type'] = value;
	  }
	}
	
	module.exports = {
	  transformRequest: [function transformRequest(data, headers) {
	    normalizeHeaderName(headers, 'Content-Type');
	    if (utils.isFormData(data) || utils.isArrayBuffer(data) || utils.isStream(data) || utils.isFile(data) || utils.isBlob(data)) {
	      return data;
	    }
	    if (utils.isArrayBufferView(data)) {
	      return data.buffer;
	    }
	    if (utils.isURLSearchParams(data)) {
	      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
	      return data.toString();
	    }
	    if (utils.isObject(data)) {
	      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
	      return JSON.stringify(data);
	    }
	    return data;
	  }],
	
	  transformResponse: [function transformResponse(data) {
	    /*eslint no-param-reassign:0*/
	    if (typeof data === 'string') {
	      data = data.replace(PROTECTION_PREFIX, '');
	      try {
	        data = JSON.parse(data);
	      } catch (e) {/* Ignore */}
	    }
	    return data;
	  }],
	
	  headers: {
	    common: {
	      'Accept': 'application/json, text/plain, */*'
	    },
	    patch: utils.merge(DEFAULT_CONTENT_TYPE),
	    post: utils.merge(DEFAULT_CONTENT_TYPE),
	    put: utils.merge(DEFAULT_CONTENT_TYPE)
	  },
	
	  timeout: 0,
	
	  xsrfCookieName: 'XSRF-TOKEN',
	  xsrfHeaderName: 'X-XSRF-TOKEN',
	
	  maxContentLength: -1,
	
	  validateStatus: function validateStatus(status) {
	    return status >= 200 && status < 300;
	  }
	};

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	
	module.exports = function normalizeHeaderName(headers, normalizedName) {
	  utils.forEach(headers, function processHeader(value, name) {
	    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
	      headers[normalizedName] = value;
	      delete headers[name];
	    }
	  });
	};

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	
	function InterceptorManager() {
	  this.handlers = [];
	}
	
	/**
	 * Add a new interceptor to the stack
	 *
	 * @param {Function} fulfilled The function to handle `then` for a `Promise`
	 * @param {Function} rejected The function to handle `reject` for a `Promise`
	 *
	 * @return {Number} An ID used to remove interceptor later
	 */
	InterceptorManager.prototype.use = function use(fulfilled, rejected) {
	  this.handlers.push({
	    fulfilled: fulfilled,
	    rejected: rejected
	  });
	  return this.handlers.length - 1;
	};
	
	/**
	 * Remove an interceptor from the stack
	 *
	 * @param {Number} id The ID that was returned by `use`
	 */
	InterceptorManager.prototype.eject = function eject(id) {
	  if (this.handlers[id]) {
	    this.handlers[id] = null;
	  }
	};
	
	/**
	 * Iterate over all the registered interceptors
	 *
	 * This method is particularly useful for skipping over any
	 * interceptors that may have become `null` calling `eject`.
	 *
	 * @param {Function} fn The function to call for each interceptor
	 */
	InterceptorManager.prototype.forEach = function forEach(fn) {
	  utils.forEach(this.handlers, function forEachHandler(h) {
	    if (h !== null) {
	      fn(h);
	    }
	  });
	};
	
	module.exports = InterceptorManager;

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	var utils = __webpack_require__(18);
	var transformData = __webpack_require__(25);
	
	/**
	 * Dispatch a request to the server using whichever adapter
	 * is supported by the current environment.
	 *
	 * @param {object} config The config that is to be used for the request
	 * @returns {Promise} The Promise to be fulfilled
	 */
	module.exports = function dispatchRequest(config) {
	  // Ensure headers exist
	  config.headers = config.headers || {};
	
	  // Transform request data
	  config.data = transformData(config.data, config.headers, config.transformRequest);
	
	  // Flatten headers
	  config.headers = utils.merge(config.headers.common || {}, config.headers[config.method] || {}, config.headers || {});
	
	  utils.forEach(['delete', 'get', 'head', 'post', 'put', 'patch', 'common'], function cleanHeaderConfig(method) {
	    delete config.headers[method];
	  });
	
	  var adapter;
	
	  if (typeof config.adapter === 'function') {
	    // For custom adapter support
	    adapter = config.adapter;
	  } else if (typeof XMLHttpRequest !== 'undefined') {
	    // For browsers use XHR adapter
	    adapter = __webpack_require__(26);
	  } else if (typeof process !== 'undefined') {
	    // For node use HTTP adapter
	    adapter = __webpack_require__(26);
	  }
	
	  return Promise.resolve(config)
	  // Wrap synchronous adapter errors and pass configuration
	  .then(adapter).then(function onFulfilled(response) {
	    // Transform response data
	    response.data = transformData(response.data, response.headers, config.transformResponse);
	
	    return response;
	  }, function onRejected(error) {
	    // Transform response data
	    if (error && error.response) {
	      error.response.data = transformData(error.response.data, error.response.headers, config.transformResponse);
	    }
	
	    return Promise.reject(error);
	  });
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	
	/**
	 * Transform the data for a request or a response
	 *
	 * @param {Object|String} data The data to be transformed
	 * @param {Array} headers The headers for the request or response
	 * @param {Array|Function} fns A single function or Array of functions
	 * @returns {*} The resulting transformed data
	 */
	module.exports = function transformData(data, headers, fns) {
	  /*eslint no-param-reassign:0*/
	  utils.forEach(fns, function transform(fn) {
	    data = fn(data, headers);
	  });
	
	  return data;
	};

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	var utils = __webpack_require__(18);
	var settle = __webpack_require__(27);
	var buildURL = __webpack_require__(30);
	var parseHeaders = __webpack_require__(31);
	var isURLSameOrigin = __webpack_require__(32);
	var createError = __webpack_require__(28);
	var btoa = typeof window !== 'undefined' && window.btoa || __webpack_require__(33);
	
	module.exports = function xhrAdapter(config) {
	  return new Promise(function dispatchXhrRequest(resolve, reject) {
	    var requestData = config.data;
	    var requestHeaders = config.headers;
	
	    if (utils.isFormData(requestData)) {
	      delete requestHeaders['Content-Type']; // Let the browser set it
	    }
	
	    var request = new XMLHttpRequest();
	    var loadEvent = 'onreadystatechange';
	    var xDomain = false;
	
	    // For IE 8/9 CORS support
	    // Only supports POST and GET calls and doesn't returns the response headers.
	    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
	    if (process.env.NODE_ENV !== 'test' && typeof window !== 'undefined' && window.XDomainRequest && !('withCredentials' in request) && !isURLSameOrigin(config.url)) {
	      request = new window.XDomainRequest();
	      loadEvent = 'onload';
	      xDomain = true;
	      request.onprogress = function handleProgress() {};
	      request.ontimeout = function handleTimeout() {};
	    }
	
	    // HTTP basic authentication
	    if (config.auth) {
	      var username = config.auth.username || '';
	      var password = config.auth.password || '';
	      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
	    }
	
	    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);
	
	    // Set the request timeout in MS
	    request.timeout = config.timeout;
	
	    // Listen for ready state
	    request[loadEvent] = function handleLoad() {
	      if (!request || request.readyState !== 4 && !xDomain) {
	        return;
	      }
	
	      // The request errored out and we didn't get a response, this will be
	      // handled by onerror instead
	      if (request.status === 0) {
	        return;
	      }
	
	      // Prepare the response
	      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
	      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
	      var response = {
	        data: responseData,
	        // IE sends 1223 instead of 204 (https://github.com/mzabriskie/axios/issues/201)
	        status: request.status === 1223 ? 204 : request.status,
	        statusText: request.status === 1223 ? 'No Content' : request.statusText,
	        headers: responseHeaders,
	        config: config,
	        request: request
	      };
	
	      settle(resolve, reject, response);
	
	      // Clean up request
	      request = null;
	    };
	
	    // Handle low level network errors
	    request.onerror = function handleError() {
	      // Real errors are hidden from us by the browser
	      // onerror should only fire if it's a network error
	      reject(createError('Network Error', config));
	
	      // Clean up request
	      request = null;
	    };
	
	    // Handle timeout
	    request.ontimeout = function handleTimeout() {
	      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED'));
	
	      // Clean up request
	      request = null;
	    };
	
	    // Add xsrf header
	    // This is only done if running in a standard browser environment.
	    // Specifically not if we're in a web worker, or react-native.
	    if (utils.isStandardBrowserEnv()) {
	      var cookies = __webpack_require__(34);
	
	      // Add xsrf header
	      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ? cookies.read(config.xsrfCookieName) : undefined;
	
	      if (xsrfValue) {
	        requestHeaders[config.xsrfHeaderName] = xsrfValue;
	      }
	    }
	
	    // Add headers to the request
	    if ('setRequestHeader' in request) {
	      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
	        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
	          // Remove Content-Type if data is undefined
	          delete requestHeaders[key];
	        } else {
	          // Otherwise add header to the request
	          request.setRequestHeader(key, val);
	        }
	      });
	    }
	
	    // Add withCredentials to request if needed
	    if (config.withCredentials) {
	      request.withCredentials = true;
	    }
	
	    // Add responseType to request if needed
	    if (config.responseType) {
	      try {
	        request.responseType = config.responseType;
	      } catch (e) {
	        if (request.responseType !== 'json') {
	          throw e;
	        }
	      }
	    }
	
	    // Handle progress if needed
	    if (typeof config.onDownloadProgress === 'function') {
	      request.addEventListener('progress', config.onDownloadProgress);
	    }
	
	    // Not all browsers support upload events
	    if (typeof config.onUploadProgress === 'function' && request.upload) {
	      request.upload.addEventListener('progress', config.onUploadProgress);
	    }
	
	    if (requestData === undefined) {
	      requestData = null;
	    }
	
	    // Send the request
	    request.send(requestData);
	  });
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var createError = __webpack_require__(28);
	
	/**
	 * Resolve or reject a Promise based on response status.
	 *
	 * @param {Function} resolve A function that resolves the promise.
	 * @param {Function} reject A function that rejects the promise.
	 * @param {object} response The response.
	 */
	module.exports = function settle(resolve, reject, response) {
	  var validateStatus = response.config.validateStatus;
	  // Note: status is not exposed by XDomainRequest
	  if (!response.status || !validateStatus || validateStatus(response.status)) {
	    resolve(response);
	  } else {
	    reject(createError('Request failed with status code ' + response.status, response.config, null, response));
	  }
	};

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var enhanceError = __webpack_require__(29);
	
	/**
	 * Create an Error with the specified message, config, error code, and response.
	 *
	 * @param {string} message The error message.
	 * @param {Object} config The config.
	 * @param {string} [code] The error code (for example, 'ECONNABORTED').
	 @ @param {Object} [response] The response.
	 * @returns {Error} The created error.
	 */
	module.exports = function createError(message, config, code, response) {
	  var error = new Error(message);
	  return enhanceError(error, config, code, response);
	};

/***/ },
/* 29 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 * Update an Error with the specified config, error code, and response.
	 *
	 * @param {Error} error The error to update.
	 * @param {Object} config The config.
	 * @param {string} [code] The error code (for example, 'ECONNABORTED').
	 @ @param {Object} [response] The response.
	 * @returns {Error} The error.
	 */
	
	module.exports = function enhanceError(error, config, code, response) {
	  error.config = config;
	  if (code) {
	    error.code = code;
	  }
	  error.response = response;
	  return error;
	};

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	
	function encode(val) {
	  return encodeURIComponent(val).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, '+').replace(/%5B/gi, '[').replace(/%5D/gi, ']');
	}
	
	/**
	 * Build a URL by appending params to the end
	 *
	 * @param {string} url The base of the url (e.g., http://www.google.com)
	 * @param {object} [params] The params to be appended
	 * @returns {string} The formatted url
	 */
	module.exports = function buildURL(url, params, paramsSerializer) {
	  /*eslint no-param-reassign:0*/
	  if (!params) {
	    return url;
	  }
	
	  var serializedParams;
	  if (paramsSerializer) {
	    serializedParams = paramsSerializer(params);
	  } else if (utils.isURLSearchParams(params)) {
	    serializedParams = params.toString();
	  } else {
	    var parts = [];
	
	    utils.forEach(params, function serialize(val, key) {
	      if (val === null || typeof val === 'undefined') {
	        return;
	      }
	
	      if (utils.isArray(val)) {
	        key = key + '[]';
	      }
	
	      if (!utils.isArray(val)) {
	        val = [val];
	      }
	
	      utils.forEach(val, function parseValue(v) {
	        if (utils.isDate(v)) {
	          v = v.toISOString();
	        } else if (utils.isObject(v)) {
	          v = JSON.stringify(v);
	        }
	        parts.push(encode(key) + '=' + encode(v));
	      });
	    });
	
	    serializedParams = parts.join('&');
	  }
	
	  if (serializedParams) {
	    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
	  }
	
	  return url;
	};

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	
	/**
	 * Parse headers into an object
	 *
	 * ```
	 * Date: Wed, 27 Aug 2014 08:58:49 GMT
	 * Content-Type: application/json
	 * Connection: keep-alive
	 * Transfer-Encoding: chunked
	 * ```
	 *
	 * @param {String} headers Headers needing to be parsed
	 * @returns {Object} Headers parsed into an object
	 */
	module.exports = function parseHeaders(headers) {
	  var parsed = {};
	  var key;
	  var val;
	  var i;
	
	  if (!headers) {
	    return parsed;
	  }
	
	  utils.forEach(headers.split('\n'), function parser(line) {
	    i = line.indexOf(':');
	    key = utils.trim(line.substr(0, i)).toLowerCase();
	    val = utils.trim(line.substr(i + 1));
	
	    if (key) {
	      parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
	    }
	  });
	
	  return parsed;
	};

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	
	module.exports = utils.isStandardBrowserEnv() ?
	
	// Standard browser envs have full support of the APIs needed to test
	// whether the request URL is of the same origin as current location.
	function standardBrowserEnv() {
	  var msie = /(msie|trident)/i.test(navigator.userAgent);
	  var urlParsingNode = document.createElement('a');
	  var originURL;
	
	  /**
	  * Parse a URL to discover it's components
	  *
	  * @param {String} url The URL to be parsed
	  * @returns {Object}
	  */
	  function resolveURL(url) {
	    var href = url;
	
	    if (msie) {
	      // IE needs attribute set twice to normalize properties
	      urlParsingNode.setAttribute('href', href);
	      href = urlParsingNode.href;
	    }
	
	    urlParsingNode.setAttribute('href', href);
	
	    // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
	    return {
	      href: urlParsingNode.href,
	      protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
	      host: urlParsingNode.host,
	      search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
	      hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
	      hostname: urlParsingNode.hostname,
	      port: urlParsingNode.port,
	      pathname: urlParsingNode.pathname.charAt(0) === '/' ? urlParsingNode.pathname : '/' + urlParsingNode.pathname
	    };
	  }
	
	  originURL = resolveURL(window.location.href);
	
	  /**
	  * Determine if a URL shares the same origin as the current location
	  *
	  * @param {String} requestURL The URL to test
	  * @returns {boolean} True if URL shares the same origin, otherwise false
	  */
	  return function isURLSameOrigin(requestURL) {
	    var parsed = utils.isString(requestURL) ? resolveURL(requestURL) : requestURL;
	    return parsed.protocol === originURL.protocol && parsed.host === originURL.host;
	  };
	}() :
	
	// Non standard browser envs (web workers, react-native) lack needed support.
	function nonStandardBrowserEnv() {
	  return function isURLSameOrigin() {
	    return true;
	  };
	}();

/***/ },
/* 33 */
/***/ function(module, exports) {

	'use strict';
	
	// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js
	
	var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
	
	function E() {
	  this.message = 'String contains an invalid character';
	}
	E.prototype = new Error();
	E.prototype.code = 5;
	E.prototype.name = 'InvalidCharacterError';
	
	function btoa(input) {
	  var str = String(input);
	  var output = '';
	  for (
	  // initialize result and counter
	  var block, charCode, idx = 0, map = chars;
	  // if the next str index does not exist:
	  //   change the mapping table to "="
	  //   check if d has no fractional digits
	  str.charAt(idx | 0) || (map = '=', idx % 1);
	  // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
	  output += map.charAt(63 & block >> 8 - idx % 1 * 8)) {
	    charCode = str.charCodeAt(idx += 3 / 4);
	    if (charCode > 0xFF) {
	      throw new E();
	    }
	    block = block << 8 | charCode;
	  }
	  return output;
	}
	
	module.exports = btoa;

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var utils = __webpack_require__(18);
	
	module.exports = utils.isStandardBrowserEnv() ?
	
	// Standard browser envs support document.cookie
	function standardBrowserEnv() {
	  return {
	    write: function write(name, value, expires, path, domain, secure) {
	      var cookie = [];
	      cookie.push(name + '=' + encodeURIComponent(value));
	
	      if (utils.isNumber(expires)) {
	        cookie.push('expires=' + new Date(expires).toGMTString());
	      }
	
	      if (utils.isString(path)) {
	        cookie.push('path=' + path);
	      }
	
	      if (utils.isString(domain)) {
	        cookie.push('domain=' + domain);
	      }
	
	      if (secure === true) {
	        cookie.push('secure');
	      }
	
	      document.cookie = cookie.join('; ');
	    },
	
	    read: function read(name) {
	      var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
	      return match ? decodeURIComponent(match[3]) : null;
	    },
	
	    remove: function remove(name) {
	      this.write(name, '', Date.now() - 86400000);
	    }
	  };
	}() :
	
	// Non standard browser env (web workers, react-native) lack needed support.
	function nonStandardBrowserEnv() {
	  return {
	    write: function write() {},
	    read: function read() {
	      return null;
	    },
	    remove: function remove() {}
	  };
	}();

/***/ },
/* 35 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 * Determines whether the specified URL is absolute
	 *
	 * @param {string} url The URL to test
	 * @returns {boolean} True if the specified URL is absolute, otherwise false
	 */
	
	module.exports = function isAbsoluteURL(url) {
	  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
	  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
	  // by any combination of letters, digits, plus, period, or hyphen.
	  return (/^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url)
	  );
	};

/***/ },
/* 36 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 * Creates a new URL by combining the specified URLs
	 *
	 * @param {string} baseURL The base URL
	 * @param {string} relativeURL The relative URL
	 * @returns {string} The combined URL
	 */
	
	module.exports = function combineURLs(baseURL, relativeURL) {
	  return baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '');
	};

/***/ },
/* 37 */
/***/ function(module, exports) {

	'use strict';
	
	/**
	 * Syntactic sugar for invoking a function and expanding an array for arguments.
	 *
	 * Common use case would be to use `Function.prototype.apply`.
	 *
	 *  ```js
	 *  function f(x, y, z) {}
	 *  var args = [1, 2, 3];
	 *  f.apply(null, args);
	 *  ```
	 *
	 * With `spread` this example can be re-written.
	 *
	 *  ```js
	 *  spread(function(x, y, z) {})([1, 2, 3]);
	 *  ```
	 *
	 * @param {Function} callback
	 * @returns {Function}
	 */
	
	module.exports = function spread(callback) {
	  return function wrap(arr) {
	    return callback.apply(null, arr);
	  };
	};

/***/ },
/* 38 */
/***/ function(module, exports) {

	"use strict";
	
	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	function _toConsumableArray(arr) {
	  if (Array.isArray(arr)) {
	    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
	      arr2[i] = arr[i];
	    }return arr2;
	  } else {
	    return Array.from(arr);
	  }
	}
	
	function _typeof(obj) {
	  return obj && typeof Symbol !== "undefined" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	}
	
	var repeat = function repeat(str, times) {
	  return new Array(times + 1).join(str);
	};
	var pad = function pad(num, maxLength) {
	  return repeat("0", maxLength - num.toString().length) + num;
	};
	var formatTime = function formatTime(time) {
	  return "@ " + pad(time.getHours(), 2) + ":" + pad(time.getMinutes(), 2) + ":" + pad(time.getSeconds(), 2) + "." + pad(time.getMilliseconds(), 3);
	};
	
	// Use the new performance api to get better precision if available
	var timer = typeof performance !== "undefined" && typeof performance.now === "function" ? performance : Date;
	
	/**
	 * parse the level option of createLogger
	 *
	 * @property {string | function | object} level - console[level]
	 * @property {object} action
	 * @property {array} payload
	 * @property {string} type
	 */
	
	function getLogLevel(level, action, payload, type) {
	  switch (typeof level === "undefined" ? "undefined" : _typeof(level)) {
	    case "object":
	      return typeof level[type] === "function" ? level[type].apply(level, _toConsumableArray(payload)) : level[type];
	    case "function":
	      return level(action);
	    default:
	      return level;
	  }
	}
	
	/**
	 * Creates logger with followed options
	 *
	 * @namespace
	 * @property {object} options - options for logger
	 * @property {string | function | object} options.level - console[level]
	 * @property {boolean} options.duration - print duration of each action?
	 * @property {boolean} options.timestamp - print timestamp with each action?
	 * @property {object} options.colors - custom colors
	 * @property {object} options.logger - implementation of the `console` API
	 * @property {boolean} options.logErrors - should errors in action execution be caught, logged, and re-thrown?
	 * @property {boolean} options.collapsed - is group collapsed?
	 * @property {boolean} options.predicate - condition which resolves logger behavior
	 * @property {function} options.stateTransformer - transform state before print
	 * @property {function} options.actionTransformer - transform action before print
	 * @property {function} options.errorTransformer - transform error before print
	 */
	
	function createLogger() {
	  var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
	  var _options$level = options.level;
	  var level = _options$level === undefined ? "log" : _options$level;
	  var _options$logger = options.logger;
	  var logger = _options$logger === undefined ? console : _options$logger;
	  var _options$logErrors = options.logErrors;
	  var logErrors = _options$logErrors === undefined ? true : _options$logErrors;
	  var collapsed = options.collapsed;
	  var predicate = options.predicate;
	  var _options$duration = options.duration;
	  var duration = _options$duration === undefined ? false : _options$duration;
	  var _options$timestamp = options.timestamp;
	  var timestamp = _options$timestamp === undefined ? true : _options$timestamp;
	  var transformer = options.transformer;
	  var _options$stateTransfo = options.stateTransformer;
	  var // deprecated
	  stateTransformer = _options$stateTransfo === undefined ? function (state) {
	    return state;
	  } : _options$stateTransfo;
	  var _options$actionTransf = options.actionTransformer;
	  var actionTransformer = _options$actionTransf === undefined ? function (actn) {
	    return actn;
	  } : _options$actionTransf;
	  var _options$errorTransfo = options.errorTransformer;
	  var errorTransformer = _options$errorTransfo === undefined ? function (error) {
	    return error;
	  } : _options$errorTransfo;
	  var _options$colors = options.colors;
	  var colors = _options$colors === undefined ? {
	    title: function title() {
	      return "#000000";
	    },
	    prevState: function prevState() {
	      return "#9E9E9E";
	    },
	    action: function action() {
	      return "#03A9F4";
	    },
	    nextState: function nextState() {
	      return "#4CAF50";
	    },
	    error: function error() {
	      return "#F20404";
	    }
	  } : _options$colors;
	
	  // exit if console undefined
	
	  if (typeof logger === "undefined") {
	    return function () {
	      return function (next) {
	        return function (action) {
	          return next(action);
	        };
	      };
	    };
	  }
	
	  if (transformer) {
	    console.error("Option 'transformer' is deprecated, use stateTransformer instead");
	  }
	
	  var logBuffer = [];
	  function printBuffer() {
	    logBuffer.forEach(function (logEntry, key) {
	      var started = logEntry.started;
	      var startedTime = logEntry.startedTime;
	      var action = logEntry.action;
	      var prevState = logEntry.prevState;
	      var error = logEntry.error;
	      var took = logEntry.took;
	      var nextState = logEntry.nextState;
	
	      var nextEntry = logBuffer[key + 1];
	      if (nextEntry) {
	        nextState = nextEntry.prevState;
	        took = nextEntry.started - started;
	      }
	      // message
	      var formattedAction = actionTransformer(action);
	      var isCollapsed = typeof collapsed === "function" ? collapsed(function () {
	        return nextState;
	      }, action) : collapsed;
	
	      var formattedTime = formatTime(startedTime);
	      var titleCSS = colors.title ? "color: " + colors.title(formattedAction) + ";" : null;
	      var title = "action " + (timestamp ? formattedTime : "") + " " + formattedAction.type + " " + (duration ? "(in " + took.toFixed(2) + " ms)" : "");
	
	      // render
	      try {
	        if (isCollapsed) {
	          if (colors.title) logger.groupCollapsed("%c " + title, titleCSS);else logger.groupCollapsed(title);
	        } else {
	          if (colors.title) logger.group("%c " + title, titleCSS);else logger.group(title);
	        }
	      } catch (e) {
	        logger.log(title);
	      }
	
	      var prevStateLevel = getLogLevel(level, formattedAction, [prevState], "prevState");
	      var actionLevel = getLogLevel(level, formattedAction, [formattedAction], "action");
	      var errorLevel = getLogLevel(level, formattedAction, [error, prevState], "error");
	      var nextStateLevel = getLogLevel(level, formattedAction, [nextState], "nextState");
	
	      if (prevStateLevel) {
	        if (colors.prevState) logger[prevStateLevel]("%c prev state", "color: " + colors.prevState(prevState) + "; font-weight: bold", prevState);else logger[prevStateLevel]("prev state", prevState);
	      }
	
	      if (actionLevel) {
	        if (colors.action) logger[actionLevel]("%c action", "color: " + colors.action(formattedAction) + "; font-weight: bold", formattedAction);else logger[actionLevel]("action", formattedAction);
	      }
	
	      if (error && errorLevel) {
	        if (colors.error) logger[errorLevel]("%c error", "color: " + colors.error(error, prevState) + "; font-weight: bold", error);else logger[errorLevel]("error", error);
	      }
	
	      if (nextStateLevel) {
	        if (colors.nextState) logger[nextStateLevel]("%c next state", "color: " + colors.nextState(nextState) + "; font-weight: bold", nextState);else logger[nextStateLevel]("next state", nextState);
	      }
	
	      try {
	        logger.groupEnd();
	      } catch (e) {
	        logger.log("—— log end ——");
	      }
	    });
	    logBuffer.length = 0;
	  }
	
	  return function (_ref) {
	    var getState = _ref.getState;
	    return function (next) {
	      return function (action) {
	        // exit early if predicate function returns false
	        if (typeof predicate === "function" && !predicate(getState, action)) {
	          return next(action);
	        }
	
	        var logEntry = {};
	        logBuffer.push(logEntry);
	
	        logEntry.started = timer.now();
	        logEntry.startedTime = new Date();
	        logEntry.prevState = stateTransformer(getState());
	        logEntry.action = action;
	
	        var returnedValue = undefined;
	        if (logErrors) {
	          try {
	            returnedValue = next(action);
	          } catch (e) {
	            logEntry.error = errorTransformer(e);
	          }
	        } else {
	          returnedValue = next(action);
	        }
	
	        logEntry.took = timer.now() - logEntry.started;
	        logEntry.nextState = stateTransformer(getState());
	
	        printBuffer();
	
	        if (logEntry.error) throw logEntry.error;
	        return returnedValue;
	      };
	    };
	  };
	}
	
	module.exports = createLogger;

/***/ },
/* 39 */
/***/ function(module, exports) {

	'use strict';
	
	exports.__esModule = true;
	function createThunkMiddleware(extraArgument) {
	  return function (_ref) {
	    var dispatch = _ref.dispatch;
	    var getState = _ref.getState;
	    return function (next) {
	      return function (action) {
	        if (typeof action === 'function') {
	          return action(dispatch, getState, extraArgument);
	        }
	
	        return next(action);
	      };
	    };
	  };
	}
	
	var thunk = createThunkMiddleware();
	thunk.withExtraArgument = createThunkMiddleware;
	
	exports['default'] = thunk;

/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};
	
	var _extends = Object.assign || function (target) {
	  for (var i = 1; i < arguments.length; i++) {
	    var source = arguments[i];for (var key in source) {
	      if (Object.prototype.hasOwnProperty.call(source, key)) {
	        target[key] = source[key];
	      }
	    }
	  }return target;
	};
	
	var _slicedToArray = function () {
	  function sliceIterator(arr, i) {
	    var _arr = [];var _n = true;var _d = false;var _e = undefined;try {
	      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
	        _arr.push(_s.value);if (i && _arr.length === i) break;
	      }
	    } catch (err) {
	      _d = true;_e = err;
	    } finally {
	      try {
	        if (!_n && _i["return"]) _i["return"]();
	      } finally {
	        if (_d) throw _e;
	      }
	    }return _arr;
	  }return function (arr, i) {
	    if (Array.isArray(arr)) {
	      return arr;
	    } else if (Symbol.iterator in Object(arr)) {
	      return sliceIterator(arr, i);
	    } else {
	      throw new TypeError("Invalid attempt to destructure non-iterable instance");
	    }
	  };
	}();
	
	exports.default = promiseMiddleware;
	
	var _isPromise = __webpack_require__(41);
	
	var _isPromise2 = _interopRequireDefault(_isPromise);
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}
	
	var defaultTypes = ['PENDING', 'FULFILLED', 'REJECTED'];
	
	/**
	 * @function promiseMiddleware
	 * @description
	 * @returns {function} thunk
	 */
	function promiseMiddleware() {
	  var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
	
	  var promiseTypeSuffixes = config.promiseTypeSuffixes || defaultTypes;
	
	  return function (ref) {
	    var dispatch = ref.dispatch;
	
	    return function (next) {
	      return function (action) {
	        if (action.payload) {
	          if (!(0, _isPromise2.default)(action.payload) && !(0, _isPromise2.default)(action.payload.promise)) {
	            return next(action);
	          }
	        } else {
	          return next(action);
	        }
	
	        // Deconstruct the properties of the original action object to constants
	        var type = action.type;
	        var payload = action.payload;
	        var meta = action.meta;
	
	        // Assign values for promise type suffixes
	
	        var _promiseTypeSuffixes = _slicedToArray(promiseTypeSuffixes, 3);
	
	        var PENDING = _promiseTypeSuffixes[0];
	        var FULFILLED = _promiseTypeSuffixes[1];
	        var REJECTED = _promiseTypeSuffixes[2];
	
	        /**
	         * @function getAction
	         * @description Utility function for creating a rejected or fulfilled
	         * flux standard action object.
	         * @param {boolean} Is the action rejected?
	         * @returns {object} action
	         */
	
	        var getAction = function getAction(newPayload, isRejected) {
	          return _extends({
	            type: type + '_' + (isRejected ? REJECTED : FULFILLED)
	          }, newPayload ? {
	            payload: newPayload
	          } : {}, !!meta ? { meta: meta } : {}, isRejected ? {
	            error: true
	          } : {});
	        };
	
	        /**
	         * Assign values for promise and data variables. In the case the payload
	         * is an object with a `promise` and `data` property, the values of those
	         * properties will be used. In the case the payload is a promise, the
	         * value of the payload will be used and data will be null.
	         */
	        var promise = void 0;
	        var data = void 0;
	
	        if (!(0, _isPromise2.default)(action.payload) && _typeof(action.payload) === 'object') {
	          promise = payload.promise;
	          data = payload.data;
	        } else {
	          promise = payload;
	          data = null;
	        }
	
	        /**
	         * First, dispatch the pending action. This flux standard action object
	         * describes the pending state of a promise and will include any data
	         * (for optimistic updates) and/or meta from the original action.
	         */
	        next(_extends({
	          type: type + '_' + PENDING
	        }, !!data ? { payload: data } : {}, !!meta ? { meta: meta } : {}));
	
	        /*
	         * @function handleReject
	         * @description Dispatch the rejected action and return
	         * an error object. The error object is the original error
	         * that was thrown. The user of the library is responsible for
	         * best practices in ensure that they are throwing an Error object.
	         * @params reason The reason the promise was rejected
	         * @returns {object}
	         */
	        var handleReject = function handleReject(reason) {
	          var rejectedAction = getAction(reason, true);
	          dispatch(rejectedAction);
	          throw reason;
	        };
	
	        /*
	         * @function handleFulfill
	         * @description Dispatch the fulfilled action and
	         * return the success object. The success object should
	         * contain the value and the dispatched action.
	         * @param value The value the promise was resloved with
	         * @returns {object}
	         */
	        var handleFulfill = function handleFulfill() {
	          var value = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
	
	          var resolvedAction = getAction(value, false);
	          dispatch(resolvedAction);
	
	          return { value: value, action: resolvedAction };
	        };
	
	        /**
	         * Second, dispatch a rejected or fulfilled action. This flux standard
	         * action object will describe the resolved state of the promise. In
	         * the case of a rejected promise, it will include an `error` property.
	         *
	         * In order to allow proper chaining of actions using `then`, a new
	         * promise is constructed and returned. This promise will resolve
	         * with two properties: (1) the value (if fulfilled) or reason
	         * (if rejected) and (2) the flux standard action.
	         *
	         * Rejected object:
	         * {
	         *   reason: ...
	         *   action: {
	         *     error: true,
	         *     type: 'ACTION_REJECTED',
	         *     payload: ...
	         *   }
	         * }
	         *
	         * Fulfilled object:
	         * {
	         *   value: ...
	         *   action: {
	         *     type: 'ACTION_FULFILLED',
	         *     payload: ...
	         *   }
	         * }
	         */
	        return promise.then(handleFulfill, handleReject);
	      };
	    };
	  };
	}

/***/ },
/* 41 */
/***/ function(module, exports) {

	'use strict';
	
	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};
	
	exports.default = isPromise;
	function isPromise(value) {
	  if (value !== null && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object') {
	    return value && typeof value.then === 'function';
	  }
	
	  return false;
	}

/***/ }
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgN2U5MDkwMjA5MzI4OWVlZmM2MWIiLCJ3ZWJwYWNrOi8vLy4vanMvY2xpZW50LmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4L2xpYi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi4vfi9wcm9jZXNzL2Jyb3dzZXIuanMiLCJ3ZWJwYWNrOi8vLy4uL34vcmVkdXgvbGliL2NyZWF0ZVN0b3JlLmpzIiwid2VicGFjazovLy8uLi9+L2xvZGFzaC9pc1BsYWluT2JqZWN0LmpzIiwid2VicGFjazovLy8uLi9+L2xvZGFzaC9fZ2V0UHJvdG90eXBlLmpzIiwid2VicGFjazovLy8uLi9+L2xvZGFzaC9fb3ZlckFyZy5qcyIsIndlYnBhY2s6Ly8vLi4vfi9sb2Rhc2gvX2lzSG9zdE9iamVjdC5qcyIsIndlYnBhY2s6Ly8vLi4vfi9sb2Rhc2gvaXNPYmplY3RMaWtlLmpzIiwid2VicGFjazovLy8uLi9+L3N5bWJvbC1vYnNlcnZhYmxlL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9+L3N5bWJvbC1vYnNlcnZhYmxlL3BvbnlmaWxsLmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4L2xpYi9jb21iaW5lUmVkdWNlcnMuanMiLCJ3ZWJwYWNrOi8vLy4uL34vcmVkdXgvbGliL3V0aWxzL3dhcm5pbmcuanMiLCJ3ZWJwYWNrOi8vLy4uL34vcmVkdXgvbGliL2JpbmRBY3Rpb25DcmVhdG9ycy5qcyIsIndlYnBhY2s6Ly8vLi4vfi9yZWR1eC9saWIvYXBwbHlNaWRkbGV3YXJlLmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4L2xpYi9jb21wb3NlLmpzIiwid2VicGFjazovLy8uLi9+L2F4aW9zL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9+L2F4aW9zL2xpYi9heGlvcy5qcyIsIndlYnBhY2s6Ly8vLi4vfi9heGlvcy9saWIvdXRpbHMuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2hlbHBlcnMvYmluZC5qcyIsIndlYnBhY2s6Ly8vLi4vfi9heGlvcy9saWIvY29yZS9BeGlvcy5qcyIsIndlYnBhY2s6Ly8vLi4vfi9heGlvcy9saWIvZGVmYXVsdHMuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2hlbHBlcnMvbm9ybWFsaXplSGVhZGVyTmFtZS5qcyIsIndlYnBhY2s6Ly8vLi4vfi9heGlvcy9saWIvY29yZS9JbnRlcmNlcHRvck1hbmFnZXIuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2NvcmUvZGlzcGF0Y2hSZXF1ZXN0LmpzIiwid2VicGFjazovLy8uLi9+L2F4aW9zL2xpYi9jb3JlL3RyYW5zZm9ybURhdGEuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2FkYXB0ZXJzL3hoci5qcyIsIndlYnBhY2s6Ly8vLi4vfi9heGlvcy9saWIvY29yZS9zZXR0bGUuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2NvcmUvY3JlYXRlRXJyb3IuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2NvcmUvZW5oYW5jZUVycm9yLmpzIiwid2VicGFjazovLy8uLi9+L2F4aW9zL2xpYi9oZWxwZXJzL2J1aWxkVVJMLmpzIiwid2VicGFjazovLy8uLi9+L2F4aW9zL2xpYi9oZWxwZXJzL3BhcnNlSGVhZGVycy5qcyIsIndlYnBhY2s6Ly8vLi4vfi9heGlvcy9saWIvaGVscGVycy9pc1VSTFNhbWVPcmlnaW4uanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2hlbHBlcnMvYnRvYS5qcyIsIndlYnBhY2s6Ly8vLi4vfi9heGlvcy9saWIvaGVscGVycy9jb29raWVzLmpzIiwid2VicGFjazovLy8uLi9+L2F4aW9zL2xpYi9oZWxwZXJzL2lzQWJzb2x1dGVVUkwuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2hlbHBlcnMvY29tYmluZVVSTHMuanMiLCJ3ZWJwYWNrOi8vLy4uL34vYXhpb3MvbGliL2hlbHBlcnMvc3ByZWFkLmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4LWxvZ2dlci9saWIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL34vcmVkdXgtdGh1bmsvbGliL2luZGV4LmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4LXByb21pc2UtbWlkZGxld2FyZS9kaXN0L2luZGV4LmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4LXByb21pc2UtbWlkZGxld2FyZS9kaXN0L2lzUHJvbWlzZS5qcyJdLCJuYW1lcyI6WyJpbml0aWFsU3RhdGUiLCJmZXRjaGluZyIsImZldGNoZWQiLCJ1c2VycyIsImVycm9yIiwicmVkdWNlciIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsInBheWxvYWQiLCJtaWRkbGV3YXJlIiwic3RvcmUiLCJkaXNwYXRjaCIsImdldCIsImV4cG9ydHMiLCJfX2VzTW9kdWxlIiwiY29tcG9zZSIsImFwcGx5TWlkZGxld2FyZSIsImJpbmRBY3Rpb25DcmVhdG9ycyIsImNvbWJpbmVSZWR1Y2VycyIsImNyZWF0ZVN0b3JlIiwidW5kZWZpbmVkIiwiX2NyZWF0ZVN0b3JlIiwicmVxdWlyZSIsIl9jcmVhdGVTdG9yZTIiLCJfaW50ZXJvcFJlcXVpcmVEZWZhdWx0IiwiX2NvbWJpbmVSZWR1Y2VycyIsIl9jb21iaW5lUmVkdWNlcnMyIiwiX2JpbmRBY3Rpb25DcmVhdG9ycyIsIl9iaW5kQWN0aW9uQ3JlYXRvcnMyIiwiX2FwcGx5TWlkZGxld2FyZSIsIl9hcHBseU1pZGRsZXdhcmUyIiwiX2NvbXBvc2UiLCJfY29tcG9zZTIiLCJfd2FybmluZyIsIl93YXJuaW5nMiIsIm9iaiIsImlzQ3J1c2hlZCIsInByb2Nlc3MiLCJlbnYiLCJOT0RFX0VOViIsIm5hbWUiLCJtb2R1bGUiLCJjYWNoZWRTZXRUaW1lb3V0IiwiY2FjaGVkQ2xlYXJUaW1lb3V0IiwiZGVmYXVsdFNldFRpbW91dCIsIkVycm9yIiwiZGVmYXVsdENsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJlIiwiY2xlYXJUaW1lb3V0IiwicnVuVGltZW91dCIsImZ1biIsImNhbGwiLCJydW5DbGVhclRpbWVvdXQiLCJtYXJrZXIiLCJxdWV1ZSIsImRyYWluaW5nIiwiY3VycmVudFF1ZXVlIiwicXVldWVJbmRleCIsImNsZWFuVXBOZXh0VGljayIsImxlbmd0aCIsImNvbmNhdCIsImRyYWluUXVldWUiLCJ0aW1lb3V0IiwibGVuIiwicnVuIiwibmV4dFRpY2siLCJhcmdzIiwiQXJyYXkiLCJhcmd1bWVudHMiLCJpIiwicHVzaCIsIkl0ZW0iLCJhcnJheSIsInByb3RvdHlwZSIsImFwcGx5IiwidGl0bGUiLCJicm93c2VyIiwiYXJndiIsInZlcnNpb24iLCJ2ZXJzaW9ucyIsIm5vb3AiLCJvbiIsImFkZExpc3RlbmVyIiwib25jZSIsIm9mZiIsInJlbW92ZUxpc3RlbmVyIiwicmVtb3ZlQWxsTGlzdGVuZXJzIiwiZW1pdCIsImJpbmRpbmciLCJjd2QiLCJjaGRpciIsImRpciIsInVtYXNrIiwiQWN0aW9uVHlwZXMiLCJfaXNQbGFpbk9iamVjdCIsIl9pc1BsYWluT2JqZWN0MiIsIl9zeW1ib2xPYnNlcnZhYmxlIiwiX3N5bWJvbE9ic2VydmFibGUyIiwiSU5JVCIsImVuaGFuY2VyIiwiX3JlZjIiLCJjdXJyZW50UmVkdWNlciIsImN1cnJlbnRTdGF0ZSIsImN1cnJlbnRMaXN0ZW5lcnMiLCJuZXh0TGlzdGVuZXJzIiwiaXNEaXNwYXRjaGluZyIsImVuc3VyZUNhbk11dGF0ZU5leHRMaXN0ZW5lcnMiLCJzbGljZSIsImdldFN0YXRlIiwic3Vic2NyaWJlIiwibGlzdGVuZXIiLCJpc1N1YnNjcmliZWQiLCJ1bnN1YnNjcmliZSIsImluZGV4IiwiaW5kZXhPZiIsInNwbGljZSIsImxpc3RlbmVycyIsInJlcGxhY2VSZWR1Y2VyIiwibmV4dFJlZHVjZXIiLCJvYnNlcnZhYmxlIiwiX3JlZiIsIm91dGVyU3Vic2NyaWJlIiwib2JzZXJ2ZXIiLCJUeXBlRXJyb3IiLCJvYnNlcnZlU3RhdGUiLCJuZXh0IiwiZ2V0UHJvdG90eXBlIiwiaXNIb3N0T2JqZWN0IiwiaXNPYmplY3RMaWtlIiwib2JqZWN0VGFnIiwiZnVuY1Byb3RvIiwiRnVuY3Rpb24iLCJvYmplY3RQcm90byIsIk9iamVjdCIsImZ1bmNUb1N0cmluZyIsInRvU3RyaW5nIiwiaGFzT3duUHJvcGVydHkiLCJvYmplY3RDdG9yU3RyaW5nIiwib2JqZWN0VG9TdHJpbmciLCJpc1BsYWluT2JqZWN0IiwidmFsdWUiLCJwcm90byIsIkN0b3IiLCJjb25zdHJ1Y3RvciIsIm92ZXJBcmciLCJnZXRQcm90b3R5cGVPZiIsImZ1bmMiLCJ0cmFuc2Zvcm0iLCJhcmciLCJyZXN1bHQiLCJnbG9iYWwiLCJ3aW5kb3ciLCJzeW1ib2xPYnNlcnZhYmxlUG9ueWZpbGwiLCJyb290IiwiU3ltYm9sIiwiZ2V0VW5kZWZpbmVkU3RhdGVFcnJvck1lc3NhZ2UiLCJrZXkiLCJhY3Rpb25UeXBlIiwiYWN0aW9uTmFtZSIsImdldFVuZXhwZWN0ZWRTdGF0ZVNoYXBlV2FybmluZ01lc3NhZ2UiLCJpbnB1dFN0YXRlIiwicmVkdWNlcnMiLCJyZWR1Y2VyS2V5cyIsImtleXMiLCJhcmd1bWVudE5hbWUiLCJtYXRjaCIsImpvaW4iLCJ1bmV4cGVjdGVkS2V5cyIsImZpbHRlciIsImFzc2VydFJlZHVjZXJTYW5pdHkiLCJmb3JFYWNoIiwiTWF0aCIsInJhbmRvbSIsInN1YnN0cmluZyIsInNwbGl0IiwiZmluYWxSZWR1Y2VycyIsImZpbmFsUmVkdWNlcktleXMiLCJzYW5pdHlFcnJvciIsImNvbWJpbmF0aW9uIiwid2FybmluZ01lc3NhZ2UiLCJoYXNDaGFuZ2VkIiwibmV4dFN0YXRlIiwicHJldmlvdXNTdGF0ZUZvcktleSIsIm5leHRTdGF0ZUZvcktleSIsImVycm9yTWVzc2FnZSIsIndhcm5pbmciLCJtZXNzYWdlIiwiY29uc29sZSIsImJpbmRBY3Rpb25DcmVhdG9yIiwiYWN0aW9uQ3JlYXRvciIsImFjdGlvbkNyZWF0b3JzIiwiYm91bmRBY3Rpb25DcmVhdG9ycyIsIl9leHRlbmRzIiwiYXNzaWduIiwidGFyZ2V0Iiwic291cmNlIiwiX2xlbiIsIm1pZGRsZXdhcmVzIiwiX2tleSIsIl9kaXNwYXRjaCIsImNoYWluIiwibWlkZGxld2FyZUFQSSIsIm1hcCIsImZ1bmNzIiwiX3JldCIsImxhc3QiLCJyZXN0IiwidiIsInJlZHVjZVJpZ2h0IiwiY29tcG9zZWQiLCJmIiwidXRpbHMiLCJiaW5kIiwiQXhpb3MiLCJjcmVhdGVJbnN0YW5jZSIsImRlZmF1bHRDb25maWciLCJjb250ZXh0IiwiaW5zdGFuY2UiLCJyZXF1ZXN0IiwiZXh0ZW5kIiwiYXhpb3MiLCJjcmVhdGUiLCJhbGwiLCJwcm9taXNlcyIsIlByb21pc2UiLCJzcHJlYWQiLCJkZWZhdWx0IiwiaXNBcnJheSIsInZhbCIsImlzQXJyYXlCdWZmZXIiLCJpc0Zvcm1EYXRhIiwiRm9ybURhdGEiLCJpc0FycmF5QnVmZmVyVmlldyIsIkFycmF5QnVmZmVyIiwiaXNWaWV3IiwiYnVmZmVyIiwiaXNTdHJpbmciLCJpc051bWJlciIsImlzVW5kZWZpbmVkIiwiaXNPYmplY3QiLCJpc0RhdGUiLCJpc0ZpbGUiLCJpc0Jsb2IiLCJpc0Z1bmN0aW9uIiwiaXNTdHJlYW0iLCJwaXBlIiwiaXNVUkxTZWFyY2hQYXJhbXMiLCJVUkxTZWFyY2hQYXJhbXMiLCJ0cmltIiwic3RyIiwicmVwbGFjZSIsImlzU3RhbmRhcmRCcm93c2VyRW52IiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50IiwiZm4iLCJsIiwibWVyZ2UiLCJhc3NpZ25WYWx1ZSIsImEiLCJiIiwidGhpc0FyZyIsIndyYXAiLCJkZWZhdWx0cyIsIkludGVyY2VwdG9yTWFuYWdlciIsImRpc3BhdGNoUmVxdWVzdCIsImlzQWJzb2x1dGVVUkwiLCJjb21iaW5lVVJMcyIsImludGVyY2VwdG9ycyIsInJlc3BvbnNlIiwiY29uZmlnIiwidXJsIiwibWV0aG9kIiwiYmFzZVVSTCIsInByb21pc2UiLCJyZXNvbHZlIiwidW5zaGlmdFJlcXVlc3RJbnRlcmNlcHRvcnMiLCJpbnRlcmNlcHRvciIsInVuc2hpZnQiLCJmdWxmaWxsZWQiLCJyZWplY3RlZCIsInB1c2hSZXNwb25zZUludGVyY2VwdG9ycyIsInRoZW4iLCJzaGlmdCIsImZvckVhY2hNZXRob2ROb0RhdGEiLCJmb3JFYWNoTWV0aG9kV2l0aERhdGEiLCJkYXRhIiwibm9ybWFsaXplSGVhZGVyTmFtZSIsIlBST1RFQ1RJT05fUFJFRklYIiwiREVGQVVMVF9DT05URU5UX1RZUEUiLCJzZXRDb250ZW50VHlwZUlmVW5zZXQiLCJoZWFkZXJzIiwidHJhbnNmb3JtUmVxdWVzdCIsIkpTT04iLCJzdHJpbmdpZnkiLCJ0cmFuc2Zvcm1SZXNwb25zZSIsInBhcnNlIiwiY29tbW9uIiwicGF0Y2giLCJwb3N0IiwicHV0IiwieHNyZkNvb2tpZU5hbWUiLCJ4c3JmSGVhZGVyTmFtZSIsIm1heENvbnRlbnRMZW5ndGgiLCJ2YWxpZGF0ZVN0YXR1cyIsInN0YXR1cyIsIm5vcm1hbGl6ZWROYW1lIiwicHJvY2Vzc0hlYWRlciIsInRvVXBwZXJDYXNlIiwiaGFuZGxlcnMiLCJ1c2UiLCJlamVjdCIsImlkIiwiZm9yRWFjaEhhbmRsZXIiLCJoIiwidHJhbnNmb3JtRGF0YSIsImNsZWFuSGVhZGVyQ29uZmlnIiwiYWRhcHRlciIsIlhNTEh0dHBSZXF1ZXN0Iiwib25GdWxmaWxsZWQiLCJvblJlamVjdGVkIiwicmVqZWN0IiwiZm5zIiwic2V0dGxlIiwiYnVpbGRVUkwiLCJwYXJzZUhlYWRlcnMiLCJpc1VSTFNhbWVPcmlnaW4iLCJjcmVhdGVFcnJvciIsImJ0b2EiLCJ4aHJBZGFwdGVyIiwiZGlzcGF0Y2hYaHJSZXF1ZXN0IiwicmVxdWVzdERhdGEiLCJyZXF1ZXN0SGVhZGVycyIsImxvYWRFdmVudCIsInhEb21haW4iLCJYRG9tYWluUmVxdWVzdCIsIm9ucHJvZ3Jlc3MiLCJoYW5kbGVQcm9ncmVzcyIsIm9udGltZW91dCIsImhhbmRsZVRpbWVvdXQiLCJhdXRoIiwidXNlcm5hbWUiLCJwYXNzd29yZCIsIkF1dGhvcml6YXRpb24iLCJvcGVuIiwicGFyYW1zIiwicGFyYW1zU2VyaWFsaXplciIsImhhbmRsZUxvYWQiLCJyZWFkeVN0YXRlIiwicmVzcG9uc2VIZWFkZXJzIiwiZ2V0QWxsUmVzcG9uc2VIZWFkZXJzIiwicmVzcG9uc2VEYXRhIiwicmVzcG9uc2VUeXBlIiwicmVzcG9uc2VUZXh0Iiwic3RhdHVzVGV4dCIsIm9uZXJyb3IiLCJoYW5kbGVFcnJvciIsImNvb2tpZXMiLCJ4c3JmVmFsdWUiLCJ3aXRoQ3JlZGVudGlhbHMiLCJyZWFkIiwic2V0UmVxdWVzdEhlYWRlciIsInRvTG93ZXJDYXNlIiwib25Eb3dubG9hZFByb2dyZXNzIiwiYWRkRXZlbnRMaXN0ZW5lciIsIm9uVXBsb2FkUHJvZ3Jlc3MiLCJ1cGxvYWQiLCJzZW5kIiwiZW5oYW5jZUVycm9yIiwiY29kZSIsImVuY29kZSIsImVuY29kZVVSSUNvbXBvbmVudCIsInNlcmlhbGl6ZWRQYXJhbXMiLCJwYXJ0cyIsInNlcmlhbGl6ZSIsInBhcnNlVmFsdWUiLCJ0b0lTT1N0cmluZyIsInBhcnNlZCIsInBhcnNlciIsImxpbmUiLCJzdWJzdHIiLCJzdGFuZGFyZEJyb3dzZXJFbnYiLCJtc2llIiwidGVzdCIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsInVybFBhcnNpbmdOb2RlIiwib3JpZ2luVVJMIiwicmVzb2x2ZVVSTCIsImhyZWYiLCJzZXRBdHRyaWJ1dGUiLCJwcm90b2NvbCIsImhvc3QiLCJzZWFyY2giLCJoYXNoIiwiaG9zdG5hbWUiLCJwb3J0IiwicGF0aG5hbWUiLCJjaGFyQXQiLCJsb2NhdGlvbiIsInJlcXVlc3RVUkwiLCJub25TdGFuZGFyZEJyb3dzZXJFbnYiLCJjaGFycyIsIkUiLCJpbnB1dCIsIlN0cmluZyIsIm91dHB1dCIsImJsb2NrIiwiY2hhckNvZGUiLCJpZHgiLCJjaGFyQ29kZUF0Iiwid3JpdGUiLCJleHBpcmVzIiwicGF0aCIsImRvbWFpbiIsInNlY3VyZSIsImNvb2tpZSIsIkRhdGUiLCJ0b0dNVFN0cmluZyIsIlJlZ0V4cCIsImRlY29kZVVSSUNvbXBvbmVudCIsInJlbW92ZSIsIm5vdyIsInJlbGF0aXZlVVJMIiwiY2FsbGJhY2siLCJhcnIiLCJfdG9Db25zdW1hYmxlQXJyYXkiLCJhcnIyIiwiZnJvbSIsIl90eXBlb2YiLCJyZXBlYXQiLCJ0aW1lcyIsInBhZCIsIm51bSIsIm1heExlbmd0aCIsImZvcm1hdFRpbWUiLCJ0aW1lIiwiZ2V0SG91cnMiLCJnZXRNaW51dGVzIiwiZ2V0U2Vjb25kcyIsImdldE1pbGxpc2Vjb25kcyIsInRpbWVyIiwicGVyZm9ybWFuY2UiLCJnZXRMb2dMZXZlbCIsImxldmVsIiwiY3JlYXRlTG9nZ2VyIiwib3B0aW9ucyIsIl9vcHRpb25zJGxldmVsIiwiX29wdGlvbnMkbG9nZ2VyIiwibG9nZ2VyIiwiX29wdGlvbnMkbG9nRXJyb3JzIiwibG9nRXJyb3JzIiwiY29sbGFwc2VkIiwicHJlZGljYXRlIiwiX29wdGlvbnMkZHVyYXRpb24iLCJkdXJhdGlvbiIsIl9vcHRpb25zJHRpbWVzdGFtcCIsInRpbWVzdGFtcCIsInRyYW5zZm9ybWVyIiwiX29wdGlvbnMkc3RhdGVUcmFuc2ZvIiwic3RhdGVUcmFuc2Zvcm1lciIsIl9vcHRpb25zJGFjdGlvblRyYW5zZiIsImFjdGlvblRyYW5zZm9ybWVyIiwiYWN0biIsIl9vcHRpb25zJGVycm9yVHJhbnNmbyIsImVycm9yVHJhbnNmb3JtZXIiLCJfb3B0aW9ucyRjb2xvcnMiLCJjb2xvcnMiLCJwcmV2U3RhdGUiLCJsb2dCdWZmZXIiLCJwcmludEJ1ZmZlciIsImxvZ0VudHJ5Iiwic3RhcnRlZCIsInN0YXJ0ZWRUaW1lIiwidG9vayIsIm5leHRFbnRyeSIsImZvcm1hdHRlZEFjdGlvbiIsImlzQ29sbGFwc2VkIiwiZm9ybWF0dGVkVGltZSIsInRpdGxlQ1NTIiwidG9GaXhlZCIsImdyb3VwQ29sbGFwc2VkIiwiZ3JvdXAiLCJsb2ciLCJwcmV2U3RhdGVMZXZlbCIsImFjdGlvbkxldmVsIiwiZXJyb3JMZXZlbCIsIm5leHRTdGF0ZUxldmVsIiwiZ3JvdXBFbmQiLCJyZXR1cm5lZFZhbHVlIiwiY3JlYXRlVGh1bmtNaWRkbGV3YXJlIiwiZXh0cmFBcmd1bWVudCIsInRodW5rIiwid2l0aEV4dHJhQXJndW1lbnQiLCJkZWZpbmVQcm9wZXJ0eSIsIml0ZXJhdG9yIiwiX3NsaWNlZFRvQXJyYXkiLCJzbGljZUl0ZXJhdG9yIiwiX2FyciIsIl9uIiwiX2QiLCJfZSIsIl9pIiwiX3MiLCJkb25lIiwiZXJyIiwicHJvbWlzZU1pZGRsZXdhcmUiLCJfaXNQcm9taXNlIiwiX2lzUHJvbWlzZTIiLCJkZWZhdWx0VHlwZXMiLCJwcm9taXNlVHlwZVN1ZmZpeGVzIiwicmVmIiwibWV0YSIsIl9wcm9taXNlVHlwZVN1ZmZpeGVzIiwiUEVORElORyIsIkZVTEZJTExFRCIsIlJFSkVDVEVEIiwiZ2V0QWN0aW9uIiwibmV3UGF5bG9hZCIsImlzUmVqZWN0ZWQiLCJoYW5kbGVSZWplY3QiLCJyZWFzb24iLCJyZWplY3RlZEFjdGlvbiIsImhhbmRsZUZ1bGZpbGwiLCJyZXNvbHZlZEFjdGlvbiIsImlzUHJvbWlzZSJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHVCQUFlO0FBQ2Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7O0FDdENBOztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQSxLQUFNQSxlQUFlO0FBQ2pCQyxlQUFVLEtBRE87QUFFakJDLGNBQVMsS0FGUTtBQUdqQkMsWUFBTyxFQUhVO0FBSWpCQyxZQUFPO0FBSlUsRUFBckI7O0FBT0EsS0FBTUMsVUFBVSxTQUFWQSxPQUFVLEdBQWdDO0FBQUEsU0FBL0JDLEtBQStCLHlEQUF6Qk4sWUFBeUI7QUFBQSxTQUFYTyxNQUFXOztBQUM1QyxhQUFPQSxPQUFPQyxJQUFkO0FBQ0ksY0FBSyxxQkFBTDtBQUE0QjtBQUN4QkYsc0NBQ09BLEtBRFA7QUFFSUwsK0JBQVU7QUFGZDtBQUlBO0FBQ0g7QUFDRCxjQUFLLHNCQUFMO0FBQTZCO0FBQ3pCSyxzQ0FDT0EsS0FEUDtBQUVJTCwrQkFBVSxLQUZkO0FBR0lHLDRCQUFPRyxPQUFPRTtBQUhsQjtBQUtBO0FBQ0g7QUFDRCxjQUFLLHVCQUFMO0FBQThCO0FBQzFCSCxzQ0FDT0EsS0FEUDtBQUVJTCwrQkFBVSxLQUZkO0FBR0lDLDhCQUFTLElBSGI7QUFJSUMsNEJBQU9JLE9BQU9FO0FBSmxCO0FBTUE7QUFDSDtBQXhCTDtBQTBCQSxZQUFPSCxLQUFQO0FBQ0gsRUE1QkQ7O0FBOEJBLEtBQU1JLGFBQWEsNEJBQWdCLHVDQUFoQix3QkFBa0MsNEJBQWxDLENBQW5COztBQUVBLEtBQU1DLFFBQVEsd0JBQVlOLE9BQVosRUFBcUJLLFVBQXJCLENBQWQ7O0FBRUFDLE9BQU1DLFFBQU4sQ0FBZTtBQUNYSixXQUFNLGFBREs7QUFFWEMsY0FBUyxnQkFBTUksR0FBTixDQUFVLHFEQUFWO0FBRkUsRUFBZixFOzs7Ozs7QUMvQ0E7O0FBRUFDLFNBQVFDLFVBQVIsR0FBcUIsSUFBckI7QUFDQUQsU0FBUUUsT0FBUixHQUFrQkYsUUFBUUcsZUFBUixHQUEwQkgsUUFBUUksa0JBQVIsR0FBNkJKLFFBQVFLLGVBQVIsR0FBMEJMLFFBQVFNLFdBQVIsR0FBc0JDLFNBQXpIOztBQUVBLEtBQUlDLGVBQWUsbUJBQUFDLENBQVEsQ0FBUixDQUFuQjs7QUFFQSxLQUFJQyxnQkFBZ0JDLHVCQUF1QkgsWUFBdkIsQ0FBcEI7O0FBRUEsS0FBSUksbUJBQW1CLG1CQUFBSCxDQUFRLEVBQVIsQ0FBdkI7O0FBRUEsS0FBSUksb0JBQW9CRix1QkFBdUJDLGdCQUF2QixDQUF4Qjs7QUFFQSxLQUFJRSxzQkFBc0IsbUJBQUFMLENBQVEsRUFBUixDQUExQjs7QUFFQSxLQUFJTSx1QkFBdUJKLHVCQUF1QkcsbUJBQXZCLENBQTNCOztBQUVBLEtBQUlFLG1CQUFtQixtQkFBQVAsQ0FBUSxFQUFSLENBQXZCOztBQUVBLEtBQUlRLG9CQUFvQk4sdUJBQXVCSyxnQkFBdkIsQ0FBeEI7O0FBRUEsS0FBSUUsV0FBVyxtQkFBQVQsQ0FBUSxFQUFSLENBQWY7O0FBRUEsS0FBSVUsWUFBWVIsdUJBQXVCTyxRQUF2QixDQUFoQjs7QUFFQSxLQUFJRSxXQUFXLG1CQUFBWCxDQUFRLEVBQVIsQ0FBZjs7QUFFQSxLQUFJWSxZQUFZVix1QkFBdUJTLFFBQXZCLENBQWhCOztBQUVBLFVBQVNULHNCQUFULENBQWdDVyxHQUFoQyxFQUFxQztBQUFFLFVBQU9BLE9BQU9BLElBQUlyQixVQUFYLEdBQXdCcUIsR0FBeEIsR0FBOEIsRUFBRSxXQUFXQSxHQUFiLEVBQXJDO0FBQTBEOztBQUVqRzs7OztBQUlBLFVBQVNDLFNBQVQsR0FBcUIsQ0FBRTs7QUFFdkIsS0FBSUMsUUFBUUMsR0FBUixDQUFZQyxRQUFaLEtBQXlCLFlBQXpCLElBQXlDLE9BQU9ILFVBQVVJLElBQWpCLEtBQTBCLFFBQW5FLElBQStFSixVQUFVSSxJQUFWLEtBQW1CLFdBQXRHLEVBQW1IO0FBQ2pILElBQUMsR0FBR04sVUFBVSxTQUFWLENBQUosRUFBMEIsbUZBQW1GLHVFQUFuRixHQUE2SixvRkFBN0osR0FBb1AsNEVBQXBQLEdBQW1VLGdFQUE3VjtBQUNEOztBQUVEckIsU0FBUU0sV0FBUixHQUFzQkksY0FBYyxTQUFkLENBQXRCO0FBQ0FWLFNBQVFLLGVBQVIsR0FBMEJRLGtCQUFrQixTQUFsQixDQUExQjtBQUNBYixTQUFRSSxrQkFBUixHQUE2QlcscUJBQXFCLFNBQXJCLENBQTdCO0FBQ0FmLFNBQVFHLGVBQVIsR0FBMEJjLGtCQUFrQixTQUFsQixDQUExQjtBQUNBakIsU0FBUUUsT0FBUixHQUFrQmlCLFVBQVUsU0FBVixDQUFsQixDOzs7Ozs7Ozs7QUM3Q0E7QUFDQSxLQUFJSyxVQUFVSSxPQUFPNUIsT0FBUCxHQUFpQixFQUEvQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxLQUFJNkIsZ0JBQUo7QUFDQSxLQUFJQyxrQkFBSjs7QUFFQSxVQUFTQyxnQkFBVCxHQUE0QjtBQUN4QixXQUFNLElBQUlDLEtBQUosQ0FBVSxpQ0FBVixDQUFOO0FBQ0g7QUFDRCxVQUFTQyxtQkFBVCxHQUFnQztBQUM1QixXQUFNLElBQUlELEtBQUosQ0FBVSxtQ0FBVixDQUFOO0FBQ0g7QUFDQSxjQUFZO0FBQ1QsU0FBSTtBQUNBLGFBQUksT0FBT0UsVUFBUCxLQUFzQixVQUExQixFQUFzQztBQUNsQ0wsZ0NBQW1CSyxVQUFuQjtBQUNILFVBRkQsTUFFTztBQUNITCxnQ0FBbUJFLGdCQUFuQjtBQUNIO0FBQ0osTUFORCxDQU1FLE9BQU9JLENBQVAsRUFBVTtBQUNSTiw0QkFBbUJFLGdCQUFuQjtBQUNIO0FBQ0QsU0FBSTtBQUNBLGFBQUksT0FBT0ssWUFBUCxLQUF3QixVQUE1QixFQUF3QztBQUNwQ04sa0NBQXFCTSxZQUFyQjtBQUNILFVBRkQsTUFFTztBQUNITixrQ0FBcUJHLG1CQUFyQjtBQUNIO0FBQ0osTUFORCxDQU1FLE9BQU9FLENBQVAsRUFBVTtBQUNSTCw4QkFBcUJHLG1CQUFyQjtBQUNIO0FBQ0osRUFuQkEsR0FBRDtBQW9CQSxVQUFTSSxVQUFULENBQW9CQyxHQUFwQixFQUF5QjtBQUNyQixTQUFJVCxxQkFBcUJLLFVBQXpCLEVBQXFDO0FBQ2pDO0FBQ0EsZ0JBQU9BLFdBQVdJLEdBQVgsRUFBZ0IsQ0FBaEIsQ0FBUDtBQUNIO0FBQ0Q7QUFDQSxTQUFJLENBQUNULHFCQUFxQkUsZ0JBQXJCLElBQXlDLENBQUNGLGdCQUEzQyxLQUFnRUssVUFBcEUsRUFBZ0Y7QUFDNUVMLDRCQUFtQkssVUFBbkI7QUFDQSxnQkFBT0EsV0FBV0ksR0FBWCxFQUFnQixDQUFoQixDQUFQO0FBQ0g7QUFDRCxTQUFJO0FBQ0E7QUFDQSxnQkFBT1QsaUJBQWlCUyxHQUFqQixFQUFzQixDQUF0QixDQUFQO0FBQ0gsTUFIRCxDQUdFLE9BQU1ILENBQU4sRUFBUTtBQUNOLGFBQUk7QUFDQTtBQUNBLG9CQUFPTixpQkFBaUJVLElBQWpCLENBQXNCLElBQXRCLEVBQTRCRCxHQUE1QixFQUFpQyxDQUFqQyxDQUFQO0FBQ0gsVUFIRCxDQUdFLE9BQU1ILENBQU4sRUFBUTtBQUNOO0FBQ0Esb0JBQU9OLGlCQUFpQlUsSUFBakIsQ0FBc0IsSUFBdEIsRUFBNEJELEdBQTVCLEVBQWlDLENBQWpDLENBQVA7QUFDSDtBQUNKO0FBR0o7QUFDRCxVQUFTRSxlQUFULENBQXlCQyxNQUF6QixFQUFpQztBQUM3QixTQUFJWCx1QkFBdUJNLFlBQTNCLEVBQXlDO0FBQ3JDO0FBQ0EsZ0JBQU9BLGFBQWFLLE1BQWIsQ0FBUDtBQUNIO0FBQ0Q7QUFDQSxTQUFJLENBQUNYLHVCQUF1QkcsbUJBQXZCLElBQThDLENBQUNILGtCQUFoRCxLQUF1RU0sWUFBM0UsRUFBeUY7QUFDckZOLDhCQUFxQk0sWUFBckI7QUFDQSxnQkFBT0EsYUFBYUssTUFBYixDQUFQO0FBQ0g7QUFDRCxTQUFJO0FBQ0E7QUFDQSxnQkFBT1gsbUJBQW1CVyxNQUFuQixDQUFQO0FBQ0gsTUFIRCxDQUdFLE9BQU9OLENBQVAsRUFBUztBQUNQLGFBQUk7QUFDQTtBQUNBLG9CQUFPTCxtQkFBbUJTLElBQW5CLENBQXdCLElBQXhCLEVBQThCRSxNQUE5QixDQUFQO0FBQ0gsVUFIRCxDQUdFLE9BQU9OLENBQVAsRUFBUztBQUNQO0FBQ0E7QUFDQSxvQkFBT0wsbUJBQW1CUyxJQUFuQixDQUF3QixJQUF4QixFQUE4QkUsTUFBOUIsQ0FBUDtBQUNIO0FBQ0o7QUFJSjtBQUNELEtBQUlDLFFBQVEsRUFBWjtBQUNBLEtBQUlDLFdBQVcsS0FBZjtBQUNBLEtBQUlDLFlBQUo7QUFDQSxLQUFJQyxhQUFhLENBQUMsQ0FBbEI7O0FBRUEsVUFBU0MsZUFBVCxHQUEyQjtBQUN2QixTQUFJLENBQUNILFFBQUQsSUFBYSxDQUFDQyxZQUFsQixFQUFnQztBQUM1QjtBQUNIO0FBQ0RELGdCQUFXLEtBQVg7QUFDQSxTQUFJQyxhQUFhRyxNQUFqQixFQUF5QjtBQUNyQkwsaUJBQVFFLGFBQWFJLE1BQWIsQ0FBb0JOLEtBQXBCLENBQVI7QUFDSCxNQUZELE1BRU87QUFDSEcsc0JBQWEsQ0FBQyxDQUFkO0FBQ0g7QUFDRCxTQUFJSCxNQUFNSyxNQUFWLEVBQWtCO0FBQ2RFO0FBQ0g7QUFDSjs7QUFFRCxVQUFTQSxVQUFULEdBQXNCO0FBQ2xCLFNBQUlOLFFBQUosRUFBYztBQUNWO0FBQ0g7QUFDRCxTQUFJTyxVQUFVYixXQUFXUyxlQUFYLENBQWQ7QUFDQUgsZ0JBQVcsSUFBWDs7QUFFQSxTQUFJUSxNQUFNVCxNQUFNSyxNQUFoQjtBQUNBLFlBQU1JLEdBQU4sRUFBVztBQUNQUCx3QkFBZUYsS0FBZjtBQUNBQSxpQkFBUSxFQUFSO0FBQ0EsZ0JBQU8sRUFBRUcsVUFBRixHQUFlTSxHQUF0QixFQUEyQjtBQUN2QixpQkFBSVAsWUFBSixFQUFrQjtBQUNkQSw4QkFBYUMsVUFBYixFQUF5Qk8sR0FBekI7QUFDSDtBQUNKO0FBQ0RQLHNCQUFhLENBQUMsQ0FBZDtBQUNBTSxlQUFNVCxNQUFNSyxNQUFaO0FBQ0g7QUFDREgsb0JBQWUsSUFBZjtBQUNBRCxnQkFBVyxLQUFYO0FBQ0FILHFCQUFnQlUsT0FBaEI7QUFDSDs7QUFFRDFCLFNBQVE2QixRQUFSLEdBQW1CLFVBQVVmLEdBQVYsRUFBZTtBQUM5QixTQUFJZ0IsT0FBTyxJQUFJQyxLQUFKLENBQVVDLFVBQVVULE1BQVYsR0FBbUIsQ0FBN0IsQ0FBWDtBQUNBLFNBQUlTLFVBQVVULE1BQVYsR0FBbUIsQ0FBdkIsRUFBMEI7QUFDdEIsY0FBSyxJQUFJVSxJQUFJLENBQWIsRUFBZ0JBLElBQUlELFVBQVVULE1BQTlCLEVBQXNDVSxHQUF0QyxFQUEyQztBQUN2Q0gsa0JBQUtHLElBQUksQ0FBVCxJQUFjRCxVQUFVQyxDQUFWLENBQWQ7QUFDSDtBQUNKO0FBQ0RmLFdBQU1nQixJQUFOLENBQVcsSUFBSUMsSUFBSixDQUFTckIsR0FBVCxFQUFjZ0IsSUFBZCxDQUFYO0FBQ0EsU0FBSVosTUFBTUssTUFBTixLQUFpQixDQUFqQixJQUFzQixDQUFDSixRQUEzQixFQUFxQztBQUNqQ04sb0JBQVdZLFVBQVg7QUFDSDtBQUNKLEVBWEQ7O0FBYUE7QUFDQSxVQUFTVSxJQUFULENBQWNyQixHQUFkLEVBQW1Cc0IsS0FBbkIsRUFBMEI7QUFDdEIsVUFBS3RCLEdBQUwsR0FBV0EsR0FBWDtBQUNBLFVBQUtzQixLQUFMLEdBQWFBLEtBQWI7QUFDSDtBQUNERCxNQUFLRSxTQUFMLENBQWVULEdBQWYsR0FBcUIsWUFBWTtBQUM3QixVQUFLZCxHQUFMLENBQVN3QixLQUFULENBQWUsSUFBZixFQUFxQixLQUFLRixLQUExQjtBQUNILEVBRkQ7QUFHQXBDLFNBQVF1QyxLQUFSLEdBQWdCLFNBQWhCO0FBQ0F2QyxTQUFRd0MsT0FBUixHQUFrQixJQUFsQjtBQUNBeEMsU0FBUUMsR0FBUixHQUFjLEVBQWQ7QUFDQUQsU0FBUXlDLElBQVIsR0FBZSxFQUFmO0FBQ0F6QyxTQUFRMEMsT0FBUixHQUFrQixFQUFsQixDLENBQXNCO0FBQ3RCMUMsU0FBUTJDLFFBQVIsR0FBbUIsRUFBbkI7O0FBRUEsVUFBU0MsSUFBVCxHQUFnQixDQUFFOztBQUVsQjVDLFNBQVE2QyxFQUFSLEdBQWFELElBQWI7QUFDQTVDLFNBQVE4QyxXQUFSLEdBQXNCRixJQUF0QjtBQUNBNUMsU0FBUStDLElBQVIsR0FBZUgsSUFBZjtBQUNBNUMsU0FBUWdELEdBQVIsR0FBY0osSUFBZDtBQUNBNUMsU0FBUWlELGNBQVIsR0FBeUJMLElBQXpCO0FBQ0E1QyxTQUFRa0Qsa0JBQVIsR0FBNkJOLElBQTdCO0FBQ0E1QyxTQUFRbUQsSUFBUixHQUFlUCxJQUFmOztBQUVBNUMsU0FBUW9ELE9BQVIsR0FBa0IsVUFBVWpELElBQVYsRUFBZ0I7QUFDOUIsV0FBTSxJQUFJSyxLQUFKLENBQVUsa0NBQVYsQ0FBTjtBQUNILEVBRkQ7O0FBSUFSLFNBQVFxRCxHQUFSLEdBQWMsWUFBWTtBQUFFLFlBQU8sR0FBUDtBQUFZLEVBQXhDO0FBQ0FyRCxTQUFRc0QsS0FBUixHQUFnQixVQUFVQyxHQUFWLEVBQWU7QUFDM0IsV0FBTSxJQUFJL0MsS0FBSixDQUFVLGdDQUFWLENBQU47QUFDSCxFQUZEO0FBR0FSLFNBQVF3RCxLQUFSLEdBQWdCLFlBQVc7QUFBRSxZQUFPLENBQVA7QUFBVyxFQUF4QyxDOzs7Ozs7QUNuTEE7Ozs7QUFFQWhGLFNBQVFDLFVBQVIsR0FBcUIsSUFBckI7QUFDQUQsU0FBUWlGLFdBQVIsR0FBc0IxRSxTQUF0QjtBQUNBUCxTQUFRLFNBQVIsSUFBcUJNLFdBQXJCOztBQUVBLEtBQUk0RSxpQkFBaUIsbUJBQUF6RSxDQUFRLENBQVIsQ0FBckI7O0FBRUEsS0FBSTBFLGtCQUFrQnhFLHVCQUF1QnVFLGNBQXZCLENBQXRCOztBQUVBLEtBQUlFLG9CQUFvQixtQkFBQTNFLENBQVEsQ0FBUixDQUF4Qjs7QUFFQSxLQUFJNEUscUJBQXFCMUUsdUJBQXVCeUUsaUJBQXZCLENBQXpCOztBQUVBLFVBQVN6RSxzQkFBVCxDQUFnQ1csR0FBaEMsRUFBcUM7QUFBRSxVQUFPQSxPQUFPQSxJQUFJckIsVUFBWCxHQUF3QnFCLEdBQXhCLEdBQThCLEVBQUUsV0FBV0EsR0FBYixFQUFyQztBQUEwRDs7QUFFakc7Ozs7OztBQU1BLEtBQUkyRCxjQUFjakYsUUFBUWlGLFdBQVIsR0FBc0I7QUFDdENLLFNBQU07QUFEZ0MsRUFBeEM7O0FBSUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkEsVUFBU2hGLFdBQVQsQ0FBcUJmLE9BQXJCLEVBQThCTCxZQUE5QixFQUE0Q3FHLFFBQTVDLEVBQXNEO0FBQ3BELE9BQUlDLEtBQUo7O0FBRUEsT0FBSSxPQUFPdEcsWUFBUCxLQUF3QixVQUF4QixJQUFzQyxPQUFPcUcsUUFBUCxLQUFvQixXQUE5RCxFQUEyRTtBQUN6RUEsZ0JBQVdyRyxZQUFYO0FBQ0FBLG9CQUFlcUIsU0FBZjtBQUNEOztBQUVELE9BQUksT0FBT2dGLFFBQVAsS0FBb0IsV0FBeEIsRUFBcUM7QUFDbkMsU0FBSSxPQUFPQSxRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ2xDLGFBQU0sSUFBSXZELEtBQUosQ0FBVSx5Q0FBVixDQUFOO0FBQ0Q7O0FBRUQsWUFBT3VELFNBQVNqRixXQUFULEVBQXNCZixPQUF0QixFQUErQkwsWUFBL0IsQ0FBUDtBQUNEOztBQUVELE9BQUksT0FBT0ssT0FBUCxLQUFtQixVQUF2QixFQUFtQztBQUNqQyxXQUFNLElBQUl5QyxLQUFKLENBQVUsd0NBQVYsQ0FBTjtBQUNEOztBQUVELE9BQUl5RCxpQkFBaUJsRyxPQUFyQjtBQUNBLE9BQUltRyxlQUFleEcsWUFBbkI7QUFDQSxPQUFJeUcsbUJBQW1CLEVBQXZCO0FBQ0EsT0FBSUMsZ0JBQWdCRCxnQkFBcEI7QUFDQSxPQUFJRSxnQkFBZ0IsS0FBcEI7O0FBRUEsWUFBU0MsNEJBQVQsR0FBd0M7QUFDdEMsU0FBSUYsa0JBQWtCRCxnQkFBdEIsRUFBd0M7QUFDdENDLHVCQUFnQkQsaUJBQWlCSSxLQUFqQixFQUFoQjtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7O0FBS0EsWUFBU0MsUUFBVCxHQUFvQjtBQUNsQixZQUFPTixZQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBdUJBLFlBQVNPLFNBQVQsQ0FBbUJDLFFBQW5CLEVBQTZCO0FBQzNCLFNBQUksT0FBT0EsUUFBUCxLQUFvQixVQUF4QixFQUFvQztBQUNsQyxhQUFNLElBQUlsRSxLQUFKLENBQVUscUNBQVYsQ0FBTjtBQUNEOztBQUVELFNBQUltRSxlQUFlLElBQW5COztBQUVBTDtBQUNBRixtQkFBY2xDLElBQWQsQ0FBbUJ3QyxRQUFuQjs7QUFFQSxZQUFPLFNBQVNFLFdBQVQsR0FBdUI7QUFDNUIsV0FBSSxDQUFDRCxZQUFMLEVBQW1CO0FBQ2pCO0FBQ0Q7O0FBRURBLHNCQUFlLEtBQWY7O0FBRUFMO0FBQ0EsV0FBSU8sUUFBUVQsY0FBY1UsT0FBZCxDQUFzQkosUUFBdEIsQ0FBWjtBQUNBTixxQkFBY1csTUFBZCxDQUFxQkYsS0FBckIsRUFBNEIsQ0FBNUI7QUFDRCxNQVZEO0FBV0Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkEsWUFBU3ZHLFFBQVQsQ0FBa0JMLE1BQWxCLEVBQTBCO0FBQ3hCLFNBQUksQ0FBQyxDQUFDLEdBQUcwRixnQkFBZ0IsU0FBaEIsQ0FBSixFQUFnQzFGLE1BQWhDLENBQUwsRUFBOEM7QUFDNUMsYUFBTSxJQUFJdUMsS0FBSixDQUFVLG9DQUFvQywwQ0FBOUMsQ0FBTjtBQUNEOztBQUVELFNBQUksT0FBT3ZDLE9BQU9DLElBQWQsS0FBdUIsV0FBM0IsRUFBd0M7QUFDdEMsYUFBTSxJQUFJc0MsS0FBSixDQUFVLHdEQUF3RCxpQ0FBbEUsQ0FBTjtBQUNEOztBQUVELFNBQUk2RCxhQUFKLEVBQW1CO0FBQ2pCLGFBQU0sSUFBSTdELEtBQUosQ0FBVSxvQ0FBVixDQUFOO0FBQ0Q7O0FBRUQsU0FBSTtBQUNGNkQsdUJBQWdCLElBQWhCO0FBQ0FILHNCQUFlRCxlQUFlQyxZQUFmLEVBQTZCakcsTUFBN0IsQ0FBZjtBQUNELE1BSEQsU0FHVTtBQUNSb0csdUJBQWdCLEtBQWhCO0FBQ0Q7O0FBRUQsU0FBSVcsWUFBWWIsbUJBQW1CQyxhQUFuQztBQUNBLFVBQUssSUFBSW5DLElBQUksQ0FBYixFQUFnQkEsSUFBSStDLFVBQVV6RCxNQUE5QixFQUFzQ1UsR0FBdEMsRUFBMkM7QUFDekMrQyxpQkFBVS9DLENBQVY7QUFDRDs7QUFFRCxZQUFPaEUsTUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7O0FBVUEsWUFBU2dILGNBQVQsQ0FBd0JDLFdBQXhCLEVBQXFDO0FBQ25DLFNBQUksT0FBT0EsV0FBUCxLQUF1QixVQUEzQixFQUF1QztBQUNyQyxhQUFNLElBQUkxRSxLQUFKLENBQVUsNENBQVYsQ0FBTjtBQUNEOztBQUVEeUQsc0JBQWlCaUIsV0FBakI7QUFDQTVHLGNBQVMsRUFBRUosTUFBTXVGLFlBQVlLLElBQXBCLEVBQVQ7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsWUFBU3FCLFVBQVQsR0FBc0I7QUFDcEIsU0FBSUMsSUFBSjs7QUFFQSxTQUFJQyxpQkFBaUJaLFNBQXJCO0FBQ0EsWUFBT1csT0FBTztBQUNaOzs7Ozs7Ozs7QUFTQVgsa0JBQVcsU0FBU0EsU0FBVCxDQUFtQmEsUUFBbkIsRUFBNkI7QUFDdEMsYUFBSSxRQUFPQSxRQUFQLHlDQUFPQSxRQUFQLE9BQW9CLFFBQXhCLEVBQWtDO0FBQ2hDLGlCQUFNLElBQUlDLFNBQUosQ0FBYyx3Q0FBZCxDQUFOO0FBQ0Q7O0FBRUQsa0JBQVNDLFlBQVQsR0FBd0I7QUFDdEIsZUFBSUYsU0FBU0csSUFBYixFQUFtQjtBQUNqQkgsc0JBQVNHLElBQVQsQ0FBY2pCLFVBQWQ7QUFDRDtBQUNGOztBQUVEZ0I7QUFDQSxhQUFJWixjQUFjUyxlQUFlRyxZQUFmLENBQWxCO0FBQ0EsZ0JBQU8sRUFBRVosYUFBYUEsV0FBZixFQUFQO0FBQ0Q7QUF4QlcsTUFBUCxFQXlCSlEsS0FBS3ZCLG1CQUFtQixTQUFuQixDQUFMLElBQXNDLFlBQVk7QUFDbkQsY0FBTyxJQUFQO0FBQ0QsTUEzQk0sRUEyQkp1QixJQTNCSDtBQTRCRDs7QUFFRDtBQUNBO0FBQ0E7QUFDQTlHLFlBQVMsRUFBRUosTUFBTXVGLFlBQVlLLElBQXBCLEVBQVQ7O0FBRUEsVUFBT0UsUUFBUTtBQUNiMUYsZUFBVUEsUUFERztBQUVibUcsZ0JBQVdBLFNBRkU7QUFHYkQsZUFBVUEsUUFIRztBQUliUyxxQkFBZ0JBO0FBSkgsSUFBUixFQUtKakIsTUFBTUgsbUJBQW1CLFNBQW5CLENBQU4sSUFBdUNzQixVQUxuQyxFQUsrQ25CLEtBTHREO0FBTUQsRTs7Ozs7Ozs7QUNyUUQsS0FBSTBCLGVBQWUsbUJBQUF6RyxDQUFRLENBQVIsQ0FBbkI7QUFBQSxLQUNJMEcsZUFBZSxtQkFBQTFHLENBQVEsQ0FBUixDQURuQjtBQUFBLEtBRUkyRyxlQUFlLG1CQUFBM0csQ0FBUSxDQUFSLENBRm5COztBQUlBO0FBQ0EsS0FBSTRHLFlBQVksaUJBQWhCOztBQUVBO0FBQ0EsS0FBSUMsWUFBWUMsU0FBUzFELFNBQXpCO0FBQUEsS0FDSTJELGNBQWNDLE9BQU81RCxTQUR6Qjs7QUFHQTtBQUNBLEtBQUk2RCxlQUFlSixVQUFVSyxRQUE3Qjs7QUFFQTtBQUNBLEtBQUlDLGlCQUFpQkosWUFBWUksY0FBakM7O0FBRUE7QUFDQSxLQUFJQyxtQkFBbUJILGFBQWFuRixJQUFiLENBQWtCa0YsTUFBbEIsQ0FBdkI7O0FBRUE7Ozs7O0FBS0EsS0FBSUssaUJBQWlCTixZQUFZRyxRQUFqQzs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTRCQSxVQUFTSSxhQUFULENBQXVCQyxLQUF2QixFQUE4QjtBQUM1QixTQUFJLENBQUNaLGFBQWFZLEtBQWIsQ0FBRCxJQUNBRixlQUFldkYsSUFBZixDQUFvQnlGLEtBQXBCLEtBQThCWCxTQUQ5QixJQUMyQ0YsYUFBYWEsS0FBYixDQUQvQyxFQUNvRTtBQUNsRSxnQkFBTyxLQUFQO0FBQ0Q7QUFDRCxTQUFJQyxRQUFRZixhQUFhYyxLQUFiLENBQVo7QUFDQSxTQUFJQyxVQUFVLElBQWQsRUFBb0I7QUFDbEIsZ0JBQU8sSUFBUDtBQUNEO0FBQ0QsU0FBSUMsT0FBT04sZUFBZXJGLElBQWYsQ0FBb0IwRixLQUFwQixFQUEyQixhQUEzQixLQUE2Q0EsTUFBTUUsV0FBOUQ7QUFDQSxZQUFRLE9BQU9ELElBQVAsSUFBZSxVQUFmLElBQ05BLGdCQUFnQkEsSUFEVixJQUNrQlIsYUFBYW5GLElBQWIsQ0FBa0IyRixJQUFsQixLQUEyQkwsZ0JBRHJEO0FBRUQ7O0FBRURqRyxRQUFPNUIsT0FBUCxHQUFpQitILGFBQWpCLEM7Ozs7Ozs7O0FDckVBLEtBQUlLLFVBQVUsbUJBQUEzSCxDQUFRLENBQVIsQ0FBZDs7QUFFQTtBQUNBLEtBQUl5RyxlQUFla0IsUUFBUVgsT0FBT1ksY0FBZixFQUErQlosTUFBL0IsQ0FBbkI7O0FBRUE3RixRQUFPNUIsT0FBUCxHQUFpQmtILFlBQWpCLEM7Ozs7Ozs7O0FDTEE7Ozs7Ozs7O0FBUUEsVUFBU2tCLE9BQVQsQ0FBaUJFLElBQWpCLEVBQXVCQyxTQUF2QixFQUFrQztBQUNoQyxVQUFPLFVBQVNDLEdBQVQsRUFBYztBQUNuQixZQUFPRixLQUFLQyxVQUFVQyxHQUFWLENBQUwsQ0FBUDtBQUNELElBRkQ7QUFHRDs7QUFFRDVHLFFBQU81QixPQUFQLEdBQWlCb0ksT0FBakIsQzs7Ozs7Ozs7QUNkQTs7Ozs7OztBQU9BLFVBQVNqQixZQUFULENBQXNCYSxLQUF0QixFQUE2QjtBQUMzQjtBQUNBO0FBQ0EsT0FBSVMsU0FBUyxLQUFiO0FBQ0EsT0FBSVQsU0FBUyxJQUFULElBQWlCLE9BQU9BLE1BQU1MLFFBQWIsSUFBeUIsVUFBOUMsRUFBMEQ7QUFDeEQsU0FBSTtBQUNGYyxnQkFBUyxDQUFDLEVBQUVULFFBQVEsRUFBVixDQUFWO0FBQ0QsTUFGRCxDQUVFLE9BQU83RixDQUFQLEVBQVUsQ0FBRTtBQUNmO0FBQ0QsVUFBT3NHLE1BQVA7QUFDRDs7QUFFRDdHLFFBQU81QixPQUFQLEdBQWlCbUgsWUFBakIsQzs7Ozs7Ozs7OztBQ25CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBLFVBQVNDLFlBQVQsQ0FBc0JZLEtBQXRCLEVBQTZCO0FBQzNCLFVBQU8sQ0FBQyxDQUFDQSxLQUFGLElBQVcsUUFBT0EsS0FBUCx5Q0FBT0EsS0FBUCxNQUFnQixRQUFsQztBQUNEOztBQUVEcEcsUUFBTzVCLE9BQVAsR0FBaUJvSCxZQUFqQixDOzs7Ozs7QUM1QkE7QUFDQTs7QUFFQXhGLFFBQU81QixPQUFQLEdBQWlCLG1CQUFBUyxDQUFRLEVBQVIsRUFBc0JpSSxVQUFVQyxNQUFWLGFBQXRCLENBQWpCLEM7Ozs7Ozs7QUNIQTs7QUFFQS9HLFFBQU81QixPQUFQLEdBQWlCLFNBQVM0SSx3QkFBVCxDQUFrQ0MsSUFBbEMsRUFBd0M7QUFDeEQsTUFBSUosTUFBSjtBQUNBLE1BQUlLLFVBQVNELEtBQUtDLE1BQWxCOztBQUVBLE1BQUksT0FBT0EsT0FBUCxLQUFrQixVQUF0QixFQUFrQztBQUNqQyxPQUFJQSxRQUFPbkMsVUFBWCxFQUF1QjtBQUN0QjhCLGFBQVNLLFFBQU9uQyxVQUFoQjtBQUNBLElBRkQsTUFFTztBQUNOOEIsYUFBU0ssUUFBTyxZQUFQLENBQVQ7QUFDQUEsWUFBT25DLFVBQVAsR0FBb0I4QixNQUFwQjtBQUNBO0FBQ0QsR0FQRCxNQU9PO0FBQ05BLFlBQVMsY0FBVDtBQUNBOztBQUVELFNBQU9BLE1BQVA7QUFDQSxFQWhCRCxDOzs7Ozs7QUNGQTs7QUFFQXpJLFNBQVFDLFVBQVIsR0FBcUIsSUFBckI7QUFDQUQsU0FBUSxTQUFSLElBQXFCSyxlQUFyQjs7QUFFQSxLQUFJRyxlQUFlLG1CQUFBQyxDQUFRLENBQVIsQ0FBbkI7O0FBRUEsS0FBSXlFLGlCQUFpQixtQkFBQXpFLENBQVEsQ0FBUixDQUFyQjs7QUFFQSxLQUFJMEUsa0JBQWtCeEUsdUJBQXVCdUUsY0FBdkIsQ0FBdEI7O0FBRUEsS0FBSTlELFdBQVcsbUJBQUFYLENBQVEsRUFBUixDQUFmOztBQUVBLEtBQUlZLFlBQVlWLHVCQUF1QlMsUUFBdkIsQ0FBaEI7O0FBRUEsVUFBU1Qsc0JBQVQsQ0FBZ0NXLEdBQWhDLEVBQXFDO0FBQUUsVUFBT0EsT0FBT0EsSUFBSXJCLFVBQVgsR0FBd0JxQixHQUF4QixHQUE4QixFQUFFLFdBQVdBLEdBQWIsRUFBckM7QUFBMEQ7O0FBRWpHLFVBQVN5SCw2QkFBVCxDQUF1Q0MsR0FBdkMsRUFBNEN2SixNQUE1QyxFQUFvRDtBQUNsRCxPQUFJd0osYUFBYXhKLFVBQVVBLE9BQU9DLElBQWxDO0FBQ0EsT0FBSXdKLGFBQWFELGNBQWMsTUFBTUEsV0FBV3RCLFFBQVgsRUFBTixHQUE4QixHQUE1QyxJQUFtRCxXQUFwRTs7QUFFQSxVQUFPLGtCQUFrQnVCLFVBQWxCLEdBQStCLGFBQS9CLEdBQStDRixHQUEvQyxHQUFxRCx3QkFBckQsR0FBZ0YscUVBQXZGO0FBQ0Q7O0FBRUQsVUFBU0cscUNBQVQsQ0FBK0NDLFVBQS9DLEVBQTJEQyxRQUEzRCxFQUFxRTVKLE1BQXJFLEVBQTZFO0FBQzNFLE9BQUk2SixjQUFjN0IsT0FBTzhCLElBQVAsQ0FBWUYsUUFBWixDQUFsQjtBQUNBLE9BQUlHLGVBQWUvSixVQUFVQSxPQUFPQyxJQUFQLEtBQWdCYyxhQUFheUUsV0FBYixDQUF5QkssSUFBbkQsR0FBMEQsNkNBQTFELEdBQTBHLHdDQUE3SDs7QUFFQSxPQUFJZ0UsWUFBWXZHLE1BQVosS0FBdUIsQ0FBM0IsRUFBOEI7QUFDNUIsWUFBTyx3RUFBd0UsNERBQS9FO0FBQ0Q7O0FBRUQsT0FBSSxDQUFDLENBQUMsR0FBR29DLGdCQUFnQixTQUFoQixDQUFKLEVBQWdDaUUsVUFBaEMsQ0FBTCxFQUFrRDtBQUNoRCxZQUFPLFNBQVNJLFlBQVQsR0FBd0IsMkJBQXhCLEdBQXNELEdBQUc3QixRQUFILENBQVlwRixJQUFaLENBQWlCNkcsVUFBakIsRUFBNkJLLEtBQTdCLENBQW1DLGdCQUFuQyxFQUFxRCxDQUFyRCxDQUF0RCxHQUFnSCwwREFBaEgsSUFBOEssWUFBWUgsWUFBWUksSUFBWixDQUFpQixNQUFqQixDQUFaLEdBQXVDLEdBQXJOLENBQVA7QUFDRDs7QUFFRCxPQUFJQyxpQkFBaUJsQyxPQUFPOEIsSUFBUCxDQUFZSCxVQUFaLEVBQXdCUSxNQUF4QixDQUErQixVQUFVWixHQUFWLEVBQWU7QUFDakUsWUFBTyxDQUFDSyxTQUFTekIsY0FBVCxDQUF3Qm9CLEdBQXhCLENBQVI7QUFDRCxJQUZvQixDQUFyQjs7QUFJQSxPQUFJVyxlQUFlNUcsTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUM3QixZQUFPLGlCQUFpQjRHLGVBQWU1RyxNQUFmLEdBQXdCLENBQXhCLEdBQTRCLE1BQTVCLEdBQXFDLEtBQXRELElBQStELEdBQS9ELElBQXNFLE1BQU00RyxlQUFlRCxJQUFmLENBQW9CLE1BQXBCLENBQU4sR0FBb0MsYUFBcEMsR0FBb0RGLFlBQXBELEdBQW1FLElBQXpJLElBQWlKLDBEQUFqSixJQUErTSxNQUFNRixZQUFZSSxJQUFaLENBQWlCLE1BQWpCLENBQU4sR0FBaUMscUNBQWhQLENBQVA7QUFDRDtBQUNGOztBQUVELFVBQVNHLG1CQUFULENBQTZCUixRQUE3QixFQUF1QztBQUNyQzVCLFVBQU84QixJQUFQLENBQVlGLFFBQVosRUFBc0JTLE9BQXRCLENBQThCLFVBQVVkLEdBQVYsRUFBZTtBQUMzQyxTQUFJekosVUFBVThKLFNBQVNMLEdBQVQsQ0FBZDtBQUNBLFNBQUk5SixlQUFlSyxRQUFRZ0IsU0FBUixFQUFtQixFQUFFYixNQUFNYyxhQUFheUUsV0FBYixDQUF5QkssSUFBakMsRUFBbkIsQ0FBbkI7O0FBRUEsU0FBSSxPQUFPcEcsWUFBUCxLQUF3QixXQUE1QixFQUF5QztBQUN2QyxhQUFNLElBQUk4QyxLQUFKLENBQVUsY0FBY2dILEdBQWQsR0FBb0IsOENBQXBCLEdBQXFFLDREQUFyRSxHQUFvSSw2REFBcEksR0FBb00sbUJBQTlNLENBQU47QUFDRDs7QUFFRCxTQUFJdEosT0FBTyxrQ0FBa0NxSyxLQUFLQyxNQUFMLEdBQWNyQyxRQUFkLENBQXVCLEVBQXZCLEVBQTJCc0MsU0FBM0IsQ0FBcUMsQ0FBckMsRUFBd0NDLEtBQXhDLENBQThDLEVBQTlDLEVBQWtEUixJQUFsRCxDQUF1RCxHQUF2RCxDQUE3QztBQUNBLFNBQUksT0FBT25LLFFBQVFnQixTQUFSLEVBQW1CLEVBQUViLE1BQU1BLElBQVIsRUFBbkIsQ0FBUCxLQUE4QyxXQUFsRCxFQUErRDtBQUM3RCxhQUFNLElBQUlzQyxLQUFKLENBQVUsY0FBY2dILEdBQWQsR0FBb0IsdURBQXBCLElBQStFLDBCQUEwQnhJLGFBQWF5RSxXQUFiLENBQXlCSyxJQUFuRCxHQUEwRCxpQ0FBekksSUFBOEssdUVBQTlLLEdBQXdQLGlFQUF4UCxHQUE0VCxxRUFBNVQsR0FBb1ksc0RBQTlZLENBQU47QUFDRDtBQUNGLElBWkQ7QUFhRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxVQUFTakYsZUFBVCxDQUF5QmdKLFFBQXpCLEVBQW1DO0FBQ2pDLE9BQUlDLGNBQWM3QixPQUFPOEIsSUFBUCxDQUFZRixRQUFaLENBQWxCO0FBQ0EsT0FBSWMsZ0JBQWdCLEVBQXBCO0FBQ0EsUUFBSyxJQUFJMUcsSUFBSSxDQUFiLEVBQWdCQSxJQUFJNkYsWUFBWXZHLE1BQWhDLEVBQXdDVSxHQUF4QyxFQUE2QztBQUMzQyxTQUFJdUYsTUFBTU0sWUFBWTdGLENBQVosQ0FBVjtBQUNBLFNBQUksT0FBTzRGLFNBQVNMLEdBQVQsQ0FBUCxLQUF5QixVQUE3QixFQUF5QztBQUN2Q21CLHFCQUFjbkIsR0FBZCxJQUFxQkssU0FBU0wsR0FBVCxDQUFyQjtBQUNEO0FBQ0Y7QUFDRCxPQUFJb0IsbUJBQW1CM0MsT0FBTzhCLElBQVAsQ0FBWVksYUFBWixDQUF2Qjs7QUFFQSxPQUFJRSxXQUFKO0FBQ0EsT0FBSTtBQUNGUix5QkFBb0JNLGFBQXBCO0FBQ0QsSUFGRCxDQUVFLE9BQU9oSSxDQUFQLEVBQVU7QUFDVmtJLG1CQUFjbEksQ0FBZDtBQUNEOztBQUVELFVBQU8sU0FBU21JLFdBQVQsR0FBdUI7QUFDNUIsU0FBSTlLLFFBQVFnRSxVQUFVVCxNQUFWLElBQW9CLENBQXBCLElBQXlCUyxVQUFVLENBQVYsTUFBaUJqRCxTQUExQyxHQUFzRCxFQUF0RCxHQUEyRGlELFVBQVUsQ0FBVixDQUF2RTtBQUNBLFNBQUkvRCxTQUFTK0QsVUFBVSxDQUFWLENBQWI7O0FBRUEsU0FBSTZHLFdBQUosRUFBaUI7QUFDZixhQUFNQSxXQUFOO0FBQ0Q7O0FBRUQsU0FBSTdJLFFBQVFDLEdBQVIsQ0FBWUMsUUFBWixLQUF5QixZQUE3QixFQUEyQztBQUN6QyxXQUFJNkksaUJBQWlCcEIsc0NBQXNDM0osS0FBdEMsRUFBNkMySyxhQUE3QyxFQUE0RDFLLE1BQTVELENBQXJCO0FBQ0EsV0FBSThLLGNBQUosRUFBb0I7QUFDbEIsVUFBQyxHQUFHbEosVUFBVSxTQUFWLENBQUosRUFBMEJrSixjQUExQjtBQUNEO0FBQ0Y7O0FBRUQsU0FBSUMsYUFBYSxLQUFqQjtBQUNBLFNBQUlDLFlBQVksRUFBaEI7QUFDQSxVQUFLLElBQUloSCxJQUFJLENBQWIsRUFBZ0JBLElBQUkyRyxpQkFBaUJySCxNQUFyQyxFQUE2Q1UsR0FBN0MsRUFBa0Q7QUFDaEQsV0FBSXVGLE1BQU1vQixpQkFBaUIzRyxDQUFqQixDQUFWO0FBQ0EsV0FBSWxFLFVBQVU0SyxjQUFjbkIsR0FBZCxDQUFkO0FBQ0EsV0FBSTBCLHNCQUFzQmxMLE1BQU13SixHQUFOLENBQTFCO0FBQ0EsV0FBSTJCLGtCQUFrQnBMLFFBQVFtTCxtQkFBUixFQUE2QmpMLE1BQTdCLENBQXRCO0FBQ0EsV0FBSSxPQUFPa0wsZUFBUCxLQUEyQixXQUEvQixFQUE0QztBQUMxQyxhQUFJQyxlQUFlN0IsOEJBQThCQyxHQUE5QixFQUFtQ3ZKLE1BQW5DLENBQW5CO0FBQ0EsZUFBTSxJQUFJdUMsS0FBSixDQUFVNEksWUFBVixDQUFOO0FBQ0Q7QUFDREgsaUJBQVV6QixHQUFWLElBQWlCMkIsZUFBakI7QUFDQUgsb0JBQWFBLGNBQWNHLG9CQUFvQkQsbUJBQS9DO0FBQ0Q7QUFDRCxZQUFPRixhQUFhQyxTQUFiLEdBQXlCakwsS0FBaEM7QUFDRCxJQTlCRDtBQStCRCxFOzs7Ozs7O0FDOUhEOztBQUVBUSxTQUFRQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELFNBQVEsU0FBUixJQUFxQjZLLE9BQXJCO0FBQ0E7Ozs7OztBQU1BLFVBQVNBLE9BQVQsQ0FBaUJDLE9BQWpCLEVBQTBCO0FBQ3hCO0FBQ0EsT0FBSSxPQUFPQyxPQUFQLEtBQW1CLFdBQW5CLElBQWtDLE9BQU9BLFFBQVF6TCxLQUFmLEtBQXlCLFVBQS9ELEVBQTJFO0FBQ3pFeUwsYUFBUXpMLEtBQVIsQ0FBY3dMLE9BQWQ7QUFDRDtBQUNEO0FBQ0EsT0FBSTtBQUNGO0FBQ0E7QUFDQTtBQUNBLFdBQU0sSUFBSTlJLEtBQUosQ0FBVThJLE9BQVYsQ0FBTjtBQUNBO0FBQ0QsSUFORCxDQU1FLE9BQU8zSSxDQUFQLEVBQVUsQ0FBRTtBQUNkO0FBQ0QsRTs7Ozs7O0FDeEJEOzs7O0FBRUFuQyxTQUFRQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELFNBQVEsU0FBUixJQUFxQkksa0JBQXJCO0FBQ0EsVUFBUzRLLGlCQUFULENBQTJCQyxhQUEzQixFQUEwQ25MLFFBQTFDLEVBQW9EO0FBQ2xELFVBQU8sWUFBWTtBQUNqQixZQUFPQSxTQUFTbUwsY0FBY25ILEtBQWQsQ0FBb0J2RCxTQUFwQixFQUErQmlELFNBQS9CLENBQVQsQ0FBUDtBQUNELElBRkQ7QUFHRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBLFVBQVNwRCxrQkFBVCxDQUE0QjhLLGNBQTVCLEVBQTRDcEwsUUFBNUMsRUFBc0Q7QUFDcEQsT0FBSSxPQUFPb0wsY0FBUCxLQUEwQixVQUE5QixFQUEwQztBQUN4QyxZQUFPRixrQkFBa0JFLGNBQWxCLEVBQWtDcEwsUUFBbEMsQ0FBUDtBQUNEOztBQUVELE9BQUksUUFBT29MLGNBQVAseUNBQU9BLGNBQVAsT0FBMEIsUUFBMUIsSUFBc0NBLG1CQUFtQixJQUE3RCxFQUFtRTtBQUNqRSxXQUFNLElBQUlsSixLQUFKLENBQVUsNEVBQTRFa0osbUJBQW1CLElBQW5CLEdBQTBCLE1BQTFCLFVBQTBDQSxjQUExQyx5Q0FBMENBLGNBQTFDLENBQTVFLElBQXdJLElBQXhJLEdBQStJLDBGQUF6SixDQUFOO0FBQ0Q7O0FBRUQsT0FBSTNCLE9BQU85QixPQUFPOEIsSUFBUCxDQUFZMkIsY0FBWixDQUFYO0FBQ0EsT0FBSUMsc0JBQXNCLEVBQTFCO0FBQ0EsUUFBSyxJQUFJMUgsSUFBSSxDQUFiLEVBQWdCQSxJQUFJOEYsS0FBS3hHLE1BQXpCLEVBQWlDVSxHQUFqQyxFQUFzQztBQUNwQyxTQUFJdUYsTUFBTU8sS0FBSzlGLENBQUwsQ0FBVjtBQUNBLFNBQUl3SCxnQkFBZ0JDLGVBQWVsQyxHQUFmLENBQXBCO0FBQ0EsU0FBSSxPQUFPaUMsYUFBUCxLQUF5QixVQUE3QixFQUF5QztBQUN2Q0UsMkJBQW9CbkMsR0FBcEIsSUFBMkJnQyxrQkFBa0JDLGFBQWxCLEVBQWlDbkwsUUFBakMsQ0FBM0I7QUFDRDtBQUNGO0FBQ0QsVUFBT3FMLG1CQUFQO0FBQ0QsRTs7Ozs7O0FDbEREOztBQUVBbkwsU0FBUUMsVUFBUixHQUFxQixJQUFyQjs7QUFFQSxLQUFJbUwsV0FBVzNELE9BQU80RCxNQUFQLElBQWlCLFVBQVVDLE1BQVYsRUFBa0I7QUFBRSxRQUFLLElBQUk3SCxJQUFJLENBQWIsRUFBZ0JBLElBQUlELFVBQVVULE1BQTlCLEVBQXNDVSxHQUF0QyxFQUEyQztBQUFFLFNBQUk4SCxTQUFTL0gsVUFBVUMsQ0FBVixDQUFiLENBQTJCLEtBQUssSUFBSXVGLEdBQVQsSUFBZ0J1QyxNQUFoQixFQUF3QjtBQUFFLFdBQUk5RCxPQUFPNUQsU0FBUCxDQUFpQitELGNBQWpCLENBQWdDckYsSUFBaEMsQ0FBcUNnSixNQUFyQyxFQUE2Q3ZDLEdBQTdDLENBQUosRUFBdUQ7QUFBRXNDLGdCQUFPdEMsR0FBUCxJQUFjdUMsT0FBT3ZDLEdBQVAsQ0FBZDtBQUE0QjtBQUFFO0FBQUUsSUFBQyxPQUFPc0MsTUFBUDtBQUFnQixFQUFoUTs7QUFFQXRMLFNBQVEsU0FBUixJQUFxQkcsZUFBckI7O0FBRUEsS0FBSWUsV0FBVyxtQkFBQVQsQ0FBUSxFQUFSLENBQWY7O0FBRUEsS0FBSVUsWUFBWVIsdUJBQXVCTyxRQUF2QixDQUFoQjs7QUFFQSxVQUFTUCxzQkFBVCxDQUFnQ1csR0FBaEMsRUFBcUM7QUFBRSxVQUFPQSxPQUFPQSxJQUFJckIsVUFBWCxHQUF3QnFCLEdBQXhCLEdBQThCLEVBQUUsV0FBV0EsR0FBYixFQUFyQztBQUEwRDs7QUFFakc7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkEsVUFBU25CLGVBQVQsR0FBMkI7QUFDekIsUUFBSyxJQUFJcUwsT0FBT2hJLFVBQVVULE1BQXJCLEVBQTZCMEksY0FBY2xJLE1BQU1pSSxJQUFOLENBQTNDLEVBQXdERSxPQUFPLENBQXBFLEVBQXVFQSxPQUFPRixJQUE5RSxFQUFvRkUsTUFBcEYsRUFBNEY7QUFDMUZELGlCQUFZQyxJQUFaLElBQW9CbEksVUFBVWtJLElBQVYsQ0FBcEI7QUFDRDs7QUFFRCxVQUFPLFVBQVVwTCxXQUFWLEVBQXVCO0FBQzVCLFlBQU8sVUFBVWYsT0FBVixFQUFtQkwsWUFBbkIsRUFBaUNxRyxRQUFqQyxFQUEyQztBQUNoRCxXQUFJMUYsUUFBUVMsWUFBWWYsT0FBWixFQUFxQkwsWUFBckIsRUFBbUNxRyxRQUFuQyxDQUFaO0FBQ0EsV0FBSW9HLFlBQVk5TCxNQUFNQyxRQUF0QjtBQUNBLFdBQUk4TCxRQUFRLEVBQVo7O0FBRUEsV0FBSUMsZ0JBQWdCO0FBQ2xCN0YsbUJBQVVuRyxNQUFNbUcsUUFERTtBQUVsQmxHLG1CQUFVLFNBQVNBLFFBQVQsQ0FBa0JMLE1BQWxCLEVBQTBCO0FBQ2xDLGtCQUFPa00sVUFBVWxNLE1BQVYsQ0FBUDtBQUNEO0FBSmlCLFFBQXBCO0FBTUFtTSxlQUFRSCxZQUFZSyxHQUFaLENBQWdCLFVBQVVsTSxVQUFWLEVBQXNCO0FBQzVDLGdCQUFPQSxXQUFXaU0sYUFBWCxDQUFQO0FBQ0QsUUFGTyxDQUFSO0FBR0FGLG1CQUFZeEssVUFBVSxTQUFWLEVBQXFCMkMsS0FBckIsQ0FBMkJ2RCxTQUEzQixFQUFzQ3FMLEtBQXRDLEVBQTZDL0wsTUFBTUMsUUFBbkQsQ0FBWjs7QUFFQSxjQUFPc0wsU0FBUyxFQUFULEVBQWF2TCxLQUFiLEVBQW9CO0FBQ3pCQyxtQkFBVTZMO0FBRGUsUUFBcEIsQ0FBUDtBQUdELE1BbkJEO0FBb0JELElBckJEO0FBc0JELEU7Ozs7OztBQ3pERDs7OztBQUVBM0wsU0FBUUMsVUFBUixHQUFxQixJQUFyQjtBQUNBRCxTQUFRLFNBQVIsSUFBcUJFLE9BQXJCO0FBQ0E7Ozs7Ozs7Ozs7O0FBV0EsVUFBU0EsT0FBVCxHQUFtQjtBQUNqQixRQUFLLElBQUlzTCxPQUFPaEksVUFBVVQsTUFBckIsRUFBNkJnSixRQUFReEksTUFBTWlJLElBQU4sQ0FBckMsRUFBa0RFLE9BQU8sQ0FBOUQsRUFBaUVBLE9BQU9GLElBQXhFLEVBQThFRSxNQUE5RSxFQUFzRjtBQUNwRkssV0FBTUwsSUFBTixJQUFjbEksVUFBVWtJLElBQVYsQ0FBZDtBQUNEOztBQUVELE9BQUlLLE1BQU1oSixNQUFOLEtBQWlCLENBQXJCLEVBQXdCO0FBQ3RCLFlBQU8sVUFBVXlGLEdBQVYsRUFBZTtBQUNwQixjQUFPQSxHQUFQO0FBQ0QsTUFGRDtBQUdELElBSkQsTUFJTztBQUNMLFNBQUl3RCxPQUFPLFlBQVk7QUFDckIsV0FBSUMsT0FBT0YsTUFBTUEsTUFBTWhKLE1BQU4sR0FBZSxDQUFyQixDQUFYO0FBQ0EsV0FBSW1KLE9BQU9ILE1BQU1oRyxLQUFOLENBQVksQ0FBWixFQUFlLENBQUMsQ0FBaEIsQ0FBWDtBQUNBLGNBQU87QUFDTG9HLFlBQUcsU0FBU0EsQ0FBVCxHQUFhO0FBQ2Qsa0JBQU9ELEtBQUtFLFdBQUwsQ0FBaUIsVUFBVUMsUUFBVixFQUFvQkMsQ0FBcEIsRUFBdUI7QUFDN0Msb0JBQU9BLEVBQUVELFFBQUYsQ0FBUDtBQUNELFlBRk0sRUFFSkosS0FBS25JLEtBQUwsQ0FBV3ZELFNBQVgsRUFBc0JpRCxTQUF0QixDQUZJLENBQVA7QUFHRDtBQUxJLFFBQVA7QUFPRCxNQVZVLEVBQVg7O0FBWUEsU0FBSSxRQUFPd0ksSUFBUCx5Q0FBT0EsSUFBUCxPQUFnQixRQUFwQixFQUE4QixPQUFPQSxLQUFLRyxDQUFaO0FBQy9CO0FBQ0YsRTs7Ozs7Ozs7QUN2Q0R2SyxRQUFPNUIsT0FBUCxHQUFpQixtQkFBQVMsQ0FBUSxFQUFSLENBQWpCLEM7Ozs7OztBQ0FBOztBQUVBLEtBQUk4TCxRQUFRLG1CQUFBOUwsQ0FBUSxFQUFSLENBQVo7QUFDQSxLQUFJK0wsT0FBTyxtQkFBQS9MLENBQVEsRUFBUixDQUFYO0FBQ0EsS0FBSWdNLFFBQVEsbUJBQUFoTSxDQUFRLEVBQVIsQ0FBWjs7QUFFQTs7Ozs7O0FBTUEsVUFBU2lNLGNBQVQsQ0FBd0JDLGFBQXhCLEVBQXVDO0FBQ3JDLE9BQUlDLFVBQVUsSUFBSUgsS0FBSixDQUFVRSxhQUFWLENBQWQ7QUFDQSxPQUFJRSxXQUFXTCxLQUFLQyxNQUFNNUksU0FBTixDQUFnQmlKLE9BQXJCLEVBQThCRixPQUE5QixDQUFmOztBQUVBO0FBQ0FMLFNBQU1RLE1BQU4sQ0FBYUYsUUFBYixFQUF1QkosTUFBTTVJLFNBQTdCLEVBQXdDK0ksT0FBeEM7O0FBRUE7QUFDQUwsU0FBTVEsTUFBTixDQUFhRixRQUFiLEVBQXVCRCxPQUF2Qjs7QUFFQSxVQUFPQyxRQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxLQUFJRyxRQUFRTixnQkFBWjs7QUFFQTtBQUNBTSxPQUFNUCxLQUFOLEdBQWNBLEtBQWQ7O0FBRUE7QUFDQU8sT0FBTUMsTUFBTixHQUFlLFNBQVNBLE1BQVQsQ0FBZ0JOLGFBQWhCLEVBQStCO0FBQzVDLFVBQU9ELGVBQWVDLGFBQWYsQ0FBUDtBQUNELEVBRkQ7O0FBSUE7QUFDQUssT0FBTUUsR0FBTixHQUFZLFNBQVNBLEdBQVQsQ0FBYUMsUUFBYixFQUF1QjtBQUNqQyxVQUFPQyxRQUFRRixHQUFSLENBQVlDLFFBQVosQ0FBUDtBQUNELEVBRkQ7QUFHQUgsT0FBTUssTUFBTixHQUFlLG1CQUFBNU0sQ0FBUSxFQUFSLENBQWY7O0FBRUFtQixRQUFPNUIsT0FBUCxHQUFpQmdOLEtBQWpCOztBQUVBO0FBQ0FwTCxRQUFPNUIsT0FBUCxDQUFlc04sT0FBZixHQUF5Qk4sS0FBekIsQzs7Ozs7O0FDN0NBOzs7O0FBRUEsS0FBSVIsT0FBTyxtQkFBQS9MLENBQVEsRUFBUixDQUFYOztBQUVBOztBQUVBOztBQUVBLEtBQUlrSCxXQUFXRixPQUFPNUQsU0FBUCxDQUFpQjhELFFBQWhDOztBQUVBOzs7Ozs7QUFNQSxVQUFTNEYsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDcEIsVUFBTzdGLFNBQVNwRixJQUFULENBQWNpTCxHQUFkLE1BQXVCLGdCQUE5QjtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTQyxhQUFULENBQXVCRCxHQUF2QixFQUE0QjtBQUMxQixVQUFPN0YsU0FBU3BGLElBQVQsQ0FBY2lMLEdBQWQsTUFBdUIsc0JBQTlCO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFVBQVNFLFVBQVQsQ0FBb0JGLEdBQXBCLEVBQXlCO0FBQ3ZCLFVBQVEsT0FBT0csUUFBUCxLQUFvQixXQUFyQixJQUFzQ0gsZUFBZUcsUUFBNUQ7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsVUFBU0MsaUJBQVQsQ0FBMkJKLEdBQTNCLEVBQWdDO0FBQzlCLE9BQUkvRSxNQUFKO0FBQ0EsT0FBSyxPQUFPb0YsV0FBUCxLQUF1QixXQUF4QixJQUF5Q0EsWUFBWUMsTUFBekQsRUFBa0U7QUFDaEVyRixjQUFTb0YsWUFBWUMsTUFBWixDQUFtQk4sR0FBbkIsQ0FBVDtBQUNELElBRkQsTUFFTztBQUNML0UsY0FBVStFLEdBQUQsSUFBVUEsSUFBSU8sTUFBZCxJQUEwQlAsSUFBSU8sTUFBSixZQUFzQkYsV0FBekQ7QUFDRDtBQUNELFVBQU9wRixNQUFQO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFVBQVN1RixRQUFULENBQWtCUixHQUFsQixFQUF1QjtBQUNyQixVQUFPLE9BQU9BLEdBQVAsS0FBZSxRQUF0QjtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTUyxRQUFULENBQWtCVCxHQUFsQixFQUF1QjtBQUNyQixVQUFPLE9BQU9BLEdBQVAsS0FBZSxRQUF0QjtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTVSxXQUFULENBQXFCVixHQUFyQixFQUEwQjtBQUN4QixVQUFPLE9BQU9BLEdBQVAsS0FBZSxXQUF0QjtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTVyxRQUFULENBQWtCWCxHQUFsQixFQUF1QjtBQUNyQixVQUFPQSxRQUFRLElBQVIsSUFBZ0IsUUFBT0EsR0FBUCx5Q0FBT0EsR0FBUCxPQUFlLFFBQXRDO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFVBQVNZLE1BQVQsQ0FBZ0JaLEdBQWhCLEVBQXFCO0FBQ25CLFVBQU83RixTQUFTcEYsSUFBVCxDQUFjaUwsR0FBZCxNQUF1QixlQUE5QjtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTYSxNQUFULENBQWdCYixHQUFoQixFQUFxQjtBQUNuQixVQUFPN0YsU0FBU3BGLElBQVQsQ0FBY2lMLEdBQWQsTUFBdUIsZUFBOUI7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsVUFBU2MsTUFBVCxDQUFnQmQsR0FBaEIsRUFBcUI7QUFDbkIsVUFBTzdGLFNBQVNwRixJQUFULENBQWNpTCxHQUFkLE1BQXVCLGVBQTlCO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFVBQVNlLFVBQVQsQ0FBb0JmLEdBQXBCLEVBQXlCO0FBQ3ZCLFVBQU83RixTQUFTcEYsSUFBVCxDQUFjaUwsR0FBZCxNQUF1QixtQkFBOUI7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsVUFBU2dCLFFBQVQsQ0FBa0JoQixHQUFsQixFQUF1QjtBQUNyQixVQUFPVyxTQUFTWCxHQUFULEtBQWlCZSxXQUFXZixJQUFJaUIsSUFBZixDQUF4QjtBQUNEOztBQUVEOzs7Ozs7QUFNQSxVQUFTQyxpQkFBVCxDQUEyQmxCLEdBQTNCLEVBQWdDO0FBQzlCLFVBQU8sT0FBT21CLGVBQVAsS0FBMkIsV0FBM0IsSUFBMENuQixlQUFlbUIsZUFBaEU7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsVUFBU0MsSUFBVCxDQUFjQyxHQUFkLEVBQW1CO0FBQ2pCLFVBQU9BLElBQUlDLE9BQUosQ0FBWSxNQUFaLEVBQW9CLEVBQXBCLEVBQXdCQSxPQUF4QixDQUFnQyxNQUFoQyxFQUF3QyxFQUF4QyxDQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7QUFhQSxVQUFTQyxvQkFBVCxHQUFnQztBQUM5QixVQUNFLE9BQU9wRyxNQUFQLEtBQWtCLFdBQWxCLElBQ0EsT0FBT3FHLFFBQVAsS0FBb0IsV0FEcEIsSUFFQSxPQUFPQSxTQUFTQyxhQUFoQixLQUFrQyxVQUhwQztBQUtEOztBQUVEOzs7Ozs7Ozs7Ozs7QUFZQSxVQUFTbkYsT0FBVCxDQUFpQnhJLEdBQWpCLEVBQXNCNE4sRUFBdEIsRUFBMEI7QUFDeEI7QUFDQSxPQUFJNU4sUUFBUSxJQUFSLElBQWdCLE9BQU9BLEdBQVAsS0FBZSxXQUFuQyxFQUFnRDtBQUM5QztBQUNEOztBQUVEO0FBQ0EsT0FBSSxRQUFPQSxHQUFQLHlDQUFPQSxHQUFQLE9BQWUsUUFBZixJQUEyQixDQUFDaU0sUUFBUWpNLEdBQVIsQ0FBaEMsRUFBOEM7QUFDNUM7QUFDQUEsV0FBTSxDQUFDQSxHQUFELENBQU47QUFDRDs7QUFFRCxPQUFJaU0sUUFBUWpNLEdBQVIsQ0FBSixFQUFrQjtBQUNoQjtBQUNBLFVBQUssSUFBSW1DLElBQUksQ0FBUixFQUFXMEwsSUFBSTdOLElBQUl5QixNQUF4QixFQUFnQ1UsSUFBSTBMLENBQXBDLEVBQXVDMUwsR0FBdkMsRUFBNEM7QUFDMUN5TCxVQUFHM00sSUFBSCxDQUFRLElBQVIsRUFBY2pCLElBQUltQyxDQUFKLENBQWQsRUFBc0JBLENBQXRCLEVBQXlCbkMsR0FBekI7QUFDRDtBQUNGLElBTEQsTUFLTztBQUNMO0FBQ0EsVUFBSyxJQUFJMEgsR0FBVCxJQUFnQjFILEdBQWhCLEVBQXFCO0FBQ25CLFdBQUlBLElBQUlzRyxjQUFKLENBQW1Cb0IsR0FBbkIsQ0FBSixFQUE2QjtBQUMzQmtHLFlBQUczTSxJQUFILENBQVEsSUFBUixFQUFjakIsSUFBSTBILEdBQUosQ0FBZCxFQUF3QkEsR0FBeEIsRUFBNkIxSCxHQUE3QjtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVEOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxVQUFTOE4sS0FBVCxHQUFlLDJCQUE2QjtBQUMxQyxPQUFJM0csU0FBUyxFQUFiO0FBQ0EsWUFBUzRHLFdBQVQsQ0FBcUI3QixHQUFyQixFQUEwQnhFLEdBQTFCLEVBQStCO0FBQzdCLFNBQUksUUFBT1AsT0FBT08sR0FBUCxDQUFQLE1BQXVCLFFBQXZCLElBQW1DLFFBQU93RSxHQUFQLHlDQUFPQSxHQUFQLE9BQWUsUUFBdEQsRUFBZ0U7QUFDOUQvRSxjQUFPTyxHQUFQLElBQWNvRyxNQUFNM0csT0FBT08sR0FBUCxDQUFOLEVBQW1Cd0UsR0FBbkIsQ0FBZDtBQUNELE1BRkQsTUFFTztBQUNML0UsY0FBT08sR0FBUCxJQUFjd0UsR0FBZDtBQUNEO0FBQ0Y7O0FBRUQsUUFBSyxJQUFJL0osSUFBSSxDQUFSLEVBQVcwTCxJQUFJM0wsVUFBVVQsTUFBOUIsRUFBc0NVLElBQUkwTCxDQUExQyxFQUE2QzFMLEdBQTdDLEVBQWtEO0FBQ2hEcUcsYUFBUXRHLFVBQVVDLENBQVYsQ0FBUixFQUFzQjRMLFdBQXRCO0FBQ0Q7QUFDRCxVQUFPNUcsTUFBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFVBQVNzRSxNQUFULENBQWdCdUMsQ0FBaEIsRUFBbUJDLENBQW5CLEVBQXNCQyxPQUF0QixFQUErQjtBQUM3QjFGLFdBQVF5RixDQUFSLEVBQVcsU0FBU0YsV0FBVCxDQUFxQjdCLEdBQXJCLEVBQTBCeEUsR0FBMUIsRUFBK0I7QUFDeEMsU0FBSXdHLFdBQVcsT0FBT2hDLEdBQVAsS0FBZSxVQUE5QixFQUEwQztBQUN4QzhCLFNBQUV0RyxHQUFGLElBQVN3RCxLQUFLZ0IsR0FBTCxFQUFVZ0MsT0FBVixDQUFUO0FBQ0QsTUFGRCxNQUVPO0FBQ0xGLFNBQUV0RyxHQUFGLElBQVN3RSxHQUFUO0FBQ0Q7QUFDRixJQU5EO0FBT0EsVUFBTzhCLENBQVA7QUFDRDs7QUFFRDFOLFFBQU81QixPQUFQLEdBQWlCO0FBQ2Z1TixZQUFTQSxPQURNO0FBRWZFLGtCQUFlQSxhQUZBO0FBR2ZDLGVBQVlBLFVBSEc7QUFJZkUsc0JBQW1CQSxpQkFKSjtBQUtmSSxhQUFVQSxRQUxLO0FBTWZDLGFBQVVBLFFBTks7QUFPZkUsYUFBVUEsUUFQSztBQVFmRCxnQkFBYUEsV0FSRTtBQVNmRSxXQUFRQSxNQVRPO0FBVWZDLFdBQVFBLE1BVk87QUFXZkMsV0FBUUEsTUFYTztBQVlmQyxlQUFZQSxVQVpHO0FBYWZDLGFBQVVBLFFBYks7QUFjZkUsc0JBQW1CQSxpQkFkSjtBQWVmSyx5QkFBc0JBLG9CQWZQO0FBZ0JmakYsWUFBU0EsT0FoQk07QUFpQmZzRixVQUFPQSxLQWpCUTtBQWtCZnJDLFdBQVFBLE1BbEJPO0FBbUJmNkIsU0FBTUE7QUFuQlMsRUFBakIsQzs7Ozs7O0FDdFJBOztBQUVBaE4sUUFBTzVCLE9BQVAsR0FBaUIsU0FBU3dNLElBQVQsQ0FBYzBDLEVBQWQsRUFBa0JNLE9BQWxCLEVBQTJCO0FBQzFDLFVBQU8sU0FBU0MsSUFBVCxHQUFnQjtBQUNyQixTQUFJbk0sT0FBTyxJQUFJQyxLQUFKLENBQVVDLFVBQVVULE1BQXBCLENBQVg7QUFDQSxVQUFLLElBQUlVLElBQUksQ0FBYixFQUFnQkEsSUFBSUgsS0FBS1AsTUFBekIsRUFBaUNVLEdBQWpDLEVBQXNDO0FBQ3BDSCxZQUFLRyxDQUFMLElBQVVELFVBQVVDLENBQVYsQ0FBVjtBQUNEO0FBQ0QsWUFBT3lMLEdBQUdwTCxLQUFILENBQVMwTCxPQUFULEVBQWtCbE0sSUFBbEIsQ0FBUDtBQUNELElBTkQ7QUFPRCxFQVJELEM7Ozs7OztBQ0ZBOztBQUVBLEtBQUlvTSxXQUFXLG1CQUFBalAsQ0FBUSxFQUFSLENBQWY7QUFDQSxLQUFJOEwsUUFBUSxtQkFBQTlMLENBQVEsRUFBUixDQUFaO0FBQ0EsS0FBSWtQLHFCQUFxQixtQkFBQWxQLENBQVEsRUFBUixDQUF6QjtBQUNBLEtBQUltUCxrQkFBa0IsbUJBQUFuUCxDQUFRLEVBQVIsQ0FBdEI7QUFDQSxLQUFJb1AsZ0JBQWdCLG1CQUFBcFAsQ0FBUSxFQUFSLENBQXBCO0FBQ0EsS0FBSXFQLGNBQWMsbUJBQUFyUCxDQUFRLEVBQVIsQ0FBbEI7O0FBRUE7Ozs7O0FBS0EsVUFBU2dNLEtBQVQsQ0FBZUUsYUFBZixFQUE4QjtBQUM1QixRQUFLK0MsUUFBTCxHQUFnQm5ELE1BQU02QyxLQUFOLENBQVlNLFFBQVosRUFBc0IvQyxhQUF0QixDQUFoQjtBQUNBLFFBQUtvRCxZQUFMLEdBQW9CO0FBQ2xCakQsY0FBUyxJQUFJNkMsa0JBQUosRUFEUztBQUVsQkssZUFBVSxJQUFJTCxrQkFBSjtBQUZRLElBQXBCO0FBSUQ7O0FBRUQ7Ozs7O0FBS0FsRCxPQUFNNUksU0FBTixDQUFnQmlKLE9BQWhCLEdBQTBCLFNBQVNBLE9BQVQsQ0FBaUJtRCxNQUFqQixFQUF5QjtBQUNqRDtBQUNBO0FBQ0EsT0FBSSxPQUFPQSxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCQSxjQUFTMUQsTUFBTTZDLEtBQU4sQ0FBWTtBQUNuQmMsWUFBSzFNLFVBQVUsQ0FBVjtBQURjLE1BQVosRUFFTkEsVUFBVSxDQUFWLENBRk0sQ0FBVDtBQUdEOztBQUVEeU0sWUFBUzFELE1BQU02QyxLQUFOLENBQVlNLFFBQVosRUFBc0IsS0FBS0EsUUFBM0IsRUFBcUMsRUFBRVMsUUFBUSxLQUFWLEVBQXJDLEVBQXdERixNQUF4RCxDQUFUOztBQUVBO0FBQ0EsT0FBSUEsT0FBT0csT0FBUCxJQUFrQixDQUFDUCxjQUFjSSxPQUFPQyxHQUFyQixDQUF2QixFQUFrRDtBQUNoREQsWUFBT0MsR0FBUCxHQUFhSixZQUFZRyxPQUFPRyxPQUFuQixFQUE0QkgsT0FBT0MsR0FBbkMsQ0FBYjtBQUNEOztBQUVEO0FBQ0EsT0FBSXRFLFFBQVEsQ0FBQ2dFLGVBQUQsRUFBa0JyUCxTQUFsQixDQUFaO0FBQ0EsT0FBSThQLFVBQVVqRCxRQUFRa0QsT0FBUixDQUFnQkwsTUFBaEIsQ0FBZDs7QUFFQSxRQUFLRixZQUFMLENBQWtCakQsT0FBbEIsQ0FBMEJoRCxPQUExQixDQUFrQyxTQUFTeUcsMEJBQVQsQ0FBb0NDLFdBQXBDLEVBQWlEO0FBQ2pGNUUsV0FBTTZFLE9BQU4sQ0FBY0QsWUFBWUUsU0FBMUIsRUFBcUNGLFlBQVlHLFFBQWpEO0FBQ0QsSUFGRDs7QUFJQSxRQUFLWixZQUFMLENBQWtCQyxRQUFsQixDQUEyQmxHLE9BQTNCLENBQW1DLFNBQVM4Ryx3QkFBVCxDQUFrQ0osV0FBbEMsRUFBK0M7QUFDaEY1RSxXQUFNbEksSUFBTixDQUFXOE0sWUFBWUUsU0FBdkIsRUFBa0NGLFlBQVlHLFFBQTlDO0FBQ0QsSUFGRDs7QUFJQSxVQUFPL0UsTUFBTTdJLE1BQWIsRUFBcUI7QUFDbkJzTixlQUFVQSxRQUFRUSxJQUFSLENBQWFqRixNQUFNa0YsS0FBTixFQUFiLEVBQTRCbEYsTUFBTWtGLEtBQU4sRUFBNUIsQ0FBVjtBQUNEOztBQUVELFVBQU9ULE9BQVA7QUFDRCxFQWpDRDs7QUFtQ0E7QUFDQTlELE9BQU16QyxPQUFOLENBQWMsQ0FBQyxRQUFELEVBQVcsS0FBWCxFQUFrQixNQUFsQixDQUFkLEVBQXlDLFNBQVNpSCxtQkFBVCxDQUE2QlosTUFBN0IsRUFBcUM7QUFDNUU7QUFDQTFELFNBQU01SSxTQUFOLENBQWdCc00sTUFBaEIsSUFBMEIsVUFBU0QsR0FBVCxFQUFjRCxNQUFkLEVBQXNCO0FBQzlDLFlBQU8sS0FBS25ELE9BQUwsQ0FBYVAsTUFBTTZDLEtBQU4sQ0FBWWEsVUFBVSxFQUF0QixFQUEwQjtBQUM1Q0UsZUFBUUEsTUFEb0M7QUFFNUNELFlBQUtBO0FBRnVDLE1BQTFCLENBQWIsQ0FBUDtBQUlELElBTEQ7QUFNRCxFQVJEOztBQVVBM0QsT0FBTXpDLE9BQU4sQ0FBYyxDQUFDLE1BQUQsRUFBUyxLQUFULEVBQWdCLE9BQWhCLENBQWQsRUFBd0MsU0FBU2tILHFCQUFULENBQStCYixNQUEvQixFQUF1QztBQUM3RTtBQUNBMUQsU0FBTTVJLFNBQU4sQ0FBZ0JzTSxNQUFoQixJQUEwQixVQUFTRCxHQUFULEVBQWNlLElBQWQsRUFBb0JoQixNQUFwQixFQUE0QjtBQUNwRCxZQUFPLEtBQUtuRCxPQUFMLENBQWFQLE1BQU02QyxLQUFOLENBQVlhLFVBQVUsRUFBdEIsRUFBMEI7QUFDNUNFLGVBQVFBLE1BRG9DO0FBRTVDRCxZQUFLQSxHQUZ1QztBQUc1Q2UsYUFBTUE7QUFIc0MsTUFBMUIsQ0FBYixDQUFQO0FBS0QsSUFORDtBQU9ELEVBVEQ7O0FBV0FyUCxRQUFPNUIsT0FBUCxHQUFpQnlNLEtBQWpCLEM7Ozs7OztBQ3BGQTs7QUFFQSxLQUFJRixRQUFRLG1CQUFBOUwsQ0FBUSxFQUFSLENBQVo7QUFDQSxLQUFJeVEsc0JBQXNCLG1CQUFBelEsQ0FBUSxFQUFSLENBQTFCOztBQUVBLEtBQUkwUSxvQkFBb0IsY0FBeEI7QUFDQSxLQUFJQyx1QkFBdUI7QUFDekIsbUJBQWdCO0FBRFMsRUFBM0I7O0FBSUEsVUFBU0MscUJBQVQsQ0FBK0JDLE9BQS9CLEVBQXdDdEosS0FBeEMsRUFBK0M7QUFDN0MsT0FBSSxDQUFDdUUsTUFBTTJCLFdBQU4sQ0FBa0JvRCxPQUFsQixDQUFELElBQStCL0UsTUFBTTJCLFdBQU4sQ0FBa0JvRCxRQUFRLGNBQVIsQ0FBbEIsQ0FBbkMsRUFBK0U7QUFDN0VBLGFBQVEsY0FBUixJQUEwQnRKLEtBQTFCO0FBQ0Q7QUFDRjs7QUFFRHBHLFFBQU81QixPQUFQLEdBQWlCO0FBQ2Z1UixxQkFBa0IsQ0FBQyxTQUFTQSxnQkFBVCxDQUEwQk4sSUFBMUIsRUFBZ0NLLE9BQWhDLEVBQXlDO0FBQzFESix5QkFBb0JJLE9BQXBCLEVBQTZCLGNBQTdCO0FBQ0EsU0FBSS9FLE1BQU1tQixVQUFOLENBQWlCdUQsSUFBakIsS0FDRjFFLE1BQU1rQixhQUFOLENBQW9Cd0QsSUFBcEIsQ0FERSxJQUVGMUUsTUFBTWlDLFFBQU4sQ0FBZXlDLElBQWYsQ0FGRSxJQUdGMUUsTUFBTThCLE1BQU4sQ0FBYTRDLElBQWIsQ0FIRSxJQUlGMUUsTUFBTStCLE1BQU4sQ0FBYTJDLElBQWIsQ0FKRixFQUtFO0FBQ0EsY0FBT0EsSUFBUDtBQUNEO0FBQ0QsU0FBSTFFLE1BQU1xQixpQkFBTixDQUF3QnFELElBQXhCLENBQUosRUFBbUM7QUFDakMsY0FBT0EsS0FBS2xELE1BQVo7QUFDRDtBQUNELFNBQUl4QixNQUFNbUMsaUJBQU4sQ0FBd0J1QyxJQUF4QixDQUFKLEVBQW1DO0FBQ2pDSSw2QkFBc0JDLE9BQXRCLEVBQStCLGlEQUEvQjtBQUNBLGNBQU9MLEtBQUt0SixRQUFMLEVBQVA7QUFDRDtBQUNELFNBQUk0RSxNQUFNNEIsUUFBTixDQUFlOEMsSUFBZixDQUFKLEVBQTBCO0FBQ3hCSSw2QkFBc0JDLE9BQXRCLEVBQStCLGdDQUEvQjtBQUNBLGNBQU9FLEtBQUtDLFNBQUwsQ0FBZVIsSUFBZixDQUFQO0FBQ0Q7QUFDRCxZQUFPQSxJQUFQO0FBQ0QsSUF0QmlCLENBREg7O0FBeUJmUyxzQkFBbUIsQ0FBQyxTQUFTQSxpQkFBVCxDQUEyQlQsSUFBM0IsRUFBaUM7QUFDbkQ7QUFDQSxTQUFJLE9BQU9BLElBQVAsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDNUJBLGNBQU9BLEtBQUtuQyxPQUFMLENBQWFxQyxpQkFBYixFQUFnQyxFQUFoQyxDQUFQO0FBQ0EsV0FBSTtBQUNGRixnQkFBT08sS0FBS0csS0FBTCxDQUFXVixJQUFYLENBQVA7QUFDRCxRQUZELENBRUUsT0FBTzlPLENBQVAsRUFBVSxDQUFFLFlBQWM7QUFDN0I7QUFDRCxZQUFPOE8sSUFBUDtBQUNELElBVGtCLENBekJKOztBQW9DZkssWUFBUztBQUNQTSxhQUFRO0FBQ04saUJBQVU7QUFESixNQUREO0FBSVBDLFlBQU90RixNQUFNNkMsS0FBTixDQUFZZ0Msb0JBQVosQ0FKQTtBQUtQVSxXQUFNdkYsTUFBTTZDLEtBQU4sQ0FBWWdDLG9CQUFaLENBTEM7QUFNUFcsVUFBS3hGLE1BQU02QyxLQUFOLENBQVlnQyxvQkFBWjtBQU5FLElBcENNOztBQTZDZmxPLFlBQVMsQ0E3Q007O0FBK0NmOE8sbUJBQWdCLFlBL0NEO0FBZ0RmQyxtQkFBZ0IsY0FoREQ7O0FBa0RmQyxxQkFBa0IsQ0FBQyxDQWxESjs7QUFvRGZDLG1CQUFnQixTQUFTQSxjQUFULENBQXdCQyxNQUF4QixFQUFnQztBQUM5QyxZQUFPQSxVQUFVLEdBQVYsSUFBaUJBLFNBQVMsR0FBakM7QUFDRDtBQXREYyxFQUFqQixDOzs7Ozs7QUNoQkE7O0FBRUEsS0FBSTdGLFFBQVEsbUJBQUE5TCxDQUFRLEVBQVIsQ0FBWjs7QUFFQW1CLFFBQU81QixPQUFQLEdBQWlCLFNBQVNrUixtQkFBVCxDQUE2QkksT0FBN0IsRUFBc0NlLGNBQXRDLEVBQXNEO0FBQ3JFOUYsU0FBTXpDLE9BQU4sQ0FBY3dILE9BQWQsRUFBdUIsU0FBU2dCLGFBQVQsQ0FBdUJ0SyxLQUF2QixFQUE4QnJHLElBQTlCLEVBQW9DO0FBQ3pELFNBQUlBLFNBQVMwUSxjQUFULElBQTJCMVEsS0FBSzRRLFdBQUwsT0FBdUJGLGVBQWVFLFdBQWYsRUFBdEQsRUFBb0Y7QUFDbEZqQixlQUFRZSxjQUFSLElBQTBCckssS0FBMUI7QUFDQSxjQUFPc0osUUFBUTNQLElBQVIsQ0FBUDtBQUNEO0FBQ0YsSUFMRDtBQU1ELEVBUEQsQzs7Ozs7O0FDSkE7O0FBRUEsS0FBSTRLLFFBQVEsbUJBQUE5TCxDQUFRLEVBQVIsQ0FBWjs7QUFFQSxVQUFTa1Asa0JBQVQsR0FBOEI7QUFDNUIsUUFBSzZDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQTdDLG9CQUFtQjlMLFNBQW5CLENBQTZCNE8sR0FBN0IsR0FBbUMsU0FBU0EsR0FBVCxDQUFhL0IsU0FBYixFQUF3QkMsUUFBeEIsRUFBa0M7QUFDbkUsUUFBSzZCLFFBQUwsQ0FBYzlPLElBQWQsQ0FBbUI7QUFDakJnTixnQkFBV0EsU0FETTtBQUVqQkMsZUFBVUE7QUFGTyxJQUFuQjtBQUlBLFVBQU8sS0FBSzZCLFFBQUwsQ0FBY3pQLE1BQWQsR0FBdUIsQ0FBOUI7QUFDRCxFQU5EOztBQVFBOzs7OztBQUtBNE0sb0JBQW1COUwsU0FBbkIsQ0FBNkI2TyxLQUE3QixHQUFxQyxTQUFTQSxLQUFULENBQWVDLEVBQWYsRUFBbUI7QUFDdEQsT0FBSSxLQUFLSCxRQUFMLENBQWNHLEVBQWQsQ0FBSixFQUF1QjtBQUNyQixVQUFLSCxRQUFMLENBQWNHLEVBQWQsSUFBb0IsSUFBcEI7QUFDRDtBQUNGLEVBSkQ7O0FBTUE7Ozs7Ozs7O0FBUUFoRCxvQkFBbUI5TCxTQUFuQixDQUE2QmlHLE9BQTdCLEdBQXVDLFNBQVNBLE9BQVQsQ0FBaUJvRixFQUFqQixFQUFxQjtBQUMxRDNDLFNBQU16QyxPQUFOLENBQWMsS0FBSzBJLFFBQW5CLEVBQTZCLFNBQVNJLGNBQVQsQ0FBd0JDLENBQXhCLEVBQTJCO0FBQ3RELFNBQUlBLE1BQU0sSUFBVixFQUFnQjtBQUNkM0QsVUFBRzJELENBQUg7QUFDRDtBQUNGLElBSkQ7QUFLRCxFQU5EOztBQVFBalIsUUFBTzVCLE9BQVAsR0FBaUIyUCxrQkFBakIsQzs7Ozs7O0FDbkRBOztBQUVBLEtBQUlwRCxRQUFRLG1CQUFBOUwsQ0FBUSxFQUFSLENBQVo7QUFDQSxLQUFJcVMsZ0JBQWdCLG1CQUFBclMsQ0FBUSxFQUFSLENBQXBCOztBQUVBOzs7Ozs7O0FBT0FtQixRQUFPNUIsT0FBUCxHQUFpQixTQUFTNFAsZUFBVCxDQUF5QkssTUFBekIsRUFBaUM7QUFDaEQ7QUFDQUEsVUFBT3FCLE9BQVAsR0FBaUJyQixPQUFPcUIsT0FBUCxJQUFrQixFQUFuQzs7QUFFQTtBQUNBckIsVUFBT2dCLElBQVAsR0FBYzZCLGNBQ1o3QyxPQUFPZ0IsSUFESyxFQUVaaEIsT0FBT3FCLE9BRkssRUFHWnJCLE9BQU9zQixnQkFISyxDQUFkOztBQU1BO0FBQ0F0QixVQUFPcUIsT0FBUCxHQUFpQi9FLE1BQU02QyxLQUFOLENBQ2ZhLE9BQU9xQixPQUFQLENBQWVNLE1BQWYsSUFBeUIsRUFEVixFQUVmM0IsT0FBT3FCLE9BQVAsQ0FBZXJCLE9BQU9FLE1BQXRCLEtBQWlDLEVBRmxCLEVBR2ZGLE9BQU9xQixPQUFQLElBQWtCLEVBSEgsQ0FBakI7O0FBTUEvRSxTQUFNekMsT0FBTixDQUNFLENBQUMsUUFBRCxFQUFXLEtBQVgsRUFBa0IsTUFBbEIsRUFBMEIsTUFBMUIsRUFBa0MsS0FBbEMsRUFBeUMsT0FBekMsRUFBa0QsUUFBbEQsQ0FERixFQUVFLFNBQVNpSixpQkFBVCxDQUEyQjVDLE1BQTNCLEVBQW1DO0FBQ2pDLFlBQU9GLE9BQU9xQixPQUFQLENBQWVuQixNQUFmLENBQVA7QUFDRCxJQUpIOztBQU9BLE9BQUk2QyxPQUFKOztBQUVBLE9BQUksT0FBTy9DLE9BQU8rQyxPQUFkLEtBQTBCLFVBQTlCLEVBQTBDO0FBQ3hDO0FBQ0FBLGVBQVUvQyxPQUFPK0MsT0FBakI7QUFDRCxJQUhELE1BR08sSUFBSSxPQUFPQyxjQUFQLEtBQTBCLFdBQTlCLEVBQTJDO0FBQ2hEO0FBQ0FELGVBQVUsbUJBQUF2UyxDQUFRLEVBQVIsQ0FBVjtBQUNELElBSE0sTUFHQSxJQUFJLE9BQU9lLE9BQVAsS0FBbUIsV0FBdkIsRUFBb0M7QUFDekM7QUFDQXdSLGVBQVUsbUJBQUF2UyxDQUFRLEVBQVIsQ0FBVjtBQUNEOztBQUVELFVBQU8yTSxRQUFRa0QsT0FBUixDQUFnQkwsTUFBaEI7QUFDTDtBQURLLElBRUpZLElBRkksQ0FFQ21DLE9BRkQsRUFHSm5DLElBSEksQ0FHQyxTQUFTcUMsV0FBVCxDQUFxQmxELFFBQXJCLEVBQStCO0FBQ25DO0FBQ0FBLGNBQVNpQixJQUFULEdBQWdCNkIsY0FDZDlDLFNBQVNpQixJQURLLEVBRWRqQixTQUFTc0IsT0FGSyxFQUdkckIsT0FBT3lCLGlCQUhPLENBQWhCOztBQU1BLFlBQU8xQixRQUFQO0FBQ0QsSUFaSSxFQVlGLFNBQVNtRCxVQUFULENBQW9CN1QsS0FBcEIsRUFBMkI7QUFDNUI7QUFDQSxTQUFJQSxTQUFTQSxNQUFNMFEsUUFBbkIsRUFBNkI7QUFDM0IxUSxhQUFNMFEsUUFBTixDQUFlaUIsSUFBZixHQUFzQjZCLGNBQ3BCeFQsTUFBTTBRLFFBQU4sQ0FBZWlCLElBREssRUFFcEIzUixNQUFNMFEsUUFBTixDQUFlc0IsT0FGSyxFQUdwQnJCLE9BQU95QixpQkFIYSxDQUF0QjtBQUtEOztBQUVELFlBQU90RSxRQUFRZ0csTUFBUixDQUFlOVQsS0FBZixDQUFQO0FBQ0QsSUF2QkksQ0FBUDtBQXdCRCxFQTlERCxDOzs7Ozs7O0FDWkE7O0FBRUEsS0FBSWlOLFFBQVEsbUJBQUE5TCxDQUFRLEVBQVIsQ0FBWjs7QUFFQTs7Ozs7Ozs7QUFRQW1CLFFBQU81QixPQUFQLEdBQWlCLFNBQVM4UyxhQUFULENBQXVCN0IsSUFBdkIsRUFBNkJLLE9BQTdCLEVBQXNDK0IsR0FBdEMsRUFBMkM7QUFDMUQ7QUFDQTlHLFNBQU16QyxPQUFOLENBQWN1SixHQUFkLEVBQW1CLFNBQVM5SyxTQUFULENBQW1CMkcsRUFBbkIsRUFBdUI7QUFDeEMrQixZQUFPL0IsR0FBRytCLElBQUgsRUFBU0ssT0FBVCxDQUFQO0FBQ0QsSUFGRDs7QUFJQSxVQUFPTCxJQUFQO0FBQ0QsRUFQRCxDOzs7Ozs7QUNaQTs7QUFFQSxLQUFJMUUsUUFBUSxtQkFBQTlMLENBQVEsRUFBUixDQUFaO0FBQ0EsS0FBSTZTLFNBQVMsbUJBQUE3UyxDQUFRLEVBQVIsQ0FBYjtBQUNBLEtBQUk4UyxXQUFXLG1CQUFBOVMsQ0FBUSxFQUFSLENBQWY7QUFDQSxLQUFJK1MsZUFBZSxtQkFBQS9TLENBQVEsRUFBUixDQUFuQjtBQUNBLEtBQUlnVCxrQkFBa0IsbUJBQUFoVCxDQUFRLEVBQVIsQ0FBdEI7QUFDQSxLQUFJaVQsY0FBYyxtQkFBQWpULENBQVEsRUFBUixDQUFsQjtBQUNBLEtBQUlrVCxPQUFRLE9BQU9oTCxNQUFQLEtBQWtCLFdBQWxCLElBQWlDQSxPQUFPZ0wsSUFBekMsSUFBa0QsbUJBQUFsVCxDQUFRLEVBQVIsQ0FBN0Q7O0FBRUFtQixRQUFPNUIsT0FBUCxHQUFpQixTQUFTNFQsVUFBVCxDQUFvQjNELE1BQXBCLEVBQTRCO0FBQzNDLFVBQU8sSUFBSTdDLE9BQUosQ0FBWSxTQUFTeUcsa0JBQVQsQ0FBNEJ2RCxPQUE1QixFQUFxQzhDLE1BQXJDLEVBQTZDO0FBQzlELFNBQUlVLGNBQWM3RCxPQUFPZ0IsSUFBekI7QUFDQSxTQUFJOEMsaUJBQWlCOUQsT0FBT3FCLE9BQTVCOztBQUVBLFNBQUkvRSxNQUFNbUIsVUFBTixDQUFpQm9HLFdBQWpCLENBQUosRUFBbUM7QUFDakMsY0FBT0MsZUFBZSxjQUFmLENBQVAsQ0FEaUMsQ0FDTTtBQUN4Qzs7QUFFRCxTQUFJakgsVUFBVSxJQUFJbUcsY0FBSixFQUFkO0FBQ0EsU0FBSWUsWUFBWSxvQkFBaEI7QUFDQSxTQUFJQyxVQUFVLEtBQWQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBSXpTLFFBQVFDLEdBQVIsQ0FBWUMsUUFBWixLQUF5QixNQUF6QixJQUNBLE9BQU9pSCxNQUFQLEtBQWtCLFdBRGxCLElBRUFBLE9BQU91TCxjQUZQLElBRXlCLEVBQUUscUJBQXFCcEgsT0FBdkIsQ0FGekIsSUFHQSxDQUFDMkcsZ0JBQWdCeEQsT0FBT0MsR0FBdkIsQ0FITCxFQUdrQztBQUNoQ3BELGlCQUFVLElBQUluRSxPQUFPdUwsY0FBWCxFQUFWO0FBQ0FGLG1CQUFZLFFBQVo7QUFDQUMsaUJBQVUsSUFBVjtBQUNBbkgsZUFBUXFILFVBQVIsR0FBcUIsU0FBU0MsY0FBVCxHQUEwQixDQUFFLENBQWpEO0FBQ0F0SCxlQUFRdUgsU0FBUixHQUFvQixTQUFTQyxhQUFULEdBQXlCLENBQUUsQ0FBL0M7QUFDRDs7QUFFRDtBQUNBLFNBQUlyRSxPQUFPc0UsSUFBWCxFQUFpQjtBQUNmLFdBQUlDLFdBQVd2RSxPQUFPc0UsSUFBUCxDQUFZQyxRQUFaLElBQXdCLEVBQXZDO0FBQ0EsV0FBSUMsV0FBV3hFLE9BQU9zRSxJQUFQLENBQVlFLFFBQVosSUFBd0IsRUFBdkM7QUFDQVYsc0JBQWVXLGFBQWYsR0FBK0IsV0FBV2YsS0FBS2EsV0FBVyxHQUFYLEdBQWlCQyxRQUF0QixDQUExQztBQUNEOztBQUVEM0gsYUFBUTZILElBQVIsQ0FBYTFFLE9BQU9FLE1BQVAsQ0FBY29DLFdBQWQsRUFBYixFQUEwQ2dCLFNBQVN0RCxPQUFPQyxHQUFoQixFQUFxQkQsT0FBTzJFLE1BQTVCLEVBQW9DM0UsT0FBTzRFLGdCQUEzQyxDQUExQyxFQUF3RyxJQUF4Rzs7QUFFQTtBQUNBL0gsYUFBUTVKLE9BQVIsR0FBa0IrTSxPQUFPL00sT0FBekI7O0FBRUE7QUFDQTRKLGFBQVFrSCxTQUFSLElBQXFCLFNBQVNjLFVBQVQsR0FBc0I7QUFDekMsV0FBSSxDQUFDaEksT0FBRCxJQUFhQSxRQUFRaUksVUFBUixLQUF1QixDQUF2QixJQUE0QixDQUFDZCxPQUE5QyxFQUF3RDtBQUN0RDtBQUNEOztBQUVEO0FBQ0E7QUFDQSxXQUFJbkgsUUFBUXNGLE1BQVIsS0FBbUIsQ0FBdkIsRUFBMEI7QUFDeEI7QUFDRDs7QUFFRDtBQUNBLFdBQUk0QyxrQkFBa0IsMkJBQTJCbEksT0FBM0IsR0FBcUMwRyxhQUFhMUcsUUFBUW1JLHFCQUFSLEVBQWIsQ0FBckMsR0FBcUYsSUFBM0c7QUFDQSxXQUFJQyxlQUFlLENBQUNqRixPQUFPa0YsWUFBUixJQUF3QmxGLE9BQU9rRixZQUFQLEtBQXdCLE1BQWhELEdBQXlEckksUUFBUXNJLFlBQWpFLEdBQWdGdEksUUFBUWtELFFBQTNHO0FBQ0EsV0FBSUEsV0FBVztBQUNiaUIsZUFBTWlFLFlBRE87QUFFYjtBQUNBOUMsaUJBQVF0RixRQUFRc0YsTUFBUixLQUFtQixJQUFuQixHQUEwQixHQUExQixHQUFnQ3RGLFFBQVFzRixNQUhuQztBQUliaUQscUJBQVl2SSxRQUFRc0YsTUFBUixLQUFtQixJQUFuQixHQUEwQixZQUExQixHQUF5Q3RGLFFBQVF1SSxVQUpoRDtBQUtiL0Qsa0JBQVMwRCxlQUxJO0FBTWIvRSxpQkFBUUEsTUFOSztBQU9ibkQsa0JBQVNBO0FBUEksUUFBZjs7QUFVQXdHLGNBQU9oRCxPQUFQLEVBQWdCOEMsTUFBaEIsRUFBd0JwRCxRQUF4Qjs7QUFFQTtBQUNBbEQsaUJBQVUsSUFBVjtBQUNELE1BNUJEOztBQThCQTtBQUNBQSxhQUFRd0ksT0FBUixHQUFrQixTQUFTQyxXQUFULEdBQXVCO0FBQ3ZDO0FBQ0E7QUFDQW5DLGNBQU9NLFlBQVksZUFBWixFQUE2QnpELE1BQTdCLENBQVA7O0FBRUE7QUFDQW5ELGlCQUFVLElBQVY7QUFDRCxNQVBEOztBQVNBO0FBQ0FBLGFBQVF1SCxTQUFSLEdBQW9CLFNBQVNDLGFBQVQsR0FBeUI7QUFDM0NsQixjQUFPTSxZQUFZLGdCQUFnQnpELE9BQU8vTSxPQUF2QixHQUFpQyxhQUE3QyxFQUE0RCtNLE1BQTVELEVBQW9FLGNBQXBFLENBQVA7O0FBRUE7QUFDQW5ELGlCQUFVLElBQVY7QUFDRCxNQUxEOztBQU9BO0FBQ0E7QUFDQTtBQUNBLFNBQUlQLE1BQU13QyxvQkFBTixFQUFKLEVBQWtDO0FBQ2hDLFdBQUl5RyxVQUFVLG1CQUFBL1UsQ0FBUSxFQUFSLENBQWQ7O0FBRUE7QUFDQSxXQUFJZ1YsWUFBWSxDQUFDeEYsT0FBT3lGLGVBQVAsSUFBMEJqQyxnQkFBZ0J4RCxPQUFPQyxHQUF2QixDQUEzQixLQUEyREQsT0FBTytCLGNBQWxFLEdBQ1p3RCxRQUFRRyxJQUFSLENBQWExRixPQUFPK0IsY0FBcEIsQ0FEWSxHQUVaelIsU0FGSjs7QUFJQSxXQUFJa1YsU0FBSixFQUFlO0FBQ2IxQix3QkFBZTlELE9BQU9nQyxjQUF0QixJQUF3Q3dELFNBQXhDO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBLFNBQUksc0JBQXNCM0ksT0FBMUIsRUFBbUM7QUFDakNQLGFBQU16QyxPQUFOLENBQWNpSyxjQUFkLEVBQThCLFNBQVM2QixnQkFBVCxDQUEwQnBJLEdBQTFCLEVBQStCeEUsR0FBL0IsRUFBb0M7QUFDaEUsYUFBSSxPQUFPOEssV0FBUCxLQUF1QixXQUF2QixJQUFzQzlLLElBQUk2TSxXQUFKLE9BQXNCLGNBQWhFLEVBQWdGO0FBQzlFO0FBQ0Esa0JBQU85QixlQUFlL0ssR0FBZixDQUFQO0FBQ0QsVUFIRCxNQUdPO0FBQ0w7QUFDQThELG1CQUFROEksZ0JBQVIsQ0FBeUI1TSxHQUF6QixFQUE4QndFLEdBQTlCO0FBQ0Q7QUFDRixRQVJEO0FBU0Q7O0FBRUQ7QUFDQSxTQUFJeUMsT0FBT3lGLGVBQVgsRUFBNEI7QUFDMUI1SSxlQUFRNEksZUFBUixHQUEwQixJQUExQjtBQUNEOztBQUVEO0FBQ0EsU0FBSXpGLE9BQU9rRixZQUFYLEVBQXlCO0FBQ3ZCLFdBQUk7QUFDRnJJLGlCQUFRcUksWUFBUixHQUF1QmxGLE9BQU9rRixZQUE5QjtBQUNELFFBRkQsQ0FFRSxPQUFPaFQsQ0FBUCxFQUFVO0FBQ1YsYUFBSTJLLFFBQVFxSSxZQUFSLEtBQXlCLE1BQTdCLEVBQXFDO0FBQ25DLGlCQUFNaFQsQ0FBTjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRDtBQUNBLFNBQUksT0FBTzhOLE9BQU82RixrQkFBZCxLQUFxQyxVQUF6QyxFQUFxRDtBQUNuRGhKLGVBQVFpSixnQkFBUixDQUF5QixVQUF6QixFQUFxQzlGLE9BQU82RixrQkFBNUM7QUFDRDs7QUFFRDtBQUNBLFNBQUksT0FBTzdGLE9BQU8rRixnQkFBZCxLQUFtQyxVQUFuQyxJQUFpRGxKLFFBQVFtSixNQUE3RCxFQUFxRTtBQUNuRW5KLGVBQVFtSixNQUFSLENBQWVGLGdCQUFmLENBQWdDLFVBQWhDLEVBQTRDOUYsT0FBTytGLGdCQUFuRDtBQUNEOztBQUdELFNBQUlsQyxnQkFBZ0J2VCxTQUFwQixFQUErQjtBQUM3QnVULHFCQUFjLElBQWQ7QUFDRDs7QUFFRDtBQUNBaEgsYUFBUW9KLElBQVIsQ0FBYXBDLFdBQWI7QUFDRCxJQXJKTSxDQUFQO0FBc0pELEVBdkpELEM7Ozs7Ozs7QUNWQTs7QUFFQSxLQUFJSixjQUFjLG1CQUFBalQsQ0FBUSxFQUFSLENBQWxCOztBQUVBOzs7Ozs7O0FBT0FtQixRQUFPNUIsT0FBUCxHQUFpQixTQUFTc1QsTUFBVCxDQUFnQmhELE9BQWhCLEVBQXlCOEMsTUFBekIsRUFBaUNwRCxRQUFqQyxFQUEyQztBQUMxRCxPQUFJbUMsaUJBQWlCbkMsU0FBU0MsTUFBVCxDQUFnQmtDLGNBQXJDO0FBQ0E7QUFDQSxPQUFJLENBQUNuQyxTQUFTb0MsTUFBVixJQUFvQixDQUFDRCxjQUFyQixJQUF1Q0EsZUFBZW5DLFNBQVNvQyxNQUF4QixDQUEzQyxFQUE0RTtBQUMxRTlCLGFBQVFOLFFBQVI7QUFDRCxJQUZELE1BRU87QUFDTG9ELFlBQU9NLFlBQ0wscUNBQXFDMUQsU0FBU29DLE1BRHpDLEVBRUxwQyxTQUFTQyxNQUZKLEVBR0wsSUFISyxFQUlMRCxRQUpLLENBQVA7QUFNRDtBQUNGLEVBYkQsQzs7Ozs7O0FDWEE7O0FBRUEsS0FBSW1HLGVBQWUsbUJBQUExVixDQUFRLEVBQVIsQ0FBbkI7O0FBRUE7Ozs7Ozs7OztBQVNBbUIsUUFBTzVCLE9BQVAsR0FBaUIsU0FBUzBULFdBQVQsQ0FBcUI1SSxPQUFyQixFQUE4Qm1GLE1BQTlCLEVBQXNDbUcsSUFBdEMsRUFBNENwRyxRQUE1QyxFQUFzRDtBQUNyRSxPQUFJMVEsUUFBUSxJQUFJMEMsS0FBSixDQUFVOEksT0FBVixDQUFaO0FBQ0EsVUFBT3FMLGFBQWE3VyxLQUFiLEVBQW9CMlEsTUFBcEIsRUFBNEJtRyxJQUE1QixFQUFrQ3BHLFFBQWxDLENBQVA7QUFDRCxFQUhELEM7Ozs7OztBQ2JBOztBQUVBOzs7Ozs7Ozs7O0FBU0FwTyxRQUFPNUIsT0FBUCxHQUFpQixTQUFTbVcsWUFBVCxDQUFzQjdXLEtBQXRCLEVBQTZCMlEsTUFBN0IsRUFBcUNtRyxJQUFyQyxFQUEyQ3BHLFFBQTNDLEVBQXFEO0FBQ3BFMVEsU0FBTTJRLE1BQU4sR0FBZUEsTUFBZjtBQUNBLE9BQUltRyxJQUFKLEVBQVU7QUFDUjlXLFdBQU04VyxJQUFOLEdBQWFBLElBQWI7QUFDRDtBQUNEOVcsU0FBTTBRLFFBQU4sR0FBaUJBLFFBQWpCO0FBQ0EsVUFBTzFRLEtBQVA7QUFDRCxFQVBELEM7Ozs7OztBQ1hBOztBQUVBLEtBQUlpTixRQUFRLG1CQUFBOUwsQ0FBUSxFQUFSLENBQVo7O0FBRUEsVUFBUzRWLE1BQVQsQ0FBZ0I3SSxHQUFoQixFQUFxQjtBQUNuQixVQUFPOEksbUJBQW1COUksR0FBbkIsRUFDTHNCLE9BREssQ0FDRyxPQURILEVBQ1ksR0FEWixFQUVMQSxPQUZLLENBRUcsT0FGSCxFQUVZLEdBRlosRUFHTEEsT0FISyxDQUdHLE1BSEgsRUFHVyxHQUhYLEVBSUxBLE9BSkssQ0FJRyxPQUpILEVBSVksR0FKWixFQUtMQSxPQUxLLENBS0csTUFMSCxFQUtXLEdBTFgsRUFNTEEsT0FOSyxDQU1HLE9BTkgsRUFNWSxHQU5aLEVBT0xBLE9BUEssQ0FPRyxPQVBILEVBT1ksR0FQWixDQUFQO0FBUUQ7O0FBRUQ7Ozs7Ozs7QUFPQWxOLFFBQU81QixPQUFQLEdBQWlCLFNBQVN1VCxRQUFULENBQWtCckQsR0FBbEIsRUFBdUIwRSxNQUF2QixFQUErQkMsZ0JBQS9CLEVBQWlEO0FBQ2hFO0FBQ0EsT0FBSSxDQUFDRCxNQUFMLEVBQWE7QUFDWCxZQUFPMUUsR0FBUDtBQUNEOztBQUVELE9BQUlxRyxnQkFBSjtBQUNBLE9BQUkxQixnQkFBSixFQUFzQjtBQUNwQjBCLHdCQUFtQjFCLGlCQUFpQkQsTUFBakIsQ0FBbkI7QUFDRCxJQUZELE1BRU8sSUFBSXJJLE1BQU1tQyxpQkFBTixDQUF3QmtHLE1BQXhCLENBQUosRUFBcUM7QUFDMUMyQix3QkFBbUIzQixPQUFPak4sUUFBUCxFQUFuQjtBQUNELElBRk0sTUFFQTtBQUNMLFNBQUk2TyxRQUFRLEVBQVo7O0FBRUFqSyxXQUFNekMsT0FBTixDQUFjOEssTUFBZCxFQUFzQixTQUFTNkIsU0FBVCxDQUFtQmpKLEdBQW5CLEVBQXdCeEUsR0FBeEIsRUFBNkI7QUFDakQsV0FBSXdFLFFBQVEsSUFBUixJQUFnQixPQUFPQSxHQUFQLEtBQWUsV0FBbkMsRUFBZ0Q7QUFDOUM7QUFDRDs7QUFFRCxXQUFJakIsTUFBTWdCLE9BQU4sQ0FBY0MsR0FBZCxDQUFKLEVBQXdCO0FBQ3RCeEUsZUFBTUEsTUFBTSxJQUFaO0FBQ0Q7O0FBRUQsV0FBSSxDQUFDdUQsTUFBTWdCLE9BQU4sQ0FBY0MsR0FBZCxDQUFMLEVBQXlCO0FBQ3ZCQSxlQUFNLENBQUNBLEdBQUQsQ0FBTjtBQUNEOztBQUVEakIsYUFBTXpDLE9BQU4sQ0FBYzBELEdBQWQsRUFBbUIsU0FBU2tKLFVBQVQsQ0FBb0J2SyxDQUFwQixFQUF1QjtBQUN4QyxhQUFJSSxNQUFNNkIsTUFBTixDQUFhakMsQ0FBYixDQUFKLEVBQXFCO0FBQ25CQSxlQUFJQSxFQUFFd0ssV0FBRixFQUFKO0FBQ0QsVUFGRCxNQUVPLElBQUlwSyxNQUFNNEIsUUFBTixDQUFlaEMsQ0FBZixDQUFKLEVBQXVCO0FBQzVCQSxlQUFJcUYsS0FBS0MsU0FBTCxDQUFldEYsQ0FBZixDQUFKO0FBQ0Q7QUFDRHFLLGVBQU05UyxJQUFOLENBQVcyUyxPQUFPck4sR0FBUCxJQUFjLEdBQWQsR0FBb0JxTixPQUFPbEssQ0FBUCxDQUEvQjtBQUNELFFBUEQ7QUFRRCxNQXJCRDs7QUF1QkFvSyx3QkFBbUJDLE1BQU05TSxJQUFOLENBQVcsR0FBWCxDQUFuQjtBQUNEOztBQUVELE9BQUk2TSxnQkFBSixFQUFzQjtBQUNwQnJHLFlBQU8sQ0FBQ0EsSUFBSTVKLE9BQUosQ0FBWSxHQUFaLE1BQXFCLENBQUMsQ0FBdEIsR0FBMEIsR0FBMUIsR0FBZ0MsR0FBakMsSUFBd0NpUSxnQkFBL0M7QUFDRDs7QUFFRCxVQUFPckcsR0FBUDtBQUNELEVBN0NELEM7Ozs7OztBQ3RCQTs7QUFFQSxLQUFJM0QsUUFBUSxtQkFBQTlMLENBQVEsRUFBUixDQUFaOztBQUVBOzs7Ozs7Ozs7Ozs7O0FBYUFtQixRQUFPNUIsT0FBUCxHQUFpQixTQUFTd1QsWUFBVCxDQUFzQmxDLE9BQXRCLEVBQStCO0FBQzlDLE9BQUlzRixTQUFTLEVBQWI7QUFDQSxPQUFJNU4sR0FBSjtBQUNBLE9BQUl3RSxHQUFKO0FBQ0EsT0FBSS9KLENBQUo7O0FBRUEsT0FBSSxDQUFDNk4sT0FBTCxFQUFjO0FBQUUsWUFBT3NGLE1BQVA7QUFBZ0I7O0FBRWhDckssU0FBTXpDLE9BQU4sQ0FBY3dILFFBQVFwSCxLQUFSLENBQWMsSUFBZCxDQUFkLEVBQW1DLFNBQVMyTSxNQUFULENBQWdCQyxJQUFoQixFQUFzQjtBQUN2RHJULFNBQUlxVCxLQUFLeFEsT0FBTCxDQUFhLEdBQWIsQ0FBSjtBQUNBMEMsV0FBTXVELE1BQU1xQyxJQUFOLENBQVdrSSxLQUFLQyxNQUFMLENBQVksQ0FBWixFQUFldFQsQ0FBZixDQUFYLEVBQThCb1MsV0FBOUIsRUFBTjtBQUNBckksV0FBTWpCLE1BQU1xQyxJQUFOLENBQVdrSSxLQUFLQyxNQUFMLENBQVl0VCxJQUFJLENBQWhCLENBQVgsQ0FBTjs7QUFFQSxTQUFJdUYsR0FBSixFQUFTO0FBQ1A0TixjQUFPNU4sR0FBUCxJQUFjNE4sT0FBTzVOLEdBQVAsSUFBYzROLE9BQU81TixHQUFQLElBQWMsSUFBZCxHQUFxQndFLEdBQW5DLEdBQXlDQSxHQUF2RDtBQUNEO0FBQ0YsSUFSRDs7QUFVQSxVQUFPb0osTUFBUDtBQUNELEVBbkJELEM7Ozs7OztBQ2pCQTs7QUFFQSxLQUFJckssUUFBUSxtQkFBQTlMLENBQVEsRUFBUixDQUFaOztBQUVBbUIsUUFBTzVCLE9BQVAsR0FDRXVNLE1BQU13QyxvQkFBTjs7QUFFQTtBQUNBO0FBQ0MsVUFBU2lJLGtCQUFULEdBQThCO0FBQzdCLE9BQUlDLE9BQU8sa0JBQWtCQyxJQUFsQixDQUF1QkMsVUFBVUMsU0FBakMsQ0FBWDtBQUNBLE9BQUlDLGlCQUFpQnJJLFNBQVNDLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBckI7QUFDQSxPQUFJcUksU0FBSjs7QUFFQTs7Ozs7O0FBTUEsWUFBU0MsVUFBVCxDQUFvQnJILEdBQXBCLEVBQXlCO0FBQ3ZCLFNBQUlzSCxPQUFPdEgsR0FBWDs7QUFFQSxTQUFJK0csSUFBSixFQUFVO0FBQ1I7QUFDQUksc0JBQWVJLFlBQWYsQ0FBNEIsTUFBNUIsRUFBb0NELElBQXBDO0FBQ0FBLGNBQU9ILGVBQWVHLElBQXRCO0FBQ0Q7O0FBRURILG9CQUFlSSxZQUFmLENBQTRCLE1BQTVCLEVBQW9DRCxJQUFwQzs7QUFFQTtBQUNBLFlBQU87QUFDTEEsYUFBTUgsZUFBZUcsSUFEaEI7QUFFTEUsaUJBQVVMLGVBQWVLLFFBQWYsR0FBMEJMLGVBQWVLLFFBQWYsQ0FBd0I1SSxPQUF4QixDQUFnQyxJQUFoQyxFQUFzQyxFQUF0QyxDQUExQixHQUFzRSxFQUYzRTtBQUdMNkksYUFBTU4sZUFBZU0sSUFIaEI7QUFJTEMsZUFBUVAsZUFBZU8sTUFBZixHQUF3QlAsZUFBZU8sTUFBZixDQUFzQjlJLE9BQXRCLENBQThCLEtBQTlCLEVBQXFDLEVBQXJDLENBQXhCLEdBQW1FLEVBSnRFO0FBS0wrSSxhQUFNUixlQUFlUSxJQUFmLEdBQXNCUixlQUFlUSxJQUFmLENBQW9CL0ksT0FBcEIsQ0FBNEIsSUFBNUIsRUFBa0MsRUFBbEMsQ0FBdEIsR0FBOEQsRUFML0Q7QUFNTGdKLGlCQUFVVCxlQUFlUyxRQU5wQjtBQU9MQyxhQUFNVixlQUFlVSxJQVBoQjtBQVFMQyxpQkFBV1gsZUFBZVcsUUFBZixDQUF3QkMsTUFBeEIsQ0FBK0IsQ0FBL0IsTUFBc0MsR0FBdkMsR0FDQVosZUFBZVcsUUFEZixHQUVBLE1BQU1YLGVBQWVXO0FBVjFCLE1BQVA7QUFZRDs7QUFFRFYsZUFBWUMsV0FBVzVPLE9BQU91UCxRQUFQLENBQWdCVixJQUEzQixDQUFaOztBQUVBOzs7Ozs7QUFNQSxVQUFPLFNBQVMvRCxlQUFULENBQXlCMEUsVUFBekIsRUFBcUM7QUFDMUMsU0FBSXZCLFNBQVVySyxNQUFNeUIsUUFBTixDQUFlbUssVUFBZixDQUFELEdBQStCWixXQUFXWSxVQUFYLENBQS9CLEdBQXdEQSxVQUFyRTtBQUNBLFlBQVF2QixPQUFPYyxRQUFQLEtBQW9CSixVQUFVSSxRQUE5QixJQUNGZCxPQUFPZSxJQUFQLEtBQWdCTCxVQUFVSyxJQURoQztBQUVELElBSkQ7QUFLRCxFQWxERCxFQUpBOztBQXdEQTtBQUNDLFVBQVNTLHFCQUFULEdBQWlDO0FBQ2hDLFVBQU8sU0FBUzNFLGVBQVQsR0FBMkI7QUFDaEMsWUFBTyxJQUFQO0FBQ0QsSUFGRDtBQUdELEVBSkQsRUExREYsQzs7Ozs7O0FDSkE7O0FBRUE7O0FBRUEsS0FBSTRFLFFBQVEsbUVBQVo7O0FBRUEsVUFBU0MsQ0FBVCxHQUFhO0FBQ1gsUUFBS3hOLE9BQUwsR0FBZSxzQ0FBZjtBQUNEO0FBQ0R3TixHQUFFelUsU0FBRixHQUFjLElBQUk3QixLQUFKLEVBQWQ7QUFDQXNXLEdBQUV6VSxTQUFGLENBQVl1UyxJQUFaLEdBQW1CLENBQW5CO0FBQ0FrQyxHQUFFelUsU0FBRixDQUFZbEMsSUFBWixHQUFtQix1QkFBbkI7O0FBRUEsVUFBU2dTLElBQVQsQ0FBYzRFLEtBQWQsRUFBcUI7QUFDbkIsT0FBSTFKLE1BQU0ySixPQUFPRCxLQUFQLENBQVY7QUFDQSxPQUFJRSxTQUFTLEVBQWI7QUFDQTtBQUNFO0FBQ0EsT0FBSUMsS0FBSixFQUFXQyxRQUFYLEVBQXFCQyxNQUFNLENBQTNCLEVBQThCOU0sTUFBTXVNLEtBRnRDO0FBR0U7QUFDQTtBQUNBO0FBQ0F4SixPQUFJb0osTUFBSixDQUFXVyxNQUFNLENBQWpCLE1BQXdCOU0sTUFBTSxHQUFOLEVBQVc4TSxNQUFNLENBQXpDLENBTkY7QUFPRTtBQUNBSCxhQUFVM00sSUFBSW1NLE1BQUosQ0FBVyxLQUFLUyxTQUFTLElBQUlFLE1BQU0sQ0FBTixHQUFVLENBQXZDLENBUlosRUFTRTtBQUNBRCxnQkFBVzlKLElBQUlnSyxVQUFKLENBQWVELE9BQU8sSUFBSSxDQUExQixDQUFYO0FBQ0EsU0FBSUQsV0FBVyxJQUFmLEVBQXFCO0FBQ25CLGFBQU0sSUFBSUwsQ0FBSixFQUFOO0FBQ0Q7QUFDREksYUFBUUEsU0FBUyxDQUFULEdBQWFDLFFBQXJCO0FBQ0Q7QUFDRCxVQUFPRixNQUFQO0FBQ0Q7O0FBRUQ3VyxRQUFPNUIsT0FBUCxHQUFpQjJULElBQWpCLEM7Ozs7OztBQ25DQTs7QUFFQSxLQUFJcEgsUUFBUSxtQkFBQTlMLENBQVEsRUFBUixDQUFaOztBQUVBbUIsUUFBTzVCLE9BQVAsR0FDRXVNLE1BQU13QyxvQkFBTjs7QUFFQTtBQUNDLFVBQVNpSSxrQkFBVCxHQUE4QjtBQUM3QixVQUFPO0FBQ0w4QixZQUFPLFNBQVNBLEtBQVQsQ0FBZW5YLElBQWYsRUFBcUJxRyxLQUFyQixFQUE0QitRLE9BQTVCLEVBQXFDQyxJQUFyQyxFQUEyQ0MsTUFBM0MsRUFBbURDLE1BQW5ELEVBQTJEO0FBQ2hFLFdBQUlDLFNBQVMsRUFBYjtBQUNBQSxjQUFPelYsSUFBUCxDQUFZL0IsT0FBTyxHQUFQLEdBQWEyVSxtQkFBbUJ0TyxLQUFuQixDQUF6Qjs7QUFFQSxXQUFJdUUsTUFBTTBCLFFBQU4sQ0FBZThLLE9BQWYsQ0FBSixFQUE2QjtBQUMzQkksZ0JBQU96VixJQUFQLENBQVksYUFBYSxJQUFJMFYsSUFBSixDQUFTTCxPQUFULEVBQWtCTSxXQUFsQixFQUF6QjtBQUNEOztBQUVELFdBQUk5TSxNQUFNeUIsUUFBTixDQUFlZ0wsSUFBZixDQUFKLEVBQTBCO0FBQ3hCRyxnQkFBT3pWLElBQVAsQ0FBWSxVQUFVc1YsSUFBdEI7QUFDRDs7QUFFRCxXQUFJek0sTUFBTXlCLFFBQU4sQ0FBZWlMLE1BQWYsQ0FBSixFQUE0QjtBQUMxQkUsZ0JBQU96VixJQUFQLENBQVksWUFBWXVWLE1BQXhCO0FBQ0Q7O0FBRUQsV0FBSUMsV0FBVyxJQUFmLEVBQXFCO0FBQ25CQyxnQkFBT3pWLElBQVAsQ0FBWSxRQUFaO0FBQ0Q7O0FBRURzTCxnQkFBU21LLE1BQVQsR0FBa0JBLE9BQU96UCxJQUFQLENBQVksSUFBWixDQUFsQjtBQUNELE1BdEJJOztBQXdCTGlNLFdBQU0sU0FBU0EsSUFBVCxDQUFjaFUsSUFBZCxFQUFvQjtBQUN4QixXQUFJOEgsUUFBUXVGLFNBQVNtSyxNQUFULENBQWdCMVAsS0FBaEIsQ0FBc0IsSUFBSTZQLE1BQUosQ0FBVyxlQUFlM1gsSUFBZixHQUFzQixXQUFqQyxDQUF0QixDQUFaO0FBQ0EsY0FBUThILFFBQVE4UCxtQkFBbUI5UCxNQUFNLENBQU4sQ0FBbkIsQ0FBUixHQUF1QyxJQUEvQztBQUNELE1BM0JJOztBQTZCTCtQLGFBQVEsU0FBU0EsTUFBVCxDQUFnQjdYLElBQWhCLEVBQXNCO0FBQzVCLFlBQUttWCxLQUFMLENBQVduWCxJQUFYLEVBQWlCLEVBQWpCLEVBQXFCeVgsS0FBS0ssR0FBTCxLQUFhLFFBQWxDO0FBQ0Q7QUEvQkksSUFBUDtBQWlDRCxFQWxDRCxFQUhBOztBQXVDQTtBQUNDLFVBQVNyQixxQkFBVCxHQUFpQztBQUNoQyxVQUFPO0FBQ0xVLFlBQU8sU0FBU0EsS0FBVCxHQUFpQixDQUFFLENBRHJCO0FBRUxuRCxXQUFNLFNBQVNBLElBQVQsR0FBZ0I7QUFBRSxjQUFPLElBQVA7QUFBYyxNQUZqQztBQUdMNkQsYUFBUSxTQUFTQSxNQUFULEdBQWtCLENBQUU7QUFIdkIsSUFBUDtBQUtELEVBTkQsRUF6Q0YsQzs7Ozs7O0FDSkE7O0FBRUE7Ozs7Ozs7QUFNQTVYLFFBQU81QixPQUFQLEdBQWlCLFNBQVM2UCxhQUFULENBQXVCSyxHQUF2QixFQUE0QjtBQUMzQztBQUNBO0FBQ0E7QUFDQSxVQUFPLGlDQUFnQ2dILElBQWhDLENBQXFDaEgsR0FBckM7QUFBUDtBQUNELEVBTEQsQzs7Ozs7O0FDUkE7O0FBRUE7Ozs7Ozs7O0FBT0F0TyxRQUFPNUIsT0FBUCxHQUFpQixTQUFTOFAsV0FBVCxDQUFxQk0sT0FBckIsRUFBOEJzSixXQUE5QixFQUEyQztBQUMxRCxVQUFPdEosUUFBUXRCLE9BQVIsQ0FBZ0IsTUFBaEIsRUFBd0IsRUFBeEIsSUFBOEIsR0FBOUIsR0FBb0M0SyxZQUFZNUssT0FBWixDQUFvQixNQUFwQixFQUE0QixFQUE1QixDQUEzQztBQUNELEVBRkQsQzs7Ozs7O0FDVEE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQWxOLFFBQU81QixPQUFQLEdBQWlCLFNBQVNxTixNQUFULENBQWdCc00sUUFBaEIsRUFBMEI7QUFDekMsVUFBTyxTQUFTbEssSUFBVCxDQUFjbUssR0FBZCxFQUFtQjtBQUN4QixZQUFPRCxTQUFTN1YsS0FBVCxDQUFlLElBQWYsRUFBcUI4VixHQUFyQixDQUFQO0FBQ0QsSUFGRDtBQUdELEVBSkQsQzs7Ozs7O0FDdEJBOzs7O0FBRUEsVUFBU0Msa0JBQVQsQ0FBNEJELEdBQTVCLEVBQWlDO0FBQUUsT0FBSXJXLE1BQU1nSyxPQUFOLENBQWNxTSxHQUFkLENBQUosRUFBd0I7QUFBRSxVQUFLLElBQUluVyxJQUFJLENBQVIsRUFBV3FXLE9BQU92VyxNQUFNcVcsSUFBSTdXLE1BQVYsQ0FBdkIsRUFBMENVLElBQUltVyxJQUFJN1csTUFBbEQsRUFBMERVLEdBQTFELEVBQStEO0FBQUVxVyxZQUFLclcsQ0FBTCxJQUFVbVcsSUFBSW5XLENBQUosQ0FBVjtBQUFtQixNQUFDLE9BQU9xVyxJQUFQO0FBQWMsSUFBN0gsTUFBbUk7QUFBRSxZQUFPdlcsTUFBTXdXLElBQU4sQ0FBV0gsR0FBWCxDQUFQO0FBQXlCO0FBQUU7O0FBRW5NLFVBQVNJLE9BQVQsQ0FBaUIxWSxHQUFqQixFQUFzQjtBQUFFLFVBQU9BLE9BQU8sT0FBT3dILE1BQVAsS0FBa0IsV0FBekIsSUFBd0N4SCxJQUFJNkcsV0FBSixLQUFvQlcsTUFBNUQsR0FBcUUsUUFBckUsVUFBdUZ4SCxHQUF2RiwwQ0FBdUZBLEdBQXZGLENBQVA7QUFBb0c7O0FBRTVILEtBQUkyWSxTQUFTLFNBQVNBLE1BQVQsQ0FBZ0JwTCxHQUFoQixFQUFxQnFMLEtBQXJCLEVBQTRCO0FBQ3ZDLFVBQU8sSUFBSTNXLEtBQUosQ0FBVTJXLFFBQVEsQ0FBbEIsRUFBcUJ4USxJQUFyQixDQUEwQm1GLEdBQTFCLENBQVA7QUFDRCxFQUZEO0FBR0EsS0FBSXNMLE1BQU0sU0FBU0EsR0FBVCxDQUFhQyxHQUFiLEVBQWtCQyxTQUFsQixFQUE2QjtBQUNyQyxVQUFPSixPQUFPLEdBQVAsRUFBWUksWUFBWUQsSUFBSXpTLFFBQUosR0FBZTVFLE1BQXZDLElBQWlEcVgsR0FBeEQ7QUFDRCxFQUZEO0FBR0EsS0FBSUUsYUFBYSxTQUFTQSxVQUFULENBQW9CQyxJQUFwQixFQUEwQjtBQUN6QyxVQUFPLE9BQU9KLElBQUlJLEtBQUtDLFFBQUwsRUFBSixFQUFxQixDQUFyQixDQUFQLEdBQWlDLEdBQWpDLEdBQXVDTCxJQUFJSSxLQUFLRSxVQUFMLEVBQUosRUFBdUIsQ0FBdkIsQ0FBdkMsR0FBbUUsR0FBbkUsR0FBeUVOLElBQUlJLEtBQUtHLFVBQUwsRUFBSixFQUF1QixDQUF2QixDQUF6RSxHQUFxRyxHQUFyRyxHQUEyR1AsSUFBSUksS0FBS0ksZUFBTCxFQUFKLEVBQTRCLENBQTVCLENBQWxIO0FBQ0QsRUFGRDs7QUFJQTtBQUNBLEtBQUlDLFFBQVEsT0FBT0MsV0FBUCxLQUF1QixXQUF2QixJQUFzQyxPQUFPQSxZQUFZcEIsR0FBbkIsS0FBMkIsVUFBakUsR0FBOEVvQixXQUE5RSxHQUE0RnpCLElBQXhHOztBQUVBOzs7Ozs7Ozs7QUFTQSxVQUFTMEIsV0FBVCxDQUFxQkMsS0FBckIsRUFBNEJ0YixNQUE1QixFQUFvQ0UsT0FBcEMsRUFBNkNELElBQTdDLEVBQW1EO0FBQ2pELFdBQVEsT0FBT3FiLEtBQVAsS0FBaUIsV0FBakIsR0FBK0IsV0FBL0IsR0FBNkNmLFFBQVFlLEtBQVIsQ0FBckQ7QUFDRSxVQUFLLFFBQUw7QUFDRSxjQUFPLE9BQU9BLE1BQU1yYixJQUFOLENBQVAsS0FBdUIsVUFBdkIsR0FBb0NxYixNQUFNcmIsSUFBTixFQUFZb0UsS0FBWixDQUFrQmlYLEtBQWxCLEVBQXlCbEIsbUJBQW1CbGEsT0FBbkIsQ0FBekIsQ0FBcEMsR0FBNEZvYixNQUFNcmIsSUFBTixDQUFuRztBQUNGLFVBQUssVUFBTDtBQUNFLGNBQU9xYixNQUFNdGIsTUFBTixDQUFQO0FBQ0Y7QUFDRSxjQUFPc2IsS0FBUDtBQU5KO0FBUUQ7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQSxVQUFTQyxZQUFULEdBQXdCO0FBQ3RCLE9BQUlDLFVBQVV6WCxVQUFVVCxNQUFWLElBQW9CLENBQXBCLElBQXlCUyxVQUFVLENBQVYsTUFBaUJqRCxTQUExQyxHQUFzRCxFQUF0RCxHQUEyRGlELFVBQVUsQ0FBVixDQUF6RTtBQUNBLE9BQUkwWCxpQkFBaUJELFFBQVFGLEtBQTdCO0FBQ0EsT0FBSUEsUUFBUUcsbUJBQW1CM2EsU0FBbkIsR0FBK0IsS0FBL0IsR0FBdUMyYSxjQUFuRDtBQUNBLE9BQUlDLGtCQUFrQkYsUUFBUUcsTUFBOUI7QUFDQSxPQUFJQSxTQUFTRCxvQkFBb0I1YSxTQUFwQixHQUFnQ3dLLE9BQWhDLEdBQTBDb1EsZUFBdkQ7QUFDQSxPQUFJRSxxQkFBcUJKLFFBQVFLLFNBQWpDO0FBQ0EsT0FBSUEsWUFBWUQsdUJBQXVCOWEsU0FBdkIsR0FBbUMsSUFBbkMsR0FBMEM4YSxrQkFBMUQ7QUFDQSxPQUFJRSxZQUFZTixRQUFRTSxTQUF4QjtBQUNBLE9BQUlDLFlBQVlQLFFBQVFPLFNBQXhCO0FBQ0EsT0FBSUMsb0JBQW9CUixRQUFRUyxRQUFoQztBQUNBLE9BQUlBLFdBQVdELHNCQUFzQmxiLFNBQXRCLEdBQWtDLEtBQWxDLEdBQTBDa2IsaUJBQXpEO0FBQ0EsT0FBSUUscUJBQXFCVixRQUFRVyxTQUFqQztBQUNBLE9BQUlBLFlBQVlELHVCQUF1QnBiLFNBQXZCLEdBQW1DLElBQW5DLEdBQTBDb2Isa0JBQTFEO0FBQ0EsT0FBSUUsY0FBY1osUUFBUVksV0FBMUI7QUFDQSxPQUFJQyx3QkFBd0JiLFFBQVFjLGdCQUFwQztBQUNBLE9BQUk7QUFDSkEsc0JBQW1CRCwwQkFBMEJ2YixTQUExQixHQUFzQyxVQUFVZixLQUFWLEVBQWlCO0FBQ3hFLFlBQU9BLEtBQVA7QUFDRCxJQUZrQixHQUVmc2MscUJBSEo7QUFJQSxPQUFJRSx3QkFBd0JmLFFBQVFnQixpQkFBcEM7QUFDQSxPQUFJQSxvQkFBb0JELDBCQUEwQnpiLFNBQTFCLEdBQXNDLFVBQVUyYixJQUFWLEVBQWdCO0FBQzVFLFlBQU9BLElBQVA7QUFDRCxJQUZ1QixHQUVwQkYscUJBRko7QUFHQSxPQUFJRyx3QkFBd0JsQixRQUFRbUIsZ0JBQXBDO0FBQ0EsT0FBSUEsbUJBQW1CRCwwQkFBMEI1YixTQUExQixHQUFzQyxVQUFVakIsS0FBVixFQUFpQjtBQUM1RSxZQUFPQSxLQUFQO0FBQ0QsSUFGc0IsR0FFbkI2YyxxQkFGSjtBQUdBLE9BQUlFLGtCQUFrQnBCLFFBQVFxQixNQUE5QjtBQUNBLE9BQUlBLFNBQVNELG9CQUFvQjliLFNBQXBCLEdBQWdDO0FBQzNDd0QsWUFBTyxTQUFTQSxLQUFULEdBQWlCO0FBQ3RCLGNBQU8sU0FBUDtBQUNELE1BSDBDO0FBSTNDd1ksZ0JBQVcsU0FBU0EsU0FBVCxHQUFxQjtBQUM5QixjQUFPLFNBQVA7QUFDRCxNQU4wQztBQU8zQzljLGFBQVEsU0FBU0EsTUFBVCxHQUFrQjtBQUN4QixjQUFPLFNBQVA7QUFDRCxNQVQwQztBQVUzQ2dMLGdCQUFXLFNBQVNBLFNBQVQsR0FBcUI7QUFDOUIsY0FBTyxTQUFQO0FBQ0QsTUFaMEM7QUFhM0NuTCxZQUFPLFNBQVNBLEtBQVQsR0FBaUI7QUFDdEIsY0FBTyxTQUFQO0FBQ0Q7QUFmMEMsSUFBaEMsR0FnQlQrYyxlQWhCSjs7QUFrQkE7O0FBRUEsT0FBSSxPQUFPakIsTUFBUCxLQUFrQixXQUF0QixFQUFtQztBQUNqQyxZQUFPLFlBQVk7QUFDakIsY0FBTyxVQUFVblUsSUFBVixFQUFnQjtBQUNyQixnQkFBTyxVQUFVeEgsTUFBVixFQUFrQjtBQUN2QixrQkFBT3dILEtBQUt4SCxNQUFMLENBQVA7QUFDRCxVQUZEO0FBR0QsUUFKRDtBQUtELE1BTkQ7QUFPRDs7QUFFRCxPQUFJb2MsV0FBSixFQUFpQjtBQUNmOVEsYUFBUXpMLEtBQVIsQ0FBYyxrRUFBZDtBQUNEOztBQUVELE9BQUlrZCxZQUFZLEVBQWhCO0FBQ0EsWUFBU0MsV0FBVCxHQUF1QjtBQUNyQkQsZUFBVTFTLE9BQVYsQ0FBa0IsVUFBVTRTLFFBQVYsRUFBb0IxVCxHQUFwQixFQUF5QjtBQUN6QyxXQUFJMlQsVUFBVUQsU0FBU0MsT0FBdkI7QUFDQSxXQUFJQyxjQUFjRixTQUFTRSxXQUEzQjtBQUNBLFdBQUluZCxTQUFTaWQsU0FBU2pkLE1BQXRCO0FBQ0EsV0FBSThjLFlBQVlHLFNBQVNILFNBQXpCO0FBQ0EsV0FBSWpkLFFBQVFvZCxTQUFTcGQsS0FBckI7QUFDQSxXQUFJdWQsT0FBT0gsU0FBU0csSUFBcEI7QUFDQSxXQUFJcFMsWUFBWWlTLFNBQVNqUyxTQUF6Qjs7QUFFQSxXQUFJcVMsWUFBWU4sVUFBVXhULE1BQU0sQ0FBaEIsQ0FBaEI7QUFDQSxXQUFJOFQsU0FBSixFQUFlO0FBQ2JyUyxxQkFBWXFTLFVBQVVQLFNBQXRCO0FBQ0FNLGdCQUFPQyxVQUFVSCxPQUFWLEdBQW9CQSxPQUEzQjtBQUNEO0FBQ0Q7QUFDQSxXQUFJSSxrQkFBa0JkLGtCQUFrQnhjLE1BQWxCLENBQXRCO0FBQ0EsV0FBSXVkLGNBQWMsT0FBT3pCLFNBQVAsS0FBcUIsVUFBckIsR0FBa0NBLFVBQVUsWUFBWTtBQUN4RSxnQkFBTzlRLFNBQVA7QUFDRCxRQUZtRCxFQUVqRGhMLE1BRmlELENBQWxDLEdBRUw4YixTQUZiOztBQUlBLFdBQUkwQixnQkFBZ0IzQyxXQUFXc0MsV0FBWCxDQUFwQjtBQUNBLFdBQUlNLFdBQVdaLE9BQU92WSxLQUFQLEdBQWUsWUFBWXVZLE9BQU92WSxLQUFQLENBQWFnWixlQUFiLENBQVosR0FBNEMsR0FBM0QsR0FBaUUsSUFBaEY7QUFDQSxXQUFJaFosUUFBUSxhQUFhNlgsWUFBWXFCLGFBQVosR0FBNEIsRUFBekMsSUFBK0MsR0FBL0MsR0FBcURGLGdCQUFnQnJkLElBQXJFLEdBQTRFLEdBQTVFLElBQW1GZ2MsV0FBVyxTQUFTbUIsS0FBS00sT0FBTCxDQUFhLENBQWIsQ0FBVCxHQUEyQixNQUF0QyxHQUErQyxFQUFsSSxDQUFaOztBQUVBO0FBQ0EsV0FBSTtBQUNGLGFBQUlILFdBQUosRUFBaUI7QUFDZixlQUFJVixPQUFPdlksS0FBWCxFQUFrQnFYLE9BQU9nQyxjQUFQLENBQXNCLFFBQVFyWixLQUE5QixFQUFxQ21aLFFBQXJDLEVBQWxCLEtBQXNFOUIsT0FBT2dDLGNBQVAsQ0FBc0JyWixLQUF0QjtBQUN2RSxVQUZELE1BRU87QUFDTCxlQUFJdVksT0FBT3ZZLEtBQVgsRUFBa0JxWCxPQUFPaUMsS0FBUCxDQUFhLFFBQVF0WixLQUFyQixFQUE0Qm1aLFFBQTVCLEVBQWxCLEtBQTZEOUIsT0FBT2lDLEtBQVAsQ0FBYXRaLEtBQWI7QUFDOUQ7QUFDRixRQU5ELENBTUUsT0FBTzVCLENBQVAsRUFBVTtBQUNWaVosZ0JBQU9rQyxHQUFQLENBQVd2WixLQUFYO0FBQ0Q7O0FBRUQsV0FBSXdaLGlCQUFpQnpDLFlBQVlDLEtBQVosRUFBbUJnQyxlQUFuQixFQUFvQyxDQUFDUixTQUFELENBQXBDLEVBQWlELFdBQWpELENBQXJCO0FBQ0EsV0FBSWlCLGNBQWMxQyxZQUFZQyxLQUFaLEVBQW1CZ0MsZUFBbkIsRUFBb0MsQ0FBQ0EsZUFBRCxDQUFwQyxFQUF1RCxRQUF2RCxDQUFsQjtBQUNBLFdBQUlVLGFBQWEzQyxZQUFZQyxLQUFaLEVBQW1CZ0MsZUFBbkIsRUFBb0MsQ0FBQ3pkLEtBQUQsRUFBUWlkLFNBQVIsQ0FBcEMsRUFBd0QsT0FBeEQsQ0FBakI7QUFDQSxXQUFJbUIsaUJBQWlCNUMsWUFBWUMsS0FBWixFQUFtQmdDLGVBQW5CLEVBQW9DLENBQUN0UyxTQUFELENBQXBDLEVBQWlELFdBQWpELENBQXJCOztBQUVBLFdBQUk4UyxjQUFKLEVBQW9CO0FBQ2xCLGFBQUlqQixPQUFPQyxTQUFYLEVBQXNCbkIsT0FBT21DLGNBQVAsRUFBdUIsZUFBdkIsRUFBd0MsWUFBWWpCLE9BQU9DLFNBQVAsQ0FBaUJBLFNBQWpCLENBQVosR0FBMEMscUJBQWxGLEVBQXlHQSxTQUF6RyxFQUF0QixLQUErSW5CLE9BQU9tQyxjQUFQLEVBQXVCLFlBQXZCLEVBQXFDaEIsU0FBckM7QUFDaEo7O0FBRUQsV0FBSWlCLFdBQUosRUFBaUI7QUFDZixhQUFJbEIsT0FBTzdjLE1BQVgsRUFBbUIyYixPQUFPb0MsV0FBUCxFQUFvQixXQUFwQixFQUFpQyxZQUFZbEIsT0FBTzdjLE1BQVAsQ0FBY3NkLGVBQWQsQ0FBWixHQUE2QyxxQkFBOUUsRUFBcUdBLGVBQXJHLEVBQW5CLEtBQThJM0IsT0FBT29DLFdBQVAsRUFBb0IsUUFBcEIsRUFBOEJULGVBQTlCO0FBQy9JOztBQUVELFdBQUl6ZCxTQUFTbWUsVUFBYixFQUF5QjtBQUN2QixhQUFJbkIsT0FBT2hkLEtBQVgsRUFBa0I4YixPQUFPcUMsVUFBUCxFQUFtQixVQUFuQixFQUErQixZQUFZbkIsT0FBT2hkLEtBQVAsQ0FBYUEsS0FBYixFQUFvQmlkLFNBQXBCLENBQVosR0FBNkMscUJBQTVFLEVBQW1HamQsS0FBbkcsRUFBbEIsS0FBaUk4YixPQUFPcUMsVUFBUCxFQUFtQixPQUFuQixFQUE0Qm5lLEtBQTVCO0FBQ2xJOztBQUVELFdBQUlvZSxjQUFKLEVBQW9CO0FBQ2xCLGFBQUlwQixPQUFPN1IsU0FBWCxFQUFzQjJRLE9BQU9zQyxjQUFQLEVBQXVCLGVBQXZCLEVBQXdDLFlBQVlwQixPQUFPN1IsU0FBUCxDQUFpQkEsU0FBakIsQ0FBWixHQUEwQyxxQkFBbEYsRUFBeUdBLFNBQXpHLEVBQXRCLEtBQStJMlEsT0FBT3NDLGNBQVAsRUFBdUIsWUFBdkIsRUFBcUNqVCxTQUFyQztBQUNoSjs7QUFFRCxXQUFJO0FBQ0YyUSxnQkFBT3VDLFFBQVA7QUFDRCxRQUZELENBRUUsT0FBT3hiLENBQVAsRUFBVTtBQUNWaVosZ0JBQU9rQyxHQUFQLENBQVcsZUFBWDtBQUNEO0FBQ0YsTUE3REQ7QUE4REFkLGVBQVV6WixNQUFWLEdBQW1CLENBQW5CO0FBQ0Q7O0FBRUQsVUFBTyxVQUFVNkQsSUFBVixFQUFnQjtBQUNyQixTQUFJWixXQUFXWSxLQUFLWixRQUFwQjtBQUNBLFlBQU8sVUFBVWlCLElBQVYsRUFBZ0I7QUFDckIsY0FBTyxVQUFVeEgsTUFBVixFQUFrQjtBQUN2QjtBQUNBLGFBQUksT0FBTytiLFNBQVAsS0FBcUIsVUFBckIsSUFBbUMsQ0FBQ0EsVUFBVXhWLFFBQVYsRUFBb0J2RyxNQUFwQixDQUF4QyxFQUFxRTtBQUNuRSxrQkFBT3dILEtBQUt4SCxNQUFMLENBQVA7QUFDRDs7QUFFRCxhQUFJaWQsV0FBVyxFQUFmO0FBQ0FGLG1CQUFVOVksSUFBVixDQUFlZ1osUUFBZjs7QUFFQUEsa0JBQVNDLE9BQVQsR0FBbUIvQixNQUFNbkIsR0FBTixFQUFuQjtBQUNBaUQsa0JBQVNFLFdBQVQsR0FBdUIsSUFBSXhELElBQUosRUFBdkI7QUFDQXNELGtCQUFTSCxTQUFULEdBQXFCUixpQkFBaUIvVixVQUFqQixDQUFyQjtBQUNBMFcsa0JBQVNqZCxNQUFULEdBQWtCQSxNQUFsQjs7QUFFQSxhQUFJbWUsZ0JBQWdCcmQsU0FBcEI7QUFDQSxhQUFJK2EsU0FBSixFQUFlO0FBQ2IsZUFBSTtBQUNGc0MsNkJBQWdCM1csS0FBS3hILE1BQUwsQ0FBaEI7QUFDRCxZQUZELENBRUUsT0FBTzBDLENBQVAsRUFBVTtBQUNWdWEsc0JBQVNwZCxLQUFULEdBQWlCOGMsaUJBQWlCamEsQ0FBakIsQ0FBakI7QUFDRDtBQUNGLFVBTkQsTUFNTztBQUNMeWIsMkJBQWdCM1csS0FBS3hILE1BQUwsQ0FBaEI7QUFDRDs7QUFFRGlkLGtCQUFTRyxJQUFULEdBQWdCakMsTUFBTW5CLEdBQU4sS0FBY2lELFNBQVNDLE9BQXZDO0FBQ0FELGtCQUFTalMsU0FBVCxHQUFxQnNSLGlCQUFpQi9WLFVBQWpCLENBQXJCOztBQUVBeVc7O0FBRUEsYUFBSUMsU0FBU3BkLEtBQWIsRUFBb0IsTUFBTW9kLFNBQVNwZCxLQUFmO0FBQ3BCLGdCQUFPc2UsYUFBUDtBQUNELFFBaENEO0FBaUNELE1BbENEO0FBbUNELElBckNEO0FBc0NEOztBQUVEaGMsUUFBTzVCLE9BQVAsR0FBaUJnYixZQUFqQixDOzs7Ozs7QUNuT0E7O0FBRUFoYixTQUFRQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0EsVUFBUzRkLHFCQUFULENBQStCQyxhQUEvQixFQUE4QztBQUM1QyxVQUFPLFVBQVVsWCxJQUFWLEVBQWdCO0FBQ3JCLFNBQUk5RyxXQUFXOEcsS0FBSzlHLFFBQXBCO0FBQ0EsU0FBSWtHLFdBQVdZLEtBQUtaLFFBQXBCO0FBQ0EsWUFBTyxVQUFVaUIsSUFBVixFQUFnQjtBQUNyQixjQUFPLFVBQVV4SCxNQUFWLEVBQWtCO0FBQ3ZCLGFBQUksT0FBT0EsTUFBUCxLQUFrQixVQUF0QixFQUFrQztBQUNoQyxrQkFBT0EsT0FBT0ssUUFBUCxFQUFpQmtHLFFBQWpCLEVBQTJCOFgsYUFBM0IsQ0FBUDtBQUNEOztBQUVELGdCQUFPN1csS0FBS3hILE1BQUwsQ0FBUDtBQUNELFFBTkQ7QUFPRCxNQVJEO0FBU0QsSUFaRDtBQWFEOztBQUVELEtBQUlzZSxRQUFRRix1QkFBWjtBQUNBRSxPQUFNQyxpQkFBTixHQUEwQkgscUJBQTFCOztBQUVBN2QsU0FBUSxTQUFSLElBQXFCK2QsS0FBckIsQzs7Ozs7O0FDdEJBOzs7O0FBRUF0VyxRQUFPd1csY0FBUCxDQUFzQmplLE9BQXRCLEVBQStCLFlBQS9CLEVBQTZDO0FBQzNDZ0ksVUFBTztBQURvQyxFQUE3Qzs7QUFJQSxLQUFJZ1MsVUFBVSxPQUFPbFIsTUFBUCxLQUFrQixVQUFsQixJQUFnQyxTQUFPQSxPQUFPb1YsUUFBZCxNQUEyQixRQUEzRCxHQUFzRSxVQUFVNWMsR0FBVixFQUFlO0FBQUUsaUJBQWNBLEdBQWQsMENBQWNBLEdBQWQ7QUFBb0IsRUFBM0csR0FBOEcsVUFBVUEsR0FBVixFQUFlO0FBQUUsVUFBT0EsT0FBTyxPQUFPd0gsTUFBUCxLQUFrQixVQUF6QixJQUF1Q3hILElBQUk2RyxXQUFKLEtBQW9CVyxNQUEzRCxHQUFvRSxRQUFwRSxVQUFzRnhILEdBQXRGLDBDQUFzRkEsR0FBdEYsQ0FBUDtBQUFtRyxFQUFoUDs7QUFFQSxLQUFJOEosV0FBVzNELE9BQU80RCxNQUFQLElBQWlCLFVBQVVDLE1BQVYsRUFBa0I7QUFBRSxRQUFLLElBQUk3SCxJQUFJLENBQWIsRUFBZ0JBLElBQUlELFVBQVVULE1BQTlCLEVBQXNDVSxHQUF0QyxFQUEyQztBQUFFLFNBQUk4SCxTQUFTL0gsVUFBVUMsQ0FBVixDQUFiLENBQTJCLEtBQUssSUFBSXVGLEdBQVQsSUFBZ0J1QyxNQUFoQixFQUF3QjtBQUFFLFdBQUk5RCxPQUFPNUQsU0FBUCxDQUFpQitELGNBQWpCLENBQWdDckYsSUFBaEMsQ0FBcUNnSixNQUFyQyxFQUE2Q3ZDLEdBQTdDLENBQUosRUFBdUQ7QUFBRXNDLGdCQUFPdEMsR0FBUCxJQUFjdUMsT0FBT3ZDLEdBQVAsQ0FBZDtBQUE0QjtBQUFFO0FBQUUsSUFBQyxPQUFPc0MsTUFBUDtBQUFnQixFQUFoUTs7QUFFQSxLQUFJNlMsaUJBQWlCLFlBQVk7QUFBRSxZQUFTQyxhQUFULENBQXVCeEUsR0FBdkIsRUFBNEJuVyxDQUE1QixFQUErQjtBQUFFLFNBQUk0YSxPQUFPLEVBQVgsQ0FBZSxJQUFJQyxLQUFLLElBQVQsQ0FBZSxJQUFJQyxLQUFLLEtBQVQsQ0FBZ0IsSUFBSUMsS0FBS2plLFNBQVQsQ0FBb0IsSUFBSTtBQUFFLFlBQUssSUFBSWtlLEtBQUs3RSxJQUFJOVEsT0FBT29WLFFBQVgsR0FBVCxFQUFpQ1EsRUFBdEMsRUFBMEMsRUFBRUosS0FBSyxDQUFDSSxLQUFLRCxHQUFHeFgsSUFBSCxFQUFOLEVBQWlCMFgsSUFBeEIsQ0FBMUMsRUFBeUVMLEtBQUssSUFBOUUsRUFBb0Y7QUFBRUQsY0FBSzNhLElBQUwsQ0FBVWdiLEdBQUcxVyxLQUFiLEVBQXFCLElBQUl2RSxLQUFLNGEsS0FBS3RiLE1BQUwsS0FBZ0JVLENBQXpCLEVBQTRCO0FBQVE7QUFBRSxNQUF2SixDQUF3SixPQUFPbWIsR0FBUCxFQUFZO0FBQUVMLFlBQUssSUFBTCxDQUFXQyxLQUFLSSxHQUFMO0FBQVcsTUFBNUwsU0FBcU07QUFBRSxXQUFJO0FBQUUsYUFBSSxDQUFDTixFQUFELElBQU9HLEdBQUcsUUFBSCxDQUFYLEVBQXlCQSxHQUFHLFFBQUg7QUFBaUIsUUFBaEQsU0FBeUQ7QUFBRSxhQUFJRixFQUFKLEVBQVEsTUFBTUMsRUFBTjtBQUFXO0FBQUUsTUFBQyxPQUFPSCxJQUFQO0FBQWMsSUFBQyxPQUFPLFVBQVV6RSxHQUFWLEVBQWVuVyxDQUFmLEVBQWtCO0FBQUUsU0FBSUYsTUFBTWdLLE9BQU4sQ0FBY3FNLEdBQWQsQ0FBSixFQUF3QjtBQUFFLGNBQU9BLEdBQVA7QUFBYSxNQUF2QyxNQUE2QyxJQUFJOVEsT0FBT29WLFFBQVAsSUFBbUJ6VyxPQUFPbVMsR0FBUCxDQUF2QixFQUFvQztBQUFFLGNBQU93RSxjQUFjeEUsR0FBZCxFQUFtQm5XLENBQW5CLENBQVA7QUFBK0IsTUFBckUsTUFBMkU7QUFBRSxhQUFNLElBQUlzRCxTQUFKLENBQWMsc0RBQWQsQ0FBTjtBQUE4RTtBQUFFLElBQXJPO0FBQXdPLEVBQWhvQixFQUFyQjs7QUFFQS9HLFNBQVFzTixPQUFSLEdBQWtCdVIsaUJBQWxCOztBQUVBLEtBQUlDLGFBQWEsbUJBQUFyZSxDQUFRLEVBQVIsQ0FBakI7O0FBRUEsS0FBSXNlLGNBQWNwZSx1QkFBdUJtZSxVQUF2QixDQUFsQjs7QUFFQSxVQUFTbmUsc0JBQVQsQ0FBZ0NXLEdBQWhDLEVBQXFDO0FBQUUsVUFBT0EsT0FBT0EsSUFBSXJCLFVBQVgsR0FBd0JxQixHQUF4QixHQUE4QixFQUFFZ00sU0FBU2hNLEdBQVgsRUFBckM7QUFBd0Q7O0FBRS9GLEtBQUkwZCxlQUFlLENBQUMsU0FBRCxFQUFZLFdBQVosRUFBeUIsVUFBekIsQ0FBbkI7O0FBRUE7Ozs7O0FBS0EsVUFBU0gsaUJBQVQsR0FBNkI7QUFDM0IsT0FBSTVPLFNBQVN6TSxVQUFVVCxNQUFWLElBQW9CLENBQXBCLElBQXlCUyxVQUFVLENBQVYsTUFBaUJqRCxTQUExQyxHQUFzRCxFQUF0RCxHQUEyRGlELFVBQVUsQ0FBVixDQUF4RTs7QUFFQSxPQUFJeWIsc0JBQXNCaFAsT0FBT2dQLG1CQUFQLElBQThCRCxZQUF4RDs7QUFFQSxVQUFPLFVBQVVFLEdBQVYsRUFBZTtBQUNwQixTQUFJcGYsV0FBV29mLElBQUlwZixRQUFuQjs7QUFHQSxZQUFPLFVBQVVtSCxJQUFWLEVBQWdCO0FBQ3JCLGNBQU8sVUFBVXhILE1BQVYsRUFBa0I7QUFDdkIsYUFBSUEsT0FBT0UsT0FBWCxFQUFvQjtBQUNsQixlQUFJLENBQUMsQ0FBQyxHQUFHb2YsWUFBWXpSLE9BQWhCLEVBQXlCN04sT0FBT0UsT0FBaEMsQ0FBRCxJQUE2QyxDQUFDLENBQUMsR0FBR29mLFlBQVl6UixPQUFoQixFQUF5QjdOLE9BQU9FLE9BQVAsQ0FBZTBRLE9BQXhDLENBQWxELEVBQW9HO0FBQ2xHLG9CQUFPcEosS0FBS3hILE1BQUwsQ0FBUDtBQUNEO0FBQ0YsVUFKRCxNQUlPO0FBQ0wsa0JBQU93SCxLQUFLeEgsTUFBTCxDQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxhQUFJQyxPQUFPRCxPQUFPQyxJQUFsQjtBQUNBLGFBQUlDLFVBQVVGLE9BQU9FLE9BQXJCO0FBQ0EsYUFBSXdmLE9BQU8xZixPQUFPMGYsSUFBbEI7O0FBRUE7O0FBRUEsYUFBSUMsdUJBQXVCakIsZUFBZWMsbUJBQWYsRUFBb0MsQ0FBcEMsQ0FBM0I7O0FBRUEsYUFBSUksVUFBVUQscUJBQXFCLENBQXJCLENBQWQ7QUFDQSxhQUFJRSxZQUFZRixxQkFBcUIsQ0FBckIsQ0FBaEI7QUFDQSxhQUFJRyxXQUFXSCxxQkFBcUIsQ0FBckIsQ0FBZjs7QUFFQTs7Ozs7Ozs7QUFRQSxhQUFJSSxZQUFZLFNBQVNBLFNBQVQsQ0FBbUJDLFVBQW5CLEVBQStCQyxVQUEvQixFQUEyQztBQUN6RCxrQkFBT3RVLFNBQVM7QUFDZDFMLG1CQUFNQSxPQUFPLEdBQVAsSUFBY2dnQixhQUFhSCxRQUFiLEdBQXdCRCxTQUF0QztBQURRLFlBQVQsRUFFSkcsYUFBYTtBQUNkOWYsc0JBQVM4ZjtBQURLLFlBQWIsR0FFQyxFQUpHLEVBSUMsQ0FBQyxDQUFDTixJQUFGLEdBQVMsRUFBRUEsTUFBTUEsSUFBUixFQUFULEdBQTBCLEVBSjNCLEVBSStCTyxhQUFhO0FBQ2pEcGdCLG9CQUFPO0FBRDBDLFlBQWIsR0FFbEMsRUFORyxDQUFQO0FBT0QsVUFSRDs7QUFVQTs7Ozs7O0FBTUEsYUFBSStRLFVBQVUsS0FBSyxDQUFuQjtBQUNBLGFBQUlZLE9BQU8sS0FBSyxDQUFoQjs7QUFFQSxhQUFJLENBQUMsQ0FBQyxHQUFHOE4sWUFBWXpSLE9BQWhCLEVBQXlCN04sT0FBT0UsT0FBaEMsQ0FBRCxJQUE2Q3FhLFFBQVF2YSxPQUFPRSxPQUFmLE1BQTRCLFFBQTdFLEVBQXVGO0FBQ3JGMFEscUJBQVUxUSxRQUFRMFEsT0FBbEI7QUFDQVksa0JBQU90UixRQUFRc1IsSUFBZjtBQUNELFVBSEQsTUFHTztBQUNMWixxQkFBVTFRLE9BQVY7QUFDQXNSLGtCQUFPLElBQVA7QUFDRDs7QUFFRDs7Ozs7QUFLQWhLLGNBQUttRSxTQUFTO0FBQ1oxTCxpQkFBTUEsT0FBTyxHQUFQLEdBQWEyZjtBQURQLFVBQVQsRUFFRixDQUFDLENBQUNwTyxJQUFGLEdBQVMsRUFBRXRSLFNBQVNzUixJQUFYLEVBQVQsR0FBNkIsRUFGM0IsRUFFK0IsQ0FBQyxDQUFDa08sSUFBRixHQUFTLEVBQUVBLE1BQU1BLElBQVIsRUFBVCxHQUEwQixFQUZ6RCxDQUFMOztBQUlBOzs7Ozs7Ozs7QUFTQSxhQUFJUSxlQUFlLFNBQVNBLFlBQVQsQ0FBc0JDLE1BQXRCLEVBQThCO0FBQy9DLGVBQUlDLGlCQUFpQkwsVUFBVUksTUFBVixFQUFrQixJQUFsQixDQUFyQjtBQUNBOWYsb0JBQVMrZixjQUFUO0FBQ0EsaUJBQU1ELE1BQU47QUFDRCxVQUpEOztBQU1BOzs7Ozs7OztBQVFBLGFBQUlFLGdCQUFnQixTQUFTQSxhQUFULEdBQXlCO0FBQzNDLGVBQUk5WCxRQUFReEUsVUFBVVQsTUFBVixJQUFvQixDQUFwQixJQUF5QlMsVUFBVSxDQUFWLE1BQWlCakQsU0FBMUMsR0FBc0QsSUFBdEQsR0FBNkRpRCxVQUFVLENBQVYsQ0FBekU7O0FBRUEsZUFBSXVjLGlCQUFpQlAsVUFBVXhYLEtBQVYsRUFBaUIsS0FBakIsQ0FBckI7QUFDQWxJLG9CQUFTaWdCLGNBQVQ7O0FBRUEsa0JBQU8sRUFBRS9YLE9BQU9BLEtBQVQsRUFBZ0J2SSxRQUFRc2dCLGNBQXhCLEVBQVA7QUFDRCxVQVBEOztBQVNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTZCQSxnQkFBTzFQLFFBQVFRLElBQVIsQ0FBYWlQLGFBQWIsRUFBNEJILFlBQTVCLENBQVA7QUFDRCxRQWhJRDtBQWlJRCxNQWxJRDtBQW1JRCxJQXZJRDtBQXdJRCxFOzs7Ozs7QUN4S0Q7Ozs7QUFFQWxZLFFBQU93VyxjQUFQLENBQXNCamUsT0FBdEIsRUFBK0IsWUFBL0IsRUFBNkM7QUFDM0NnSSxVQUFPO0FBRG9DLEVBQTdDOztBQUlBLEtBQUlnUyxVQUFVLE9BQU9sUixNQUFQLEtBQWtCLFVBQWxCLElBQWdDLFNBQU9BLE9BQU9vVixRQUFkLE1BQTJCLFFBQTNELEdBQXNFLFVBQVU1YyxHQUFWLEVBQWU7QUFBRSxpQkFBY0EsR0FBZCwwQ0FBY0EsR0FBZDtBQUFvQixFQUEzRyxHQUE4RyxVQUFVQSxHQUFWLEVBQWU7QUFBRSxVQUFPQSxPQUFPLE9BQU93SCxNQUFQLEtBQWtCLFVBQXpCLElBQXVDeEgsSUFBSTZHLFdBQUosS0FBb0JXLE1BQTNELEdBQW9FLFFBQXBFLFVBQXNGeEgsR0FBdEYsMENBQXNGQSxHQUF0RixDQUFQO0FBQW1HLEVBQWhQOztBQUVBdEIsU0FBUXNOLE9BQVIsR0FBa0IwUyxTQUFsQjtBQUNBLFVBQVNBLFNBQVQsQ0FBbUJoWSxLQUFuQixFQUEwQjtBQUN4QixPQUFJQSxVQUFVLElBQVYsSUFBa0IsQ0FBQyxPQUFPQSxLQUFQLEtBQWlCLFdBQWpCLEdBQStCLFdBQS9CLEdBQTZDZ1MsUUFBUWhTLEtBQVIsQ0FBOUMsTUFBa0UsUUFBeEYsRUFBa0c7QUFDaEcsWUFBT0EsU0FBUyxPQUFPQSxNQUFNNkksSUFBYixLQUFzQixVQUF0QztBQUNEOztBQUVELFVBQU8sS0FBUDtBQUNELEUiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pXG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG5cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGV4cG9ydHM6IHt9LFxuIFx0XHRcdGlkOiBtb2R1bGVJZCxcbiBcdFx0XHRsb2FkZWQ6IGZhbHNlXG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmxvYWRlZCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiB3ZWJwYWNrL2Jvb3RzdHJhcCA3ZTkwOTAyMDkzMjg5ZWVmYzYxYlxuICoqLyIsImltcG9ydCB7IGFwcGx5TWlkZGxld2FyZSwgY3JlYXRlU3RvcmUgfSBmcm9tICdyZWR1eCc7XHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCBsb2dnZXIgZnJvbSAncmVkdXgtbG9nZ2VyJztcclxuaW1wb3J0IHRodW5rIGZyb20gJ3JlZHV4LXRodW5rJztcclxuaW1wb3J0IHByb21pc2UgZnJvbSAncmVkdXgtcHJvbWlzZS1taWRkbGV3YXJlJztcclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZSA9IHtcclxuICAgIGZldGNoaW5nOiBmYWxzZSxcclxuICAgIGZldGNoZWQ6IGZhbHNlLFxyXG4gICAgdXNlcnM6IFtdLFxyXG4gICAgZXJyb3I6IG51bGwsXHJcbn07XHJcblxyXG5jb25zdCByZWR1Y2VyID0gKHN0YXRlPWluaXRpYWxTdGF0ZSwgYWN0aW9uKSA9PiB7XHJcbiAgICBzd2l0Y2goYWN0aW9uLnR5cGUpIHtcclxuICAgICAgICBjYXNlICdGRVRDSF9VU0VSU19QRU5ESU5HJzoge1xyXG4gICAgICAgICAgICBzdGF0ZSA9IHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLCBcclxuICAgICAgICAgICAgICAgIGZldGNoaW5nOiB0cnVlXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXNlICdGRVRDSF9VU0VSU19SRUpFQ1RFRCc6IHtcclxuICAgICAgICAgICAgc3RhdGUgPSB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSwgXHJcbiAgICAgICAgICAgICAgICBmZXRjaGluZzogZmFsc2UsIFxyXG4gICAgICAgICAgICAgICAgZXJyb3I6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXNlICdGRVRDSF9VU0VSU19GVUxGSUxMRUQnOiB7XHJcbiAgICAgICAgICAgIHN0YXRlID0ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsIFxyXG4gICAgICAgICAgICAgICAgZmV0Y2hpbmc6IGZhbHNlLCBcclxuICAgICAgICAgICAgICAgIGZldGNoZWQ6IHRydWUsIFxyXG4gICAgICAgICAgICAgICAgdXNlcnM6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBzdGF0ZTtcclxufTtcclxuXHJcbmNvbnN0IG1pZGRsZXdhcmUgPSBhcHBseU1pZGRsZXdhcmUocHJvbWlzZSgpLCB0aHVuaywgbG9nZ2VyKCkpO1xyXG5cclxuY29uc3Qgc3RvcmUgPSBjcmVhdGVTdG9yZShyZWR1Y2VyLCBtaWRkbGV3YXJlKTtcclxuXHJcbnN0b3JlLmRpc3BhdGNoKHtcclxuICAgIHR5cGU6ICdGRVRDSF9VU0VSUycsXHJcbiAgICBwYXlsb2FkOiBheGlvcy5nZXQoJ2h0dHA6Ly9yZXN0LmxlYXJuY29kZS5hY2FkZW15L2FwaS9sZWFybmNvZGUvZnJpZW5kcycpLFxyXG59KTtcclxuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9qcy9jbGllbnQuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5leHBvcnRzLmNvbXBvc2UgPSBleHBvcnRzLmFwcGx5TWlkZGxld2FyZSA9IGV4cG9ydHMuYmluZEFjdGlvbkNyZWF0b3JzID0gZXhwb3J0cy5jb21iaW5lUmVkdWNlcnMgPSBleHBvcnRzLmNyZWF0ZVN0b3JlID0gdW5kZWZpbmVkO1xuXG52YXIgX2NyZWF0ZVN0b3JlID0gcmVxdWlyZSgnLi9jcmVhdGVTdG9yZScpO1xuXG52YXIgX2NyZWF0ZVN0b3JlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZVN0b3JlKTtcblxudmFyIF9jb21iaW5lUmVkdWNlcnMgPSByZXF1aXJlKCcuL2NvbWJpbmVSZWR1Y2VycycpO1xuXG52YXIgX2NvbWJpbmVSZWR1Y2VyczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb21iaW5lUmVkdWNlcnMpO1xuXG52YXIgX2JpbmRBY3Rpb25DcmVhdG9ycyA9IHJlcXVpcmUoJy4vYmluZEFjdGlvbkNyZWF0b3JzJyk7XG5cbnZhciBfYmluZEFjdGlvbkNyZWF0b3JzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2JpbmRBY3Rpb25DcmVhdG9ycyk7XG5cbnZhciBfYXBwbHlNaWRkbGV3YXJlID0gcmVxdWlyZSgnLi9hcHBseU1pZGRsZXdhcmUnKTtcblxudmFyIF9hcHBseU1pZGRsZXdhcmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfYXBwbHlNaWRkbGV3YXJlKTtcblxudmFyIF9jb21wb3NlID0gcmVxdWlyZSgnLi9jb21wb3NlJyk7XG5cbnZhciBfY29tcG9zZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb21wb3NlKTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnLi91dGlscy93YXJuaW5nJyk7XG5cbnZhciBfd2FybmluZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93YXJuaW5nKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgXCJkZWZhdWx0XCI6IG9iaiB9OyB9XG5cbi8qXG4qIFRoaXMgaXMgYSBkdW1teSBmdW5jdGlvbiB0byBjaGVjayBpZiB0aGUgZnVuY3Rpb24gbmFtZSBoYXMgYmVlbiBhbHRlcmVkIGJ5IG1pbmlmaWNhdGlvbi5cbiogSWYgdGhlIGZ1bmN0aW9uIGhhcyBiZWVuIG1pbmlmaWVkIGFuZCBOT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nLCB3YXJuIHRoZSB1c2VyLlxuKi9cbmZ1bmN0aW9uIGlzQ3J1c2hlZCgpIHt9XG5cbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nICYmIHR5cGVvZiBpc0NydXNoZWQubmFtZSA9PT0gJ3N0cmluZycgJiYgaXNDcnVzaGVkLm5hbWUgIT09ICdpc0NydXNoZWQnKSB7XG4gICgwLCBfd2FybmluZzJbXCJkZWZhdWx0XCJdKSgnWW91IGFyZSBjdXJyZW50bHkgdXNpbmcgbWluaWZpZWQgY29kZSBvdXRzaWRlIG9mIE5PREVfRU5WID09PSBcXCdwcm9kdWN0aW9uXFwnLiAnICsgJ1RoaXMgbWVhbnMgdGhhdCB5b3UgYXJlIHJ1bm5pbmcgYSBzbG93ZXIgZGV2ZWxvcG1lbnQgYnVpbGQgb2YgUmVkdXguICcgKyAnWW91IGNhbiB1c2UgbG9vc2UtZW52aWZ5IChodHRwczovL2dpdGh1Yi5jb20vemVydG9zaC9sb29zZS1lbnZpZnkpIGZvciBicm93c2VyaWZ5ICcgKyAnb3IgRGVmaW5lUGx1Z2luIGZvciB3ZWJwYWNrIChodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzMwMDMwMDMxKSAnICsgJ3RvIGVuc3VyZSB5b3UgaGF2ZSB0aGUgY29ycmVjdCBjb2RlIGZvciB5b3VyIHByb2R1Y3Rpb24gYnVpbGQuJyk7XG59XG5cbmV4cG9ydHMuY3JlYXRlU3RvcmUgPSBfY3JlYXRlU3RvcmUyW1wiZGVmYXVsdFwiXTtcbmV4cG9ydHMuY29tYmluZVJlZHVjZXJzID0gX2NvbWJpbmVSZWR1Y2VyczJbXCJkZWZhdWx0XCJdO1xuZXhwb3J0cy5iaW5kQWN0aW9uQ3JlYXRvcnMgPSBfYmluZEFjdGlvbkNyZWF0b3JzMltcImRlZmF1bHRcIl07XG5leHBvcnRzLmFwcGx5TWlkZGxld2FyZSA9IF9hcHBseU1pZGRsZXdhcmUyW1wiZGVmYXVsdFwiXTtcbmV4cG9ydHMuY29tcG9zZSA9IF9jb21wb3NlMltcImRlZmF1bHRcIl07XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9yZWR1eC9saWIvaW5kZXguanNcbiAqKi8iLCIvLyBzaGltIGZvciB1c2luZyBwcm9jZXNzIGluIGJyb3dzZXJcbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxuLy8gY2FjaGVkIGZyb20gd2hhdGV2ZXIgZ2xvYmFsIGlzIHByZXNlbnQgc28gdGhhdCB0ZXN0IHJ1bm5lcnMgdGhhdCBzdHViIGl0XG4vLyBkb24ndCBicmVhayB0aGluZ3MuICBCdXQgd2UgbmVlZCB0byB3cmFwIGl0IGluIGEgdHJ5IGNhdGNoIGluIGNhc2UgaXQgaXNcbi8vIHdyYXBwZWQgaW4gc3RyaWN0IG1vZGUgY29kZSB3aGljaCBkb2Vzbid0IGRlZmluZSBhbnkgZ2xvYmFscy4gIEl0J3MgaW5zaWRlIGFcbi8vIGZ1bmN0aW9uIGJlY2F1c2UgdHJ5L2NhdGNoZXMgZGVvcHRpbWl6ZSBpbiBjZXJ0YWluIGVuZ2luZXMuXG5cbnZhciBjYWNoZWRTZXRUaW1lb3V0O1xudmFyIGNhY2hlZENsZWFyVGltZW91dDtcblxuZnVuY3Rpb24gZGVmYXVsdFNldFRpbW91dCgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3NldFRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcbn1cbmZ1bmN0aW9uIGRlZmF1bHRDbGVhclRpbWVvdXQgKCkge1xuICAgIHRocm93IG5ldyBFcnJvcignY2xlYXJUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG4oZnVuY3Rpb24gKCkge1xuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2Ygc2V0VGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2YgY2xlYXJUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgIH1cbn0gKCkpXG5mdW5jdGlvbiBydW5UaW1lb3V0KGZ1bikge1xuICAgIGlmIChjYWNoZWRTZXRUaW1lb3V0ID09PSBzZXRUaW1lb3V0KSB7XG4gICAgICAgIC8vbm9ybWFsIGVudmlyb21lbnRzIGluIHNhbmUgc2l0dWF0aW9uc1xuICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xuICAgIH1cbiAgICAvLyBpZiBzZXRUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkU2V0VGltZW91dCA9PT0gZGVmYXVsdFNldFRpbW91dCB8fCAhY2FjaGVkU2V0VGltZW91dCkgJiYgc2V0VGltZW91dCkge1xuICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dDtcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dChmdW4sIDApO1xuICAgIH0gY2F0Y2goZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0LmNhbGwobnVsbCwgZnVuLCAwKTtcbiAgICAgICAgfSBjYXRjaChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yXG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKHRoaXMsIGZ1biwgMCk7XG4gICAgICAgIH1cbiAgICB9XG5cblxufVxuZnVuY3Rpb24gcnVuQ2xlYXJUaW1lb3V0KG1hcmtlcikge1xuICAgIGlmIChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGNsZWFyVGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICAvLyBpZiBjbGVhclRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXG4gICAgaWYgKChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGRlZmF1bHRDbGVhclRpbWVvdXQgfHwgIWNhY2hlZENsZWFyVGltZW91dCkgJiYgY2xlYXJUaW1lb3V0KSB7XG4gICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGNsZWFyVGltZW91dDtcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXG4gICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIFdoZW4gd2UgYXJlIGluIEkuRS4gYnV0IHRoZSBzY3JpcHQgaGFzIGJlZW4gZXZhbGVkIHNvIEkuRS4gZG9lc24ndCAgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbChudWxsLCBtYXJrZXIpO1xuICAgICAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yLlxuICAgICAgICAgICAgLy8gU29tZSB2ZXJzaW9ucyBvZiBJLkUuIGhhdmUgZGlmZmVyZW50IHJ1bGVzIGZvciBjbGVhclRpbWVvdXQgdnMgc2V0VGltZW91dFxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKHRoaXMsIG1hcmtlcik7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG59XG52YXIgcXVldWUgPSBbXTtcbnZhciBkcmFpbmluZyA9IGZhbHNlO1xudmFyIGN1cnJlbnRRdWV1ZTtcbnZhciBxdWV1ZUluZGV4ID0gLTE7XG5cbmZ1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpIHtcbiAgICBpZiAoIWRyYWluaW5nIHx8ICFjdXJyZW50UXVldWUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIGlmIChjdXJyZW50UXVldWUubGVuZ3RoKSB7XG4gICAgICAgIHF1ZXVlID0gY3VycmVudFF1ZXVlLmNvbmNhdChxdWV1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgIH1cbiAgICBpZiAocXVldWUubGVuZ3RoKSB7XG4gICAgICAgIGRyYWluUXVldWUoKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRyYWluUXVldWUoKSB7XG4gICAgaWYgKGRyYWluaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIHRpbWVvdXQgPSBydW5UaW1lb3V0KGNsZWFuVXBOZXh0VGljayk7XG4gICAgZHJhaW5pbmcgPSB0cnVlO1xuXG4gICAgdmFyIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB3aGlsZShsZW4pIHtcbiAgICAgICAgY3VycmVudFF1ZXVlID0gcXVldWU7XG4gICAgICAgIHF1ZXVlID0gW107XG4gICAgICAgIHdoaWxlICgrK3F1ZXVlSW5kZXggPCBsZW4pIHtcbiAgICAgICAgICAgIGlmIChjdXJyZW50UXVldWUpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50UXVldWVbcXVldWVJbmRleF0ucnVuKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgICAgICBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgfVxuICAgIGN1cnJlbnRRdWV1ZSA9IG51bGw7XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBydW5DbGVhclRpbWVvdXQodGltZW91dCk7XG59XG5cbnByb2Nlc3MubmV4dFRpY2sgPSBmdW5jdGlvbiAoZnVuKSB7XG4gICAgdmFyIGFyZ3MgPSBuZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aCAtIDEpO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcXVldWUucHVzaChuZXcgSXRlbShmdW4sIGFyZ3MpKTtcbiAgICBpZiAocXVldWUubGVuZ3RoID09PSAxICYmICFkcmFpbmluZykge1xuICAgICAgICBydW5UaW1lb3V0KGRyYWluUXVldWUpO1xuICAgIH1cbn07XG5cbi8vIHY4IGxpa2VzIHByZWRpY3RpYmxlIG9iamVjdHNcbmZ1bmN0aW9uIEl0ZW0oZnVuLCBhcnJheSkge1xuICAgIHRoaXMuZnVuID0gZnVuO1xuICAgIHRoaXMuYXJyYXkgPSBhcnJheTtcbn1cbkl0ZW0ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmZ1bi5hcHBseShudWxsLCB0aGlzLmFycmF5KTtcbn07XG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcbnByb2Nlc3MudmVyc2lvbiA9ICcnOyAvLyBlbXB0eSBzdHJpbmcgdG8gYXZvaWQgcmVnZXhwIGlzc3Vlc1xucHJvY2Vzcy52ZXJzaW9ucyA9IHt9O1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcblxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5wcm9jZXNzLnVtYXNrID0gZnVuY3Rpb24oKSB7IHJldHVybiAwOyB9O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9wcm9jZXNzL2Jyb3dzZXIuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5leHBvcnRzLkFjdGlvblR5cGVzID0gdW5kZWZpbmVkO1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBjcmVhdGVTdG9yZTtcblxudmFyIF9pc1BsYWluT2JqZWN0ID0gcmVxdWlyZSgnbG9kYXNoL2lzUGxhaW5PYmplY3QnKTtcblxudmFyIF9pc1BsYWluT2JqZWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUGxhaW5PYmplY3QpO1xuXG52YXIgX3N5bWJvbE9ic2VydmFibGUgPSByZXF1aXJlKCdzeW1ib2wtb2JzZXJ2YWJsZScpO1xuXG52YXIgX3N5bWJvbE9ic2VydmFibGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc3ltYm9sT2JzZXJ2YWJsZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IFwiZGVmYXVsdFwiOiBvYmogfTsgfVxuXG4vKipcbiAqIFRoZXNlIGFyZSBwcml2YXRlIGFjdGlvbiB0eXBlcyByZXNlcnZlZCBieSBSZWR1eC5cbiAqIEZvciBhbnkgdW5rbm93biBhY3Rpb25zLCB5b3UgbXVzdCByZXR1cm4gdGhlIGN1cnJlbnQgc3RhdGUuXG4gKiBJZiB0aGUgY3VycmVudCBzdGF0ZSBpcyB1bmRlZmluZWQsIHlvdSBtdXN0IHJldHVybiB0aGUgaW5pdGlhbCBzdGF0ZS5cbiAqIERvIG5vdCByZWZlcmVuY2UgdGhlc2UgYWN0aW9uIHR5cGVzIGRpcmVjdGx5IGluIHlvdXIgY29kZS5cbiAqL1xudmFyIEFjdGlvblR5cGVzID0gZXhwb3J0cy5BY3Rpb25UeXBlcyA9IHtcbiAgSU5JVDogJ0BAcmVkdXgvSU5JVCdcbn07XG5cbi8qKlxuICogQ3JlYXRlcyBhIFJlZHV4IHN0b3JlIHRoYXQgaG9sZHMgdGhlIHN0YXRlIHRyZWUuXG4gKiBUaGUgb25seSB3YXkgdG8gY2hhbmdlIHRoZSBkYXRhIGluIHRoZSBzdG9yZSBpcyB0byBjYWxsIGBkaXNwYXRjaCgpYCBvbiBpdC5cbiAqXG4gKiBUaGVyZSBzaG91bGQgb25seSBiZSBhIHNpbmdsZSBzdG9yZSBpbiB5b3VyIGFwcC4gVG8gc3BlY2lmeSBob3cgZGlmZmVyZW50XG4gKiBwYXJ0cyBvZiB0aGUgc3RhdGUgdHJlZSByZXNwb25kIHRvIGFjdGlvbnMsIHlvdSBtYXkgY29tYmluZSBzZXZlcmFsIHJlZHVjZXJzXG4gKiBpbnRvIGEgc2luZ2xlIHJlZHVjZXIgZnVuY3Rpb24gYnkgdXNpbmcgYGNvbWJpbmVSZWR1Y2Vyc2AuXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gcmVkdWNlciBBIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyB0aGUgbmV4dCBzdGF0ZSB0cmVlLCBnaXZlblxuICogdGhlIGN1cnJlbnQgc3RhdGUgdHJlZSBhbmQgdGhlIGFjdGlvbiB0byBoYW5kbGUuXG4gKlxuICogQHBhcmFtIHthbnl9IFtpbml0aWFsU3RhdGVdIFRoZSBpbml0aWFsIHN0YXRlLiBZb3UgbWF5IG9wdGlvbmFsbHkgc3BlY2lmeSBpdFxuICogdG8gaHlkcmF0ZSB0aGUgc3RhdGUgZnJvbSB0aGUgc2VydmVyIGluIHVuaXZlcnNhbCBhcHBzLCBvciB0byByZXN0b3JlIGFcbiAqIHByZXZpb3VzbHkgc2VyaWFsaXplZCB1c2VyIHNlc3Npb24uXG4gKiBJZiB5b3UgdXNlIGBjb21iaW5lUmVkdWNlcnNgIHRvIHByb2R1Y2UgdGhlIHJvb3QgcmVkdWNlciBmdW5jdGlvbiwgdGhpcyBtdXN0IGJlXG4gKiBhbiBvYmplY3Qgd2l0aCB0aGUgc2FtZSBzaGFwZSBhcyBgY29tYmluZVJlZHVjZXJzYCBrZXlzLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGVuaGFuY2VyIFRoZSBzdG9yZSBlbmhhbmNlci4gWW91IG1heSBvcHRpb25hbGx5IHNwZWNpZnkgaXRcbiAqIHRvIGVuaGFuY2UgdGhlIHN0b3JlIHdpdGggdGhpcmQtcGFydHkgY2FwYWJpbGl0aWVzIHN1Y2ggYXMgbWlkZGxld2FyZSxcbiAqIHRpbWUgdHJhdmVsLCBwZXJzaXN0ZW5jZSwgZXRjLiBUaGUgb25seSBzdG9yZSBlbmhhbmNlciB0aGF0IHNoaXBzIHdpdGggUmVkdXhcbiAqIGlzIGBhcHBseU1pZGRsZXdhcmUoKWAuXG4gKlxuICogQHJldHVybnMge1N0b3JlfSBBIFJlZHV4IHN0b3JlIHRoYXQgbGV0cyB5b3UgcmVhZCB0aGUgc3RhdGUsIGRpc3BhdGNoIGFjdGlvbnNcbiAqIGFuZCBzdWJzY3JpYmUgdG8gY2hhbmdlcy5cbiAqL1xuZnVuY3Rpb24gY3JlYXRlU3RvcmUocmVkdWNlciwgaW5pdGlhbFN0YXRlLCBlbmhhbmNlcikge1xuICB2YXIgX3JlZjI7XG5cbiAgaWYgKHR5cGVvZiBpbml0aWFsU3RhdGUgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIGVuaGFuY2VyID09PSAndW5kZWZpbmVkJykge1xuICAgIGVuaGFuY2VyID0gaW5pdGlhbFN0YXRlO1xuICAgIGluaXRpYWxTdGF0ZSA9IHVuZGVmaW5lZDtcbiAgfVxuXG4gIGlmICh0eXBlb2YgZW5oYW5jZXIgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgaWYgKHR5cGVvZiBlbmhhbmNlciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdFeHBlY3RlZCB0aGUgZW5oYW5jZXIgdG8gYmUgYSBmdW5jdGlvbi4nKTtcbiAgICB9XG5cbiAgICByZXR1cm4gZW5oYW5jZXIoY3JlYXRlU3RvcmUpKHJlZHVjZXIsIGluaXRpYWxTdGF0ZSk7XG4gIH1cblxuICBpZiAodHlwZW9mIHJlZHVjZXIgIT09ICdmdW5jdGlvbicpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0V4cGVjdGVkIHRoZSByZWR1Y2VyIHRvIGJlIGEgZnVuY3Rpb24uJyk7XG4gIH1cblxuICB2YXIgY3VycmVudFJlZHVjZXIgPSByZWR1Y2VyO1xuICB2YXIgY3VycmVudFN0YXRlID0gaW5pdGlhbFN0YXRlO1xuICB2YXIgY3VycmVudExpc3RlbmVycyA9IFtdO1xuICB2YXIgbmV4dExpc3RlbmVycyA9IGN1cnJlbnRMaXN0ZW5lcnM7XG4gIHZhciBpc0Rpc3BhdGNoaW5nID0gZmFsc2U7XG5cbiAgZnVuY3Rpb24gZW5zdXJlQ2FuTXV0YXRlTmV4dExpc3RlbmVycygpIHtcbiAgICBpZiAobmV4dExpc3RlbmVycyA9PT0gY3VycmVudExpc3RlbmVycykge1xuICAgICAgbmV4dExpc3RlbmVycyA9IGN1cnJlbnRMaXN0ZW5lcnMuc2xpY2UoKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmVhZHMgdGhlIHN0YXRlIHRyZWUgbWFuYWdlZCBieSB0aGUgc3RvcmUuXG4gICAqXG4gICAqIEByZXR1cm5zIHthbnl9IFRoZSBjdXJyZW50IHN0YXRlIHRyZWUgb2YgeW91ciBhcHBsaWNhdGlvbi5cbiAgICovXG4gIGZ1bmN0aW9uIGdldFN0YXRlKCkge1xuICAgIHJldHVybiBjdXJyZW50U3RhdGU7XG4gIH1cblxuICAvKipcbiAgICogQWRkcyBhIGNoYW5nZSBsaXN0ZW5lci4gSXQgd2lsbCBiZSBjYWxsZWQgYW55IHRpbWUgYW4gYWN0aW9uIGlzIGRpc3BhdGNoZWQsXG4gICAqIGFuZCBzb21lIHBhcnQgb2YgdGhlIHN0YXRlIHRyZWUgbWF5IHBvdGVudGlhbGx5IGhhdmUgY2hhbmdlZC4gWW91IG1heSB0aGVuXG4gICAqIGNhbGwgYGdldFN0YXRlKClgIHRvIHJlYWQgdGhlIGN1cnJlbnQgc3RhdGUgdHJlZSBpbnNpZGUgdGhlIGNhbGxiYWNrLlxuICAgKlxuICAgKiBZb3UgbWF5IGNhbGwgYGRpc3BhdGNoKClgIGZyb20gYSBjaGFuZ2UgbGlzdGVuZXIsIHdpdGggdGhlIGZvbGxvd2luZ1xuICAgKiBjYXZlYXRzOlxuICAgKlxuICAgKiAxLiBUaGUgc3Vic2NyaXB0aW9ucyBhcmUgc25hcHNob3R0ZWQganVzdCBiZWZvcmUgZXZlcnkgYGRpc3BhdGNoKClgIGNhbGwuXG4gICAqIElmIHlvdSBzdWJzY3JpYmUgb3IgdW5zdWJzY3JpYmUgd2hpbGUgdGhlIGxpc3RlbmVycyBhcmUgYmVpbmcgaW52b2tlZCwgdGhpc1xuICAgKiB3aWxsIG5vdCBoYXZlIGFueSBlZmZlY3Qgb24gdGhlIGBkaXNwYXRjaCgpYCB0aGF0IGlzIGN1cnJlbnRseSBpbiBwcm9ncmVzcy5cbiAgICogSG93ZXZlciwgdGhlIG5leHQgYGRpc3BhdGNoKClgIGNhbGwsIHdoZXRoZXIgbmVzdGVkIG9yIG5vdCwgd2lsbCB1c2UgYSBtb3JlXG4gICAqIHJlY2VudCBzbmFwc2hvdCBvZiB0aGUgc3Vic2NyaXB0aW9uIGxpc3QuXG4gICAqXG4gICAqIDIuIFRoZSBsaXN0ZW5lciBzaG91bGQgbm90IGV4cGVjdCB0byBzZWUgYWxsIHN0YXRlIGNoYW5nZXMsIGFzIHRoZSBzdGF0ZVxuICAgKiBtaWdodCBoYXZlIGJlZW4gdXBkYXRlZCBtdWx0aXBsZSB0aW1lcyBkdXJpbmcgYSBuZXN0ZWQgYGRpc3BhdGNoKClgIGJlZm9yZVxuICAgKiB0aGUgbGlzdGVuZXIgaXMgY2FsbGVkLiBJdCBpcywgaG93ZXZlciwgZ3VhcmFudGVlZCB0aGF0IGFsbCBzdWJzY3JpYmVyc1xuICAgKiByZWdpc3RlcmVkIGJlZm9yZSB0aGUgYGRpc3BhdGNoKClgIHN0YXJ0ZWQgd2lsbCBiZSBjYWxsZWQgd2l0aCB0aGUgbGF0ZXN0XG4gICAqIHN0YXRlIGJ5IHRoZSB0aW1lIGl0IGV4aXRzLlxuICAgKlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBsaXN0ZW5lciBBIGNhbGxiYWNrIHRvIGJlIGludm9rZWQgb24gZXZlcnkgZGlzcGF0Y2guXG4gICAqIEByZXR1cm5zIHtGdW5jdGlvbn0gQSBmdW5jdGlvbiB0byByZW1vdmUgdGhpcyBjaGFuZ2UgbGlzdGVuZXIuXG4gICAqL1xuICBmdW5jdGlvbiBzdWJzY3JpYmUobGlzdGVuZXIpIHtcbiAgICBpZiAodHlwZW9mIGxpc3RlbmVyICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0V4cGVjdGVkIGxpc3RlbmVyIHRvIGJlIGEgZnVuY3Rpb24uJyk7XG4gICAgfVxuXG4gICAgdmFyIGlzU3Vic2NyaWJlZCA9IHRydWU7XG5cbiAgICBlbnN1cmVDYW5NdXRhdGVOZXh0TGlzdGVuZXJzKCk7XG4gICAgbmV4dExpc3RlbmVycy5wdXNoKGxpc3RlbmVyKTtcblxuICAgIHJldHVybiBmdW5jdGlvbiB1bnN1YnNjcmliZSgpIHtcbiAgICAgIGlmICghaXNTdWJzY3JpYmVkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaXNTdWJzY3JpYmVkID0gZmFsc2U7XG5cbiAgICAgIGVuc3VyZUNhbk11dGF0ZU5leHRMaXN0ZW5lcnMoKTtcbiAgICAgIHZhciBpbmRleCA9IG5leHRMaXN0ZW5lcnMuaW5kZXhPZihsaXN0ZW5lcik7XG4gICAgICBuZXh0TGlzdGVuZXJzLnNwbGljZShpbmRleCwgMSk7XG4gICAgfTtcbiAgfVxuXG4gIC8qKlxuICAgKiBEaXNwYXRjaGVzIGFuIGFjdGlvbi4gSXQgaXMgdGhlIG9ubHkgd2F5IHRvIHRyaWdnZXIgYSBzdGF0ZSBjaGFuZ2UuXG4gICAqXG4gICAqIFRoZSBgcmVkdWNlcmAgZnVuY3Rpb24sIHVzZWQgdG8gY3JlYXRlIHRoZSBzdG9yZSwgd2lsbCBiZSBjYWxsZWQgd2l0aCB0aGVcbiAgICogY3VycmVudCBzdGF0ZSB0cmVlIGFuZCB0aGUgZ2l2ZW4gYGFjdGlvbmAuIEl0cyByZXR1cm4gdmFsdWUgd2lsbFxuICAgKiBiZSBjb25zaWRlcmVkIHRoZSAqKm5leHQqKiBzdGF0ZSBvZiB0aGUgdHJlZSwgYW5kIHRoZSBjaGFuZ2UgbGlzdGVuZXJzXG4gICAqIHdpbGwgYmUgbm90aWZpZWQuXG4gICAqXG4gICAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9ubHkgc3VwcG9ydHMgcGxhaW4gb2JqZWN0IGFjdGlvbnMuIElmIHlvdSB3YW50IHRvXG4gICAqIGRpc3BhdGNoIGEgUHJvbWlzZSwgYW4gT2JzZXJ2YWJsZSwgYSB0aHVuaywgb3Igc29tZXRoaW5nIGVsc2UsIHlvdSBuZWVkIHRvXG4gICAqIHdyYXAgeW91ciBzdG9yZSBjcmVhdGluZyBmdW5jdGlvbiBpbnRvIHRoZSBjb3JyZXNwb25kaW5nIG1pZGRsZXdhcmUuIEZvclxuICAgKiBleGFtcGxlLCBzZWUgdGhlIGRvY3VtZW50YXRpb24gZm9yIHRoZSBgcmVkdXgtdGh1bmtgIHBhY2thZ2UuIEV2ZW4gdGhlXG4gICAqIG1pZGRsZXdhcmUgd2lsbCBldmVudHVhbGx5IGRpc3BhdGNoIHBsYWluIG9iamVjdCBhY3Rpb25zIHVzaW5nIHRoaXMgbWV0aG9kLlxuICAgKlxuICAgKiBAcGFyYW0ge09iamVjdH0gYWN0aW9uIEEgcGxhaW4gb2JqZWN0IHJlcHJlc2VudGluZyDigJx3aGF0IGNoYW5nZWTigJ0uIEl0IGlzXG4gICAqIGEgZ29vZCBpZGVhIHRvIGtlZXAgYWN0aW9ucyBzZXJpYWxpemFibGUgc28geW91IGNhbiByZWNvcmQgYW5kIHJlcGxheSB1c2VyXG4gICAqIHNlc3Npb25zLCBvciB1c2UgdGhlIHRpbWUgdHJhdmVsbGluZyBgcmVkdXgtZGV2dG9vbHNgLiBBbiBhY3Rpb24gbXVzdCBoYXZlXG4gICAqIGEgYHR5cGVgIHByb3BlcnR5IHdoaWNoIG1heSBub3QgYmUgYHVuZGVmaW5lZGAuIEl0IGlzIGEgZ29vZCBpZGVhIHRvIHVzZVxuICAgKiBzdHJpbmcgY29uc3RhbnRzIGZvciBhY3Rpb24gdHlwZXMuXG4gICAqXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IEZvciBjb252ZW5pZW5jZSwgdGhlIHNhbWUgYWN0aW9uIG9iamVjdCB5b3UgZGlzcGF0Y2hlZC5cbiAgICpcbiAgICogTm90ZSB0aGF0LCBpZiB5b3UgdXNlIGEgY3VzdG9tIG1pZGRsZXdhcmUsIGl0IG1heSB3cmFwIGBkaXNwYXRjaCgpYCB0b1xuICAgKiByZXR1cm4gc29tZXRoaW5nIGVsc2UgKGZvciBleGFtcGxlLCBhIFByb21pc2UgeW91IGNhbiBhd2FpdCkuXG4gICAqL1xuICBmdW5jdGlvbiBkaXNwYXRjaChhY3Rpb24pIHtcbiAgICBpZiAoISgwLCBfaXNQbGFpbk9iamVjdDJbXCJkZWZhdWx0XCJdKShhY3Rpb24pKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0FjdGlvbnMgbXVzdCBiZSBwbGFpbiBvYmplY3RzLiAnICsgJ1VzZSBjdXN0b20gbWlkZGxld2FyZSBmb3IgYXN5bmMgYWN0aW9ucy4nKTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIGFjdGlvbi50eXBlID09PSAndW5kZWZpbmVkJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdBY3Rpb25zIG1heSBub3QgaGF2ZSBhbiB1bmRlZmluZWQgXCJ0eXBlXCIgcHJvcGVydHkuICcgKyAnSGF2ZSB5b3UgbWlzc3BlbGxlZCBhIGNvbnN0YW50PycpO1xuICAgIH1cblxuICAgIGlmIChpc0Rpc3BhdGNoaW5nKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1JlZHVjZXJzIG1heSBub3QgZGlzcGF0Y2ggYWN0aW9ucy4nKTtcbiAgICB9XG5cbiAgICB0cnkge1xuICAgICAgaXNEaXNwYXRjaGluZyA9IHRydWU7XG4gICAgICBjdXJyZW50U3RhdGUgPSBjdXJyZW50UmVkdWNlcihjdXJyZW50U3RhdGUsIGFjdGlvbik7XG4gICAgfSBmaW5hbGx5IHtcbiAgICAgIGlzRGlzcGF0Y2hpbmcgPSBmYWxzZTtcbiAgICB9XG5cbiAgICB2YXIgbGlzdGVuZXJzID0gY3VycmVudExpc3RlbmVycyA9IG5leHRMaXN0ZW5lcnM7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0ZW5lcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGxpc3RlbmVyc1tpXSgpO1xuICAgIH1cblxuICAgIHJldHVybiBhY3Rpb247XG4gIH1cblxuICAvKipcbiAgICogUmVwbGFjZXMgdGhlIHJlZHVjZXIgY3VycmVudGx5IHVzZWQgYnkgdGhlIHN0b3JlIHRvIGNhbGN1bGF0ZSB0aGUgc3RhdGUuXG4gICAqXG4gICAqIFlvdSBtaWdodCBuZWVkIHRoaXMgaWYgeW91ciBhcHAgaW1wbGVtZW50cyBjb2RlIHNwbGl0dGluZyBhbmQgeW91IHdhbnQgdG9cbiAgICogbG9hZCBzb21lIG9mIHRoZSByZWR1Y2VycyBkeW5hbWljYWxseS4gWW91IG1pZ2h0IGFsc28gbmVlZCB0aGlzIGlmIHlvdVxuICAgKiBpbXBsZW1lbnQgYSBob3QgcmVsb2FkaW5nIG1lY2hhbmlzbSBmb3IgUmVkdXguXG4gICAqXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IG5leHRSZWR1Y2VyIFRoZSByZWR1Y2VyIGZvciB0aGUgc3RvcmUgdG8gdXNlIGluc3RlYWQuXG4gICAqIEByZXR1cm5zIHt2b2lkfVxuICAgKi9cbiAgZnVuY3Rpb24gcmVwbGFjZVJlZHVjZXIobmV4dFJlZHVjZXIpIHtcbiAgICBpZiAodHlwZW9mIG5leHRSZWR1Y2VyICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0V4cGVjdGVkIHRoZSBuZXh0UmVkdWNlciB0byBiZSBhIGZ1bmN0aW9uLicpO1xuICAgIH1cblxuICAgIGN1cnJlbnRSZWR1Y2VyID0gbmV4dFJlZHVjZXI7XG4gICAgZGlzcGF0Y2goeyB0eXBlOiBBY3Rpb25UeXBlcy5JTklUIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIEludGVyb3BlcmFiaWxpdHkgcG9pbnQgZm9yIG9ic2VydmFibGUvcmVhY3RpdmUgbGlicmFyaWVzLlxuICAgKiBAcmV0dXJucyB7b2JzZXJ2YWJsZX0gQSBtaW5pbWFsIG9ic2VydmFibGUgb2Ygc3RhdGUgY2hhbmdlcy5cbiAgICogRm9yIG1vcmUgaW5mb3JtYXRpb24sIHNlZSB0aGUgb2JzZXJ2YWJsZSBwcm9wb3NhbDpcbiAgICogaHR0cHM6Ly9naXRodWIuY29tL3plbnBhcnNpbmcvZXMtb2JzZXJ2YWJsZVxuICAgKi9cbiAgZnVuY3Rpb24gb2JzZXJ2YWJsZSgpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBvdXRlclN1YnNjcmliZSA9IHN1YnNjcmliZTtcbiAgICByZXR1cm4gX3JlZiA9IHtcbiAgICAgIC8qKlxuICAgICAgICogVGhlIG1pbmltYWwgb2JzZXJ2YWJsZSBzdWJzY3JpcHRpb24gbWV0aG9kLlxuICAgICAgICogQHBhcmFtIHtPYmplY3R9IG9ic2VydmVyIEFueSBvYmplY3QgdGhhdCBjYW4gYmUgdXNlZCBhcyBhbiBvYnNlcnZlci5cbiAgICAgICAqIFRoZSBvYnNlcnZlciBvYmplY3Qgc2hvdWxkIGhhdmUgYSBgbmV4dGAgbWV0aG9kLlxuICAgICAgICogQHJldHVybnMge3N1YnNjcmlwdGlvbn0gQW4gb2JqZWN0IHdpdGggYW4gYHVuc3Vic2NyaWJlYCBtZXRob2QgdGhhdCBjYW5cbiAgICAgICAqIGJlIHVzZWQgdG8gdW5zdWJzY3JpYmUgdGhlIG9ic2VydmFibGUgZnJvbSB0aGUgc3RvcmUsIGFuZCBwcmV2ZW50IGZ1cnRoZXJcbiAgICAgICAqIGVtaXNzaW9uIG9mIHZhbHVlcyBmcm9tIHRoZSBvYnNlcnZhYmxlLlxuICAgICAgICovXG5cbiAgICAgIHN1YnNjcmliZTogZnVuY3Rpb24gc3Vic2NyaWJlKG9ic2VydmVyKSB7XG4gICAgICAgIGlmICh0eXBlb2Ygb2JzZXJ2ZXIgIT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRXhwZWN0ZWQgdGhlIG9ic2VydmVyIHRvIGJlIGFuIG9iamVjdC4nKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIG9ic2VydmVTdGF0ZSgpIHtcbiAgICAgICAgICBpZiAob2JzZXJ2ZXIubmV4dCkge1xuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChnZXRTdGF0ZSgpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBvYnNlcnZlU3RhdGUoKTtcbiAgICAgICAgdmFyIHVuc3Vic2NyaWJlID0gb3V0ZXJTdWJzY3JpYmUob2JzZXJ2ZVN0YXRlKTtcbiAgICAgICAgcmV0dXJuIHsgdW5zdWJzY3JpYmU6IHVuc3Vic2NyaWJlIH07XG4gICAgICB9XG4gICAgfSwgX3JlZltfc3ltYm9sT2JzZXJ2YWJsZTJbXCJkZWZhdWx0XCJdXSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sIF9yZWY7XG4gIH1cblxuICAvLyBXaGVuIGEgc3RvcmUgaXMgY3JlYXRlZCwgYW4gXCJJTklUXCIgYWN0aW9uIGlzIGRpc3BhdGNoZWQgc28gdGhhdCBldmVyeVxuICAvLyByZWR1Y2VyIHJldHVybnMgdGhlaXIgaW5pdGlhbCBzdGF0ZS4gVGhpcyBlZmZlY3RpdmVseSBwb3B1bGF0ZXNcbiAgLy8gdGhlIGluaXRpYWwgc3RhdGUgdHJlZS5cbiAgZGlzcGF0Y2goeyB0eXBlOiBBY3Rpb25UeXBlcy5JTklUIH0pO1xuXG4gIHJldHVybiBfcmVmMiA9IHtcbiAgICBkaXNwYXRjaDogZGlzcGF0Y2gsXG4gICAgc3Vic2NyaWJlOiBzdWJzY3JpYmUsXG4gICAgZ2V0U3RhdGU6IGdldFN0YXRlLFxuICAgIHJlcGxhY2VSZWR1Y2VyOiByZXBsYWNlUmVkdWNlclxuICB9LCBfcmVmMltfc3ltYm9sT2JzZXJ2YWJsZTJbXCJkZWZhdWx0XCJdXSA9IG9ic2VydmFibGUsIF9yZWYyO1xufVxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vcmVkdXgvbGliL2NyZWF0ZVN0b3JlLmpzXG4gKiovIiwidmFyIGdldFByb3RvdHlwZSA9IHJlcXVpcmUoJy4vX2dldFByb3RvdHlwZScpLFxuICAgIGlzSG9zdE9iamVjdCA9IHJlcXVpcmUoJy4vX2lzSG9zdE9iamVjdCcpLFxuICAgIGlzT2JqZWN0TGlrZSA9IHJlcXVpcmUoJy4vaXNPYmplY3RMaWtlJyk7XG5cbi8qKiBgT2JqZWN0I3RvU3RyaW5nYCByZXN1bHQgcmVmZXJlbmNlcy4gKi9cbnZhciBvYmplY3RUYWcgPSAnW29iamVjdCBPYmplY3RdJztcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIGZ1bmNQcm90byA9IEZ1bmN0aW9uLnByb3RvdHlwZSxcbiAgICBvYmplY3RQcm90byA9IE9iamVjdC5wcm90b3R5cGU7XG5cbi8qKiBVc2VkIHRvIHJlc29sdmUgdGhlIGRlY29tcGlsZWQgc291cmNlIG9mIGZ1bmN0aW9ucy4gKi9cbnZhciBmdW5jVG9TdHJpbmcgPSBmdW5jUHJvdG8udG9TdHJpbmc7XG5cbi8qKiBVc2VkIHRvIGNoZWNrIG9iamVjdHMgZm9yIG93biBwcm9wZXJ0aWVzLiAqL1xudmFyIGhhc093blByb3BlcnR5ID0gb2JqZWN0UHJvdG8uaGFzT3duUHJvcGVydHk7XG5cbi8qKiBVc2VkIHRvIGluZmVyIHRoZSBgT2JqZWN0YCBjb25zdHJ1Y3Rvci4gKi9cbnZhciBvYmplY3RDdG9yU3RyaW5nID0gZnVuY1RvU3RyaW5nLmNhbGwoT2JqZWN0KTtcblxuLyoqXG4gKiBVc2VkIHRvIHJlc29sdmUgdGhlXG4gKiBbYHRvU3RyaW5nVGFnYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNy4wLyNzZWMtb2JqZWN0LnByb3RvdHlwZS50b3N0cmluZylcbiAqIG9mIHZhbHVlcy5cbiAqL1xudmFyIG9iamVjdFRvU3RyaW5nID0gb2JqZWN0UHJvdG8udG9TdHJpbmc7XG5cbi8qKlxuICogQ2hlY2tzIGlmIGB2YWx1ZWAgaXMgYSBwbGFpbiBvYmplY3QsIHRoYXQgaXMsIGFuIG9iamVjdCBjcmVhdGVkIGJ5IHRoZVxuICogYE9iamVjdGAgY29uc3RydWN0b3Igb3Igb25lIHdpdGggYSBgW1tQcm90b3R5cGVdXWAgb2YgYG51bGxgLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgMC44LjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBjaGVjay5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm5zIGB0cnVlYCBpZiBgdmFsdWVgIGlzIGEgcGxhaW4gb2JqZWN0LCBlbHNlIGBmYWxzZWAuXG4gKiBAZXhhbXBsZVxuICpcbiAqIGZ1bmN0aW9uIEZvbygpIHtcbiAqICAgdGhpcy5hID0gMTtcbiAqIH1cbiAqXG4gKiBfLmlzUGxhaW5PYmplY3QobmV3IEZvbyk7XG4gKiAvLyA9PiBmYWxzZVxuICpcbiAqIF8uaXNQbGFpbk9iamVjdChbMSwgMiwgM10pO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzUGxhaW5PYmplY3QoeyAneCc6IDAsICd5JzogMCB9KTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzUGxhaW5PYmplY3QoT2JqZWN0LmNyZWF0ZShudWxsKSk7XG4gKiAvLyA9PiB0cnVlXG4gKi9cbmZ1bmN0aW9uIGlzUGxhaW5PYmplY3QodmFsdWUpIHtcbiAgaWYgKCFpc09iamVjdExpa2UodmFsdWUpIHx8XG4gICAgICBvYmplY3RUb1N0cmluZy5jYWxsKHZhbHVlKSAhPSBvYmplY3RUYWcgfHwgaXNIb3N0T2JqZWN0KHZhbHVlKSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICB2YXIgcHJvdG8gPSBnZXRQcm90b3R5cGUodmFsdWUpO1xuICBpZiAocHJvdG8gPT09IG51bGwpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuICB2YXIgQ3RvciA9IGhhc093blByb3BlcnR5LmNhbGwocHJvdG8sICdjb25zdHJ1Y3RvcicpICYmIHByb3RvLmNvbnN0cnVjdG9yO1xuICByZXR1cm4gKHR5cGVvZiBDdG9yID09ICdmdW5jdGlvbicgJiZcbiAgICBDdG9yIGluc3RhbmNlb2YgQ3RvciAmJiBmdW5jVG9TdHJpbmcuY2FsbChDdG9yKSA9PSBvYmplY3RDdG9yU3RyaW5nKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc1BsYWluT2JqZWN0O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9sb2Rhc2gvaXNQbGFpbk9iamVjdC5qc1xuICoqLyIsInZhciBvdmVyQXJnID0gcmVxdWlyZSgnLi9fb3ZlckFyZycpO1xuXG4vKiogQnVpbHQtaW4gdmFsdWUgcmVmZXJlbmNlcy4gKi9cbnZhciBnZXRQcm90b3R5cGUgPSBvdmVyQXJnKE9iamVjdC5nZXRQcm90b3R5cGVPZiwgT2JqZWN0KTtcblxubW9kdWxlLmV4cG9ydHMgPSBnZXRQcm90b3R5cGU7XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L2xvZGFzaC9fZ2V0UHJvdG90eXBlLmpzXG4gKiovIiwiLyoqXG4gKiBDcmVhdGVzIGEgdW5hcnkgZnVuY3Rpb24gdGhhdCBpbnZva2VzIGBmdW5jYCB3aXRoIGl0cyBhcmd1bWVudCB0cmFuc2Zvcm1lZC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gZnVuYyBUaGUgZnVuY3Rpb24gdG8gd3JhcC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IHRyYW5zZm9ybSBUaGUgYXJndW1lbnQgdHJhbnNmb3JtLlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBSZXR1cm5zIHRoZSBuZXcgZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIG92ZXJBcmcoZnVuYywgdHJhbnNmb3JtKSB7XG4gIHJldHVybiBmdW5jdGlvbihhcmcpIHtcbiAgICByZXR1cm4gZnVuYyh0cmFuc2Zvcm0oYXJnKSk7XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gb3ZlckFyZztcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vbG9kYXNoL19vdmVyQXJnLmpzXG4gKiovIiwiLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBhIGhvc3Qgb2JqZWN0IGluIElFIDwgOS5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBhIGhvc3Qgb2JqZWN0LCBlbHNlIGBmYWxzZWAuXG4gKi9cbmZ1bmN0aW9uIGlzSG9zdE9iamVjdCh2YWx1ZSkge1xuICAvLyBNYW55IGhvc3Qgb2JqZWN0cyBhcmUgYE9iamVjdGAgb2JqZWN0cyB0aGF0IGNhbiBjb2VyY2UgdG8gc3RyaW5nc1xuICAvLyBkZXNwaXRlIGhhdmluZyBpbXByb3Blcmx5IGRlZmluZWQgYHRvU3RyaW5nYCBtZXRob2RzLlxuICB2YXIgcmVzdWx0ID0gZmFsc2U7XG4gIGlmICh2YWx1ZSAhPSBudWxsICYmIHR5cGVvZiB2YWx1ZS50b1N0cmluZyAhPSAnZnVuY3Rpb24nKSB7XG4gICAgdHJ5IHtcbiAgICAgIHJlc3VsdCA9ICEhKHZhbHVlICsgJycpO1xuICAgIH0gY2F0Y2ggKGUpIHt9XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpc0hvc3RPYmplY3Q7XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L2xvZGFzaC9faXNIb3N0T2JqZWN0LmpzXG4gKiovIiwiLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBvYmplY3QtbGlrZS4gQSB2YWx1ZSBpcyBvYmplY3QtbGlrZSBpZiBpdCdzIG5vdCBgbnVsbGBcbiAqIGFuZCBoYXMgYSBgdHlwZW9mYCByZXN1bHQgb2YgXCJvYmplY3RcIi5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMC4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBvYmplY3QtbGlrZSwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZSh7fSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc09iamVjdExpa2UoWzEsIDIsIDNdKTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZShfLm5vb3ApO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZShudWxsKTtcbiAqIC8vID0+IGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzT2JqZWN0TGlrZSh2YWx1ZSkge1xuICByZXR1cm4gISF2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT0gJ29iamVjdCc7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXNPYmplY3RMaWtlO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9sb2Rhc2gvaXNPYmplY3RMaWtlLmpzXG4gKiovIiwiLyogZ2xvYmFsIHdpbmRvdyAqL1xuJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vcG9ueWZpbGwnKShnbG9iYWwgfHwgd2luZG93IHx8IHRoaXMpO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9zeW1ib2wtb2JzZXJ2YWJsZS9pbmRleC5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzeW1ib2xPYnNlcnZhYmxlUG9ueWZpbGwocm9vdCkge1xuXHR2YXIgcmVzdWx0O1xuXHR2YXIgU3ltYm9sID0gcm9vdC5TeW1ib2w7XG5cblx0aWYgKHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicpIHtcblx0XHRpZiAoU3ltYm9sLm9ic2VydmFibGUpIHtcblx0XHRcdHJlc3VsdCA9IFN5bWJvbC5vYnNlcnZhYmxlO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRyZXN1bHQgPSBTeW1ib2woJ29ic2VydmFibGUnKTtcblx0XHRcdFN5bWJvbC5vYnNlcnZhYmxlID0gcmVzdWx0O1xuXHRcdH1cblx0fSBlbHNlIHtcblx0XHRyZXN1bHQgPSAnQEBvYnNlcnZhYmxlJztcblx0fVxuXG5cdHJldHVybiByZXN1bHQ7XG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9zeW1ib2wtb2JzZXJ2YWJsZS9wb255ZmlsbC5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gY29tYmluZVJlZHVjZXJzO1xuXG52YXIgX2NyZWF0ZVN0b3JlID0gcmVxdWlyZSgnLi9jcmVhdGVTdG9yZScpO1xuXG52YXIgX2lzUGxhaW5PYmplY3QgPSByZXF1aXJlKCdsb2Rhc2gvaXNQbGFpbk9iamVjdCcpO1xuXG52YXIgX2lzUGxhaW5PYmplY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNQbGFpbk9iamVjdCk7XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJy4vdXRpbHMvd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IFwiZGVmYXVsdFwiOiBvYmogfTsgfVxuXG5mdW5jdGlvbiBnZXRVbmRlZmluZWRTdGF0ZUVycm9yTWVzc2FnZShrZXksIGFjdGlvbikge1xuICB2YXIgYWN0aW9uVHlwZSA9IGFjdGlvbiAmJiBhY3Rpb24udHlwZTtcbiAgdmFyIGFjdGlvbk5hbWUgPSBhY3Rpb25UeXBlICYmICdcIicgKyBhY3Rpb25UeXBlLnRvU3RyaW5nKCkgKyAnXCInIHx8ICdhbiBhY3Rpb24nO1xuXG4gIHJldHVybiAnR2l2ZW4gYWN0aW9uICcgKyBhY3Rpb25OYW1lICsgJywgcmVkdWNlciBcIicgKyBrZXkgKyAnXCIgcmV0dXJuZWQgdW5kZWZpbmVkLiAnICsgJ1RvIGlnbm9yZSBhbiBhY3Rpb24sIHlvdSBtdXN0IGV4cGxpY2l0bHkgcmV0dXJuIHRoZSBwcmV2aW91cyBzdGF0ZS4nO1xufVxuXG5mdW5jdGlvbiBnZXRVbmV4cGVjdGVkU3RhdGVTaGFwZVdhcm5pbmdNZXNzYWdlKGlucHV0U3RhdGUsIHJlZHVjZXJzLCBhY3Rpb24pIHtcbiAgdmFyIHJlZHVjZXJLZXlzID0gT2JqZWN0LmtleXMocmVkdWNlcnMpO1xuICB2YXIgYXJndW1lbnROYW1lID0gYWN0aW9uICYmIGFjdGlvbi50eXBlID09PSBfY3JlYXRlU3RvcmUuQWN0aW9uVHlwZXMuSU5JVCA/ICdpbml0aWFsU3RhdGUgYXJndW1lbnQgcGFzc2VkIHRvIGNyZWF0ZVN0b3JlJyA6ICdwcmV2aW91cyBzdGF0ZSByZWNlaXZlZCBieSB0aGUgcmVkdWNlcic7XG5cbiAgaWYgKHJlZHVjZXJLZXlzLmxlbmd0aCA9PT0gMCkge1xuICAgIHJldHVybiAnU3RvcmUgZG9lcyBub3QgaGF2ZSBhIHZhbGlkIHJlZHVjZXIuIE1ha2Ugc3VyZSB0aGUgYXJndW1lbnQgcGFzc2VkICcgKyAndG8gY29tYmluZVJlZHVjZXJzIGlzIGFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIHJlZHVjZXJzLic7XG4gIH1cblxuICBpZiAoISgwLCBfaXNQbGFpbk9iamVjdDJbXCJkZWZhdWx0XCJdKShpbnB1dFN0YXRlKSkge1xuICAgIHJldHVybiAnVGhlICcgKyBhcmd1bWVudE5hbWUgKyAnIGhhcyB1bmV4cGVjdGVkIHR5cGUgb2YgXCInICsge30udG9TdHJpbmcuY2FsbChpbnB1dFN0YXRlKS5tYXRjaCgvXFxzKFthLXp8QS1aXSspLylbMV0gKyAnXCIuIEV4cGVjdGVkIGFyZ3VtZW50IHRvIGJlIGFuIG9iamVjdCB3aXRoIHRoZSBmb2xsb3dpbmcgJyArICgna2V5czogXCInICsgcmVkdWNlcktleXMuam9pbignXCIsIFwiJykgKyAnXCInKTtcbiAgfVxuXG4gIHZhciB1bmV4cGVjdGVkS2V5cyA9IE9iamVjdC5rZXlzKGlucHV0U3RhdGUpLmZpbHRlcihmdW5jdGlvbiAoa2V5KSB7XG4gICAgcmV0dXJuICFyZWR1Y2Vycy5oYXNPd25Qcm9wZXJ0eShrZXkpO1xuICB9KTtcblxuICBpZiAodW5leHBlY3RlZEtleXMubGVuZ3RoID4gMCkge1xuICAgIHJldHVybiAnVW5leHBlY3RlZCAnICsgKHVuZXhwZWN0ZWRLZXlzLmxlbmd0aCA+IDEgPyAna2V5cycgOiAna2V5JykgKyAnICcgKyAoJ1wiJyArIHVuZXhwZWN0ZWRLZXlzLmpvaW4oJ1wiLCBcIicpICsgJ1wiIGZvdW5kIGluICcgKyBhcmd1bWVudE5hbWUgKyAnLiAnKSArICdFeHBlY3RlZCB0byBmaW5kIG9uZSBvZiB0aGUga25vd24gcmVkdWNlciBrZXlzIGluc3RlYWQ6ICcgKyAoJ1wiJyArIHJlZHVjZXJLZXlzLmpvaW4oJ1wiLCBcIicpICsgJ1wiLiBVbmV4cGVjdGVkIGtleXMgd2lsbCBiZSBpZ25vcmVkLicpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGFzc2VydFJlZHVjZXJTYW5pdHkocmVkdWNlcnMpIHtcbiAgT2JqZWN0LmtleXMocmVkdWNlcnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIHZhciByZWR1Y2VyID0gcmVkdWNlcnNba2V5XTtcbiAgICB2YXIgaW5pdGlhbFN0YXRlID0gcmVkdWNlcih1bmRlZmluZWQsIHsgdHlwZTogX2NyZWF0ZVN0b3JlLkFjdGlvblR5cGVzLklOSVQgfSk7XG5cbiAgICBpZiAodHlwZW9mIGluaXRpYWxTdGF0ZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignUmVkdWNlciBcIicgKyBrZXkgKyAnXCIgcmV0dXJuZWQgdW5kZWZpbmVkIGR1cmluZyBpbml0aWFsaXphdGlvbi4gJyArICdJZiB0aGUgc3RhdGUgcGFzc2VkIHRvIHRoZSByZWR1Y2VyIGlzIHVuZGVmaW5lZCwgeW91IG11c3QgJyArICdleHBsaWNpdGx5IHJldHVybiB0aGUgaW5pdGlhbCBzdGF0ZS4gVGhlIGluaXRpYWwgc3RhdGUgbWF5ICcgKyAnbm90IGJlIHVuZGVmaW5lZC4nKTtcbiAgICB9XG5cbiAgICB2YXIgdHlwZSA9ICdAQHJlZHV4L1BST0JFX1VOS05PV05fQUNUSU9OXycgKyBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5zdWJzdHJpbmcoNykuc3BsaXQoJycpLmpvaW4oJy4nKTtcbiAgICBpZiAodHlwZW9mIHJlZHVjZXIodW5kZWZpbmVkLCB7IHR5cGU6IHR5cGUgfSkgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1JlZHVjZXIgXCInICsga2V5ICsgJ1wiIHJldHVybmVkIHVuZGVmaW5lZCB3aGVuIHByb2JlZCB3aXRoIGEgcmFuZG9tIHR5cGUuICcgKyAoJ0RvblxcJ3QgdHJ5IHRvIGhhbmRsZSAnICsgX2NyZWF0ZVN0b3JlLkFjdGlvblR5cGVzLklOSVQgKyAnIG9yIG90aGVyIGFjdGlvbnMgaW4gXCJyZWR1eC8qXCIgJykgKyAnbmFtZXNwYWNlLiBUaGV5IGFyZSBjb25zaWRlcmVkIHByaXZhdGUuIEluc3RlYWQsIHlvdSBtdXN0IHJldHVybiB0aGUgJyArICdjdXJyZW50IHN0YXRlIGZvciBhbnkgdW5rbm93biBhY3Rpb25zLCB1bmxlc3MgaXQgaXMgdW5kZWZpbmVkLCAnICsgJ2luIHdoaWNoIGNhc2UgeW91IG11c3QgcmV0dXJuIHRoZSBpbml0aWFsIHN0YXRlLCByZWdhcmRsZXNzIG9mIHRoZSAnICsgJ2FjdGlvbiB0eXBlLiBUaGUgaW5pdGlhbCBzdGF0ZSBtYXkgbm90IGJlIHVuZGVmaW5lZC4nKTtcbiAgICB9XG4gIH0pO1xufVxuXG4vKipcbiAqIFR1cm5zIGFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIGRpZmZlcmVudCByZWR1Y2VyIGZ1bmN0aW9ucywgaW50byBhIHNpbmdsZVxuICogcmVkdWNlciBmdW5jdGlvbi4gSXQgd2lsbCBjYWxsIGV2ZXJ5IGNoaWxkIHJlZHVjZXIsIGFuZCBnYXRoZXIgdGhlaXIgcmVzdWx0c1xuICogaW50byBhIHNpbmdsZSBzdGF0ZSBvYmplY3QsIHdob3NlIGtleXMgY29ycmVzcG9uZCB0byB0aGUga2V5cyBvZiB0aGUgcGFzc2VkXG4gKiByZWR1Y2VyIGZ1bmN0aW9ucy5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcmVkdWNlcnMgQW4gb2JqZWN0IHdob3NlIHZhbHVlcyBjb3JyZXNwb25kIHRvIGRpZmZlcmVudFxuICogcmVkdWNlciBmdW5jdGlvbnMgdGhhdCBuZWVkIHRvIGJlIGNvbWJpbmVkIGludG8gb25lLiBPbmUgaGFuZHkgd2F5IHRvIG9idGFpblxuICogaXQgaXMgdG8gdXNlIEVTNiBgaW1wb3J0ICogYXMgcmVkdWNlcnNgIHN5bnRheC4gVGhlIHJlZHVjZXJzIG1heSBuZXZlciByZXR1cm5cbiAqIHVuZGVmaW5lZCBmb3IgYW55IGFjdGlvbi4gSW5zdGVhZCwgdGhleSBzaG91bGQgcmV0dXJuIHRoZWlyIGluaXRpYWwgc3RhdGVcbiAqIGlmIHRoZSBzdGF0ZSBwYXNzZWQgdG8gdGhlbSB3YXMgdW5kZWZpbmVkLCBhbmQgdGhlIGN1cnJlbnQgc3RhdGUgZm9yIGFueVxuICogdW5yZWNvZ25pemVkIGFjdGlvbi5cbiAqXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IEEgcmVkdWNlciBmdW5jdGlvbiB0aGF0IGludm9rZXMgZXZlcnkgcmVkdWNlciBpbnNpZGUgdGhlXG4gKiBwYXNzZWQgb2JqZWN0LCBhbmQgYnVpbGRzIGEgc3RhdGUgb2JqZWN0IHdpdGggdGhlIHNhbWUgc2hhcGUuXG4gKi9cbmZ1bmN0aW9uIGNvbWJpbmVSZWR1Y2VycyhyZWR1Y2Vycykge1xuICB2YXIgcmVkdWNlcktleXMgPSBPYmplY3Qua2V5cyhyZWR1Y2Vycyk7XG4gIHZhciBmaW5hbFJlZHVjZXJzID0ge307XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcmVkdWNlcktleXMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIga2V5ID0gcmVkdWNlcktleXNbaV07XG4gICAgaWYgKHR5cGVvZiByZWR1Y2Vyc1trZXldID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBmaW5hbFJlZHVjZXJzW2tleV0gPSByZWR1Y2Vyc1trZXldO1xuICAgIH1cbiAgfVxuICB2YXIgZmluYWxSZWR1Y2VyS2V5cyA9IE9iamVjdC5rZXlzKGZpbmFsUmVkdWNlcnMpO1xuXG4gIHZhciBzYW5pdHlFcnJvcjtcbiAgdHJ5IHtcbiAgICBhc3NlcnRSZWR1Y2VyU2FuaXR5KGZpbmFsUmVkdWNlcnMpO1xuICB9IGNhdGNoIChlKSB7XG4gICAgc2FuaXR5RXJyb3IgPSBlO1xuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIGNvbWJpbmF0aW9uKCkge1xuICAgIHZhciBzdGF0ZSA9IGFyZ3VtZW50cy5sZW5ndGggPD0gMCB8fCBhcmd1bWVudHNbMF0gPT09IHVuZGVmaW5lZCA/IHt9IDogYXJndW1lbnRzWzBdO1xuICAgIHZhciBhY3Rpb24gPSBhcmd1bWVudHNbMV07XG5cbiAgICBpZiAoc2FuaXR5RXJyb3IpIHtcbiAgICAgIHRocm93IHNhbml0eUVycm9yO1xuICAgIH1cblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICB2YXIgd2FybmluZ01lc3NhZ2UgPSBnZXRVbmV4cGVjdGVkU3RhdGVTaGFwZVdhcm5pbmdNZXNzYWdlKHN0YXRlLCBmaW5hbFJlZHVjZXJzLCBhY3Rpb24pO1xuICAgICAgaWYgKHdhcm5pbmdNZXNzYWdlKSB7XG4gICAgICAgICgwLCBfd2FybmluZzJbXCJkZWZhdWx0XCJdKSh3YXJuaW5nTWVzc2FnZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIGhhc0NoYW5nZWQgPSBmYWxzZTtcbiAgICB2YXIgbmV4dFN0YXRlID0ge307XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaW5hbFJlZHVjZXJLZXlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIga2V5ID0gZmluYWxSZWR1Y2VyS2V5c1tpXTtcbiAgICAgIHZhciByZWR1Y2VyID0gZmluYWxSZWR1Y2Vyc1trZXldO1xuICAgICAgdmFyIHByZXZpb3VzU3RhdGVGb3JLZXkgPSBzdGF0ZVtrZXldO1xuICAgICAgdmFyIG5leHRTdGF0ZUZvcktleSA9IHJlZHVjZXIocHJldmlvdXNTdGF0ZUZvcktleSwgYWN0aW9uKTtcbiAgICAgIGlmICh0eXBlb2YgbmV4dFN0YXRlRm9yS2V5ID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICB2YXIgZXJyb3JNZXNzYWdlID0gZ2V0VW5kZWZpbmVkU3RhdGVFcnJvck1lc3NhZ2Uoa2V5LCBhY3Rpb24pO1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3JNZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIG5leHRTdGF0ZVtrZXldID0gbmV4dFN0YXRlRm9yS2V5O1xuICAgICAgaGFzQ2hhbmdlZCA9IGhhc0NoYW5nZWQgfHwgbmV4dFN0YXRlRm9yS2V5ICE9PSBwcmV2aW91c1N0YXRlRm9yS2V5O1xuICAgIH1cbiAgICByZXR1cm4gaGFzQ2hhbmdlZCA/IG5leHRTdGF0ZSA6IHN0YXRlO1xuICB9O1xufVxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vcmVkdXgvbGliL2NvbWJpbmVSZWR1Y2Vycy5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gd2FybmluZztcbi8qKlxuICogUHJpbnRzIGEgd2FybmluZyBpbiB0aGUgY29uc29sZSBpZiBpdCBleGlzdHMuXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgVGhlIHdhcm5pbmcgbWVzc2FnZS5cbiAqIEByZXR1cm5zIHt2b2lkfVxuICovXG5mdW5jdGlvbiB3YXJuaW5nKG1lc3NhZ2UpIHtcbiAgLyogZXNsaW50LWRpc2FibGUgbm8tY29uc29sZSAqL1xuICBpZiAodHlwZW9mIGNvbnNvbGUgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBjb25zb2xlLmVycm9yID09PSAnZnVuY3Rpb24nKSB7XG4gICAgY29uc29sZS5lcnJvcihtZXNzYWdlKTtcbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlIG5vLWNvbnNvbGUgKi9cbiAgdHJ5IHtcbiAgICAvLyBUaGlzIGVycm9yIHdhcyB0aHJvd24gYXMgYSBjb252ZW5pZW5jZSBzbyB0aGF0IGlmIHlvdSBlbmFibGVcbiAgICAvLyBcImJyZWFrIG9uIGFsbCBleGNlcHRpb25zXCIgaW4geW91ciBjb25zb2xlLFxuICAgIC8vIGl0IHdvdWxkIHBhdXNlIHRoZSBleGVjdXRpb24gYXQgdGhpcyBsaW5lLlxuICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1lbXB0eSAqL1xuICB9IGNhdGNoIChlKSB7fVxuICAvKiBlc2xpbnQtZW5hYmxlIG5vLWVtcHR5ICovXG59XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9yZWR1eC9saWIvdXRpbHMvd2FybmluZy5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gYmluZEFjdGlvbkNyZWF0b3JzO1xuZnVuY3Rpb24gYmluZEFjdGlvbkNyZWF0b3IoYWN0aW9uQ3JlYXRvciwgZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZGlzcGF0Y2goYWN0aW9uQ3JlYXRvci5hcHBseSh1bmRlZmluZWQsIGFyZ3VtZW50cykpO1xuICB9O1xufVxuXG4vKipcbiAqIFR1cm5zIGFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIGFjdGlvbiBjcmVhdG9ycywgaW50byBhbiBvYmplY3Qgd2l0aCB0aGVcbiAqIHNhbWUga2V5cywgYnV0IHdpdGggZXZlcnkgZnVuY3Rpb24gd3JhcHBlZCBpbnRvIGEgYGRpc3BhdGNoYCBjYWxsIHNvIHRoZXlcbiAqIG1heSBiZSBpbnZva2VkIGRpcmVjdGx5LiBUaGlzIGlzIGp1c3QgYSBjb252ZW5pZW5jZSBtZXRob2QsIGFzIHlvdSBjYW4gY2FsbFxuICogYHN0b3JlLmRpc3BhdGNoKE15QWN0aW9uQ3JlYXRvcnMuZG9Tb21ldGhpbmcoKSlgIHlvdXJzZWxmIGp1c3QgZmluZS5cbiAqXG4gKiBGb3IgY29udmVuaWVuY2UsIHlvdSBjYW4gYWxzbyBwYXNzIGEgc2luZ2xlIGZ1bmN0aW9uIGFzIHRoZSBmaXJzdCBhcmd1bWVudCxcbiAqIGFuZCBnZXQgYSBmdW5jdGlvbiBpbiByZXR1cm4uXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbnxPYmplY3R9IGFjdGlvbkNyZWF0b3JzIEFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIGFjdGlvblxuICogY3JlYXRvciBmdW5jdGlvbnMuIE9uZSBoYW5keSB3YXkgdG8gb2J0YWluIGl0IGlzIHRvIHVzZSBFUzYgYGltcG9ydCAqIGFzYFxuICogc3ludGF4LiBZb3UgbWF5IGFsc28gcGFzcyBhIHNpbmdsZSBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBkaXNwYXRjaCBUaGUgYGRpc3BhdGNoYCBmdW5jdGlvbiBhdmFpbGFibGUgb24geW91ciBSZWR1eFxuICogc3RvcmUuXG4gKlxuICogQHJldHVybnMge0Z1bmN0aW9ufE9iamVjdH0gVGhlIG9iamVjdCBtaW1pY2tpbmcgdGhlIG9yaWdpbmFsIG9iamVjdCwgYnV0IHdpdGhcbiAqIGV2ZXJ5IGFjdGlvbiBjcmVhdG9yIHdyYXBwZWQgaW50byB0aGUgYGRpc3BhdGNoYCBjYWxsLiBJZiB5b3UgcGFzc2VkIGFcbiAqIGZ1bmN0aW9uIGFzIGBhY3Rpb25DcmVhdG9yc2AsIHRoZSByZXR1cm4gdmFsdWUgd2lsbCBhbHNvIGJlIGEgc2luZ2xlXG4gKiBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gYmluZEFjdGlvbkNyZWF0b3JzKGFjdGlvbkNyZWF0b3JzLCBkaXNwYXRjaCkge1xuICBpZiAodHlwZW9mIGFjdGlvbkNyZWF0b3JzID09PSAnZnVuY3Rpb24nKSB7XG4gICAgcmV0dXJuIGJpbmRBY3Rpb25DcmVhdG9yKGFjdGlvbkNyZWF0b3JzLCBkaXNwYXRjaCk7XG4gIH1cblxuICBpZiAodHlwZW9mIGFjdGlvbkNyZWF0b3JzICE9PSAnb2JqZWN0JyB8fCBhY3Rpb25DcmVhdG9ycyA9PT0gbnVsbCkge1xuICAgIHRocm93IG5ldyBFcnJvcignYmluZEFjdGlvbkNyZWF0b3JzIGV4cGVjdGVkIGFuIG9iamVjdCBvciBhIGZ1bmN0aW9uLCBpbnN0ZWFkIHJlY2VpdmVkICcgKyAoYWN0aW9uQ3JlYXRvcnMgPT09IG51bGwgPyAnbnVsbCcgOiB0eXBlb2YgYWN0aW9uQ3JlYXRvcnMpICsgJy4gJyArICdEaWQgeW91IHdyaXRlIFwiaW1wb3J0IEFjdGlvbkNyZWF0b3JzIGZyb21cIiBpbnN0ZWFkIG9mIFwiaW1wb3J0ICogYXMgQWN0aW9uQ3JlYXRvcnMgZnJvbVwiPycpO1xuICB9XG5cbiAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhhY3Rpb25DcmVhdG9ycyk7XG4gIHZhciBib3VuZEFjdGlvbkNyZWF0b3JzID0ge307XG4gIGZvciAodmFyIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBrZXkgPSBrZXlzW2ldO1xuICAgIHZhciBhY3Rpb25DcmVhdG9yID0gYWN0aW9uQ3JlYXRvcnNba2V5XTtcbiAgICBpZiAodHlwZW9mIGFjdGlvbkNyZWF0b3IgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIGJvdW5kQWN0aW9uQ3JlYXRvcnNba2V5XSA9IGJpbmRBY3Rpb25DcmVhdG9yKGFjdGlvbkNyZWF0b3IsIGRpc3BhdGNoKTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIGJvdW5kQWN0aW9uQ3JlYXRvcnM7XG59XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9yZWR1eC9saWIvYmluZEFjdGlvbkNyZWF0b3JzLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IGFwcGx5TWlkZGxld2FyZTtcblxudmFyIF9jb21wb3NlID0gcmVxdWlyZSgnLi9jb21wb3NlJyk7XG5cbnZhciBfY29tcG9zZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb21wb3NlKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgXCJkZWZhdWx0XCI6IG9iaiB9OyB9XG5cbi8qKlxuICogQ3JlYXRlcyBhIHN0b3JlIGVuaGFuY2VyIHRoYXQgYXBwbGllcyBtaWRkbGV3YXJlIHRvIHRoZSBkaXNwYXRjaCBtZXRob2RcbiAqIG9mIHRoZSBSZWR1eCBzdG9yZS4gVGhpcyBpcyBoYW5keSBmb3IgYSB2YXJpZXR5IG9mIHRhc2tzLCBzdWNoIGFzIGV4cHJlc3NpbmdcbiAqIGFzeW5jaHJvbm91cyBhY3Rpb25zIGluIGEgY29uY2lzZSBtYW5uZXIsIG9yIGxvZ2dpbmcgZXZlcnkgYWN0aW9uIHBheWxvYWQuXG4gKlxuICogU2VlIGByZWR1eC10aHVua2AgcGFja2FnZSBhcyBhbiBleGFtcGxlIG9mIHRoZSBSZWR1eCBtaWRkbGV3YXJlLlxuICpcbiAqIEJlY2F1c2UgbWlkZGxld2FyZSBpcyBwb3RlbnRpYWxseSBhc3luY2hyb25vdXMsIHRoaXMgc2hvdWxkIGJlIHRoZSBmaXJzdFxuICogc3RvcmUgZW5oYW5jZXIgaW4gdGhlIGNvbXBvc2l0aW9uIGNoYWluLlxuICpcbiAqIE5vdGUgdGhhdCBlYWNoIG1pZGRsZXdhcmUgd2lsbCBiZSBnaXZlbiB0aGUgYGRpc3BhdGNoYCBhbmQgYGdldFN0YXRlYCBmdW5jdGlvbnNcbiAqIGFzIG5hbWVkIGFyZ3VtZW50cy5cbiAqXG4gKiBAcGFyYW0gey4uLkZ1bmN0aW9ufSBtaWRkbGV3YXJlcyBUaGUgbWlkZGxld2FyZSBjaGFpbiB0byBiZSBhcHBsaWVkLlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBBIHN0b3JlIGVuaGFuY2VyIGFwcGx5aW5nIHRoZSBtaWRkbGV3YXJlLlxuICovXG5mdW5jdGlvbiBhcHBseU1pZGRsZXdhcmUoKSB7XG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBtaWRkbGV3YXJlcyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgIG1pZGRsZXdhcmVzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChjcmVhdGVTdG9yZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiAocmVkdWNlciwgaW5pdGlhbFN0YXRlLCBlbmhhbmNlcikge1xuICAgICAgdmFyIHN0b3JlID0gY3JlYXRlU3RvcmUocmVkdWNlciwgaW5pdGlhbFN0YXRlLCBlbmhhbmNlcik7XG4gICAgICB2YXIgX2Rpc3BhdGNoID0gc3RvcmUuZGlzcGF0Y2g7XG4gICAgICB2YXIgY2hhaW4gPSBbXTtcblxuICAgICAgdmFyIG1pZGRsZXdhcmVBUEkgPSB7XG4gICAgICAgIGdldFN0YXRlOiBzdG9yZS5nZXRTdGF0ZSxcbiAgICAgICAgZGlzcGF0Y2g6IGZ1bmN0aW9uIGRpc3BhdGNoKGFjdGlvbikge1xuICAgICAgICAgIHJldHVybiBfZGlzcGF0Y2goYWN0aW9uKTtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIGNoYWluID0gbWlkZGxld2FyZXMubWFwKGZ1bmN0aW9uIChtaWRkbGV3YXJlKSB7XG4gICAgICAgIHJldHVybiBtaWRkbGV3YXJlKG1pZGRsZXdhcmVBUEkpO1xuICAgICAgfSk7XG4gICAgICBfZGlzcGF0Y2ggPSBfY29tcG9zZTJbXCJkZWZhdWx0XCJdLmFwcGx5KHVuZGVmaW5lZCwgY2hhaW4pKHN0b3JlLmRpc3BhdGNoKTtcblxuICAgICAgcmV0dXJuIF9leHRlbmRzKHt9LCBzdG9yZSwge1xuICAgICAgICBkaXNwYXRjaDogX2Rpc3BhdGNoXG4gICAgICB9KTtcbiAgICB9O1xuICB9O1xufVxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vcmVkdXgvbGliL2FwcGx5TWlkZGxld2FyZS5qc1xuICoqLyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBjb21wb3NlO1xuLyoqXG4gKiBDb21wb3NlcyBzaW5nbGUtYXJndW1lbnQgZnVuY3Rpb25zIGZyb20gcmlnaHQgdG8gbGVmdC4gVGhlIHJpZ2h0bW9zdFxuICogZnVuY3Rpb24gY2FuIHRha2UgbXVsdGlwbGUgYXJndW1lbnRzIGFzIGl0IHByb3ZpZGVzIHRoZSBzaWduYXR1cmUgZm9yXG4gKiB0aGUgcmVzdWx0aW5nIGNvbXBvc2l0ZSBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0gey4uLkZ1bmN0aW9ufSBmdW5jcyBUaGUgZnVuY3Rpb25zIHRvIGNvbXBvc2UuXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IEEgZnVuY3Rpb24gb2J0YWluZWQgYnkgY29tcG9zaW5nIHRoZSBhcmd1bWVudCBmdW5jdGlvbnNcbiAqIGZyb20gcmlnaHQgdG8gbGVmdC4gRm9yIGV4YW1wbGUsIGNvbXBvc2UoZiwgZywgaCkgaXMgaWRlbnRpY2FsIHRvIGRvaW5nXG4gKiAoLi4uYXJncykgPT4gZihnKGgoLi4uYXJncykpKS5cbiAqL1xuXG5mdW5jdGlvbiBjb21wb3NlKCkge1xuICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgZnVuY3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBmdW5jc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIGlmIChmdW5jcy5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGFyZykge1xuICAgICAgcmV0dXJuIGFyZztcbiAgICB9O1xuICB9IGVsc2Uge1xuICAgIHZhciBfcmV0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGxhc3QgPSBmdW5jc1tmdW5jcy5sZW5ndGggLSAxXTtcbiAgICAgIHZhciByZXN0ID0gZnVuY3Muc2xpY2UoMCwgLTEpO1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdjogZnVuY3Rpb24gdigpIHtcbiAgICAgICAgICByZXR1cm4gcmVzdC5yZWR1Y2VSaWdodChmdW5jdGlvbiAoY29tcG9zZWQsIGYpIHtcbiAgICAgICAgICAgIHJldHVybiBmKGNvbXBvc2VkKTtcbiAgICAgICAgICB9LCBsYXN0LmFwcGx5KHVuZGVmaW5lZCwgYXJndW1lbnRzKSk7XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfSgpO1xuXG4gICAgaWYgKHR5cGVvZiBfcmV0ID09PSBcIm9iamVjdFwiKSByZXR1cm4gX3JldC52O1xuICB9XG59XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9yZWR1eC9saWIvY29tcG9zZS5qc1xuICoqLyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9saWIvYXhpb3MnKTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L2F4aW9zL2luZGV4LmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuL3V0aWxzJyk7XG52YXIgYmluZCA9IHJlcXVpcmUoJy4vaGVscGVycy9iaW5kJyk7XG52YXIgQXhpb3MgPSByZXF1aXJlKCcuL2NvcmUvQXhpb3MnKTtcblxuLyoqXG4gKiBDcmVhdGUgYW4gaW5zdGFuY2Ugb2YgQXhpb3NcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gZGVmYXVsdENvbmZpZyBUaGUgZGVmYXVsdCBjb25maWcgZm9yIHRoZSBpbnN0YW5jZVxuICogQHJldHVybiB7QXhpb3N9IEEgbmV3IGluc3RhbmNlIG9mIEF4aW9zXG4gKi9cbmZ1bmN0aW9uIGNyZWF0ZUluc3RhbmNlKGRlZmF1bHRDb25maWcpIHtcbiAgdmFyIGNvbnRleHQgPSBuZXcgQXhpb3MoZGVmYXVsdENvbmZpZyk7XG4gIHZhciBpbnN0YW5jZSA9IGJpbmQoQXhpb3MucHJvdG90eXBlLnJlcXVlc3QsIGNvbnRleHQpO1xuXG4gIC8vIENvcHkgYXhpb3MucHJvdG90eXBlIHRvIGluc3RhbmNlXG4gIHV0aWxzLmV4dGVuZChpbnN0YW5jZSwgQXhpb3MucHJvdG90eXBlLCBjb250ZXh0KTtcblxuICAvLyBDb3B5IGNvbnRleHQgdG8gaW5zdGFuY2VcbiAgdXRpbHMuZXh0ZW5kKGluc3RhbmNlLCBjb250ZXh0KTtcblxuICByZXR1cm4gaW5zdGFuY2U7XG59XG5cbi8vIENyZWF0ZSB0aGUgZGVmYXVsdCBpbnN0YW5jZSB0byBiZSBleHBvcnRlZFxudmFyIGF4aW9zID0gY3JlYXRlSW5zdGFuY2UoKTtcblxuLy8gRXhwb3NlIEF4aW9zIGNsYXNzIHRvIGFsbG93IGNsYXNzIGluaGVyaXRhbmNlXG5heGlvcy5BeGlvcyA9IEF4aW9zO1xuXG4vLyBGYWN0b3J5IGZvciBjcmVhdGluZyBuZXcgaW5zdGFuY2VzXG5heGlvcy5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUoZGVmYXVsdENvbmZpZykge1xuICByZXR1cm4gY3JlYXRlSW5zdGFuY2UoZGVmYXVsdENvbmZpZyk7XG59O1xuXG4vLyBFeHBvc2UgYWxsL3NwcmVhZFxuYXhpb3MuYWxsID0gZnVuY3Rpb24gYWxsKHByb21pc2VzKSB7XG4gIHJldHVybiBQcm9taXNlLmFsbChwcm9taXNlcyk7XG59O1xuYXhpb3Muc3ByZWFkID0gcmVxdWlyZSgnLi9oZWxwZXJzL3NwcmVhZCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGF4aW9zO1xuXG4vLyBBbGxvdyB1c2Ugb2YgZGVmYXVsdCBpbXBvcnQgc3ludGF4IGluIFR5cGVTY3JpcHRcbm1vZHVsZS5leHBvcnRzLmRlZmF1bHQgPSBheGlvcztcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2F4aW9zLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgYmluZCA9IHJlcXVpcmUoJy4vaGVscGVycy9iaW5kJyk7XG5cbi8qZ2xvYmFsIHRvU3RyaW5nOnRydWUqL1xuXG4vLyB1dGlscyBpcyBhIGxpYnJhcnkgb2YgZ2VuZXJpYyBoZWxwZXIgZnVuY3Rpb25zIG5vbi1zcGVjaWZpYyB0byBheGlvc1xuXG52YXIgdG9TdHJpbmcgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGFuIEFycmF5XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYW4gQXJyYXksIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0FycmF5KHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBBcnJheV0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGFuIEFycmF5QnVmZmVyXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYW4gQXJyYXlCdWZmZXIsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0FycmF5QnVmZmVyKHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBBcnJheUJ1ZmZlcl0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRm9ybURhdGFcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhbiBGb3JtRGF0YSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzRm9ybURhdGEodmFsKSB7XG4gIHJldHVybiAodHlwZW9mIEZvcm1EYXRhICE9PSAndW5kZWZpbmVkJykgJiYgKHZhbCBpbnN0YW5jZW9mIEZvcm1EYXRhKTtcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIHZpZXcgb24gYW4gQXJyYXlCdWZmZXJcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIHZpZXcgb24gYW4gQXJyYXlCdWZmZXIsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0FycmF5QnVmZmVyVmlldyh2YWwpIHtcbiAgdmFyIHJlc3VsdDtcbiAgaWYgKCh0eXBlb2YgQXJyYXlCdWZmZXIgIT09ICd1bmRlZmluZWQnKSAmJiAoQXJyYXlCdWZmZXIuaXNWaWV3KSkge1xuICAgIHJlc3VsdCA9IEFycmF5QnVmZmVyLmlzVmlldyh2YWwpO1xuICB9IGVsc2Uge1xuICAgIHJlc3VsdCA9ICh2YWwpICYmICh2YWwuYnVmZmVyKSAmJiAodmFsLmJ1ZmZlciBpbnN0YW5jZW9mIEFycmF5QnVmZmVyKTtcbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgU3RyaW5nXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBTdHJpbmcsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc1N0cmluZyh2YWwpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICdzdHJpbmcnO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgTnVtYmVyXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBOdW1iZXIsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc051bWJlcih2YWwpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICdudW1iZXInO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIHVuZGVmaW5lZFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSB2YWx1ZSBpcyB1bmRlZmluZWQsIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc1VuZGVmaW5lZCh2YWwpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICd1bmRlZmluZWQnO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGFuIE9iamVjdFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGFuIE9iamVjdCwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzT2JqZWN0KHZhbCkge1xuICByZXR1cm4gdmFsICE9PSBudWxsICYmIHR5cGVvZiB2YWwgPT09ICdvYmplY3QnO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRGF0ZVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgRGF0ZSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzRGF0ZSh2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgRGF0ZV0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRmlsZVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgRmlsZSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzRmlsZSh2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgRmlsZV0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgQmxvYlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgQmxvYiwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzQmxvYih2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgQmxvYl0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgRnVuY3Rpb25cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIEZ1bmN0aW9uLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNGdW5jdGlvbih2YWwpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwodmFsKSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgYSB2YWx1ZSBpcyBhIFN0cmVhbVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgU3RyZWFtLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNTdHJlYW0odmFsKSB7XG4gIHJldHVybiBpc09iamVjdCh2YWwpICYmIGlzRnVuY3Rpb24odmFsLnBpcGUpO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgVVJMU2VhcmNoUGFyYW1zIG9iamVjdFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgVVJMU2VhcmNoUGFyYW1zIG9iamVjdCwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzVVJMU2VhcmNoUGFyYW1zKHZhbCkge1xuICByZXR1cm4gdHlwZW9mIFVSTFNlYXJjaFBhcmFtcyAhPT0gJ3VuZGVmaW5lZCcgJiYgdmFsIGluc3RhbmNlb2YgVVJMU2VhcmNoUGFyYW1zO1xufVxuXG4vKipcbiAqIFRyaW0gZXhjZXNzIHdoaXRlc3BhY2Ugb2ZmIHRoZSBiZWdpbm5pbmcgYW5kIGVuZCBvZiBhIHN0cmluZ1xuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBzdHIgVGhlIFN0cmluZyB0byB0cmltXG4gKiBAcmV0dXJucyB7U3RyaW5nfSBUaGUgU3RyaW5nIGZyZWVkIG9mIGV4Y2VzcyB3aGl0ZXNwYWNlXG4gKi9cbmZ1bmN0aW9uIHRyaW0oc3RyKSB7XG4gIHJldHVybiBzdHIucmVwbGFjZSgvXlxccyovLCAnJykucmVwbGFjZSgvXFxzKiQvLCAnJyk7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIHdlJ3JlIHJ1bm5pbmcgaW4gYSBzdGFuZGFyZCBicm93c2VyIGVudmlyb25tZW50XG4gKlxuICogVGhpcyBhbGxvd3MgYXhpb3MgdG8gcnVuIGluIGEgd2ViIHdvcmtlciwgYW5kIHJlYWN0LW5hdGl2ZS5cbiAqIEJvdGggZW52aXJvbm1lbnRzIHN1cHBvcnQgWE1MSHR0cFJlcXVlc3QsIGJ1dCBub3QgZnVsbHkgc3RhbmRhcmQgZ2xvYmFscy5cbiAqXG4gKiB3ZWIgd29ya2VyczpcbiAqICB0eXBlb2Ygd2luZG93IC0+IHVuZGVmaW5lZFxuICogIHR5cGVvZiBkb2N1bWVudCAtPiB1bmRlZmluZWRcbiAqXG4gKiByZWFjdC1uYXRpdmU6XG4gKiAgdHlwZW9mIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQgLT4gdW5kZWZpbmVkXG4gKi9cbmZ1bmN0aW9uIGlzU3RhbmRhcmRCcm93c2VyRW52KCkge1xuICByZXR1cm4gKFxuICAgIHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmXG4gICAgdHlwZW9mIGRvY3VtZW50ICE9PSAndW5kZWZpbmVkJyAmJlxuICAgIHR5cGVvZiBkb2N1bWVudC5jcmVhdGVFbGVtZW50ID09PSAnZnVuY3Rpb24nXG4gICk7XG59XG5cbi8qKlxuICogSXRlcmF0ZSBvdmVyIGFuIEFycmF5IG9yIGFuIE9iamVjdCBpbnZva2luZyBhIGZ1bmN0aW9uIGZvciBlYWNoIGl0ZW0uXG4gKlxuICogSWYgYG9iamAgaXMgYW4gQXJyYXkgY2FsbGJhY2sgd2lsbCBiZSBjYWxsZWQgcGFzc2luZ1xuICogdGhlIHZhbHVlLCBpbmRleCwgYW5kIGNvbXBsZXRlIGFycmF5IGZvciBlYWNoIGl0ZW0uXG4gKlxuICogSWYgJ29iaicgaXMgYW4gT2JqZWN0IGNhbGxiYWNrIHdpbGwgYmUgY2FsbGVkIHBhc3NpbmdcbiAqIHRoZSB2YWx1ZSwga2V5LCBhbmQgY29tcGxldGUgb2JqZWN0IGZvciBlYWNoIHByb3BlcnR5LlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fEFycmF5fSBvYmogVGhlIG9iamVjdCB0byBpdGVyYXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmbiBUaGUgY2FsbGJhY2sgdG8gaW52b2tlIGZvciBlYWNoIGl0ZW1cbiAqL1xuZnVuY3Rpb24gZm9yRWFjaChvYmosIGZuKSB7XG4gIC8vIERvbid0IGJvdGhlciBpZiBubyB2YWx1ZSBwcm92aWRlZFxuICBpZiAob2JqID09PSBudWxsIHx8IHR5cGVvZiBvYmogPT09ICd1bmRlZmluZWQnKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLy8gRm9yY2UgYW4gYXJyYXkgaWYgbm90IGFscmVhZHkgc29tZXRoaW5nIGl0ZXJhYmxlXG4gIGlmICh0eXBlb2Ygb2JqICE9PSAnb2JqZWN0JyAmJiAhaXNBcnJheShvYmopKSB7XG4gICAgLyplc2xpbnQgbm8tcGFyYW0tcmVhc3NpZ246MCovXG4gICAgb2JqID0gW29ial07XG4gIH1cblxuICBpZiAoaXNBcnJheShvYmopKSB7XG4gICAgLy8gSXRlcmF0ZSBvdmVyIGFycmF5IHZhbHVlc1xuICAgIGZvciAodmFyIGkgPSAwLCBsID0gb2JqLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgZm4uY2FsbChudWxsLCBvYmpbaV0sIGksIG9iaik7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIC8vIEl0ZXJhdGUgb3ZlciBvYmplY3Qga2V5c1xuICAgIGZvciAodmFyIGtleSBpbiBvYmopIHtcbiAgICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICBmbi5jYWxsKG51bGwsIG9ialtrZXldLCBrZXksIG9iaik7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi8qKlxuICogQWNjZXB0cyB2YXJhcmdzIGV4cGVjdGluZyBlYWNoIGFyZ3VtZW50IHRvIGJlIGFuIG9iamVjdCwgdGhlblxuICogaW1tdXRhYmx5IG1lcmdlcyB0aGUgcHJvcGVydGllcyBvZiBlYWNoIG9iamVjdCBhbmQgcmV0dXJucyByZXN1bHQuXG4gKlxuICogV2hlbiBtdWx0aXBsZSBvYmplY3RzIGNvbnRhaW4gdGhlIHNhbWUga2V5IHRoZSBsYXRlciBvYmplY3QgaW5cbiAqIHRoZSBhcmd1bWVudHMgbGlzdCB3aWxsIHRha2UgcHJlY2VkZW5jZS5cbiAqXG4gKiBFeGFtcGxlOlxuICpcbiAqIGBgYGpzXG4gKiB2YXIgcmVzdWx0ID0gbWVyZ2Uoe2ZvbzogMTIzfSwge2ZvbzogNDU2fSk7XG4gKiBjb25zb2xlLmxvZyhyZXN1bHQuZm9vKTsgLy8gb3V0cHV0cyA0NTZcbiAqIGBgYFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmoxIE9iamVjdCB0byBtZXJnZVxuICogQHJldHVybnMge09iamVjdH0gUmVzdWx0IG9mIGFsbCBtZXJnZSBwcm9wZXJ0aWVzXG4gKi9cbmZ1bmN0aW9uIG1lcmdlKC8qIG9iajEsIG9iajIsIG9iajMsIC4uLiAqLykge1xuICB2YXIgcmVzdWx0ID0ge307XG4gIGZ1bmN0aW9uIGFzc2lnblZhbHVlKHZhbCwga2V5KSB7XG4gICAgaWYgKHR5cGVvZiByZXN1bHRba2V5XSA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIHZhbCA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJlc3VsdFtrZXldID0gbWVyZ2UocmVzdWx0W2tleV0sIHZhbCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3VsdFtrZXldID0gdmFsO1xuICAgIH1cbiAgfVxuXG4gIGZvciAodmFyIGkgPSAwLCBsID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgIGZvckVhY2goYXJndW1lbnRzW2ldLCBhc3NpZ25WYWx1ZSk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuLyoqXG4gKiBFeHRlbmRzIG9iamVjdCBhIGJ5IG11dGFibHkgYWRkaW5nIHRvIGl0IHRoZSBwcm9wZXJ0aWVzIG9mIG9iamVjdCBiLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBhIFRoZSBvYmplY3QgdG8gYmUgZXh0ZW5kZWRcbiAqIEBwYXJhbSB7T2JqZWN0fSBiIFRoZSBvYmplY3QgdG8gY29weSBwcm9wZXJ0aWVzIGZyb21cbiAqIEBwYXJhbSB7T2JqZWN0fSB0aGlzQXJnIFRoZSBvYmplY3QgdG8gYmluZCBmdW5jdGlvbiB0b1xuICogQHJldHVybiB7T2JqZWN0fSBUaGUgcmVzdWx0aW5nIHZhbHVlIG9mIG9iamVjdCBhXG4gKi9cbmZ1bmN0aW9uIGV4dGVuZChhLCBiLCB0aGlzQXJnKSB7XG4gIGZvckVhY2goYiwgZnVuY3Rpb24gYXNzaWduVmFsdWUodmFsLCBrZXkpIHtcbiAgICBpZiAodGhpc0FyZyAmJiB0eXBlb2YgdmFsID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBhW2tleV0gPSBiaW5kKHZhbCwgdGhpc0FyZyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGFba2V5XSA9IHZhbDtcbiAgICB9XG4gIH0pO1xuICByZXR1cm4gYTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIGlzQXJyYXk6IGlzQXJyYXksXG4gIGlzQXJyYXlCdWZmZXI6IGlzQXJyYXlCdWZmZXIsXG4gIGlzRm9ybURhdGE6IGlzRm9ybURhdGEsXG4gIGlzQXJyYXlCdWZmZXJWaWV3OiBpc0FycmF5QnVmZmVyVmlldyxcbiAgaXNTdHJpbmc6IGlzU3RyaW5nLFxuICBpc051bWJlcjogaXNOdW1iZXIsXG4gIGlzT2JqZWN0OiBpc09iamVjdCxcbiAgaXNVbmRlZmluZWQ6IGlzVW5kZWZpbmVkLFxuICBpc0RhdGU6IGlzRGF0ZSxcbiAgaXNGaWxlOiBpc0ZpbGUsXG4gIGlzQmxvYjogaXNCbG9iLFxuICBpc0Z1bmN0aW9uOiBpc0Z1bmN0aW9uLFxuICBpc1N0cmVhbTogaXNTdHJlYW0sXG4gIGlzVVJMU2VhcmNoUGFyYW1zOiBpc1VSTFNlYXJjaFBhcmFtcyxcbiAgaXNTdGFuZGFyZEJyb3dzZXJFbnY6IGlzU3RhbmRhcmRCcm93c2VyRW52LFxuICBmb3JFYWNoOiBmb3JFYWNoLFxuICBtZXJnZTogbWVyZ2UsXG4gIGV4dGVuZDogZXh0ZW5kLFxuICB0cmltOiB0cmltXG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9heGlvcy9saWIvdXRpbHMuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYmluZChmbiwgdGhpc0FyZykge1xuICByZXR1cm4gZnVuY3Rpb24gd3JhcCgpIHtcbiAgICB2YXIgYXJncyA9IG5ldyBBcnJheShhcmd1bWVudHMubGVuZ3RoKTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgIGFyZ3NbaV0gPSBhcmd1bWVudHNbaV07XG4gICAgfVxuICAgIHJldHVybiBmbi5hcHBseSh0aGlzQXJnLCBhcmdzKTtcbiAgfTtcbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L2F4aW9zL2xpYi9oZWxwZXJzL2JpbmQuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbnZhciBkZWZhdWx0cyA9IHJlcXVpcmUoJy4vLi4vZGVmYXVsdHMnKTtcbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcbnZhciBJbnRlcmNlcHRvck1hbmFnZXIgPSByZXF1aXJlKCcuL0ludGVyY2VwdG9yTWFuYWdlcicpO1xudmFyIGRpc3BhdGNoUmVxdWVzdCA9IHJlcXVpcmUoJy4vZGlzcGF0Y2hSZXF1ZXN0Jyk7XG52YXIgaXNBYnNvbHV0ZVVSTCA9IHJlcXVpcmUoJy4vLi4vaGVscGVycy9pc0Fic29sdXRlVVJMJyk7XG52YXIgY29tYmluZVVSTHMgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvY29tYmluZVVSTHMnKTtcblxuLyoqXG4gKiBDcmVhdGUgYSBuZXcgaW5zdGFuY2Ugb2YgQXhpb3NcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gZGVmYXVsdENvbmZpZyBUaGUgZGVmYXVsdCBjb25maWcgZm9yIHRoZSBpbnN0YW5jZVxuICovXG5mdW5jdGlvbiBBeGlvcyhkZWZhdWx0Q29uZmlnKSB7XG4gIHRoaXMuZGVmYXVsdHMgPSB1dGlscy5tZXJnZShkZWZhdWx0cywgZGVmYXVsdENvbmZpZyk7XG4gIHRoaXMuaW50ZXJjZXB0b3JzID0ge1xuICAgIHJlcXVlc3Q6IG5ldyBJbnRlcmNlcHRvck1hbmFnZXIoKSxcbiAgICByZXNwb25zZTogbmV3IEludGVyY2VwdG9yTWFuYWdlcigpXG4gIH07XG59XG5cbi8qKlxuICogRGlzcGF0Y2ggYSByZXF1ZXN0XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IGNvbmZpZyBUaGUgY29uZmlnIHNwZWNpZmljIGZvciB0aGlzIHJlcXVlc3QgKG1lcmdlZCB3aXRoIHRoaXMuZGVmYXVsdHMpXG4gKi9cbkF4aW9zLnByb3RvdHlwZS5yZXF1ZXN0ID0gZnVuY3Rpb24gcmVxdWVzdChjb25maWcpIHtcbiAgLyplc2xpbnQgbm8tcGFyYW0tcmVhc3NpZ246MCovXG4gIC8vIEFsbG93IGZvciBheGlvcygnZXhhbXBsZS91cmwnWywgY29uZmlnXSkgYSBsYSBmZXRjaCBBUElcbiAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XG4gICAgY29uZmlnID0gdXRpbHMubWVyZ2Uoe1xuICAgICAgdXJsOiBhcmd1bWVudHNbMF1cbiAgICB9LCBhcmd1bWVudHNbMV0pO1xuICB9XG5cbiAgY29uZmlnID0gdXRpbHMubWVyZ2UoZGVmYXVsdHMsIHRoaXMuZGVmYXVsdHMsIHsgbWV0aG9kOiAnZ2V0JyB9LCBjb25maWcpO1xuXG4gIC8vIFN1cHBvcnQgYmFzZVVSTCBjb25maWdcbiAgaWYgKGNvbmZpZy5iYXNlVVJMICYmICFpc0Fic29sdXRlVVJMKGNvbmZpZy51cmwpKSB7XG4gICAgY29uZmlnLnVybCA9IGNvbWJpbmVVUkxzKGNvbmZpZy5iYXNlVVJMLCBjb25maWcudXJsKTtcbiAgfVxuXG4gIC8vIEhvb2sgdXAgaW50ZXJjZXB0b3JzIG1pZGRsZXdhcmVcbiAgdmFyIGNoYWluID0gW2Rpc3BhdGNoUmVxdWVzdCwgdW5kZWZpbmVkXTtcbiAgdmFyIHByb21pc2UgPSBQcm9taXNlLnJlc29sdmUoY29uZmlnKTtcblxuICB0aGlzLmludGVyY2VwdG9ycy5yZXF1ZXN0LmZvckVhY2goZnVuY3Rpb24gdW5zaGlmdFJlcXVlc3RJbnRlcmNlcHRvcnMoaW50ZXJjZXB0b3IpIHtcbiAgICBjaGFpbi51bnNoaWZ0KGludGVyY2VwdG9yLmZ1bGZpbGxlZCwgaW50ZXJjZXB0b3IucmVqZWN0ZWQpO1xuICB9KTtcblxuICB0aGlzLmludGVyY2VwdG9ycy5yZXNwb25zZS5mb3JFYWNoKGZ1bmN0aW9uIHB1c2hSZXNwb25zZUludGVyY2VwdG9ycyhpbnRlcmNlcHRvcikge1xuICAgIGNoYWluLnB1c2goaW50ZXJjZXB0b3IuZnVsZmlsbGVkLCBpbnRlcmNlcHRvci5yZWplY3RlZCk7XG4gIH0pO1xuXG4gIHdoaWxlIChjaGFpbi5sZW5ndGgpIHtcbiAgICBwcm9taXNlID0gcHJvbWlzZS50aGVuKGNoYWluLnNoaWZ0KCksIGNoYWluLnNoaWZ0KCkpO1xuICB9XG5cbiAgcmV0dXJuIHByb21pc2U7XG59O1xuXG4vLyBQcm92aWRlIGFsaWFzZXMgZm9yIHN1cHBvcnRlZCByZXF1ZXN0IG1ldGhvZHNcbnV0aWxzLmZvckVhY2goWydkZWxldGUnLCAnZ2V0JywgJ2hlYWQnXSwgZnVuY3Rpb24gZm9yRWFjaE1ldGhvZE5vRGF0YShtZXRob2QpIHtcbiAgLyplc2xpbnQgZnVuYy1uYW1lczowKi9cbiAgQXhpb3MucHJvdG90eXBlW21ldGhvZF0gPSBmdW5jdGlvbih1cmwsIGNvbmZpZykge1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QodXRpbHMubWVyZ2UoY29uZmlnIHx8IHt9LCB7XG4gICAgICBtZXRob2Q6IG1ldGhvZCxcbiAgICAgIHVybDogdXJsXG4gICAgfSkpO1xuICB9O1xufSk7XG5cbnV0aWxzLmZvckVhY2goWydwb3N0JywgJ3B1dCcsICdwYXRjaCddLCBmdW5jdGlvbiBmb3JFYWNoTWV0aG9kV2l0aERhdGEobWV0aG9kKSB7XG4gIC8qZXNsaW50IGZ1bmMtbmFtZXM6MCovXG4gIEF4aW9zLnByb3RvdHlwZVttZXRob2RdID0gZnVuY3Rpb24odXJsLCBkYXRhLCBjb25maWcpIHtcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0KHV0aWxzLm1lcmdlKGNvbmZpZyB8fCB7fSwge1xuICAgICAgbWV0aG9kOiBtZXRob2QsXG4gICAgICB1cmw6IHVybCxcbiAgICAgIGRhdGE6IGRhdGFcbiAgICB9KSk7XG4gIH07XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBBeGlvcztcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2NvcmUvQXhpb3MuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKTtcbnZhciBub3JtYWxpemVIZWFkZXJOYW1lID0gcmVxdWlyZSgnLi9oZWxwZXJzL25vcm1hbGl6ZUhlYWRlck5hbWUnKTtcblxudmFyIFBST1RFQ1RJT05fUFJFRklYID0gL15cXClcXF1cXH0nLD9cXG4vO1xudmFyIERFRkFVTFRfQ09OVEVOVF9UWVBFID0ge1xuICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcbn07XG5cbmZ1bmN0aW9uIHNldENvbnRlbnRUeXBlSWZVbnNldChoZWFkZXJzLCB2YWx1ZSkge1xuICBpZiAoIXV0aWxzLmlzVW5kZWZpbmVkKGhlYWRlcnMpICYmIHV0aWxzLmlzVW5kZWZpbmVkKGhlYWRlcnNbJ0NvbnRlbnQtVHlwZSddKSkge1xuICAgIGhlYWRlcnNbJ0NvbnRlbnQtVHlwZSddID0gdmFsdWU7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIHRyYW5zZm9ybVJlcXVlc3Q6IFtmdW5jdGlvbiB0cmFuc2Zvcm1SZXF1ZXN0KGRhdGEsIGhlYWRlcnMpIHtcbiAgICBub3JtYWxpemVIZWFkZXJOYW1lKGhlYWRlcnMsICdDb250ZW50LVR5cGUnKTtcbiAgICBpZiAodXRpbHMuaXNGb3JtRGF0YShkYXRhKSB8fFxuICAgICAgdXRpbHMuaXNBcnJheUJ1ZmZlcihkYXRhKSB8fFxuICAgICAgdXRpbHMuaXNTdHJlYW0oZGF0YSkgfHxcbiAgICAgIHV0aWxzLmlzRmlsZShkYXRhKSB8fFxuICAgICAgdXRpbHMuaXNCbG9iKGRhdGEpXG4gICAgKSB7XG4gICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG4gICAgaWYgKHV0aWxzLmlzQXJyYXlCdWZmZXJWaWV3KGRhdGEpKSB7XG4gICAgICByZXR1cm4gZGF0YS5idWZmZXI7XG4gICAgfVxuICAgIGlmICh1dGlscy5pc1VSTFNlYXJjaFBhcmFtcyhkYXRhKSkge1xuICAgICAgc2V0Q29udGVudFR5cGVJZlVuc2V0KGhlYWRlcnMsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQ7Y2hhcnNldD11dGYtOCcpO1xuICAgICAgcmV0dXJuIGRhdGEudG9TdHJpbmcoKTtcbiAgICB9XG4gICAgaWYgKHV0aWxzLmlzT2JqZWN0KGRhdGEpKSB7XG4gICAgICBzZXRDb250ZW50VHlwZUlmVW5zZXQoaGVhZGVycywgJ2FwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtOCcpO1xuICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGRhdGEpO1xuICAgIH1cbiAgICByZXR1cm4gZGF0YTtcbiAgfV0sXG5cbiAgdHJhbnNmb3JtUmVzcG9uc2U6IFtmdW5jdGlvbiB0cmFuc2Zvcm1SZXNwb25zZShkYXRhKSB7XG4gICAgLyplc2xpbnQgbm8tcGFyYW0tcmVhc3NpZ246MCovXG4gICAgaWYgKHR5cGVvZiBkYXRhID09PSAnc3RyaW5nJykge1xuICAgICAgZGF0YSA9IGRhdGEucmVwbGFjZShQUk9URUNUSU9OX1BSRUZJWCwgJycpO1xuICAgICAgdHJ5IHtcbiAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UoZGF0YSk7XG4gICAgICB9IGNhdGNoIChlKSB7IC8qIElnbm9yZSAqLyB9XG4gICAgfVxuICAgIHJldHVybiBkYXRhO1xuICB9XSxcblxuICBoZWFkZXJzOiB7XG4gICAgY29tbW9uOiB7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24sIHRleHQvcGxhaW4sICovKidcbiAgICB9LFxuICAgIHBhdGNoOiB1dGlscy5tZXJnZShERUZBVUxUX0NPTlRFTlRfVFlQRSksXG4gICAgcG9zdDogdXRpbHMubWVyZ2UoREVGQVVMVF9DT05URU5UX1RZUEUpLFxuICAgIHB1dDogdXRpbHMubWVyZ2UoREVGQVVMVF9DT05URU5UX1RZUEUpXG4gIH0sXG5cbiAgdGltZW91dDogMCxcblxuICB4c3JmQ29va2llTmFtZTogJ1hTUkYtVE9LRU4nLFxuICB4c3JmSGVhZGVyTmFtZTogJ1gtWFNSRi1UT0tFTicsXG5cbiAgbWF4Q29udGVudExlbmd0aDogLTEsXG5cbiAgdmFsaWRhdGVTdGF0dXM6IGZ1bmN0aW9uIHZhbGlkYXRlU3RhdHVzKHN0YXR1cykge1xuICAgIHJldHVybiBzdGF0dXMgPj0gMjAwICYmIHN0YXR1cyA8IDMwMDtcbiAgfVxufTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2RlZmF1bHRzLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLi91dGlscycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUhlYWRlck5hbWUoaGVhZGVycywgbm9ybWFsaXplZE5hbWUpIHtcbiAgdXRpbHMuZm9yRWFjaChoZWFkZXJzLCBmdW5jdGlvbiBwcm9jZXNzSGVhZGVyKHZhbHVlLCBuYW1lKSB7XG4gICAgaWYgKG5hbWUgIT09IG5vcm1hbGl6ZWROYW1lICYmIG5hbWUudG9VcHBlckNhc2UoKSA9PT0gbm9ybWFsaXplZE5hbWUudG9VcHBlckNhc2UoKSkge1xuICAgICAgaGVhZGVyc1tub3JtYWxpemVkTmFtZV0gPSB2YWx1ZTtcbiAgICAgIGRlbGV0ZSBoZWFkZXJzW25hbWVdO1xuICAgIH1cbiAgfSk7XG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9heGlvcy9saWIvaGVscGVycy9ub3JtYWxpemVIZWFkZXJOYW1lLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbmZ1bmN0aW9uIEludGVyY2VwdG9yTWFuYWdlcigpIHtcbiAgdGhpcy5oYW5kbGVycyA9IFtdO1xufVxuXG4vKipcbiAqIEFkZCBhIG5ldyBpbnRlcmNlcHRvciB0byB0aGUgc3RhY2tcbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdWxmaWxsZWQgVGhlIGZ1bmN0aW9uIHRvIGhhbmRsZSBgdGhlbmAgZm9yIGEgYFByb21pc2VgXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSByZWplY3RlZCBUaGUgZnVuY3Rpb24gdG8gaGFuZGxlIGByZWplY3RgIGZvciBhIGBQcm9taXNlYFxuICpcbiAqIEByZXR1cm4ge051bWJlcn0gQW4gSUQgdXNlZCB0byByZW1vdmUgaW50ZXJjZXB0b3IgbGF0ZXJcbiAqL1xuSW50ZXJjZXB0b3JNYW5hZ2VyLnByb3RvdHlwZS51c2UgPSBmdW5jdGlvbiB1c2UoZnVsZmlsbGVkLCByZWplY3RlZCkge1xuICB0aGlzLmhhbmRsZXJzLnB1c2goe1xuICAgIGZ1bGZpbGxlZDogZnVsZmlsbGVkLFxuICAgIHJlamVjdGVkOiByZWplY3RlZFxuICB9KTtcbiAgcmV0dXJuIHRoaXMuaGFuZGxlcnMubGVuZ3RoIC0gMTtcbn07XG5cbi8qKlxuICogUmVtb3ZlIGFuIGludGVyY2VwdG9yIGZyb20gdGhlIHN0YWNrXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IGlkIFRoZSBJRCB0aGF0IHdhcyByZXR1cm5lZCBieSBgdXNlYFxuICovXG5JbnRlcmNlcHRvck1hbmFnZXIucHJvdG90eXBlLmVqZWN0ID0gZnVuY3Rpb24gZWplY3QoaWQpIHtcbiAgaWYgKHRoaXMuaGFuZGxlcnNbaWRdKSB7XG4gICAgdGhpcy5oYW5kbGVyc1tpZF0gPSBudWxsO1xuICB9XG59O1xuXG4vKipcbiAqIEl0ZXJhdGUgb3ZlciBhbGwgdGhlIHJlZ2lzdGVyZWQgaW50ZXJjZXB0b3JzXG4gKlxuICogVGhpcyBtZXRob2QgaXMgcGFydGljdWxhcmx5IHVzZWZ1bCBmb3Igc2tpcHBpbmcgb3ZlciBhbnlcbiAqIGludGVyY2VwdG9ycyB0aGF0IG1heSBoYXZlIGJlY29tZSBgbnVsbGAgY2FsbGluZyBgZWplY3RgLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZuIFRoZSBmdW5jdGlvbiB0byBjYWxsIGZvciBlYWNoIGludGVyY2VwdG9yXG4gKi9cbkludGVyY2VwdG9yTWFuYWdlci5wcm90b3R5cGUuZm9yRWFjaCA9IGZ1bmN0aW9uIGZvckVhY2goZm4pIHtcbiAgdXRpbHMuZm9yRWFjaCh0aGlzLmhhbmRsZXJzLCBmdW5jdGlvbiBmb3JFYWNoSGFuZGxlcihoKSB7XG4gICAgaWYgKGggIT09IG51bGwpIHtcbiAgICAgIGZuKGgpO1xuICAgIH1cbiAgfSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEludGVyY2VwdG9yTWFuYWdlcjtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2NvcmUvSW50ZXJjZXB0b3JNYW5hZ2VyLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG52YXIgdHJhbnNmb3JtRGF0YSA9IHJlcXVpcmUoJy4vdHJhbnNmb3JtRGF0YScpO1xuXG4vKipcbiAqIERpc3BhdGNoIGEgcmVxdWVzdCB0byB0aGUgc2VydmVyIHVzaW5nIHdoaWNoZXZlciBhZGFwdGVyXG4gKiBpcyBzdXBwb3J0ZWQgYnkgdGhlIGN1cnJlbnQgZW52aXJvbm1lbnQuXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IGNvbmZpZyBUaGUgY29uZmlnIHRoYXQgaXMgdG8gYmUgdXNlZCBmb3IgdGhlIHJlcXVlc3RcbiAqIEByZXR1cm5zIHtQcm9taXNlfSBUaGUgUHJvbWlzZSB0byBiZSBmdWxmaWxsZWRcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBkaXNwYXRjaFJlcXVlc3QoY29uZmlnKSB7XG4gIC8vIEVuc3VyZSBoZWFkZXJzIGV4aXN0XG4gIGNvbmZpZy5oZWFkZXJzID0gY29uZmlnLmhlYWRlcnMgfHwge307XG5cbiAgLy8gVHJhbnNmb3JtIHJlcXVlc3QgZGF0YVxuICBjb25maWcuZGF0YSA9IHRyYW5zZm9ybURhdGEoXG4gICAgY29uZmlnLmRhdGEsXG4gICAgY29uZmlnLmhlYWRlcnMsXG4gICAgY29uZmlnLnRyYW5zZm9ybVJlcXVlc3RcbiAgKTtcblxuICAvLyBGbGF0dGVuIGhlYWRlcnNcbiAgY29uZmlnLmhlYWRlcnMgPSB1dGlscy5tZXJnZShcbiAgICBjb25maWcuaGVhZGVycy5jb21tb24gfHwge30sXG4gICAgY29uZmlnLmhlYWRlcnNbY29uZmlnLm1ldGhvZF0gfHwge30sXG4gICAgY29uZmlnLmhlYWRlcnMgfHwge31cbiAgKTtcblxuICB1dGlscy5mb3JFYWNoKFxuICAgIFsnZGVsZXRlJywgJ2dldCcsICdoZWFkJywgJ3Bvc3QnLCAncHV0JywgJ3BhdGNoJywgJ2NvbW1vbiddLFxuICAgIGZ1bmN0aW9uIGNsZWFuSGVhZGVyQ29uZmlnKG1ldGhvZCkge1xuICAgICAgZGVsZXRlIGNvbmZpZy5oZWFkZXJzW21ldGhvZF07XG4gICAgfVxuICApO1xuXG4gIHZhciBhZGFwdGVyO1xuXG4gIGlmICh0eXBlb2YgY29uZmlnLmFkYXB0ZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAvLyBGb3IgY3VzdG9tIGFkYXB0ZXIgc3VwcG9ydFxuICAgIGFkYXB0ZXIgPSBjb25maWcuYWRhcHRlcjtcbiAgfSBlbHNlIGlmICh0eXBlb2YgWE1MSHR0cFJlcXVlc3QgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgLy8gRm9yIGJyb3dzZXJzIHVzZSBYSFIgYWRhcHRlclxuICAgIGFkYXB0ZXIgPSByZXF1aXJlKCcuLi9hZGFwdGVycy94aHInKTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgcHJvY2VzcyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAvLyBGb3Igbm9kZSB1c2UgSFRUUCBhZGFwdGVyXG4gICAgYWRhcHRlciA9IHJlcXVpcmUoJy4uL2FkYXB0ZXJzL2h0dHAnKTtcbiAgfVxuXG4gIHJldHVybiBQcm9taXNlLnJlc29sdmUoY29uZmlnKVxuICAgIC8vIFdyYXAgc3luY2hyb25vdXMgYWRhcHRlciBlcnJvcnMgYW5kIHBhc3MgY29uZmlndXJhdGlvblxuICAgIC50aGVuKGFkYXB0ZXIpXG4gICAgLnRoZW4oZnVuY3Rpb24gb25GdWxmaWxsZWQocmVzcG9uc2UpIHtcbiAgICAgIC8vIFRyYW5zZm9ybSByZXNwb25zZSBkYXRhXG4gICAgICByZXNwb25zZS5kYXRhID0gdHJhbnNmb3JtRGF0YShcbiAgICAgICAgcmVzcG9uc2UuZGF0YSxcbiAgICAgICAgcmVzcG9uc2UuaGVhZGVycyxcbiAgICAgICAgY29uZmlnLnRyYW5zZm9ybVJlc3BvbnNlXG4gICAgICApO1xuXG4gICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgfSwgZnVuY3Rpb24gb25SZWplY3RlZChlcnJvcikge1xuICAgICAgLy8gVHJhbnNmb3JtIHJlc3BvbnNlIGRhdGFcbiAgICAgIGlmIChlcnJvciAmJiBlcnJvci5yZXNwb25zZSkge1xuICAgICAgICBlcnJvci5yZXNwb25zZS5kYXRhID0gdHJhbnNmb3JtRGF0YShcbiAgICAgICAgICBlcnJvci5yZXNwb25zZS5kYXRhLFxuICAgICAgICAgIGVycm9yLnJlc3BvbnNlLmhlYWRlcnMsXG4gICAgICAgICAgY29uZmlnLnRyYW5zZm9ybVJlc3BvbnNlXG4gICAgICAgICk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgfSk7XG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9heGlvcy9saWIvY29yZS9kaXNwYXRjaFJlcXVlc3QuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxuLyoqXG4gKiBUcmFuc2Zvcm0gdGhlIGRhdGEgZm9yIGEgcmVxdWVzdCBvciBhIHJlc3BvbnNlXG4gKlxuICogQHBhcmFtIHtPYmplY3R8U3RyaW5nfSBkYXRhIFRoZSBkYXRhIHRvIGJlIHRyYW5zZm9ybWVkXG4gKiBAcGFyYW0ge0FycmF5fSBoZWFkZXJzIFRoZSBoZWFkZXJzIGZvciB0aGUgcmVxdWVzdCBvciByZXNwb25zZVxuICogQHBhcmFtIHtBcnJheXxGdW5jdGlvbn0gZm5zIEEgc2luZ2xlIGZ1bmN0aW9uIG9yIEFycmF5IG9mIGZ1bmN0aW9uc1xuICogQHJldHVybnMgeyp9IFRoZSByZXN1bHRpbmcgdHJhbnNmb3JtZWQgZGF0YVxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHRyYW5zZm9ybURhdGEoZGF0YSwgaGVhZGVycywgZm5zKSB7XG4gIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICB1dGlscy5mb3JFYWNoKGZucywgZnVuY3Rpb24gdHJhbnNmb3JtKGZuKSB7XG4gICAgZGF0YSA9IGZuKGRhdGEsIGhlYWRlcnMpO1xuICB9KTtcblxuICByZXR1cm4gZGF0YTtcbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L2F4aW9zL2xpYi9jb3JlL3RyYW5zZm9ybURhdGEuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcbnZhciBzZXR0bGUgPSByZXF1aXJlKCcuLy4uL2NvcmUvc2V0dGxlJyk7XG52YXIgYnVpbGRVUkwgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvYnVpbGRVUkwnKTtcbnZhciBwYXJzZUhlYWRlcnMgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvcGFyc2VIZWFkZXJzJyk7XG52YXIgaXNVUkxTYW1lT3JpZ2luID0gcmVxdWlyZSgnLi8uLi9oZWxwZXJzL2lzVVJMU2FtZU9yaWdpbicpO1xudmFyIGNyZWF0ZUVycm9yID0gcmVxdWlyZSgnLi4vY29yZS9jcmVhdGVFcnJvcicpO1xudmFyIGJ0b2EgPSAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmJ0b2EpIHx8IHJlcXVpcmUoJy4vLi4vaGVscGVycy9idG9hJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24geGhyQWRhcHRlcihjb25maWcpIHtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIGRpc3BhdGNoWGhyUmVxdWVzdChyZXNvbHZlLCByZWplY3QpIHtcbiAgICB2YXIgcmVxdWVzdERhdGEgPSBjb25maWcuZGF0YTtcbiAgICB2YXIgcmVxdWVzdEhlYWRlcnMgPSBjb25maWcuaGVhZGVycztcblxuICAgIGlmICh1dGlscy5pc0Zvcm1EYXRhKHJlcXVlc3REYXRhKSkge1xuICAgICAgZGVsZXRlIHJlcXVlc3RIZWFkZXJzWydDb250ZW50LVR5cGUnXTsgLy8gTGV0IHRoZSBicm93c2VyIHNldCBpdFxuICAgIH1cblxuICAgIHZhciByZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgdmFyIGxvYWRFdmVudCA9ICdvbnJlYWR5c3RhdGVjaGFuZ2UnO1xuICAgIHZhciB4RG9tYWluID0gZmFsc2U7XG5cbiAgICAvLyBGb3IgSUUgOC85IENPUlMgc3VwcG9ydFxuICAgIC8vIE9ubHkgc3VwcG9ydHMgUE9TVCBhbmQgR0VUIGNhbGxzIGFuZCBkb2Vzbid0IHJldHVybnMgdGhlIHJlc3BvbnNlIGhlYWRlcnMuXG4gICAgLy8gRE9OJ1QgZG8gdGhpcyBmb3IgdGVzdGluZyBiL2MgWE1MSHR0cFJlcXVlc3QgaXMgbW9ja2VkLCBub3QgWERvbWFpblJlcXVlc3QuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAndGVzdCcgJiZcbiAgICAgICAgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICAgICAgd2luZG93LlhEb21haW5SZXF1ZXN0ICYmICEoJ3dpdGhDcmVkZW50aWFscycgaW4gcmVxdWVzdCkgJiZcbiAgICAgICAgIWlzVVJMU2FtZU9yaWdpbihjb25maWcudXJsKSkge1xuICAgICAgcmVxdWVzdCA9IG5ldyB3aW5kb3cuWERvbWFpblJlcXVlc3QoKTtcbiAgICAgIGxvYWRFdmVudCA9ICdvbmxvYWQnO1xuICAgICAgeERvbWFpbiA9IHRydWU7XG4gICAgICByZXF1ZXN0Lm9ucHJvZ3Jlc3MgPSBmdW5jdGlvbiBoYW5kbGVQcm9ncmVzcygpIHt9O1xuICAgICAgcmVxdWVzdC5vbnRpbWVvdXQgPSBmdW5jdGlvbiBoYW5kbGVUaW1lb3V0KCkge307XG4gICAgfVxuXG4gICAgLy8gSFRUUCBiYXNpYyBhdXRoZW50aWNhdGlvblxuICAgIGlmIChjb25maWcuYXV0aCkge1xuICAgICAgdmFyIHVzZXJuYW1lID0gY29uZmlnLmF1dGgudXNlcm5hbWUgfHwgJyc7XG4gICAgICB2YXIgcGFzc3dvcmQgPSBjb25maWcuYXV0aC5wYXNzd29yZCB8fCAnJztcbiAgICAgIHJlcXVlc3RIZWFkZXJzLkF1dGhvcml6YXRpb24gPSAnQmFzaWMgJyArIGJ0b2EodXNlcm5hbWUgKyAnOicgKyBwYXNzd29yZCk7XG4gICAgfVxuXG4gICAgcmVxdWVzdC5vcGVuKGNvbmZpZy5tZXRob2QudG9VcHBlckNhc2UoKSwgYnVpbGRVUkwoY29uZmlnLnVybCwgY29uZmlnLnBhcmFtcywgY29uZmlnLnBhcmFtc1NlcmlhbGl6ZXIpLCB0cnVlKTtcblxuICAgIC8vIFNldCB0aGUgcmVxdWVzdCB0aW1lb3V0IGluIE1TXG4gICAgcmVxdWVzdC50aW1lb3V0ID0gY29uZmlnLnRpbWVvdXQ7XG5cbiAgICAvLyBMaXN0ZW4gZm9yIHJlYWR5IHN0YXRlXG4gICAgcmVxdWVzdFtsb2FkRXZlbnRdID0gZnVuY3Rpb24gaGFuZGxlTG9hZCgpIHtcbiAgICAgIGlmICghcmVxdWVzdCB8fCAocmVxdWVzdC5yZWFkeVN0YXRlICE9PSA0ICYmICF4RG9tYWluKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIFRoZSByZXF1ZXN0IGVycm9yZWQgb3V0IGFuZCB3ZSBkaWRuJ3QgZ2V0IGEgcmVzcG9uc2UsIHRoaXMgd2lsbCBiZVxuICAgICAgLy8gaGFuZGxlZCBieSBvbmVycm9yIGluc3RlYWRcbiAgICAgIGlmIChyZXF1ZXN0LnN0YXR1cyA9PT0gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIFByZXBhcmUgdGhlIHJlc3BvbnNlXG4gICAgICB2YXIgcmVzcG9uc2VIZWFkZXJzID0gJ2dldEFsbFJlc3BvbnNlSGVhZGVycycgaW4gcmVxdWVzdCA/IHBhcnNlSGVhZGVycyhyZXF1ZXN0LmdldEFsbFJlc3BvbnNlSGVhZGVycygpKSA6IG51bGw7XG4gICAgICB2YXIgcmVzcG9uc2VEYXRhID0gIWNvbmZpZy5yZXNwb25zZVR5cGUgfHwgY29uZmlnLnJlc3BvbnNlVHlwZSA9PT0gJ3RleHQnID8gcmVxdWVzdC5yZXNwb25zZVRleHQgOiByZXF1ZXN0LnJlc3BvbnNlO1xuICAgICAgdmFyIHJlc3BvbnNlID0ge1xuICAgICAgICBkYXRhOiByZXNwb25zZURhdGEsXG4gICAgICAgIC8vIElFIHNlbmRzIDEyMjMgaW5zdGVhZCBvZiAyMDQgKGh0dHBzOi8vZ2l0aHViLmNvbS9temFicmlza2llL2F4aW9zL2lzc3Vlcy8yMDEpXG4gICAgICAgIHN0YXR1czogcmVxdWVzdC5zdGF0dXMgPT09IDEyMjMgPyAyMDQgOiByZXF1ZXN0LnN0YXR1cyxcbiAgICAgICAgc3RhdHVzVGV4dDogcmVxdWVzdC5zdGF0dXMgPT09IDEyMjMgPyAnTm8gQ29udGVudCcgOiByZXF1ZXN0LnN0YXR1c1RleHQsXG4gICAgICAgIGhlYWRlcnM6IHJlc3BvbnNlSGVhZGVycyxcbiAgICAgICAgY29uZmlnOiBjb25maWcsXG4gICAgICAgIHJlcXVlc3Q6IHJlcXVlc3RcbiAgICAgIH07XG5cbiAgICAgIHNldHRsZShyZXNvbHZlLCByZWplY3QsIHJlc3BvbnNlKTtcblxuICAgICAgLy8gQ2xlYW4gdXAgcmVxdWVzdFxuICAgICAgcmVxdWVzdCA9IG51bGw7XG4gICAgfTtcblxuICAgIC8vIEhhbmRsZSBsb3cgbGV2ZWwgbmV0d29yayBlcnJvcnNcbiAgICByZXF1ZXN0Lm9uZXJyb3IgPSBmdW5jdGlvbiBoYW5kbGVFcnJvcigpIHtcbiAgICAgIC8vIFJlYWwgZXJyb3JzIGFyZSBoaWRkZW4gZnJvbSB1cyBieSB0aGUgYnJvd3NlclxuICAgICAgLy8gb25lcnJvciBzaG91bGQgb25seSBmaXJlIGlmIGl0J3MgYSBuZXR3b3JrIGVycm9yXG4gICAgICByZWplY3QoY3JlYXRlRXJyb3IoJ05ldHdvcmsgRXJyb3InLCBjb25maWcpKTtcblxuICAgICAgLy8gQ2xlYW4gdXAgcmVxdWVzdFxuICAgICAgcmVxdWVzdCA9IG51bGw7XG4gICAgfTtcblxuICAgIC8vIEhhbmRsZSB0aW1lb3V0XG4gICAgcmVxdWVzdC5vbnRpbWVvdXQgPSBmdW5jdGlvbiBoYW5kbGVUaW1lb3V0KCkge1xuICAgICAgcmVqZWN0KGNyZWF0ZUVycm9yKCd0aW1lb3V0IG9mICcgKyBjb25maWcudGltZW91dCArICdtcyBleGNlZWRlZCcsIGNvbmZpZywgJ0VDT05OQUJPUlRFRCcpKTtcblxuICAgICAgLy8gQ2xlYW4gdXAgcmVxdWVzdFxuICAgICAgcmVxdWVzdCA9IG51bGw7XG4gICAgfTtcblxuICAgIC8vIEFkZCB4c3JmIGhlYWRlclxuICAgIC8vIFRoaXMgaXMgb25seSBkb25lIGlmIHJ1bm5pbmcgaW4gYSBzdGFuZGFyZCBicm93c2VyIGVudmlyb25tZW50LlxuICAgIC8vIFNwZWNpZmljYWxseSBub3QgaWYgd2UncmUgaW4gYSB3ZWIgd29ya2VyLCBvciByZWFjdC1uYXRpdmUuXG4gICAgaWYgKHV0aWxzLmlzU3RhbmRhcmRCcm93c2VyRW52KCkpIHtcbiAgICAgIHZhciBjb29raWVzID0gcmVxdWlyZSgnLi8uLi9oZWxwZXJzL2Nvb2tpZXMnKTtcblxuICAgICAgLy8gQWRkIHhzcmYgaGVhZGVyXG4gICAgICB2YXIgeHNyZlZhbHVlID0gKGNvbmZpZy53aXRoQ3JlZGVudGlhbHMgfHwgaXNVUkxTYW1lT3JpZ2luKGNvbmZpZy51cmwpKSAmJiBjb25maWcueHNyZkNvb2tpZU5hbWUgP1xuICAgICAgICAgIGNvb2tpZXMucmVhZChjb25maWcueHNyZkNvb2tpZU5hbWUpIDpcbiAgICAgICAgICB1bmRlZmluZWQ7XG5cbiAgICAgIGlmICh4c3JmVmFsdWUpIHtcbiAgICAgICAgcmVxdWVzdEhlYWRlcnNbY29uZmlnLnhzcmZIZWFkZXJOYW1lXSA9IHhzcmZWYWx1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBBZGQgaGVhZGVycyB0byB0aGUgcmVxdWVzdFxuICAgIGlmICgnc2V0UmVxdWVzdEhlYWRlcicgaW4gcmVxdWVzdCkge1xuICAgICAgdXRpbHMuZm9yRWFjaChyZXF1ZXN0SGVhZGVycywgZnVuY3Rpb24gc2V0UmVxdWVzdEhlYWRlcih2YWwsIGtleSkge1xuICAgICAgICBpZiAodHlwZW9mIHJlcXVlc3REYXRhID09PSAndW5kZWZpbmVkJyAmJiBrZXkudG9Mb3dlckNhc2UoKSA9PT0gJ2NvbnRlbnQtdHlwZScpIHtcbiAgICAgICAgICAvLyBSZW1vdmUgQ29udGVudC1UeXBlIGlmIGRhdGEgaXMgdW5kZWZpbmVkXG4gICAgICAgICAgZGVsZXRlIHJlcXVlc3RIZWFkZXJzW2tleV07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gT3RoZXJ3aXNlIGFkZCBoZWFkZXIgdG8gdGhlIHJlcXVlc3RcbiAgICAgICAgICByZXF1ZXN0LnNldFJlcXVlc3RIZWFkZXIoa2V5LCB2YWwpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBBZGQgd2l0aENyZWRlbnRpYWxzIHRvIHJlcXVlc3QgaWYgbmVlZGVkXG4gICAgaWYgKGNvbmZpZy53aXRoQ3JlZGVudGlhbHMpIHtcbiAgICAgIHJlcXVlc3Qud2l0aENyZWRlbnRpYWxzID0gdHJ1ZTtcbiAgICB9XG5cbiAgICAvLyBBZGQgcmVzcG9uc2VUeXBlIHRvIHJlcXVlc3QgaWYgbmVlZGVkXG4gICAgaWYgKGNvbmZpZy5yZXNwb25zZVR5cGUpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHJlcXVlc3QucmVzcG9uc2VUeXBlID0gY29uZmlnLnJlc3BvbnNlVHlwZTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgaWYgKHJlcXVlc3QucmVzcG9uc2VUeXBlICE9PSAnanNvbicpIHtcbiAgICAgICAgICB0aHJvdyBlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIHByb2dyZXNzIGlmIG5lZWRlZFxuICAgIGlmICh0eXBlb2YgY29uZmlnLm9uRG93bmxvYWRQcm9ncmVzcyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgcmVxdWVzdC5hZGRFdmVudExpc3RlbmVyKCdwcm9ncmVzcycsIGNvbmZpZy5vbkRvd25sb2FkUHJvZ3Jlc3MpO1xuICAgIH1cblxuICAgIC8vIE5vdCBhbGwgYnJvd3NlcnMgc3VwcG9ydCB1cGxvYWQgZXZlbnRzXG4gICAgaWYgKHR5cGVvZiBjb25maWcub25VcGxvYWRQcm9ncmVzcyA9PT0gJ2Z1bmN0aW9uJyAmJiByZXF1ZXN0LnVwbG9hZCkge1xuICAgICAgcmVxdWVzdC51cGxvYWQuYWRkRXZlbnRMaXN0ZW5lcigncHJvZ3Jlc3MnLCBjb25maWcub25VcGxvYWRQcm9ncmVzcyk7XG4gICAgfVxuXG5cbiAgICBpZiAocmVxdWVzdERhdGEgPT09IHVuZGVmaW5lZCkge1xuICAgICAgcmVxdWVzdERhdGEgPSBudWxsO1xuICAgIH1cblxuICAgIC8vIFNlbmQgdGhlIHJlcXVlc3RcbiAgICByZXF1ZXN0LnNlbmQocmVxdWVzdERhdGEpO1xuICB9KTtcbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L2F4aW9zL2xpYi9hZGFwdGVycy94aHIuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbnZhciBjcmVhdGVFcnJvciA9IHJlcXVpcmUoJy4vY3JlYXRlRXJyb3InKTtcblxuLyoqXG4gKiBSZXNvbHZlIG9yIHJlamVjdCBhIFByb21pc2UgYmFzZWQgb24gcmVzcG9uc2Ugc3RhdHVzLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHJlc29sdmUgQSBmdW5jdGlvbiB0aGF0IHJlc29sdmVzIHRoZSBwcm9taXNlLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gcmVqZWN0IEEgZnVuY3Rpb24gdGhhdCByZWplY3RzIHRoZSBwcm9taXNlLlxuICogQHBhcmFtIHtvYmplY3R9IHJlc3BvbnNlIFRoZSByZXNwb25zZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCByZXNwb25zZSkge1xuICB2YXIgdmFsaWRhdGVTdGF0dXMgPSByZXNwb25zZS5jb25maWcudmFsaWRhdGVTdGF0dXM7XG4gIC8vIE5vdGU6IHN0YXR1cyBpcyBub3QgZXhwb3NlZCBieSBYRG9tYWluUmVxdWVzdFxuICBpZiAoIXJlc3BvbnNlLnN0YXR1cyB8fCAhdmFsaWRhdGVTdGF0dXMgfHwgdmFsaWRhdGVTdGF0dXMocmVzcG9uc2Uuc3RhdHVzKSkge1xuICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICB9IGVsc2Uge1xuICAgIHJlamVjdChjcmVhdGVFcnJvcihcbiAgICAgICdSZXF1ZXN0IGZhaWxlZCB3aXRoIHN0YXR1cyBjb2RlICcgKyByZXNwb25zZS5zdGF0dXMsXG4gICAgICByZXNwb25zZS5jb25maWcsXG4gICAgICBudWxsLFxuICAgICAgcmVzcG9uc2VcbiAgICApKTtcbiAgfVxufTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2NvcmUvc2V0dGxlLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgZW5oYW5jZUVycm9yID0gcmVxdWlyZSgnLi9lbmhhbmNlRXJyb3InKTtcblxuLyoqXG4gKiBDcmVhdGUgYW4gRXJyb3Igd2l0aCB0aGUgc3BlY2lmaWVkIG1lc3NhZ2UsIGNvbmZpZywgZXJyb3IgY29kZSwgYW5kIHJlc3BvbnNlLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBtZXNzYWdlIFRoZSBlcnJvciBtZXNzYWdlLlxuICogQHBhcmFtIHtPYmplY3R9IGNvbmZpZyBUaGUgY29uZmlnLlxuICogQHBhcmFtIHtzdHJpbmd9IFtjb2RlXSBUaGUgZXJyb3IgY29kZSAoZm9yIGV4YW1wbGUsICdFQ09OTkFCT1JURUQnKS5cbiBAIEBwYXJhbSB7T2JqZWN0fSBbcmVzcG9uc2VdIFRoZSByZXNwb25zZS5cbiAqIEByZXR1cm5zIHtFcnJvcn0gVGhlIGNyZWF0ZWQgZXJyb3IuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gY3JlYXRlRXJyb3IobWVzc2FnZSwgY29uZmlnLCBjb2RlLCByZXNwb25zZSkge1xuICB2YXIgZXJyb3IgPSBuZXcgRXJyb3IobWVzc2FnZSk7XG4gIHJldHVybiBlbmhhbmNlRXJyb3IoZXJyb3IsIGNvbmZpZywgY29kZSwgcmVzcG9uc2UpO1xufTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2NvcmUvY3JlYXRlRXJyb3IuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogVXBkYXRlIGFuIEVycm9yIHdpdGggdGhlIHNwZWNpZmllZCBjb25maWcsIGVycm9yIGNvZGUsIGFuZCByZXNwb25zZS5cbiAqXG4gKiBAcGFyYW0ge0Vycm9yfSBlcnJvciBUaGUgZXJyb3IgdG8gdXBkYXRlLlxuICogQHBhcmFtIHtPYmplY3R9IGNvbmZpZyBUaGUgY29uZmlnLlxuICogQHBhcmFtIHtzdHJpbmd9IFtjb2RlXSBUaGUgZXJyb3IgY29kZSAoZm9yIGV4YW1wbGUsICdFQ09OTkFCT1JURUQnKS5cbiBAIEBwYXJhbSB7T2JqZWN0fSBbcmVzcG9uc2VdIFRoZSByZXNwb25zZS5cbiAqIEByZXR1cm5zIHtFcnJvcn0gVGhlIGVycm9yLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGVuaGFuY2VFcnJvcihlcnJvciwgY29uZmlnLCBjb2RlLCByZXNwb25zZSkge1xuICBlcnJvci5jb25maWcgPSBjb25maWc7XG4gIGlmIChjb2RlKSB7XG4gICAgZXJyb3IuY29kZSA9IGNvZGU7XG4gIH1cbiAgZXJyb3IucmVzcG9uc2UgPSByZXNwb25zZTtcbiAgcmV0dXJuIGVycm9yO1xufTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2NvcmUvZW5oYW5jZUVycm9yLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbmZ1bmN0aW9uIGVuY29kZSh2YWwpIHtcbiAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudCh2YWwpLlxuICAgIHJlcGxhY2UoLyU0MC9naSwgJ0AnKS5cbiAgICByZXBsYWNlKC8lM0EvZ2ksICc6JykuXG4gICAgcmVwbGFjZSgvJTI0L2csICckJykuXG4gICAgcmVwbGFjZSgvJTJDL2dpLCAnLCcpLlxuICAgIHJlcGxhY2UoLyUyMC9nLCAnKycpLlxuICAgIHJlcGxhY2UoLyU1Qi9naSwgJ1snKS5cbiAgICByZXBsYWNlKC8lNUQvZ2ksICddJyk7XG59XG5cbi8qKlxuICogQnVpbGQgYSBVUkwgYnkgYXBwZW5kaW5nIHBhcmFtcyB0byB0aGUgZW5kXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IHVybCBUaGUgYmFzZSBvZiB0aGUgdXJsIChlLmcuLCBodHRwOi8vd3d3Lmdvb2dsZS5jb20pXG4gKiBAcGFyYW0ge29iamVjdH0gW3BhcmFtc10gVGhlIHBhcmFtcyB0byBiZSBhcHBlbmRlZFxuICogQHJldHVybnMge3N0cmluZ30gVGhlIGZvcm1hdHRlZCB1cmxcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBidWlsZFVSTCh1cmwsIHBhcmFtcywgcGFyYW1zU2VyaWFsaXplcikge1xuICAvKmVzbGludCBuby1wYXJhbS1yZWFzc2lnbjowKi9cbiAgaWYgKCFwYXJhbXMpIHtcbiAgICByZXR1cm4gdXJsO1xuICB9XG5cbiAgdmFyIHNlcmlhbGl6ZWRQYXJhbXM7XG4gIGlmIChwYXJhbXNTZXJpYWxpemVyKSB7XG4gICAgc2VyaWFsaXplZFBhcmFtcyA9IHBhcmFtc1NlcmlhbGl6ZXIocGFyYW1zKTtcbiAgfSBlbHNlIGlmICh1dGlscy5pc1VSTFNlYXJjaFBhcmFtcyhwYXJhbXMpKSB7XG4gICAgc2VyaWFsaXplZFBhcmFtcyA9IHBhcmFtcy50b1N0cmluZygpO1xuICB9IGVsc2Uge1xuICAgIHZhciBwYXJ0cyA9IFtdO1xuXG4gICAgdXRpbHMuZm9yRWFjaChwYXJhbXMsIGZ1bmN0aW9uIHNlcmlhbGl6ZSh2YWwsIGtleSkge1xuICAgICAgaWYgKHZhbCA9PT0gbnVsbCB8fCB0eXBlb2YgdmFsID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmICh1dGlscy5pc0FycmF5KHZhbCkpIHtcbiAgICAgICAga2V5ID0ga2V5ICsgJ1tdJztcbiAgICAgIH1cblxuICAgICAgaWYgKCF1dGlscy5pc0FycmF5KHZhbCkpIHtcbiAgICAgICAgdmFsID0gW3ZhbF07XG4gICAgICB9XG5cbiAgICAgIHV0aWxzLmZvckVhY2godmFsLCBmdW5jdGlvbiBwYXJzZVZhbHVlKHYpIHtcbiAgICAgICAgaWYgKHV0aWxzLmlzRGF0ZSh2KSkge1xuICAgICAgICAgIHYgPSB2LnRvSVNPU3RyaW5nKCk7XG4gICAgICAgIH0gZWxzZSBpZiAodXRpbHMuaXNPYmplY3QodikpIHtcbiAgICAgICAgICB2ID0gSlNPTi5zdHJpbmdpZnkodik7XG4gICAgICAgIH1cbiAgICAgICAgcGFydHMucHVzaChlbmNvZGUoa2V5KSArICc9JyArIGVuY29kZSh2KSk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIHNlcmlhbGl6ZWRQYXJhbXMgPSBwYXJ0cy5qb2luKCcmJyk7XG4gIH1cblxuICBpZiAoc2VyaWFsaXplZFBhcmFtcykge1xuICAgIHVybCArPSAodXJsLmluZGV4T2YoJz8nKSA9PT0gLTEgPyAnPycgOiAnJicpICsgc2VyaWFsaXplZFBhcmFtcztcbiAgfVxuXG4gIHJldHVybiB1cmw7XG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9heGlvcy9saWIvaGVscGVycy9idWlsZFVSTC5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi8uLi91dGlscycpO1xuXG4vKipcbiAqIFBhcnNlIGhlYWRlcnMgaW50byBhbiBvYmplY3RcbiAqXG4gKiBgYGBcbiAqIERhdGU6IFdlZCwgMjcgQXVnIDIwMTQgMDg6NTg6NDkgR01UXG4gKiBDb250ZW50LVR5cGU6IGFwcGxpY2F0aW9uL2pzb25cbiAqIENvbm5lY3Rpb246IGtlZXAtYWxpdmVcbiAqIFRyYW5zZmVyLUVuY29kaW5nOiBjaHVua2VkXG4gKiBgYGBcbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gaGVhZGVycyBIZWFkZXJzIG5lZWRpbmcgdG8gYmUgcGFyc2VkXG4gKiBAcmV0dXJucyB7T2JqZWN0fSBIZWFkZXJzIHBhcnNlZCBpbnRvIGFuIG9iamVjdFxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHBhcnNlSGVhZGVycyhoZWFkZXJzKSB7XG4gIHZhciBwYXJzZWQgPSB7fTtcbiAgdmFyIGtleTtcbiAgdmFyIHZhbDtcbiAgdmFyIGk7XG5cbiAgaWYgKCFoZWFkZXJzKSB7IHJldHVybiBwYXJzZWQ7IH1cblxuICB1dGlscy5mb3JFYWNoKGhlYWRlcnMuc3BsaXQoJ1xcbicpLCBmdW5jdGlvbiBwYXJzZXIobGluZSkge1xuICAgIGkgPSBsaW5lLmluZGV4T2YoJzonKTtcbiAgICBrZXkgPSB1dGlscy50cmltKGxpbmUuc3Vic3RyKDAsIGkpKS50b0xvd2VyQ2FzZSgpO1xuICAgIHZhbCA9IHV0aWxzLnRyaW0obGluZS5zdWJzdHIoaSArIDEpKTtcblxuICAgIGlmIChrZXkpIHtcbiAgICAgIHBhcnNlZFtrZXldID0gcGFyc2VkW2tleV0gPyBwYXJzZWRba2V5XSArICcsICcgKyB2YWwgOiB2YWw7XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gcGFyc2VkO1xufTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2hlbHBlcnMvcGFyc2VIZWFkZXJzLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gKFxuICB1dGlscy5pc1N0YW5kYXJkQnJvd3NlckVudigpID9cblxuICAvLyBTdGFuZGFyZCBicm93c2VyIGVudnMgaGF2ZSBmdWxsIHN1cHBvcnQgb2YgdGhlIEFQSXMgbmVlZGVkIHRvIHRlc3RcbiAgLy8gd2hldGhlciB0aGUgcmVxdWVzdCBVUkwgaXMgb2YgdGhlIHNhbWUgb3JpZ2luIGFzIGN1cnJlbnQgbG9jYXRpb24uXG4gIChmdW5jdGlvbiBzdGFuZGFyZEJyb3dzZXJFbnYoKSB7XG4gICAgdmFyIG1zaWUgPSAvKG1zaWV8dHJpZGVudCkvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO1xuICAgIHZhciB1cmxQYXJzaW5nTm9kZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcbiAgICB2YXIgb3JpZ2luVVJMO1xuXG4gICAgLyoqXG4gICAgKiBQYXJzZSBhIFVSTCB0byBkaXNjb3ZlciBpdCdzIGNvbXBvbmVudHNcbiAgICAqXG4gICAgKiBAcGFyYW0ge1N0cmluZ30gdXJsIFRoZSBVUkwgdG8gYmUgcGFyc2VkXG4gICAgKiBAcmV0dXJucyB7T2JqZWN0fVxuICAgICovXG4gICAgZnVuY3Rpb24gcmVzb2x2ZVVSTCh1cmwpIHtcbiAgICAgIHZhciBocmVmID0gdXJsO1xuXG4gICAgICBpZiAobXNpZSkge1xuICAgICAgICAvLyBJRSBuZWVkcyBhdHRyaWJ1dGUgc2V0IHR3aWNlIHRvIG5vcm1hbGl6ZSBwcm9wZXJ0aWVzXG4gICAgICAgIHVybFBhcnNpbmdOb2RlLnNldEF0dHJpYnV0ZSgnaHJlZicsIGhyZWYpO1xuICAgICAgICBocmVmID0gdXJsUGFyc2luZ05vZGUuaHJlZjtcbiAgICAgIH1cblxuICAgICAgdXJsUGFyc2luZ05vZGUuc2V0QXR0cmlidXRlKCdocmVmJywgaHJlZik7XG5cbiAgICAgIC8vIHVybFBhcnNpbmdOb2RlIHByb3ZpZGVzIHRoZSBVcmxVdGlscyBpbnRlcmZhY2UgLSBodHRwOi8vdXJsLnNwZWMud2hhdHdnLm9yZy8jdXJsdXRpbHNcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGhyZWY6IHVybFBhcnNpbmdOb2RlLmhyZWYsXG4gICAgICAgIHByb3RvY29sOiB1cmxQYXJzaW5nTm9kZS5wcm90b2NvbCA/IHVybFBhcnNpbmdOb2RlLnByb3RvY29sLnJlcGxhY2UoLzokLywgJycpIDogJycsXG4gICAgICAgIGhvc3Q6IHVybFBhcnNpbmdOb2RlLmhvc3QsXG4gICAgICAgIHNlYXJjaDogdXJsUGFyc2luZ05vZGUuc2VhcmNoID8gdXJsUGFyc2luZ05vZGUuc2VhcmNoLnJlcGxhY2UoL15cXD8vLCAnJykgOiAnJyxcbiAgICAgICAgaGFzaDogdXJsUGFyc2luZ05vZGUuaGFzaCA/IHVybFBhcnNpbmdOb2RlLmhhc2gucmVwbGFjZSgvXiMvLCAnJykgOiAnJyxcbiAgICAgICAgaG9zdG5hbWU6IHVybFBhcnNpbmdOb2RlLmhvc3RuYW1lLFxuICAgICAgICBwb3J0OiB1cmxQYXJzaW5nTm9kZS5wb3J0LFxuICAgICAgICBwYXRobmFtZTogKHVybFBhcnNpbmdOb2RlLnBhdGhuYW1lLmNoYXJBdCgwKSA9PT0gJy8nKSA/XG4gICAgICAgICAgICAgICAgICB1cmxQYXJzaW5nTm9kZS5wYXRobmFtZSA6XG4gICAgICAgICAgICAgICAgICAnLycgKyB1cmxQYXJzaW5nTm9kZS5wYXRobmFtZVxuICAgICAgfTtcbiAgICB9XG5cbiAgICBvcmlnaW5VUkwgPSByZXNvbHZlVVJMKHdpbmRvdy5sb2NhdGlvbi5ocmVmKTtcblxuICAgIC8qKlxuICAgICogRGV0ZXJtaW5lIGlmIGEgVVJMIHNoYXJlcyB0aGUgc2FtZSBvcmlnaW4gYXMgdGhlIGN1cnJlbnQgbG9jYXRpb25cbiAgICAqXG4gICAgKiBAcGFyYW0ge1N0cmluZ30gcmVxdWVzdFVSTCBUaGUgVVJMIHRvIHRlc3RcbiAgICAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIFVSTCBzaGFyZXMgdGhlIHNhbWUgb3JpZ2luLCBvdGhlcndpc2UgZmFsc2VcbiAgICAqL1xuICAgIHJldHVybiBmdW5jdGlvbiBpc1VSTFNhbWVPcmlnaW4ocmVxdWVzdFVSTCkge1xuICAgICAgdmFyIHBhcnNlZCA9ICh1dGlscy5pc1N0cmluZyhyZXF1ZXN0VVJMKSkgPyByZXNvbHZlVVJMKHJlcXVlc3RVUkwpIDogcmVxdWVzdFVSTDtcbiAgICAgIHJldHVybiAocGFyc2VkLnByb3RvY29sID09PSBvcmlnaW5VUkwucHJvdG9jb2wgJiZcbiAgICAgICAgICAgIHBhcnNlZC5ob3N0ID09PSBvcmlnaW5VUkwuaG9zdCk7XG4gICAgfTtcbiAgfSkoKSA6XG5cbiAgLy8gTm9uIHN0YW5kYXJkIGJyb3dzZXIgZW52cyAod2ViIHdvcmtlcnMsIHJlYWN0LW5hdGl2ZSkgbGFjayBuZWVkZWQgc3VwcG9ydC5cbiAgKGZ1bmN0aW9uIG5vblN0YW5kYXJkQnJvd3NlckVudigpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gaXNVUkxTYW1lT3JpZ2luKCkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTtcbiAgfSkoKVxuKTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2hlbHBlcnMvaXNVUkxTYW1lT3JpZ2luLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG4vLyBidG9hIHBvbHlmaWxsIGZvciBJRTwxMCBjb3VydGVzeSBodHRwczovL2dpdGh1Yi5jb20vZGF2aWRjaGFtYmVycy9CYXNlNjQuanNcblxudmFyIGNoYXJzID0gJ0FCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXowMTIzNDU2Nzg5Ky89JztcblxuZnVuY3Rpb24gRSgpIHtcbiAgdGhpcy5tZXNzYWdlID0gJ1N0cmluZyBjb250YWlucyBhbiBpbnZhbGlkIGNoYXJhY3Rlcic7XG59XG5FLnByb3RvdHlwZSA9IG5ldyBFcnJvcjtcbkUucHJvdG90eXBlLmNvZGUgPSA1O1xuRS5wcm90b3R5cGUubmFtZSA9ICdJbnZhbGlkQ2hhcmFjdGVyRXJyb3InO1xuXG5mdW5jdGlvbiBidG9hKGlucHV0KSB7XG4gIHZhciBzdHIgPSBTdHJpbmcoaW5wdXQpO1xuICB2YXIgb3V0cHV0ID0gJyc7XG4gIGZvciAoXG4gICAgLy8gaW5pdGlhbGl6ZSByZXN1bHQgYW5kIGNvdW50ZXJcbiAgICB2YXIgYmxvY2ssIGNoYXJDb2RlLCBpZHggPSAwLCBtYXAgPSBjaGFycztcbiAgICAvLyBpZiB0aGUgbmV4dCBzdHIgaW5kZXggZG9lcyBub3QgZXhpc3Q6XG4gICAgLy8gICBjaGFuZ2UgdGhlIG1hcHBpbmcgdGFibGUgdG8gXCI9XCJcbiAgICAvLyAgIGNoZWNrIGlmIGQgaGFzIG5vIGZyYWN0aW9uYWwgZGlnaXRzXG4gICAgc3RyLmNoYXJBdChpZHggfCAwKSB8fCAobWFwID0gJz0nLCBpZHggJSAxKTtcbiAgICAvLyBcIjggLSBpZHggJSAxICogOFwiIGdlbmVyYXRlcyB0aGUgc2VxdWVuY2UgMiwgNCwgNiwgOFxuICAgIG91dHB1dCArPSBtYXAuY2hhckF0KDYzICYgYmxvY2sgPj4gOCAtIGlkeCAlIDEgKiA4KVxuICApIHtcbiAgICBjaGFyQ29kZSA9IHN0ci5jaGFyQ29kZUF0KGlkeCArPSAzIC8gNCk7XG4gICAgaWYgKGNoYXJDb2RlID4gMHhGRikge1xuICAgICAgdGhyb3cgbmV3IEUoKTtcbiAgICB9XG4gICAgYmxvY2sgPSBibG9jayA8PCA4IHwgY2hhckNvZGU7XG4gIH1cbiAgcmV0dXJuIG91dHB1dDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBidG9hO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9heGlvcy9saWIvaGVscGVycy9idG9hLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gKFxuICB1dGlscy5pc1N0YW5kYXJkQnJvd3NlckVudigpID9cblxuICAvLyBTdGFuZGFyZCBicm93c2VyIGVudnMgc3VwcG9ydCBkb2N1bWVudC5jb29raWVcbiAgKGZ1bmN0aW9uIHN0YW5kYXJkQnJvd3NlckVudigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgd3JpdGU6IGZ1bmN0aW9uIHdyaXRlKG5hbWUsIHZhbHVlLCBleHBpcmVzLCBwYXRoLCBkb21haW4sIHNlY3VyZSkge1xuICAgICAgICB2YXIgY29va2llID0gW107XG4gICAgICAgIGNvb2tpZS5wdXNoKG5hbWUgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQodmFsdWUpKTtcblxuICAgICAgICBpZiAodXRpbHMuaXNOdW1iZXIoZXhwaXJlcykpIHtcbiAgICAgICAgICBjb29raWUucHVzaCgnZXhwaXJlcz0nICsgbmV3IERhdGUoZXhwaXJlcykudG9HTVRTdHJpbmcoKSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodXRpbHMuaXNTdHJpbmcocGF0aCkpIHtcbiAgICAgICAgICBjb29raWUucHVzaCgncGF0aD0nICsgcGF0aCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodXRpbHMuaXNTdHJpbmcoZG9tYWluKSkge1xuICAgICAgICAgIGNvb2tpZS5wdXNoKCdkb21haW49JyArIGRvbWFpbik7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoc2VjdXJlID09PSB0cnVlKSB7XG4gICAgICAgICAgY29va2llLnB1c2goJ3NlY3VyZScpO1xuICAgICAgICB9XG5cbiAgICAgICAgZG9jdW1lbnQuY29va2llID0gY29va2llLmpvaW4oJzsgJyk7XG4gICAgICB9LFxuXG4gICAgICByZWFkOiBmdW5jdGlvbiByZWFkKG5hbWUpIHtcbiAgICAgICAgdmFyIG1hdGNoID0gZG9jdW1lbnQuY29va2llLm1hdGNoKG5ldyBSZWdFeHAoJyhefDtcXFxccyopKCcgKyBuYW1lICsgJyk9KFteO10qKScpKTtcbiAgICAgICAgcmV0dXJuIChtYXRjaCA/IGRlY29kZVVSSUNvbXBvbmVudChtYXRjaFszXSkgOiBudWxsKTtcbiAgICAgIH0sXG5cbiAgICAgIHJlbW92ZTogZnVuY3Rpb24gcmVtb3ZlKG5hbWUpIHtcbiAgICAgICAgdGhpcy53cml0ZShuYW1lLCAnJywgRGF0ZS5ub3coKSAtIDg2NDAwMDAwKTtcbiAgICAgIH1cbiAgICB9O1xuICB9KSgpIDpcblxuICAvLyBOb24gc3RhbmRhcmQgYnJvd3NlciBlbnYgKHdlYiB3b3JrZXJzLCByZWFjdC1uYXRpdmUpIGxhY2sgbmVlZGVkIHN1cHBvcnQuXG4gIChmdW5jdGlvbiBub25TdGFuZGFyZEJyb3dzZXJFbnYoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHdyaXRlOiBmdW5jdGlvbiB3cml0ZSgpIHt9LFxuICAgICAgcmVhZDogZnVuY3Rpb24gcmVhZCgpIHsgcmV0dXJuIG51bGw7IH0sXG4gICAgICByZW1vdmU6IGZ1bmN0aW9uIHJlbW92ZSgpIHt9XG4gICAgfTtcbiAgfSkoKVxuKTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vYXhpb3MvbGliL2hlbHBlcnMvY29va2llcy5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBEZXRlcm1pbmVzIHdoZXRoZXIgdGhlIHNwZWNpZmllZCBVUkwgaXMgYWJzb2x1dGVcbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gdXJsIFRoZSBVUkwgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdGhlIHNwZWNpZmllZCBVUkwgaXMgYWJzb2x1dGUsIG90aGVyd2lzZSBmYWxzZVxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGlzQWJzb2x1dGVVUkwodXJsKSB7XG4gIC8vIEEgVVJMIGlzIGNvbnNpZGVyZWQgYWJzb2x1dGUgaWYgaXQgYmVnaW5zIHdpdGggXCI8c2NoZW1lPjovL1wiIG9yIFwiLy9cIiAocHJvdG9jb2wtcmVsYXRpdmUgVVJMKS5cbiAgLy8gUkZDIDM5ODYgZGVmaW5lcyBzY2hlbWUgbmFtZSBhcyBhIHNlcXVlbmNlIG9mIGNoYXJhY3RlcnMgYmVnaW5uaW5nIHdpdGggYSBsZXR0ZXIgYW5kIGZvbGxvd2VkXG4gIC8vIGJ5IGFueSBjb21iaW5hdGlvbiBvZiBsZXR0ZXJzLCBkaWdpdHMsIHBsdXMsIHBlcmlvZCwgb3IgaHlwaGVuLlxuICByZXR1cm4gL14oW2Etel1bYS16XFxkXFwrXFwtXFwuXSo6KT9cXC9cXC8vaS50ZXN0KHVybCk7XG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9heGlvcy9saWIvaGVscGVycy9pc0Fic29sdXRlVVJMLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIENyZWF0ZXMgYSBuZXcgVVJMIGJ5IGNvbWJpbmluZyB0aGUgc3BlY2lmaWVkIFVSTHNcbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gYmFzZVVSTCBUaGUgYmFzZSBVUkxcbiAqIEBwYXJhbSB7c3RyaW5nfSByZWxhdGl2ZVVSTCBUaGUgcmVsYXRpdmUgVVJMXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBUaGUgY29tYmluZWQgVVJMXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gY29tYmluZVVSTHMoYmFzZVVSTCwgcmVsYXRpdmVVUkwpIHtcbiAgcmV0dXJuIGJhc2VVUkwucmVwbGFjZSgvXFwvKyQvLCAnJykgKyAnLycgKyByZWxhdGl2ZVVSTC5yZXBsYWNlKC9eXFwvKy8sICcnKTtcbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L2F4aW9zL2xpYi9oZWxwZXJzL2NvbWJpbmVVUkxzLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIFN5bnRhY3RpYyBzdWdhciBmb3IgaW52b2tpbmcgYSBmdW5jdGlvbiBhbmQgZXhwYW5kaW5nIGFuIGFycmF5IGZvciBhcmd1bWVudHMuXG4gKlxuICogQ29tbW9uIHVzZSBjYXNlIHdvdWxkIGJlIHRvIHVzZSBgRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5YC5cbiAqXG4gKiAgYGBganNcbiAqICBmdW5jdGlvbiBmKHgsIHksIHopIHt9XG4gKiAgdmFyIGFyZ3MgPSBbMSwgMiwgM107XG4gKiAgZi5hcHBseShudWxsLCBhcmdzKTtcbiAqICBgYGBcbiAqXG4gKiBXaXRoIGBzcHJlYWRgIHRoaXMgZXhhbXBsZSBjYW4gYmUgcmUtd3JpdHRlbi5cbiAqXG4gKiAgYGBganNcbiAqICBzcHJlYWQoZnVuY3Rpb24oeCwgeSwgeikge30pKFsxLCAyLCAzXSk7XG4gKiAgYGBgXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm5zIHtGdW5jdGlvbn1cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzcHJlYWQoY2FsbGJhY2spIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIHdyYXAoYXJyKSB7XG4gICAgcmV0dXJuIGNhbGxiYWNrLmFwcGx5KG51bGwsIGFycik7XG4gIH07XG59O1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9heGlvcy9saWIvaGVscGVycy9zcHJlYWQuanNcbiAqKi8iLCJcInVzZSBzdHJpY3RcIjtcblxuZnVuY3Rpb24gX3RvQ29uc3VtYWJsZUFycmF5KGFycikgeyBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSB7IGZvciAodmFyIGkgPSAwLCBhcnIyID0gQXJyYXkoYXJyLmxlbmd0aCk7IGkgPCBhcnIubGVuZ3RoOyBpKyspIHsgYXJyMltpXSA9IGFycltpXTsgfSByZXR1cm4gYXJyMjsgfSBlbHNlIHsgcmV0dXJuIEFycmF5LmZyb20oYXJyKTsgfSB9XG5cbmZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCAhPT0gXCJ1bmRlZmluZWRcIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9XG5cbnZhciByZXBlYXQgPSBmdW5jdGlvbiByZXBlYXQoc3RyLCB0aW1lcykge1xuICByZXR1cm4gbmV3IEFycmF5KHRpbWVzICsgMSkuam9pbihzdHIpO1xufTtcbnZhciBwYWQgPSBmdW5jdGlvbiBwYWQobnVtLCBtYXhMZW5ndGgpIHtcbiAgcmV0dXJuIHJlcGVhdChcIjBcIiwgbWF4TGVuZ3RoIC0gbnVtLnRvU3RyaW5nKCkubGVuZ3RoKSArIG51bTtcbn07XG52YXIgZm9ybWF0VGltZSA9IGZ1bmN0aW9uIGZvcm1hdFRpbWUodGltZSkge1xuICByZXR1cm4gXCJAIFwiICsgcGFkKHRpbWUuZ2V0SG91cnMoKSwgMikgKyBcIjpcIiArIHBhZCh0aW1lLmdldE1pbnV0ZXMoKSwgMikgKyBcIjpcIiArIHBhZCh0aW1lLmdldFNlY29uZHMoKSwgMikgKyBcIi5cIiArIHBhZCh0aW1lLmdldE1pbGxpc2Vjb25kcygpLCAzKTtcbn07XG5cbi8vIFVzZSB0aGUgbmV3IHBlcmZvcm1hbmNlIGFwaSB0byBnZXQgYmV0dGVyIHByZWNpc2lvbiBpZiBhdmFpbGFibGVcbnZhciB0aW1lciA9IHR5cGVvZiBwZXJmb3JtYW5jZSAhPT0gXCJ1bmRlZmluZWRcIiAmJiB0eXBlb2YgcGVyZm9ybWFuY2Uubm93ID09PSBcImZ1bmN0aW9uXCIgPyBwZXJmb3JtYW5jZSA6IERhdGU7XG5cbi8qKlxuICogcGFyc2UgdGhlIGxldmVsIG9wdGlvbiBvZiBjcmVhdGVMb2dnZXJcbiAqXG4gKiBAcHJvcGVydHkge3N0cmluZyB8IGZ1bmN0aW9uIHwgb2JqZWN0fSBsZXZlbCAtIGNvbnNvbGVbbGV2ZWxdXG4gKiBAcHJvcGVydHkge29iamVjdH0gYWN0aW9uXG4gKiBAcHJvcGVydHkge2FycmF5fSBwYXlsb2FkXG4gKiBAcHJvcGVydHkge3N0cmluZ30gdHlwZVxuICovXG5cbmZ1bmN0aW9uIGdldExvZ0xldmVsKGxldmVsLCBhY3Rpb24sIHBheWxvYWQsIHR5cGUpIHtcbiAgc3dpdGNoICh0eXBlb2YgbGV2ZWwgPT09IFwidW5kZWZpbmVkXCIgPyBcInVuZGVmaW5lZFwiIDogX3R5cGVvZihsZXZlbCkpIHtcbiAgICBjYXNlIFwib2JqZWN0XCI6XG4gICAgICByZXR1cm4gdHlwZW9mIGxldmVsW3R5cGVdID09PSBcImZ1bmN0aW9uXCIgPyBsZXZlbFt0eXBlXS5hcHBseShsZXZlbCwgX3RvQ29uc3VtYWJsZUFycmF5KHBheWxvYWQpKSA6IGxldmVsW3R5cGVdO1xuICAgIGNhc2UgXCJmdW5jdGlvblwiOlxuICAgICAgcmV0dXJuIGxldmVsKGFjdGlvbik7XG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBsZXZlbDtcbiAgfVxufVxuXG4vKipcbiAqIENyZWF0ZXMgbG9nZ2VyIHdpdGggZm9sbG93ZWQgb3B0aW9uc1xuICpcbiAqIEBuYW1lc3BhY2VcbiAqIEBwcm9wZXJ0eSB7b2JqZWN0fSBvcHRpb25zIC0gb3B0aW9ucyBmb3IgbG9nZ2VyXG4gKiBAcHJvcGVydHkge3N0cmluZyB8IGZ1bmN0aW9uIHwgb2JqZWN0fSBvcHRpb25zLmxldmVsIC0gY29uc29sZVtsZXZlbF1cbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gb3B0aW9ucy5kdXJhdGlvbiAtIHByaW50IGR1cmF0aW9uIG9mIGVhY2ggYWN0aW9uP1xuICogQHByb3BlcnR5IHtib29sZWFufSBvcHRpb25zLnRpbWVzdGFtcCAtIHByaW50IHRpbWVzdGFtcCB3aXRoIGVhY2ggYWN0aW9uP1xuICogQHByb3BlcnR5IHtvYmplY3R9IG9wdGlvbnMuY29sb3JzIC0gY3VzdG9tIGNvbG9yc1xuICogQHByb3BlcnR5IHtvYmplY3R9IG9wdGlvbnMubG9nZ2VyIC0gaW1wbGVtZW50YXRpb24gb2YgdGhlIGBjb25zb2xlYCBBUElcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gb3B0aW9ucy5sb2dFcnJvcnMgLSBzaG91bGQgZXJyb3JzIGluIGFjdGlvbiBleGVjdXRpb24gYmUgY2F1Z2h0LCBsb2dnZWQsIGFuZCByZS10aHJvd24/XG4gKiBAcHJvcGVydHkge2Jvb2xlYW59IG9wdGlvbnMuY29sbGFwc2VkIC0gaXMgZ3JvdXAgY29sbGFwc2VkP1xuICogQHByb3BlcnR5IHtib29sZWFufSBvcHRpb25zLnByZWRpY2F0ZSAtIGNvbmRpdGlvbiB3aGljaCByZXNvbHZlcyBsb2dnZXIgYmVoYXZpb3JcbiAqIEBwcm9wZXJ0eSB7ZnVuY3Rpb259IG9wdGlvbnMuc3RhdGVUcmFuc2Zvcm1lciAtIHRyYW5zZm9ybSBzdGF0ZSBiZWZvcmUgcHJpbnRcbiAqIEBwcm9wZXJ0eSB7ZnVuY3Rpb259IG9wdGlvbnMuYWN0aW9uVHJhbnNmb3JtZXIgLSB0cmFuc2Zvcm0gYWN0aW9uIGJlZm9yZSBwcmludFxuICogQHByb3BlcnR5IHtmdW5jdGlvbn0gb3B0aW9ucy5lcnJvclRyYW5zZm9ybWVyIC0gdHJhbnNmb3JtIGVycm9yIGJlZm9yZSBwcmludFxuICovXG5cbmZ1bmN0aW9uIGNyZWF0ZUxvZ2dlcigpIHtcbiAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoIDw9IDAgfHwgYXJndW1lbnRzWzBdID09PSB1bmRlZmluZWQgPyB7fSA6IGFyZ3VtZW50c1swXTtcbiAgdmFyIF9vcHRpb25zJGxldmVsID0gb3B0aW9ucy5sZXZlbDtcbiAgdmFyIGxldmVsID0gX29wdGlvbnMkbGV2ZWwgPT09IHVuZGVmaW5lZCA/IFwibG9nXCIgOiBfb3B0aW9ucyRsZXZlbDtcbiAgdmFyIF9vcHRpb25zJGxvZ2dlciA9IG9wdGlvbnMubG9nZ2VyO1xuICB2YXIgbG9nZ2VyID0gX29wdGlvbnMkbG9nZ2VyID09PSB1bmRlZmluZWQgPyBjb25zb2xlIDogX29wdGlvbnMkbG9nZ2VyO1xuICB2YXIgX29wdGlvbnMkbG9nRXJyb3JzID0gb3B0aW9ucy5sb2dFcnJvcnM7XG4gIHZhciBsb2dFcnJvcnMgPSBfb3B0aW9ucyRsb2dFcnJvcnMgPT09IHVuZGVmaW5lZCA/IHRydWUgOiBfb3B0aW9ucyRsb2dFcnJvcnM7XG4gIHZhciBjb2xsYXBzZWQgPSBvcHRpb25zLmNvbGxhcHNlZDtcbiAgdmFyIHByZWRpY2F0ZSA9IG9wdGlvbnMucHJlZGljYXRlO1xuICB2YXIgX29wdGlvbnMkZHVyYXRpb24gPSBvcHRpb25zLmR1cmF0aW9uO1xuICB2YXIgZHVyYXRpb24gPSBfb3B0aW9ucyRkdXJhdGlvbiA9PT0gdW5kZWZpbmVkID8gZmFsc2UgOiBfb3B0aW9ucyRkdXJhdGlvbjtcbiAgdmFyIF9vcHRpb25zJHRpbWVzdGFtcCA9IG9wdGlvbnMudGltZXN0YW1wO1xuICB2YXIgdGltZXN0YW1wID0gX29wdGlvbnMkdGltZXN0YW1wID09PSB1bmRlZmluZWQgPyB0cnVlIDogX29wdGlvbnMkdGltZXN0YW1wO1xuICB2YXIgdHJhbnNmb3JtZXIgPSBvcHRpb25zLnRyYW5zZm9ybWVyO1xuICB2YXIgX29wdGlvbnMkc3RhdGVUcmFuc2ZvID0gb3B0aW9ucy5zdGF0ZVRyYW5zZm9ybWVyO1xuICB2YXIgLy8gZGVwcmVjYXRlZFxuICBzdGF0ZVRyYW5zZm9ybWVyID0gX29wdGlvbnMkc3RhdGVUcmFuc2ZvID09PSB1bmRlZmluZWQgPyBmdW5jdGlvbiAoc3RhdGUpIHtcbiAgICByZXR1cm4gc3RhdGU7XG4gIH0gOiBfb3B0aW9ucyRzdGF0ZVRyYW5zZm87XG4gIHZhciBfb3B0aW9ucyRhY3Rpb25UcmFuc2YgPSBvcHRpb25zLmFjdGlvblRyYW5zZm9ybWVyO1xuICB2YXIgYWN0aW9uVHJhbnNmb3JtZXIgPSBfb3B0aW9ucyRhY3Rpb25UcmFuc2YgPT09IHVuZGVmaW5lZCA/IGZ1bmN0aW9uIChhY3RuKSB7XG4gICAgcmV0dXJuIGFjdG47XG4gIH0gOiBfb3B0aW9ucyRhY3Rpb25UcmFuc2Y7XG4gIHZhciBfb3B0aW9ucyRlcnJvclRyYW5zZm8gPSBvcHRpb25zLmVycm9yVHJhbnNmb3JtZXI7XG4gIHZhciBlcnJvclRyYW5zZm9ybWVyID0gX29wdGlvbnMkZXJyb3JUcmFuc2ZvID09PSB1bmRlZmluZWQgPyBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICByZXR1cm4gZXJyb3I7XG4gIH0gOiBfb3B0aW9ucyRlcnJvclRyYW5zZm87XG4gIHZhciBfb3B0aW9ucyRjb2xvcnMgPSBvcHRpb25zLmNvbG9ycztcbiAgdmFyIGNvbG9ycyA9IF9vcHRpb25zJGNvbG9ycyA9PT0gdW5kZWZpbmVkID8ge1xuICAgIHRpdGxlOiBmdW5jdGlvbiB0aXRsZSgpIHtcbiAgICAgIHJldHVybiBcIiMwMDAwMDBcIjtcbiAgICB9LFxuICAgIHByZXZTdGF0ZTogZnVuY3Rpb24gcHJldlN0YXRlKCkge1xuICAgICAgcmV0dXJuIFwiIzlFOUU5RVwiO1xuICAgIH0sXG4gICAgYWN0aW9uOiBmdW5jdGlvbiBhY3Rpb24oKSB7XG4gICAgICByZXR1cm4gXCIjMDNBOUY0XCI7XG4gICAgfSxcbiAgICBuZXh0U3RhdGU6IGZ1bmN0aW9uIG5leHRTdGF0ZSgpIHtcbiAgICAgIHJldHVybiBcIiM0Q0FGNTBcIjtcbiAgICB9LFxuICAgIGVycm9yOiBmdW5jdGlvbiBlcnJvcigpIHtcbiAgICAgIHJldHVybiBcIiNGMjA0MDRcIjtcbiAgICB9XG4gIH0gOiBfb3B0aW9ucyRjb2xvcnM7XG5cbiAgLy8gZXhpdCBpZiBjb25zb2xlIHVuZGVmaW5lZFxuXG4gIGlmICh0eXBlb2YgbG9nZ2VyID09PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAobmV4dCkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGFjdGlvbikge1xuICAgICAgICAgIHJldHVybiBuZXh0KGFjdGlvbik7XG4gICAgICAgIH07XG4gICAgICB9O1xuICAgIH07XG4gIH1cblxuICBpZiAodHJhbnNmb3JtZXIpIHtcbiAgICBjb25zb2xlLmVycm9yKFwiT3B0aW9uICd0cmFuc2Zvcm1lcicgaXMgZGVwcmVjYXRlZCwgdXNlIHN0YXRlVHJhbnNmb3JtZXIgaW5zdGVhZFwiKTtcbiAgfVxuXG4gIHZhciBsb2dCdWZmZXIgPSBbXTtcbiAgZnVuY3Rpb24gcHJpbnRCdWZmZXIoKSB7XG4gICAgbG9nQnVmZmVyLmZvckVhY2goZnVuY3Rpb24gKGxvZ0VudHJ5LCBrZXkpIHtcbiAgICAgIHZhciBzdGFydGVkID0gbG9nRW50cnkuc3RhcnRlZDtcbiAgICAgIHZhciBzdGFydGVkVGltZSA9IGxvZ0VudHJ5LnN0YXJ0ZWRUaW1lO1xuICAgICAgdmFyIGFjdGlvbiA9IGxvZ0VudHJ5LmFjdGlvbjtcbiAgICAgIHZhciBwcmV2U3RhdGUgPSBsb2dFbnRyeS5wcmV2U3RhdGU7XG4gICAgICB2YXIgZXJyb3IgPSBsb2dFbnRyeS5lcnJvcjtcbiAgICAgIHZhciB0b29rID0gbG9nRW50cnkudG9vaztcbiAgICAgIHZhciBuZXh0U3RhdGUgPSBsb2dFbnRyeS5uZXh0U3RhdGU7XG5cbiAgICAgIHZhciBuZXh0RW50cnkgPSBsb2dCdWZmZXJba2V5ICsgMV07XG4gICAgICBpZiAobmV4dEVudHJ5KSB7XG4gICAgICAgIG5leHRTdGF0ZSA9IG5leHRFbnRyeS5wcmV2U3RhdGU7XG4gICAgICAgIHRvb2sgPSBuZXh0RW50cnkuc3RhcnRlZCAtIHN0YXJ0ZWQ7XG4gICAgICB9XG4gICAgICAvLyBtZXNzYWdlXG4gICAgICB2YXIgZm9ybWF0dGVkQWN0aW9uID0gYWN0aW9uVHJhbnNmb3JtZXIoYWN0aW9uKTtcbiAgICAgIHZhciBpc0NvbGxhcHNlZCA9IHR5cGVvZiBjb2xsYXBzZWQgPT09IFwiZnVuY3Rpb25cIiA/IGNvbGxhcHNlZChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBuZXh0U3RhdGU7XG4gICAgICB9LCBhY3Rpb24pIDogY29sbGFwc2VkO1xuXG4gICAgICB2YXIgZm9ybWF0dGVkVGltZSA9IGZvcm1hdFRpbWUoc3RhcnRlZFRpbWUpO1xuICAgICAgdmFyIHRpdGxlQ1NTID0gY29sb3JzLnRpdGxlID8gXCJjb2xvcjogXCIgKyBjb2xvcnMudGl0bGUoZm9ybWF0dGVkQWN0aW9uKSArIFwiO1wiIDogbnVsbDtcbiAgICAgIHZhciB0aXRsZSA9IFwiYWN0aW9uIFwiICsgKHRpbWVzdGFtcCA/IGZvcm1hdHRlZFRpbWUgOiBcIlwiKSArIFwiIFwiICsgZm9ybWF0dGVkQWN0aW9uLnR5cGUgKyBcIiBcIiArIChkdXJhdGlvbiA/IFwiKGluIFwiICsgdG9vay50b0ZpeGVkKDIpICsgXCIgbXMpXCIgOiBcIlwiKTtcblxuICAgICAgLy8gcmVuZGVyXG4gICAgICB0cnkge1xuICAgICAgICBpZiAoaXNDb2xsYXBzZWQpIHtcbiAgICAgICAgICBpZiAoY29sb3JzLnRpdGxlKSBsb2dnZXIuZ3JvdXBDb2xsYXBzZWQoXCIlYyBcIiArIHRpdGxlLCB0aXRsZUNTUyk7ZWxzZSBsb2dnZXIuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmIChjb2xvcnMudGl0bGUpIGxvZ2dlci5ncm91cChcIiVjIFwiICsgdGl0bGUsIHRpdGxlQ1NTKTtlbHNlIGxvZ2dlci5ncm91cCh0aXRsZSk7XG4gICAgICAgIH1cbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgbG9nZ2VyLmxvZyh0aXRsZSk7XG4gICAgICB9XG5cbiAgICAgIHZhciBwcmV2U3RhdGVMZXZlbCA9IGdldExvZ0xldmVsKGxldmVsLCBmb3JtYXR0ZWRBY3Rpb24sIFtwcmV2U3RhdGVdLCBcInByZXZTdGF0ZVwiKTtcbiAgICAgIHZhciBhY3Rpb25MZXZlbCA9IGdldExvZ0xldmVsKGxldmVsLCBmb3JtYXR0ZWRBY3Rpb24sIFtmb3JtYXR0ZWRBY3Rpb25dLCBcImFjdGlvblwiKTtcbiAgICAgIHZhciBlcnJvckxldmVsID0gZ2V0TG9nTGV2ZWwobGV2ZWwsIGZvcm1hdHRlZEFjdGlvbiwgW2Vycm9yLCBwcmV2U3RhdGVdLCBcImVycm9yXCIpO1xuICAgICAgdmFyIG5leHRTdGF0ZUxldmVsID0gZ2V0TG9nTGV2ZWwobGV2ZWwsIGZvcm1hdHRlZEFjdGlvbiwgW25leHRTdGF0ZV0sIFwibmV4dFN0YXRlXCIpO1xuXG4gICAgICBpZiAocHJldlN0YXRlTGV2ZWwpIHtcbiAgICAgICAgaWYgKGNvbG9ycy5wcmV2U3RhdGUpIGxvZ2dlcltwcmV2U3RhdGVMZXZlbF0oXCIlYyBwcmV2IHN0YXRlXCIsIFwiY29sb3I6IFwiICsgY29sb3JzLnByZXZTdGF0ZShwcmV2U3RhdGUpICsgXCI7IGZvbnQtd2VpZ2h0OiBib2xkXCIsIHByZXZTdGF0ZSk7ZWxzZSBsb2dnZXJbcHJldlN0YXRlTGV2ZWxdKFwicHJldiBzdGF0ZVwiLCBwcmV2U3RhdGUpO1xuICAgICAgfVxuXG4gICAgICBpZiAoYWN0aW9uTGV2ZWwpIHtcbiAgICAgICAgaWYgKGNvbG9ycy5hY3Rpb24pIGxvZ2dlclthY3Rpb25MZXZlbF0oXCIlYyBhY3Rpb25cIiwgXCJjb2xvcjogXCIgKyBjb2xvcnMuYWN0aW9uKGZvcm1hdHRlZEFjdGlvbikgKyBcIjsgZm9udC13ZWlnaHQ6IGJvbGRcIiwgZm9ybWF0dGVkQWN0aW9uKTtlbHNlIGxvZ2dlclthY3Rpb25MZXZlbF0oXCJhY3Rpb25cIiwgZm9ybWF0dGVkQWN0aW9uKTtcbiAgICAgIH1cblxuICAgICAgaWYgKGVycm9yICYmIGVycm9yTGV2ZWwpIHtcbiAgICAgICAgaWYgKGNvbG9ycy5lcnJvcikgbG9nZ2VyW2Vycm9yTGV2ZWxdKFwiJWMgZXJyb3JcIiwgXCJjb2xvcjogXCIgKyBjb2xvcnMuZXJyb3IoZXJyb3IsIHByZXZTdGF0ZSkgKyBcIjsgZm9udC13ZWlnaHQ6IGJvbGRcIiwgZXJyb3IpO2Vsc2UgbG9nZ2VyW2Vycm9yTGV2ZWxdKFwiZXJyb3JcIiwgZXJyb3IpO1xuICAgICAgfVxuXG4gICAgICBpZiAobmV4dFN0YXRlTGV2ZWwpIHtcbiAgICAgICAgaWYgKGNvbG9ycy5uZXh0U3RhdGUpIGxvZ2dlcltuZXh0U3RhdGVMZXZlbF0oXCIlYyBuZXh0IHN0YXRlXCIsIFwiY29sb3I6IFwiICsgY29sb3JzLm5leHRTdGF0ZShuZXh0U3RhdGUpICsgXCI7IGZvbnQtd2VpZ2h0OiBib2xkXCIsIG5leHRTdGF0ZSk7ZWxzZSBsb2dnZXJbbmV4dFN0YXRlTGV2ZWxdKFwibmV4dCBzdGF0ZVwiLCBuZXh0U3RhdGUpO1xuICAgICAgfVxuXG4gICAgICB0cnkge1xuICAgICAgICBsb2dnZXIuZ3JvdXBFbmQoKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgbG9nZ2VyLmxvZyhcIuKAlOKAlCBsb2cgZW5kIOKAlOKAlFwiKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBsb2dCdWZmZXIubGVuZ3RoID0gMDtcbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAoX3JlZikge1xuICAgIHZhciBnZXRTdGF0ZSA9IF9yZWYuZ2V0U3RhdGU7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChuZXh0KSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGFjdGlvbikge1xuICAgICAgICAvLyBleGl0IGVhcmx5IGlmIHByZWRpY2F0ZSBmdW5jdGlvbiByZXR1cm5zIGZhbHNlXG4gICAgICAgIGlmICh0eXBlb2YgcHJlZGljYXRlID09PSBcImZ1bmN0aW9uXCIgJiYgIXByZWRpY2F0ZShnZXRTdGF0ZSwgYWN0aW9uKSkge1xuICAgICAgICAgIHJldHVybiBuZXh0KGFjdGlvbik7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgbG9nRW50cnkgPSB7fTtcbiAgICAgICAgbG9nQnVmZmVyLnB1c2gobG9nRW50cnkpO1xuXG4gICAgICAgIGxvZ0VudHJ5LnN0YXJ0ZWQgPSB0aW1lci5ub3coKTtcbiAgICAgICAgbG9nRW50cnkuc3RhcnRlZFRpbWUgPSBuZXcgRGF0ZSgpO1xuICAgICAgICBsb2dFbnRyeS5wcmV2U3RhdGUgPSBzdGF0ZVRyYW5zZm9ybWVyKGdldFN0YXRlKCkpO1xuICAgICAgICBsb2dFbnRyeS5hY3Rpb24gPSBhY3Rpb247XG5cbiAgICAgICAgdmFyIHJldHVybmVkVmFsdWUgPSB1bmRlZmluZWQ7XG4gICAgICAgIGlmIChsb2dFcnJvcnMpIHtcbiAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IG5leHQoYWN0aW9uKTtcbiAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICBsb2dFbnRyeS5lcnJvciA9IGVycm9yVHJhbnNmb3JtZXIoZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBuZXh0KGFjdGlvbik7XG4gICAgICAgIH1cblxuICAgICAgICBsb2dFbnRyeS50b29rID0gdGltZXIubm93KCkgLSBsb2dFbnRyeS5zdGFydGVkO1xuICAgICAgICBsb2dFbnRyeS5uZXh0U3RhdGUgPSBzdGF0ZVRyYW5zZm9ybWVyKGdldFN0YXRlKCkpO1xuXG4gICAgICAgIHByaW50QnVmZmVyKCk7XG5cbiAgICAgICAgaWYgKGxvZ0VudHJ5LmVycm9yKSB0aHJvdyBsb2dFbnRyeS5lcnJvcjtcbiAgICAgICAgcmV0dXJuIHJldHVybmVkVmFsdWU7XG4gICAgICB9O1xuICAgIH07XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY3JlYXRlTG9nZ2VyO1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vcmVkdXgtbG9nZ2VyL2xpYi9pbmRleC5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmZ1bmN0aW9uIGNyZWF0ZVRodW5rTWlkZGxld2FyZShleHRyYUFyZ3VtZW50KSB7XG4gIHJldHVybiBmdW5jdGlvbiAoX3JlZikge1xuICAgIHZhciBkaXNwYXRjaCA9IF9yZWYuZGlzcGF0Y2g7XG4gICAgdmFyIGdldFN0YXRlID0gX3JlZi5nZXRTdGF0ZTtcbiAgICByZXR1cm4gZnVuY3Rpb24gKG5leHQpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoYWN0aW9uKSB7XG4gICAgICAgIGlmICh0eXBlb2YgYWN0aW9uID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgcmV0dXJuIGFjdGlvbihkaXNwYXRjaCwgZ2V0U3RhdGUsIGV4dHJhQXJndW1lbnQpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIG5leHQoYWN0aW9uKTtcbiAgICAgIH07XG4gICAgfTtcbiAgfTtcbn1cblxudmFyIHRodW5rID0gY3JlYXRlVGh1bmtNaWRkbGV3YXJlKCk7XG50aHVuay53aXRoRXh0cmFBcmd1bWVudCA9IGNyZWF0ZVRodW5rTWlkZGxld2FyZTtcblxuZXhwb3J0c1snZGVmYXVsdCddID0gdGh1bms7XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9yZWR1eC10aHVuay9saWIvaW5kZXguanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3NsaWNlZFRvQXJyYXkgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIHNsaWNlSXRlcmF0b3IoYXJyLCBpKSB7IHZhciBfYXJyID0gW107IHZhciBfbiA9IHRydWU7IHZhciBfZCA9IGZhbHNlOyB2YXIgX2UgPSB1bmRlZmluZWQ7IHRyeSB7IGZvciAodmFyIF9pID0gYXJyW1N5bWJvbC5pdGVyYXRvcl0oKSwgX3M7ICEoX24gPSAoX3MgPSBfaS5uZXh0KCkpLmRvbmUpOyBfbiA9IHRydWUpIHsgX2Fyci5wdXNoKF9zLnZhbHVlKTsgaWYgKGkgJiYgX2Fyci5sZW5ndGggPT09IGkpIGJyZWFrOyB9IH0gY2F0Y2ggKGVycikgeyBfZCA9IHRydWU7IF9lID0gZXJyOyB9IGZpbmFsbHkgeyB0cnkgeyBpZiAoIV9uICYmIF9pW1wicmV0dXJuXCJdKSBfaVtcInJldHVyblwiXSgpOyB9IGZpbmFsbHkgeyBpZiAoX2QpIHRocm93IF9lOyB9IH0gcmV0dXJuIF9hcnI7IH0gcmV0dXJuIGZ1bmN0aW9uIChhcnIsIGkpIHsgaWYgKEFycmF5LmlzQXJyYXkoYXJyKSkgeyByZXR1cm4gYXJyOyB9IGVsc2UgaWYgKFN5bWJvbC5pdGVyYXRvciBpbiBPYmplY3QoYXJyKSkgeyByZXR1cm4gc2xpY2VJdGVyYXRvcihhcnIsIGkpOyB9IGVsc2UgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiSW52YWxpZCBhdHRlbXB0IHRvIGRlc3RydWN0dXJlIG5vbi1pdGVyYWJsZSBpbnN0YW5jZVwiKTsgfSB9OyB9KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHByb21pc2VNaWRkbGV3YXJlO1xuXG52YXIgX2lzUHJvbWlzZSA9IHJlcXVpcmUoJy4vaXNQcm9taXNlJyk7XG5cbnZhciBfaXNQcm9taXNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUHJvbWlzZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBkZWZhdWx0VHlwZXMgPSBbJ1BFTkRJTkcnLCAnRlVMRklMTEVEJywgJ1JFSkVDVEVEJ107XG5cbi8qKlxuICogQGZ1bmN0aW9uIHByb21pc2VNaWRkbGV3YXJlXG4gKiBAZGVzY3JpcHRpb25cbiAqIEByZXR1cm5zIHtmdW5jdGlvbn0gdGh1bmtcbiAqL1xuZnVuY3Rpb24gcHJvbWlzZU1pZGRsZXdhcmUoKSB7XG4gIHZhciBjb25maWcgPSBhcmd1bWVudHMubGVuZ3RoIDw9IDAgfHwgYXJndW1lbnRzWzBdID09PSB1bmRlZmluZWQgPyB7fSA6IGFyZ3VtZW50c1swXTtcblxuICB2YXIgcHJvbWlzZVR5cGVTdWZmaXhlcyA9IGNvbmZpZy5wcm9taXNlVHlwZVN1ZmZpeGVzIHx8IGRlZmF1bHRUeXBlcztcblxuICByZXR1cm4gZnVuY3Rpb24gKHJlZikge1xuICAgIHZhciBkaXNwYXRjaCA9IHJlZi5kaXNwYXRjaDtcblxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChuZXh0KSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGFjdGlvbikge1xuICAgICAgICBpZiAoYWN0aW9uLnBheWxvYWQpIHtcbiAgICAgICAgICBpZiAoISgwLCBfaXNQcm9taXNlMi5kZWZhdWx0KShhY3Rpb24ucGF5bG9hZCkgJiYgISgwLCBfaXNQcm9taXNlMi5kZWZhdWx0KShhY3Rpb24ucGF5bG9hZC5wcm9taXNlKSkge1xuICAgICAgICAgICAgcmV0dXJuIG5leHQoYWN0aW9uKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIG5leHQoYWN0aW9uKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIERlY29uc3RydWN0IHRoZSBwcm9wZXJ0aWVzIG9mIHRoZSBvcmlnaW5hbCBhY3Rpb24gb2JqZWN0IHRvIGNvbnN0YW50c1xuICAgICAgICB2YXIgdHlwZSA9IGFjdGlvbi50eXBlO1xuICAgICAgICB2YXIgcGF5bG9hZCA9IGFjdGlvbi5wYXlsb2FkO1xuICAgICAgICB2YXIgbWV0YSA9IGFjdGlvbi5tZXRhO1xuXG4gICAgICAgIC8vIEFzc2lnbiB2YWx1ZXMgZm9yIHByb21pc2UgdHlwZSBzdWZmaXhlc1xuXG4gICAgICAgIHZhciBfcHJvbWlzZVR5cGVTdWZmaXhlcyA9IF9zbGljZWRUb0FycmF5KHByb21pc2VUeXBlU3VmZml4ZXMsIDMpO1xuXG4gICAgICAgIHZhciBQRU5ESU5HID0gX3Byb21pc2VUeXBlU3VmZml4ZXNbMF07XG4gICAgICAgIHZhciBGVUxGSUxMRUQgPSBfcHJvbWlzZVR5cGVTdWZmaXhlc1sxXTtcbiAgICAgICAgdmFyIFJFSkVDVEVEID0gX3Byb21pc2VUeXBlU3VmZml4ZXNbMl07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEBmdW5jdGlvbiBnZXRBY3Rpb25cbiAgICAgICAgICogQGRlc2NyaXB0aW9uIFV0aWxpdHkgZnVuY3Rpb24gZm9yIGNyZWF0aW5nIGEgcmVqZWN0ZWQgb3IgZnVsZmlsbGVkXG4gICAgICAgICAqIGZsdXggc3RhbmRhcmQgYWN0aW9uIG9iamVjdC5cbiAgICAgICAgICogQHBhcmFtIHtib29sZWFufSBJcyB0aGUgYWN0aW9uIHJlamVjdGVkP1xuICAgICAgICAgKiBAcmV0dXJucyB7b2JqZWN0fSBhY3Rpb25cbiAgICAgICAgICovXG5cbiAgICAgICAgdmFyIGdldEFjdGlvbiA9IGZ1bmN0aW9uIGdldEFjdGlvbihuZXdQYXlsb2FkLCBpc1JlamVjdGVkKSB7XG4gICAgICAgICAgcmV0dXJuIF9leHRlbmRzKHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUgKyAnXycgKyAoaXNSZWplY3RlZCA/IFJFSkVDVEVEIDogRlVMRklMTEVEKVxuICAgICAgICAgIH0sIG5ld1BheWxvYWQgPyB7XG4gICAgICAgICAgICBwYXlsb2FkOiBuZXdQYXlsb2FkXG4gICAgICAgICAgfSA6IHt9LCAhIW1ldGEgPyB7IG1ldGE6IG1ldGEgfSA6IHt9LCBpc1JlamVjdGVkID8ge1xuICAgICAgICAgICAgZXJyb3I6IHRydWVcbiAgICAgICAgICB9IDoge30pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBBc3NpZ24gdmFsdWVzIGZvciBwcm9taXNlIGFuZCBkYXRhIHZhcmlhYmxlcy4gSW4gdGhlIGNhc2UgdGhlIHBheWxvYWRcbiAgICAgICAgICogaXMgYW4gb2JqZWN0IHdpdGggYSBgcHJvbWlzZWAgYW5kIGBkYXRhYCBwcm9wZXJ0eSwgdGhlIHZhbHVlcyBvZiB0aG9zZVxuICAgICAgICAgKiBwcm9wZXJ0aWVzIHdpbGwgYmUgdXNlZC4gSW4gdGhlIGNhc2UgdGhlIHBheWxvYWQgaXMgYSBwcm9taXNlLCB0aGVcbiAgICAgICAgICogdmFsdWUgb2YgdGhlIHBheWxvYWQgd2lsbCBiZSB1c2VkIGFuZCBkYXRhIHdpbGwgYmUgbnVsbC5cbiAgICAgICAgICovXG4gICAgICAgIHZhciBwcm9taXNlID0gdm9pZCAwO1xuICAgICAgICB2YXIgZGF0YSA9IHZvaWQgMDtcblxuICAgICAgICBpZiAoISgwLCBfaXNQcm9taXNlMi5kZWZhdWx0KShhY3Rpb24ucGF5bG9hZCkgJiYgX3R5cGVvZihhY3Rpb24ucGF5bG9hZCkgPT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgcHJvbWlzZSA9IHBheWxvYWQucHJvbWlzZTtcbiAgICAgICAgICBkYXRhID0gcGF5bG9hZC5kYXRhO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHByb21pc2UgPSBwYXlsb2FkO1xuICAgICAgICAgIGRhdGEgPSBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEZpcnN0LCBkaXNwYXRjaCB0aGUgcGVuZGluZyBhY3Rpb24uIFRoaXMgZmx1eCBzdGFuZGFyZCBhY3Rpb24gb2JqZWN0XG4gICAgICAgICAqIGRlc2NyaWJlcyB0aGUgcGVuZGluZyBzdGF0ZSBvZiBhIHByb21pc2UgYW5kIHdpbGwgaW5jbHVkZSBhbnkgZGF0YVxuICAgICAgICAgKiAoZm9yIG9wdGltaXN0aWMgdXBkYXRlcykgYW5kL29yIG1ldGEgZnJvbSB0aGUgb3JpZ2luYWwgYWN0aW9uLlxuICAgICAgICAgKi9cbiAgICAgICAgbmV4dChfZXh0ZW5kcyh7XG4gICAgICAgICAgdHlwZTogdHlwZSArICdfJyArIFBFTkRJTkdcbiAgICAgICAgfSwgISFkYXRhID8geyBwYXlsb2FkOiBkYXRhIH0gOiB7fSwgISFtZXRhID8geyBtZXRhOiBtZXRhIH0gOiB7fSkpO1xuXG4gICAgICAgIC8qXG4gICAgICAgICAqIEBmdW5jdGlvbiBoYW5kbGVSZWplY3RcbiAgICAgICAgICogQGRlc2NyaXB0aW9uIERpc3BhdGNoIHRoZSByZWplY3RlZCBhY3Rpb24gYW5kIHJldHVyblxuICAgICAgICAgKiBhbiBlcnJvciBvYmplY3QuIFRoZSBlcnJvciBvYmplY3QgaXMgdGhlIG9yaWdpbmFsIGVycm9yXG4gICAgICAgICAqIHRoYXQgd2FzIHRocm93bi4gVGhlIHVzZXIgb2YgdGhlIGxpYnJhcnkgaXMgcmVzcG9uc2libGUgZm9yXG4gICAgICAgICAqIGJlc3QgcHJhY3RpY2VzIGluIGVuc3VyZSB0aGF0IHRoZXkgYXJlIHRocm93aW5nIGFuIEVycm9yIG9iamVjdC5cbiAgICAgICAgICogQHBhcmFtcyByZWFzb24gVGhlIHJlYXNvbiB0aGUgcHJvbWlzZSB3YXMgcmVqZWN0ZWRcbiAgICAgICAgICogQHJldHVybnMge29iamVjdH1cbiAgICAgICAgICovXG4gICAgICAgIHZhciBoYW5kbGVSZWplY3QgPSBmdW5jdGlvbiBoYW5kbGVSZWplY3QocmVhc29uKSB7XG4gICAgICAgICAgdmFyIHJlamVjdGVkQWN0aW9uID0gZ2V0QWN0aW9uKHJlYXNvbiwgdHJ1ZSk7XG4gICAgICAgICAgZGlzcGF0Y2gocmVqZWN0ZWRBY3Rpb24pO1xuICAgICAgICAgIHRocm93IHJlYXNvbjtcbiAgICAgICAgfTtcblxuICAgICAgICAvKlxuICAgICAgICAgKiBAZnVuY3Rpb24gaGFuZGxlRnVsZmlsbFxuICAgICAgICAgKiBAZGVzY3JpcHRpb24gRGlzcGF0Y2ggdGhlIGZ1bGZpbGxlZCBhY3Rpb24gYW5kXG4gICAgICAgICAqIHJldHVybiB0aGUgc3VjY2VzcyBvYmplY3QuIFRoZSBzdWNjZXNzIG9iamVjdCBzaG91bGRcbiAgICAgICAgICogY29udGFpbiB0aGUgdmFsdWUgYW5kIHRoZSBkaXNwYXRjaGVkIGFjdGlvbi5cbiAgICAgICAgICogQHBhcmFtIHZhbHVlIFRoZSB2YWx1ZSB0aGUgcHJvbWlzZSB3YXMgcmVzbG92ZWQgd2l0aFxuICAgICAgICAgKiBAcmV0dXJucyB7b2JqZWN0fVxuICAgICAgICAgKi9cbiAgICAgICAgdmFyIGhhbmRsZUZ1bGZpbGwgPSBmdW5jdGlvbiBoYW5kbGVGdWxmaWxsKCkge1xuICAgICAgICAgIHZhciB2YWx1ZSA9IGFyZ3VtZW50cy5sZW5ndGggPD0gMCB8fCBhcmd1bWVudHNbMF0gPT09IHVuZGVmaW5lZCA/IG51bGwgOiBhcmd1bWVudHNbMF07XG5cbiAgICAgICAgICB2YXIgcmVzb2x2ZWRBY3Rpb24gPSBnZXRBY3Rpb24odmFsdWUsIGZhbHNlKTtcbiAgICAgICAgICBkaXNwYXRjaChyZXNvbHZlZEFjdGlvbik7XG5cbiAgICAgICAgICByZXR1cm4geyB2YWx1ZTogdmFsdWUsIGFjdGlvbjogcmVzb2x2ZWRBY3Rpb24gfTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2Vjb25kLCBkaXNwYXRjaCBhIHJlamVjdGVkIG9yIGZ1bGZpbGxlZCBhY3Rpb24uIFRoaXMgZmx1eCBzdGFuZGFyZFxuICAgICAgICAgKiBhY3Rpb24gb2JqZWN0IHdpbGwgZGVzY3JpYmUgdGhlIHJlc29sdmVkIHN0YXRlIG9mIHRoZSBwcm9taXNlLiBJblxuICAgICAgICAgKiB0aGUgY2FzZSBvZiBhIHJlamVjdGVkIHByb21pc2UsIGl0IHdpbGwgaW5jbHVkZSBhbiBgZXJyb3JgIHByb3BlcnR5LlxuICAgICAgICAgKlxuICAgICAgICAgKiBJbiBvcmRlciB0byBhbGxvdyBwcm9wZXIgY2hhaW5pbmcgb2YgYWN0aW9ucyB1c2luZyBgdGhlbmAsIGEgbmV3XG4gICAgICAgICAqIHByb21pc2UgaXMgY29uc3RydWN0ZWQgYW5kIHJldHVybmVkLiBUaGlzIHByb21pc2Ugd2lsbCByZXNvbHZlXG4gICAgICAgICAqIHdpdGggdHdvIHByb3BlcnRpZXM6ICgxKSB0aGUgdmFsdWUgKGlmIGZ1bGZpbGxlZCkgb3IgcmVhc29uXG4gICAgICAgICAqIChpZiByZWplY3RlZCkgYW5kICgyKSB0aGUgZmx1eCBzdGFuZGFyZCBhY3Rpb24uXG4gICAgICAgICAqXG4gICAgICAgICAqIFJlamVjdGVkIG9iamVjdDpcbiAgICAgICAgICoge1xuICAgICAgICAgKiAgIHJlYXNvbjogLi4uXG4gICAgICAgICAqICAgYWN0aW9uOiB7XG4gICAgICAgICAqICAgICBlcnJvcjogdHJ1ZSxcbiAgICAgICAgICogICAgIHR5cGU6ICdBQ1RJT05fUkVKRUNURUQnLFxuICAgICAgICAgKiAgICAgcGF5bG9hZDogLi4uXG4gICAgICAgICAqICAgfVxuICAgICAgICAgKiB9XG4gICAgICAgICAqXG4gICAgICAgICAqIEZ1bGZpbGxlZCBvYmplY3Q6XG4gICAgICAgICAqIHtcbiAgICAgICAgICogICB2YWx1ZTogLi4uXG4gICAgICAgICAqICAgYWN0aW9uOiB7XG4gICAgICAgICAqICAgICB0eXBlOiAnQUNUSU9OX0ZVTEZJTExFRCcsXG4gICAgICAgICAqICAgICBwYXlsb2FkOiAuLi5cbiAgICAgICAgICogICB9XG4gICAgICAgICAqIH1cbiAgICAgICAgICovXG4gICAgICAgIHJldHVybiBwcm9taXNlLnRoZW4oaGFuZGxlRnVsZmlsbCwgaGFuZGxlUmVqZWN0KTtcbiAgICAgIH07XG4gICAgfTtcbiAgfTtcbn1cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4LXByb21pc2UtbWlkZGxld2FyZS9kaXN0L2luZGV4LmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNQcm9taXNlO1xuZnVuY3Rpb24gaXNQcm9taXNlKHZhbHVlKSB7XG4gIGlmICh2YWx1ZSAhPT0gbnVsbCAmJiAodHlwZW9mIHZhbHVlID09PSAndW5kZWZpbmVkJyA/ICd1bmRlZmluZWQnIDogX3R5cGVvZih2YWx1ZSkpID09PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiB2YWx1ZSAmJiB0eXBlb2YgdmFsdWUudGhlbiA9PT0gJ2Z1bmN0aW9uJztcbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn1cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4LXByb21pc2UtbWlkZGxld2FyZS9kaXN0L2lzUHJvbWlzZS5qc1xuICoqLyJdLCJzb3VyY2VSb290IjoiIn0=