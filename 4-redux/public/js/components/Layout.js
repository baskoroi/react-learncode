import React from 'react';

import Header from './Header';
import Footer from './Footer';

export default React.createClass({
    getInitialState() {
        return {
            title: 'Welcome'
        };
    },
    changeTitle(title) {
        this.setState({ title });
    },
    render() {
        return (
            <div>
                <Header changeTitle={this.changeTitle} title={this.state.title} />
                <Footer />
            </div>
        );
    }
});