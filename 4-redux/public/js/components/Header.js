import React from 'react';

import Title from './Header/Title';

export default React.createClass({
    changeTitle(e) {
        const title = e.target.value;
        this.props.changeTitle(title);
    },
    render() {
        return (
            <div>
                <Title title={this.props.title} />
                <input type="text" value={this.props.title} onChange={this.changeTitle} />
            </div>
        );
    }
});