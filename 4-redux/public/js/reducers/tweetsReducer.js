export default function(state=[], action) {
    switch(action.type) {
        case 'CREATE_TWEET': {
            state = state.concat(action.payload);
            break;
        }
        case 'DELETE_TWEET': {
            state = state.filter((tweet) => tweet !== action.payload);
            break;
        }
    }
    return state;
}