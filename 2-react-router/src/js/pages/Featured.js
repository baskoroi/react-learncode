import React from 'react';

import Article from '../components/Article';

export default class Featured extends React.Component {
    render() {
        const Articles = [
            'Some Article',
            'Some Other Article',
            'Yet Another Article',
            'Still More'
        ].map((title, i) => <Article key={i} title={title} />);

        const randomAd = 'Ad spot #' + Math.round(Math.random() * (10)) + 'goes here';
        return (
            <div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="well text-center">
                            {randomAd}
                        </div>
                    </div>
                </div>
                <div class="row">{Articles}</div>
            </div>
        );
    }
}