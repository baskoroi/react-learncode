import React from 'react';

import Article from '../components/Article';

export default class Archives extends React.Component {
    render() {
        const Articles = [
            'Some Article',
            'Some Other Article',
            'Yet Another Article',
            'Still More',
            'Some Article',
            'Some Other Article',
            'Yet Another Article',
            'Still More',
        ].map((title, i) => <Article key={i} title={title} />); 

        const { query } = this.props.location;
        const { params } = this.props;
        const { article } = params;
        const { date, filter } = query;
        
        return (
            <div>
                <h1>Archives</h1>
                <p>Article: {article}</p>
                <p>Date: {date}</p>
                <p>Filter: {filter}</p>

                <div class="row">{Articles}</div>
            </div>
        );
    }
}