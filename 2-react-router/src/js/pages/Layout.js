import React from 'react';
import { Link } from 'react-router';

import Nav from '../components/layout/Nav';
import Footer from '../components/layout/Footer';

export default class Layout extends React.Component {
    /*navigate() {
        this.props.history.pushState(null, '/');
    }*/

    render() {
        const { location } = this.props;
        const containerStyle = {
            marginTop: '60px'
        };
        return (
            <div>

                <Nav location={location} />

                <div class="container" style={containerStyle}>
                    <div class="row">
                        <div class="col-lg-12">
                            <div>
                                <h1>KillerNews.net</h1>
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>

            </div> 
        );
    }
}