import React from 'react';

import { Link, IndexLink } from 'react-router';

export default React.createClass({
    getInitialState() {
        return {collapsed: true};
    },
    toggleCollapse() {
        const collapsed = !this.state.collapsed;
        this.setState({collapsed});
    },
    render() {
        const { location } = this.props;
        const { collapsed } = this.state;

        const navClass = collapsed ? 'collapse' : '';

        return (
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        {/*data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"*/}
                        <button type="button" class="navbar-toggle" onClick={this.toggleCollapse}>
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li activeClassName="active" onlyActiveOnIndex={true}>
                                <IndexLink to="/" onClick={this.toggleCollapse}>Featured</IndexLink>
                            </li>
                            <li activeClassName="active">
                                <Link to="archives" onClick={this.toggleCollapse}>Archives</Link>
                            </li>
                            <li activeClassName="active">
                                <Link to="settings" onClick={this.toggleCollapse}>Settings</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
});
    